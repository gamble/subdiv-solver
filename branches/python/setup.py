import os
import sys
import shutil

from Cython.Build  import cythonize
from distutils.core import setup
from distutils.sysconfig import get_config_vars
from distutils.sysconfig import get_python_lib
from distutils.extension import Extension


(opt,) = get_config_vars('OPT')
os.environ['OPT'] = " ".join(
    flag for flag in opt.split() if flag != '-Wstrict-prototypes'
)
os.environ['CC']='g++'

#import Cython.Compiler.Options
#Cython.Compiler.Options.annotate = True

COMP_OPT = "-O2"
CFLAGS = "-I./build/"

setup(
    name = 'subdivision_solver',
    version = '0.0.2',
    packages =  ['subdivision_solver']+\
                ['subdivision_solver.matrix_interval']+\
                ['subdivision_solver.system']+\
                ['subdivision_solver.solver']+\
                ['subdivision_solver.utils'],

    ext_modules = cythonize([\
		      
        Extension('subdivision_solver.matrix_interval.matrix_interval_mpfi',
            ['subdivision_solver/matrix_interval/matrix_interval_mpfi.pyx'],
            language="c++",
            include_dirs = ['.',get_python_lib()],
            libraries = ['mpfr','mpfi','matrixmpfi'],
            library_dirs = ['./build'],
            extra_compile_args=[CFLAGS, COMP_OPT]
        ),\
              
        Extension('subdivision_solver.matrix_interval.matrix_interval_boost',
            ['subdivision_solver/matrix_interval/matrix_interval_boost.pyx'],
            language="c++",
            include_dirs = ['.',get_python_lib()],
            libraries = ['mpfr','mpfi','matrixboost'],
            library_dirs = ['./build'],
            runtime_library_dirs = ['/usr/local/lib'],
            extra_compile_args=[CFLAGS, COMP_OPT]
        ),\

        Extension('subdivision_solver.system.system',
            ['subdivision_solver/system/system.pyx'],
            language="c++",
            include_dirs = ['.',get_python_lib()],
            extra_compile_args=[CFLAGS, COMP_OPT]
        ),\

        Extension('subdivision_solver.system.eval_mpfi',
            ['subdivision_solver/system/eval_mpfi.pyx'],
            language="c++",
            include_dirs = ['.',get_python_lib()],
            libraries = ['mpfr','mpfi','matrixmpfi'],
            library_dirs = ['./build'],
            runtime_library_dirs = ['/usr/local/lib'],
            extra_compile_args=[CFLAGS, COMP_OPT]
        ),\

        Extension('subdivision_solver.system.eval_boost',
            ['subdivision_solver/system/eval_boost.pyx'],
            language="c++",
            include_dirs = ['.',get_python_lib()],
            libraries = ['mpfr','mpfi','matrixboost'],
            library_dirs = ['./build'],
            runtime_library_dirs = ['/usr/local/lib'],
            extra_compile_args=[CFLAGS, COMP_OPT]
        ),\

        Extension('subdivision_solver.solver.solver_mpfi',
            ['subdivision_solver/solver/solver_mpfi.pyx'],
            language="c++",
            include_dirs = ['.',get_python_lib()],
            libraries = ['mpfr','mpfi','matrixmpfi','solvermpfi'],
            library_dirs = ['./build'],
            runtime_library_dirs = ['/usr/local/lib'],
            extra_compile_args=[CFLAGS, COMP_OPT]
        ),\

        Extension('subdivision_solver.solver.solver_boost',
            ['subdivision_solver/solver/solver_boost.pyx'],
            language="c++",
            include_dirs = ['.',get_python_lib()],
            libraries = ['mpfr','mpfi','matrixboost','solverboost'],
            library_dirs = ['./build'],
            runtime_library_dirs = ['/usr/local/lib'],
            extra_compile_args=[CFLAGS, COMP_OPT]
        ),\

        Extension('subdivision_solver.solver.solver_mp',
            ['subdivision_solver/solver/solver_mp.pyx'],
            language="c++",
            include_dirs = ['.',get_python_lib()],
            libraries = ['mpfr','mpfi','matrixmpfi','matrixboost','solvermpfi','solverboost'],
            library_dirs = ['./build'],
            runtime_library_dirs = ['/usr/local/lib'],
            extra_compile_args=[CFLAGS, COMP_OPT]
        ),\

    ],
    include_path = ['.']),
)
		    
# if not os.path.exists(SOLVER_DIR + "build"):
  # os.mkdir(SOLVER_DIR + "build")

# if not os.path.exists("./build/lib.linux-x86_64-2.7/subdivision_solver/shared"):
#     os.makedirs("./build/lib.linux-x86_64-2.7/subdivision_solver/shared")

# shutil.copy("./build/libmatrixmpfi.so",  "./build/lib.linux-x86_64-2.7/subdivision_solver/shared/libmatrixmpfi.so" )
# shutil.copy("./build/libsolvermpfi.so",  "./build/lib.linux-x86_64-2.7/subdivision_solver/shared/libsolvermpfi.so" )
# shutil.copy("./build/libmatrixboost.so", "./build/lib.linux-x86_64-2.7/subdivision_solver/shared/libmatrixboost.so" )
# shutil.copy("./build/libsolverboost.so", "./build/lib.linux-x86_64-2.7/subdivision_solver/shared/libsolverboost.so" )

#if os.path.exists(SAGE_LIBS + "libmatrixmpfi.so"):
  #os.remove(SAGE_LIBS + "libmatrixmpfi.so")
#os.link(SOLVER_DIR + "build/libmatrixmpfi.so",  SAGE_LIBS + "libmatrixmpfi.so")
  
#if os.path.exists(SAGE_LIBS + "libmatrixboost.so"):
  #os.remove(SAGE_LIBS + "libmatrixboost.so")
#os.link(SOLVER_DIR + "build/libmatrixboost.so", SAGE_LIBS + "libmatrixboost.so")
  
#if os.path.exists(SAGE_LIBS + "libsolvermpfi.so"):
  #os.remove(SAGE_LIBS + "libsolvermpfi.so")
#os.link(SOLVER_DIR + "build/libsolvermpfi.so",  SAGE_LIBS + "libsolvermpfi.so")

#if os.path.exists(SAGE_LIBS + "libsolverboost.so"):
  #os.remove(SAGE_LIBS + "libsolverboost.so")
#os.link(SOLVER_DIR + "build/libsolverboost.so", SAGE_LIBS + "libsolverboost.so")

#pour compiler: sage setup.py build_ext --inplace
