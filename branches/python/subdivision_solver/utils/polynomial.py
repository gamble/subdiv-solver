from operator import add
from parser import Parser


class Polynomial(object):
    """A multivariate polynomial class

    A multivariate polynomial class implemented using Python Lists and Tuples

    Parameters
    ----------
    arg : Either a string or a list of tuples
        String : In case of the polynomial represented as a Python str

        List: As a list of Python tuples, each tuple represents a polynomial term.
        Each tuple consists of two elements:
        first element: int represents term coefficient
        second element: tuple represents term exponents
        
        On polynomial instantiation:
        - Zero coefficient terms are removed from the list
        - List is sorted by 2 critera; summation of each term
        exponents, and term exponents

    variables : array_like, optional
        List of Python strings, each string represents a variables
        (e.g., x, y, x0, x1, ....)
        The default value will be ['x0', 'x1', ... 'xn'],
        where n is number ofthe given exponents of the polynomial

    Examples
    --------
    For a univariate polynomial x^3 - 6*x^2 + 11*x - 6
    >>> p1 = Polynomial('x^3 - 6*x^2 + 11*x - 6')
    Or
    >>> p1 = Polynomial([(1, (3,)), (-6, (2,)), (11, (1,)), (-6, (0,))])

    And to pass optional list of variables
    >>> p1 = Polynomial([(1, (3,)), (-6, (2,)), (11, (1,)), (-6, (0,))], ['x'])

    For a bivariante polynomial x^2 + y^2 -25
    >>> p2 = Polynomial('x^2 + y^2 -25')
    Or
    >>> p2 = Polynomial([(1, (2, 0)), (1, (0, 2)), (-25, (0, 0))])

    And to pass optional list of variables
    >>> p2 = Polynomial([(1, (2, 0)), (1, (0, 2)), (-25, (0, 0))], ['x0','x1'])
    Or
    >>> p2 = Polynomial([(1, (2, 0)), (1, (0, 2)), (-25, (0, 0))], ['x','y'])

    """

    def __init__(self, arg, variables=None):
        if isinstance(arg, str):
            self._coeff_exp_list, self._variables = Parser(arg).parse()
        elif isinstance(arg, list):
            self._coeff_exp_list = arg

            self._coeff_exp_list = [coeff_exp_term for coeff_exp_term in self._coeff_exp_list if coeff_exp_term[0] != 0]

            self._coeff_exp_list.sort(key=lambda coeff_exp_term: (sum(coeff_exp_term[1]), coeff_exp_term[1]), reverse=True)

            if variables is None:
                self._variables = []
                if len(self._coeff_exp_list) != 0:
                    first_term = self._coeff_exp_list[0]
                    num_of_variables = len(first_term[1])
                    for i in range(0, num_of_variables):
                        self._variables.append('x' + str(i))
            else:
                self._variables = variables
        else:
            raise TypeError('arg must be either a string or a list of tuple: %s'
                            % arg)


    def coefficients(self):
        """ List of coefficients of the polynomial

        Returns a list of coefficients of the polynomial

        Examples:
            >>> p = Polynomial([(1, (3,)), (-6, (2,)), (11, (1,))])
            >>> p.coefficients()
            [1, -6, 11]

        Returns:
            list of coefficients of the polynomial
        """
        coeffs = []
        for coeff_exp_term in self._coeff_exp_list:
            coeffs.append(coeff_exp_term[0])

        return coeffs

    def exponents(self):
        """ List of exponents of the polynomial

        Returns a list of exponents of the polynomial

        Examples:
            >>> p1 = Polynomial([(1, (3,)), (-6, (2,)), (11, (1,))])
            >>> p1.exponents()
            [(3,), (2,), (1,)]
            >>> p2 = Polynomial([(1, (2, 0)), (1, (0, 2)), (-25, (0, 0))])
            >>> p2.exponents()
            [(2,0), (0,2), (0,0)]

        Returns:
            list of exponents of the polynomial
        """
        exps = []
        for coeff_exp_term in self._coeff_exp_list:
            exps.append(coeff_exp_term[1])

        return exps

    def variables(self):
        """ List of variables of the polynomial

        Returns a list of variables of the polynomial

        Examples:
            >>> p = Polynomial('x^2 - 4')
            >>> p.variables()
            ['x']
            >>> p = Polynomial('x + y - 1')
            >>> p.variables()
            ['x', 'y']
            >>> p = Polynomial('[(1, (2, 0)), (1, (0, 2)), (-25, (0, 0))]')
            >>> p.variables()
            ['x0', 'x1']

        Returns:
            list of variables of the polynomial
        """
        return self._variables

    def derivative(self, variable):
        """ Derivative the polynomial w.r.t a variable given its index

        Args:
            variable (String): Variable name to derivative the
            polynomial w.r.t to it
            If variable is not in polynomial variables,\
            it returns empty polynomial

        Examples:
            >>> p1 = Polynomial([(1, (3,)), (-6, (2,)), (11, (1,))], ['x'])
            >>> print(p1.to_string())
            x^3 - 6*x^2 + 11*x

            >>> d1x = p1.derivative('x')
            >>> d1x.coeff_exp_list
            [(3, (2,)), (-12, (1,)), (11, (0,))]
            >>> print(d1x.to_string())
            3*x^2 - 12*x + 11

            >>> p2 = Polynomial([(1, (2, 0)), (1, (0, 2)), (-25, (0, 0))], ['x', 'y'])
            >>> print(p1.to_string())
            x^2 + y^2 - 25
            >>> d2x = p2.derivative('x')
            >>> d2x.coeff_exp_list
            [(2, (1, 0))]
            >>> print(d2x.to_string())
            2*x
            >>> d2y = p2.derivative('y')
            >>> d2y.coeff_exp_list
            [(2, (0, 1))]
            >>> print(d2y.to_string())
            2*y

        Returns:
            A new polynomial instance that represents the original polynomial
            derivative
        """
        derived_coeff_exp_list = []
        for coeff_exp_term in self._coeff_exp_list:
            derived_coeff_exp_term = self._derivative_term(variable, coeff_exp_term)
            if derived_coeff_exp_term is not None:
                derived_coeff_exp_list.append(derived_coeff_exp_term)

        return Polynomial(derived_coeff_exp_list, self._variables)

    def _derivative_term(self, variable, coeff_exp_term):
        """ Derivative the polynomial term w.r.t a variable given its index

        Args:
            variable (String): Variable name to derivative the
            polynomial w.r.t to it
            If variable is not in polynomial variables,\
            it returns None
            coeff_exp_term (tuple): term coefficient and exponents
        
        Returns:
            A tuple that represents the original polynomial term derivative
        """
        if variable not in self._variables:
            return None

        variable_index = self._variables.index(variable)

        coefficient = coeff_exp_term[0]
        if coefficient == 0:
            return None

        exponents = coeff_exp_term[1]
        exponent = exponents[variable_index]
        if exponent == 0:
            return None
        else:
            derived_coeff = exponent * coefficient
            derived_exponents = exponents[:variable_index] + (exponent - 1,) + exponents[variable_index + 1:]
            return (derived_coeff, derived_exponents)

    def degree(self, variable=None):
        """ Return the maximal degree of this polynomial w.r.t a variable given its index

        Args:
            variable (String): (default: "None"):
            variable to find its maximum exponents for this polynomial
            If variable is None, it returns the polynomial degree
            If variable is not in polynomial variables, it returns 0
            If polynomial is 0, it returns -1

        Examples:
            >>> p1 = Polynomial('x*y^3*z^4 + 2*x^2 - 3')
            >>> print(p1.degree())
            4
            >>> print(p1.degree('a'))
            0
            >>> print(p1.degree('x'))
            2
            >>> print(p1.degree('y'))
            3
            >>> print(p1.degree('z'))
            4

        Returns:
            An integer represents the degree
        """
        if len(self._coeff_exp_list) == 0:
            return -1

        max_exponents = map(max, zip(*self.exponents()))

        if variable is None:
            return max(max_exponents)

        if variable not in self._variables:
            return 0
        else:
            variable_index = self._variables.index(variable)
            return max_exponents[variable_index]

    def reverse(self, variable):
        """ Reverse the exponents of a given variable in a this polynomial and
            return the changed polynomial.
            It subtracts each variable's exponent from that variable greatest exponent

            If the given variable to be reversed is not in this polynomial
            list of variables, a copy of this polynomial is returned

            Args:
                variable to reverse its exponents

            Examples:
            >>> p = Polynomial('2x + 5')
            >>> px = p.reverse('x')
            5*x + 2

            >>> p = Polynomial('2x + 3y + 4')
            >>> px = p.reverse('x')
            3*x*y + 4*x + 2
            >>> py = p.reverse('y')
            2*x*y + 4*y + 3

            >>> p = Polynomial('2x + 3y + 4z + 5')
            >>> px = p.reverse('x')
            3*x*y + 4*x*z + 5*x + 2
            >>> py = p.reverse('y')
            2*x*y + 4*y*z + 5*y + 3
            >>> pz = p.reverse('z')
            2*x*z + 3*y*z + 5*z + 4


            Returns:
                A new polynomial instance with updated exponents
        """
        if variable not in self._variables:
            return Polynomial(list(self._coeff_exp_list), list(self._variables))

        variable_degree = self.degree(variable)
        variable_index = self._variables.index(variable)

        reversed_coeff_exp_list = []
        for coeff_exp_term in self._coeff_exp_list:
            coefficient = coeff_exp_term[0]
            exponents = coeff_exp_term[1]
            reversed_exponent = exponents[variable_index]
            reversed_exponents = exponents[:variable_index] + (variable_degree - reversed_exponent,) + exponents[variable_index + 1:]
            reversed_coeff_exp_list.append((coefficient, reversed_exponents))

        return Polynomial(reversed_coeff_exp_list, self._variables)

    def substitute(self, variable_value_dict):
        """ Substitute one given variable in a this polynomial with another
            variable (i.e., x => w) or with integer value (i.e., x => 2) and
            return the changed polynomial.
            If the given variable_value_dict is None or empty,
            a copy of this polynomial is returned
            If the given variable to be substituted is not in this polynomial
            list of variables, a copy of this polynomial is returned

        Args:
            variable_value_dict (dictionary) : A dictionary of a single item
            of a key-value pair
            key is the variable to be substituted with the given value

        Examples:
            >>> p = Polynomial('x + 5')
            >>> q = p.substitute({'x': 2})
            >>> print(q)
            7
            >>> print(q.degree())
            0

            >>> p = Polynomial('x + y + 1')
            >>> q = p.substitute({'y': 2})
            >>> print(q)
            x + 3
            >>> print(q.degree())
            1
            >>> print(q.degree('x'))
            1
            >>> print(q.degree('y'))
            0

            >>> p = Polynomial('x^2 + y^2 + xy - 25')
            >>> q = p.substitute({'y': 2})
            >>> print(q)
            x^2 + 2*x - 21
            >>> print(q.degree())
            2
            >>> print(q.degree('x'))
            2
            >>> print(q.degree('y'))
            0

        Returns:
            A new polynomial instance with updated variables
        """
        if variable_value_dict is None or len(variable_value_dict) == 0:
            return Polynomial(list(self._coeff_exp_list), list(self._variables))

        variable, value = variable_value_dict.items()[0]

        if variable not in self._variables:
            return Polynomial(list(self._coeff_exp_list), list(self._variables))

        if isinstance(value, str):
            variable_index = self._variables.index(variable)
            variables = list(self._variables)
            variables[variable_index] = value
            return Polynomial(list(self._coeff_exp_list), variables)

        if isinstance(value, float):
            if value.is_integer():
                value = int(value)
            else:
                raise ValueError('Float value is not supported')

        if isinstance(value, (int, long, float)):
            substitute_polynomials_list = []
            variable_index = self._variables.index(variable)
            subs_variables = list(self._variables[:variable_index] + self._variables[variable_index + 1:])
            self_coeff_exp_list = self._coeff_exp_list

            for self_coeff_exp_term in self_coeff_exp_list:
                coefficient = self_coeff_exp_term[0]
                exponents = self_coeff_exp_term[1]
                subs_term_coefficient = coefficient * value**exponents[variable_index]
                subs_term_exponents = exponents[:variable_index] + exponents[variable_index + 1:]
                p = Polynomial([(subs_term_coefficient, subs_term_exponents)], subs_variables)
                substitute_polynomials_list.append(p)

            substitute = Polynomial([], subs_variables)
            for p in substitute_polynomials_list:
                substitute = substitute + p

            return substitute

    def __add__(self, other):
        """ Addition operator for this polynomial instance

        Args:
            other : Either a Polynomial or a Number
            In case of Polynomial, it must have the same set of variables in the same order

        Examples:
            >>> p1 = Polynomial('x^2 + 5')
            >>> scalar = 5
            >>> p = p1 + scalar
            >>> print(p)
            x^2 + 10

            >>> p1 = Polynomial('4*x^3 + 6*x^2 + 10*x + 6')
            >>> p2 = Polynomial('x^3 - 6*x^2 + 11*x - 6')
            >>> p = p1 + p2
            >>> print(p)
            5*x^3 + 21*x

            >>> p1 = Polynomial('x^2 + y^2 -25')
            >>> p2 = Polynomial('x - y + 1')
            >>> p = p1 + p2
            >>> print(p)
            x^2 + y^2 + x - y - 24

        Returns:
            A new Polynomial with addition result
        """
        if not isinstance(other, Polynomial):
            other = Polynomial([(other, (0,)*len(self._variables))],
                               self._variables)

        if self._variables != other._variables:
            raise ValueError('Both Polynomials must have the same set of variables in the same order')

        if len(other._coeff_exp_list) == 0:
            return Polynomial(list(self._coeff_exp_list), list(self._variables))

        sum_coeff_exp_list = []
        sum_variables = self._variables
        self_coeff_exp_list = self._coeff_exp_list
        other_coeff_exp_list = other._coeff_exp_list
        i = 0
        j = 0
        while i < len(self_coeff_exp_list) and j < len(other_coeff_exp_list):
            self_coeff_exp_term = self_coeff_exp_list[i]
            other_coeff_exp_term = other_coeff_exp_list[j]

            exponents1 = self_coeff_exp_term[1]
            exponents2 = other_coeff_exp_term[1]
            if exponents1 == exponents2:
                coefficient1 = self_coeff_exp_term[0]
                coefficient2 = other_coeff_exp_term[0]
                sum_coeff = coefficient1 + coefficient2
                if sum_coeff != 0:
                    sum_coeff_exp_list.append((coefficient1 + coefficient2, exponents1))
                i += 1
                j += 1
            elif sum(exponents1) > sum(exponents2) or exponents1 > exponents2:
                coefficient1 = self_coeff_exp_term[0]
                sum_coeff_exp_list.append((coefficient1, exponents1))
                i += 1
            else:
                coefficient2 = other_coeff_exp_term[0]
                sum_coeff_exp_list.append((coefficient2, exponents2))
                j += 1

        while i < len(self_coeff_exp_list):
            self_coeff_exp_term = self_coeff_exp_list[i]
            coefficient1 = self_coeff_exp_term[0]
            exponents1 = self_coeff_exp_term[1]
            sum_coeff_exp_list.append((coefficient1, exponents1))
            i += 1

        while j < len(other_coeff_exp_list):
            other_coeff_exp_term = other_coeff_exp_list[j]
            coefficient2 = other_coeff_exp_term[0]
            exponents2 = other_coeff_exp_term[1]
            sum_coeff_exp_list.append((coefficient2, exponents2))
            j += 1

        return Polynomial(sum_coeff_exp_list, sum_variables)

    def __radd__(self, other):
        """ Right hand operand addition operator for this polynomial instance
        It is only called in case the left hand operand doensn't support addition
        with Polynomial instance (e.g., Python Number(int, long, float) values)

        Args:
            other : Either a Polynomial or a Number
            In case of Polynomial, it must have the same set of variables in the same order

        Examples:
            >>> p1 = Polynomial('x^2 + 5')
            >>> scalar = 5
            >>> p = scalar + p1
            >>> print(p)
            x^2 + 10

        Returns:
            A new Polynomial with addition result    
        """
        return self + other

    def __sub__(self, other):
        """ Subtraction operator for this polynomial instance

        Args:
            other : Either a Polynomial or a Number
            In case of Polynomial, it must have the same set of variables in the same order

        Examples:
            >>> p1 = Polynomial('x^2 + 5')
            >>> scalar = 5
            >>> p = p1 - scalar
            >>> print(p)
            x^2

            >>> p1 = Polynomial('4*x^3 + 6*x^2 + 10*x + 6')
            >>> p2 = Polynomial('x^3 - 6*x^2 + 11*x - 6')
            >>> p = p1 - p2
            >>> print(p)
            3*x^3 + 12*x^2 - x + 12

            >>> p1 = Polynomial('x^2 + y^2 -25')
            >>> p2 = Polynomial('x - y + 1')
            >>> p = p1 - p2
            >>> print(p)
            x^2 + y^2 - x + y - 26

        Returns:
            A new Polynomial with subtraction result
        """
        if not isinstance(other, Polynomial):
            other = Polynomial([(other, (0,)*len(self._variables))],
                               self._variables)

        if self._variables != other._variables:
            raise ValueError('Both Polynomials must have the same set of variables and in the same order')

        if len(other._coeff_exp_list) == 0:
            return Polynomial(list(self._coeff_exp_list), list(self._variables))

        other_coeffients = other.coefficients()
        other_coeffients = [coeff * -1 for coeff in other_coeffients]
        other_exponents = other.exponents()
        other_coeff_exp_list = zip(other_coeffients, other_exponents)
        other = Polynomial(other_coeff_exp_list, other._variables)

        return self + other

    def __rsub__(self, other):
        """ Right hand operand subtraction operator for this polynomial instance
        It is only called in case the left hand operand doensn't support subtraction
        with Polynomial instance (e.g., Python Number(int, long, float) values)

        Args:
            other : Either a Polynomial or a Number
            In case of Polynomial, it must have the same set of variables in the same order

        Examples:
            >>> p1 = Polynomial('x^2 + 5')
            >>> scalar = 5
            >>> p = scalar - p1
            >>> print(p)
            - x^2

        Returns:
            A new Polynomial with subtraction result
        """
        return self - other

    def __mul__(self, other):
        """ Multiplication operator for this polynomial instance

        Args:
            other : Either a Polynomial or a Number
            In case of Polynomial, it must have the same set of variables in the same order

        Examples:
            >>> p1 = Polynomial('x^2 + 5')
            >>> scalar = 2
            >>> p = p1 * scalar
            >>> print(p)
            2*x^2 + 10

            >>> p1 = Polynomial('x - 1')
            >>> p2 = Polynomial('x + 1')
            >>> p = p1 * p2
            >>> print(p)
            x^2 - 1

            >>> p1 = Polynomial('x^2 + y^2 -25')
            >>> p2 = Polynomial('x - y + 1')
            >>> p = p1 * p2
            >>> print(p)
            x^3 - x^2*y + x*y^2 - y^3 + x^2 + y^2 - 25*x + 25*y - 25


        Returns:
            A new Polynomial with multiplication result
        """
        if not isinstance(other, Polynomial):
            other = Polynomial([(other, (0,)*len(self._variables))],
                               self._variables)

        if self._variables != other._variables:
            raise ValueError('Both Polynomials must have the same set of variables and in the same order')

        mul_polynomials_list = []
        mul_variables = self._variables
        self_coeff_exp_list = self._coeff_exp_list
        other_coeff_exp_list = other._coeff_exp_list

        for self_coeff_exp_term in self_coeff_exp_list:
            for other_coeff_exp_term in other_coeff_exp_list:
                coefficient = self_coeff_exp_term[0] * other_coeff_exp_term[0]
                exponents = tuple(map(add, self_coeff_exp_term[1], other_coeff_exp_term[1]))
                p = Polynomial([(coefficient, exponents)], mul_variables)
                mul_polynomials_list.append(p)

        mul = Polynomial([], mul_variables)
        for p in mul_polynomials_list:
            mul = mul + p

        return mul

    def __rmul__(self, other):
        """ Right hand operand multiplication operator for this polynomial instance
        It is only called in case the left hand operand doensn't support multiplication
        with Polynomial instance (e.g., Python Number(int, long, float) values)

        Args:
            other : Either a Polynomial or a Number
            In case of Polynomial, it must have the same set of variables in the same order

        Examples:
            >>> p1 = Polynomial('x^2 + 5')
            >>> scalar = 2
            >>> p = scalar * p1
            >>> print(p)
            2*x^2 + 10

        Returns:
            A new Polynomial with multiplication result
        """
        return self * other

    def __pow__(self, other):
        """ Power operator for this polynomial instance

        Args:
            other (exponent) : A non negative integer

        Examples:
            >>> p1 = Polynomial('x - 1')
            
            >>> p = p1 ** 0
            >>> print(p)
            1
            
            >>> p = p1 ** 1
            >>> print(p)
            x - 1
            
            >>> p = p1 ** 2
            >>> print(p)
            x^2 - 2*x + 1
            
            >>> p = p1 ** 3
            >>> print(p)
            x^3 - 3*x^2 + 3*x - 1

            
            >>> p1 = Polynomial('x - y + 1')
            
            >>> p = p1 ** 0
            >>> print(p)
            1
            
            >>> p = p1 ** 1
            >>> print(p)
            x - y + 1
            
            >>> p = p1 ** 2
            >>> print(p)
            x^2 - 2*x*y + y^2 + 2*x - 2*y + 1

        Returns:
            A new Polynomial with power result
        """
        if not isinstance(other, int):
            raise TypeError('The exponent other should be a non negative integer')
        
        if other < 0:
            raise ValueError('The exponent other should be a non negative integer')
        
        if other == 0:
            return Polynomial([(1, (0,)*len(self._variables))],
                              self._variables)
        if other == 1:
            return self
        if other % 2 == 0:
            return self**(other/2) * self**(other/2)
        else:
            return self**((other-1)/2+1) * self**((other-1)/2)


    def to_string(self):
        """ polynomial string representation

        Build a human readable string for the polynomial using defult list of
        variables ['x0', 'x1', ... 'xn'] or other variables if given when
        polynomial object is instantiated.

        Examples:
            >>> p1 = Polynomial([(1, (3,)), (-6, (2,)), (11, (1,))])
            >>> print(p1.to_string())
            x0^3 - 6*x0^2 + 11*x0

            >>> p2 = Polynomial([(1, (3,)), (-6, (2,)), (11, (1,))], ['x'])
            >>> print(p2.to_string())
            x^3 - 6*x^2 + 11*x

            >>> p3 = Polynomial([(1, (1, 0)), (-1, (0, 1)), (1, (0, 0))])
            >>> print(p3.to_string())
            x0 - x1 + 1

            >>> p4 = Polynomial([(1, (1, 0)), (-1, (0, 1)), (1, (0, 0))], ['x', 'y'])
            >>> print(p4.to_string())
            x - y + 1

        Returns:
            The polynomial string representation
        """
        if len(self._coeff_exp_list) == 0:
            return ''

        poly_str = self._to_string_term(self._coeff_exp_list[0])
        for coeff_exp_term in self._coeff_exp_list[1:]:
            term_str = self._to_string_term(coeff_exp_term)
            if term_str[0] == '-':
                poly_str = poly_str + term_str.replace('-', ' - ')
            else:
                poly_str = poly_str + ' + ' + term_str
        return poly_str

    def _to_string_term(self, coeff_exp_term):

        coefficient = coeff_exp_term[0]
        exponents = coeff_exp_term[1]

        if isinstance(coefficient, float) and coefficient.is_integer() and abs(int(coefficient)) == 1:
            term_str = repr(int(coefficient))
        else:
            term_str = repr(coefficient)

        for i in range(0, len(exponents)):
            if exponents[i] == 0:
                continue
            elif exponents[i] == 1:
                if term_str == '1':
                    term_str = self._variables[i]
                elif term_str == '-1':
                    term_str = '-' + self._variables[i]
                else:
                    term_str = term_str + '*' + self._variables[i]
            else:
                if term_str == '1':
                    term_str = self._variables[i] + '^' + repr(exponents[i])
                elif term_str == '-1':
                    term_str = '-' + self._variables[i] + '^' + repr(exponents[i])
                else:
                    term_str = term_str + '*' + self._variables[i] + '^' + repr(exponents[i])
        return term_str

    def __str__(self):
        return self.to_string()

    def __repr__(self):
        return self.to_string()
