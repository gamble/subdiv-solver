class Parser(object):

    def __init__(self, expr):
        self._expr = expr
        self._variables = []
        self._coefficients = []
        self._coeff_exp_list = []
        # A list of dicts for each term, {key = 'variable', value = 'exponent'}
        self._terms_variables_exponents = []
        self._index = 0

    def parse(self):
        if self._expr is None or self._expr.strip() == '':
            return ([], [])
        
        self.skipWhitespace()
        char = self.peek()
        if char == '-':
            self._index += 1
            self.parseTerm(True)
        else:
            self.parseTerm()

        while self._index < len(self._expr):
            self.skipWhitespace()
            char = self.peek()
            if char == '+':
                self._index += 1
                self.parseTerm()
            elif char == '-':
                self._index += 1
                self.parseTerm(True)
            else:
                break

        n_variables = len(self._variables)
        n_terms = len(self._terms_variables_exponents)
        for i in range(0, n_terms):
            coeff = self._coefficients[i]
            term_variables_exponents = self._terms_variables_exponents[i]
            exponents = []
            for variable in self._variables:
                exponent = 0
                if variable in term_variables_exponents:
                    exponent = term_variables_exponents[variable]
                exponents.append(exponent)
            exponents = tuple(exponents)
            self._coeff_exp_list.append((coeff, exponents))

        return (self._coeff_exp_list, self._variables)

    def parseTerm(self, negative_term=False):
        term_variables_exponents = {}
        if negative_term:
            coeff = -1
        else:
            coeff = 1

        while self.hasNext():
            self.skipWhitespace()
            char = self.peek()
            if char in '0123456789.':
                coeff = coeff * self.parseNumber()
            elif char == '*':
                self._index += 1
            elif char.lower() in '_abcdefghijklmnopqrstuvwxyz':
                variable_exponent = self.parseVariableAndExponent()
                term_variables_exponents[variable_exponent[0]] = variable_exponent[1]
            else:
                break

        self._coefficients.append(coeff)
        self._terms_variables_exponents.append(term_variables_exponents)

    def parseVariableAndExponent(self):
        variable = self.parseVariable()
        self.addVariable(variable)
        
        exponent = 1
        while self.hasNext():
            self.skipWhitespace()
            char = self.peek()
            if char == '^':
                self._index += 1
                exponent = self.parseNumber()
            else:
                break

        return (variable, exponent)

    def parseNumber(self):
        strValue = ''
        decimal_found = False
        char = ''
        
        while self.hasNext():
            self.skipWhitespace()
            char = self.peek()
            if char in 'e':
                self._index += 1
                char2 = self.peek()
                if char2 in '+-':
                    strValue += char + char2
                else:
                    raise Exception('Expecting to find + or - '+ str(self._index) + 'after e, instead ' + char2 + ' was found.')
            elif char in '0123456789.':
                strValue += char
            else:
                break
            
            self._index += 1
        
        if len(strValue) == 0:
            if char == '':
                raise Exception("Unexpected end found")
            else:
                raise Exception(
                    "I was expecting to find a number at character " +
                    str(self._index) +
                    " but instead I found a '" +
                    char +
                    "'. What's up with that?")

        value = float(strValue)
        if value.is_integer():
            return int(value)
        else:
            return value
    
    def parseVariable(self):
        variable = ''
        while self.hasNext():
            self.skipWhitespace()
            char = self.peek()
            if char.lower() in '_abcdefghijklmnopqrstuvwxyz0123456789':
                variable += char
                self._index += 1
            else:
                break

        return variable

    def hasNext(self):
        return self._index < len(self._expr)

    def peek(self):
        return self._expr[self._index:self._index + 1]

    def addVariable(self, variable):
        if variable not in self._variables:
            self._variables.append(variable)
            self._variables.sort()


    def skipWhitespace(self):
        while self.hasNext():
            if self.peek() in ' \t\n\r':
                self._index += 1
            else:
                return
