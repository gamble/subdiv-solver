from decimal import Decimal
from decimal import getcontext
from decimal import ROUND_DOWN, ROUND_UP


class Interval(object):
    def __init__(self, lower, upper, precision=53):
        if not isinstance(lower, (int, long, float, str)) or not isinstance(upper, (int, long, float, str)):
            raise TypeError(
                'lower %s and upper %s attributes must be of type string, int, long or float' % (lower, upper))

        self._lower = lower
        self._upper = upper
        self._precision = precision

    def lower(self):
        return self._lower

    def upper(self):
        return self._upper

    def precision(self):
        return self._precision

    def absolute_diameter(self):
        """ The absolute diameter (rounded up) of this interval

        Examples:
            >>> i1 = Interval(-1, 3)
            >>> print(i1.absolute_diameter())
            4.0

        Returns:
            float
        """
        getcontext().prec = self._precision
        getcontext().rounding = ROUND_UP
        return float(Decimal(self._upper) - Decimal(self._lower))

    def center(self):
        """ The center of this interval

        Examples:
            >>> i1 = Interval(-1, 3)
            >>> print(i1.center())
            1.0

        Returns:
            float
        """
        return float((Decimal(self._upper) + Decimal(self._lower)) / 2)

    def invert(self):
        """ The inversion of this interval

        If signs of lower and upper values are equal,
        then the inverted interval is
            (1 / upper, 1 / lower)
            where the new lower is rounded down
            and the new upper is rounded up.
            lower and upper values of new Interval are of type str

        else the inverted interval is
            (-inf, inf)

        Examples:
            >>> i1 = Interval(1, 3)
            >>> print(i1.invert())
            (0.33333333333333333333333333333333333333333333333333333, 1)

            >>> i1 = Interval(-1, 3)
            >>> print(i1.invert())
            (-inf, inf)

        Returns:
            A new Interval
        """
        lower_sign = '-' if str(self._lower)[0] == '-' else '+'
        upper_sign = '-' if str(self._upper)[0] == '-' else '+'
        if lower_sign == upper_sign:
            getcontext().prec = self._precision
            getcontext().rounding = ROUND_DOWN
            lower = str(1 / Decimal(self._upper))
            getcontext().rounding = ROUND_UP
            upper = str(1 / Decimal(self._lower))
            return Interval(lower, upper)
        else:
            return Interval(float("-inf"), float("inf"))

    def signs(self):
        """ The signs of lower and upper values of this interval
            represented as 1 for positive and -1 for negative

        Examples:
            >>> i1 = Interval(1, 3)
            >>> print(i1.signs())
            ('1', '1')
            >>> i1 = Interval(-1, 3)
            >>> print(i1.signs())
            ('-1', '1')
            >>> i1 = Interval(-3, -1)
            >>> print(i1.signs())
            ('-1', '-1')

        Returns:
            A tuple of two integers
        """
        lower_sign = -1 if str(self._lower)[0] == '-' else 1
        upper_sign = -1 if str(self._upper)[0] == '-' else 1
        return (lower_sign, upper_sign)

    def union(self, other):
        """ The union (convex hull) of this interval with another interval or scalar
            lower and upper values of new Interval are of type str
        
        Args: 
            other: Interval or Scalar(int, long, float)

        Examples:
            >>> i1 = Interval(-1, 3)
            >>> i2 = Interval(2, 4)
            >>> print(i1.union(i2))
            (-1, 4)
            >>> i1 = Interval(-1, 3)
            >>> i2 = 6
            >>> print(i1.union(i2))
            (-1, 6)

        Returns:
            A new Interval
        """
        getcontext().prec = self._precision
        if isinstance(other, Interval):
            lower = min(Decimal(self.lower()), Decimal(other.lower()))
            upper = max(Decimal(self.upper()), Decimal(other.upper()))
            return Interval(str(lower), str(upper))
        else:
            lower = min(Decimal(self.lower()), Decimal(other))
            upper = max(Decimal(self.upper()), Decimal(other))
            return Interval(str(lower), str(upper))

    def __str__(self):
        return '(' + str(self._lower) + ', ' + str(self._upper) + ')'

    def __repr__(self):
        return '(' + repr(self._lower) + ', ' + repr(self._upper) + ')'
