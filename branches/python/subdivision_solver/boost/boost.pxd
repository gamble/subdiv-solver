#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++

from libc.float cimport DBL_MAX
from libc.math  cimport nextafter
from subdivision_solver.boost.interfaceboost cimport cppinterval
from subdivision_solver.utils.interval import Interval

cdef inline isnan(x):
    return x!=x

cdef inline void to_cppinterval(n, cppinterval* destination):
    if isinstance(n, Interval):
        n_lower = n.lower()
        n_upper = n.upper()
    elif isinstance(n, tuple):
        n_lower = n[0]
        n_upper = n[1]
    else:
        n_lower = n
        n_upper = n

    destination[0] = cppinterval(to_double_toward(n_lower, -DBL_MAX),
                                to_double_toward(n_upper, DBL_MAX))

cdef inline from_cppinterval (cppinterval* a):
    cdef double l = a.lower()
    cdef double u = a.upper()
    return Interval(float(l), float(u))
    
cdef inline double to_double_toward(n, direction):
    if isinstance(n, str):
        try:
            n = int(n)
        except ValueError:
            n = nextafter(float(n), direction)
    if isinstance(n, int):
        if n > -2**53 and n < 2**53:
            return float(n)
        else:
            return nextafter(float(n), direction)
    elif isinstance(n, float):
        if not isnan(n):
            return n
        elif direction < 0:
            return -float('inf')
        else:
            return float('inf')
    else:
        raise ValueError('Object cannot be converted to double')
