#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#


from subdivision_solver.utils.interval import Interval
from subdivision_solver.mpfi.interfacempfi cimport cppinterval, mpfr_exp_t, mpfr_get_str, GMP_RNDD, GMP_RNDU
from libc.float cimport DBL_MAX
from libc.math  cimport nextafter


cdef inline void to_cppinterval(n, cppinterval* destination):
    if isinstance(n, Interval):
        destination[0] = cppinterval(nextafter(float(n.lower()), -DBL_MAX),
                                     nextafter(float(n.upper()), DBL_MAX))
    elif isinstance(n, tuple):
        destination[0] = cppinterval(nextafter(float(n[0]), -DBL_MAX),
                                     nextafter(float(n[1]), DBL_MAX))
    else:
        print'====================================='
        print'====================================='
        print'====================================='
        print'WE HAVE A PROBLEM'
        print'====================================='
        print'====================================='
        print'====================================='



cdef inline from_cppinterval (cppinterval* a):
    cdef mpfr_exp_t left_exp, right_exp
    left_sign = ''
    left_mantisse = mpfr_get_str( NULL, &left_exp, 10, 0, &a.value.left, GMP_RNDD).decode('utf-8')
    right_sign = ''
    right_mantisse = mpfr_get_str( NULL, &right_exp, 10, 0, &a.value.right, GMP_RNDU).decode('utf-8')
    if left_mantisse[0] == '-':
        left_mantisse = left_mantisse[1:]
        left_sign = '-'
    if right_mantisse[0] == '-':
        right_mantisse = right_mantisse[1:]
        right_sign = '-'
    if len(left_mantisse) > left_exp:
        left_mantisse = left_mantisse[:left_exp] + '.' + left_mantisse[left_exp:]
        left_exp_str = ''
    else:
        left_mantisse = '0.' + left_mantisse
        left_exp_str = 'e' + str(left_exp)
    if len(right_mantisse) > right_exp:
        right_mantisse = right_mantisse[:right_exp] + '.' + right_mantisse[right_exp:]
        right_exp_str = ''
    else:
        right_mantisse = '0.' + right_mantisse
        right_exp_str = 'e' + str(right_exp)

    return Interval(str(left_sign + left_mantisse + left_exp_str), str(right_sign + right_mantisse + right_exp_str))
