#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++

from cysignals.signals cimport sig_on, sig_off

cdef CSolveFromList( systempols system,\
                     object l,\
                     float minsize,\
                     int dealWithBorder,\
                     object initialDomain,\
                     float minsizeBorder,\
                     int lastPrecision,\
                     int nbsteps,\
                     int orderEval,\
                     int orderKraw,\
                     int controlEvaluationPrecision,\
                     int controlFcenterZero,\
                     int controlKrawczykPrecision,\
                     int verbosity) :
  
  #print "solve..."
  #print "avant init_eval: ", system
  init_eval(system);
  #print "aprea init_eval: ", system
  #print "boost solver, order 2: ", order2
  
  cdef int counter = 0;
  
  
    
  cdef subdivision_solver_i solver = subdivision_solver_i ( fast_func_Sys, fast_func_JacSys, fast_func_HessSys );
  
  if (nbsteps >=0):
    solver.setMaxStep(nbsteps);
  solver.setMinWidth(minsize);
  
  solver.setMinWidthBorder(minsizeBorder);
  
  if verbosity<=2 :
    solver.setVerbosity(0);
  else :
    solver.setVerbosity(verbosity);
  
  if (lastPrecision==True) or (lastPrecision==False):
    solver.setLastPrecision(lastPrecision)
  
  if (controlFcenterZero==True) or (controlFcenterZero==False):
    solver.setControlFcenterZero(controlFcenterZero)
    
  if (controlEvaluationPrecision==True) or (controlEvaluationPrecision==False):
    solver.setControlEvaluationPrecision(controlEvaluationPrecision)
    
  if (controlKrawczykPrecision==True) or (controlKrawczykPrecision==False):
    solver.setControlKrawczykPrecision(controlKrawczykPrecision)
    
  if (system)._nbConstraintsEqZero :
    solver.setConstraintsEqZero ( fast_CEZ_Sys, fast_CEZ_JacSys, fast_CEZ_HessSys )
  if (system)._nbConstraintsNeqZero :
    solver.setConstraintsNeqZero ( fast_CNZ_Sys, fast_CNZ_JacSys, fast_CNZ_HessSys )
  
  for i in range(0,len(l)):
    temp = P_matrix_interval( l[i] )
    solver.addWorking((<P_matrix_interval> temp)._mat)
  
  if dealWithBorder == 1:
    temp = P_matrix_interval( initialDomain )
    solver.dealWithBorder( (<P_matrix_interval> temp)._mat )
    
  if not (orderEval==2):
    solver.setOrderEval(orderEval)
    
  if not (orderKraw==1):
    solver.setOrderKraw(orderKraw)

  sig_on()
  solver.solve( )
  sig_off()
  counter = solver.getCounter();
  
  res = []
  solver.visitSolutions();
  while not solver.endSolutions() :
    temp = P_matrix_interval( [[],[]] )
    temp._mat = ( <matrix_interval> solver.nextSolutions() )
    res.append( temp.toList() )
  
  tooSmallPrecision = []
  solver.visitTooSmallPrecision();
  while not solver.endTooSmallPrecision() :
    temp = P_matrix_interval( [[],[]] )
    temp._mat = ( <matrix_interval> solver.nextTooSmallPrecision() )
    tooSmallPrecision.append( temp.toList() )
    
  tooSmallUser = []
  #solver.visitTooSmallUser();
  #while not solver.endTooSmallUser() :
    #temp = P_matrix_interval( [[],[]] )
    #temp._mat = ( <matrix_interval> solver.nextTooSmallUser() )
    #tooSmallUser.append( temp.toList() )
  
  solver.computeConnectedComponentInTooSmallUser();
  CCIntooSmallUser = []
  solver.visitCCInTooSmallUser();
  while not solver.endCCInTooSmallUser() :
    temp = P_matrix_interval( [[],[]] )
    temp._mat = ( <matrix_interval> solver.nextCCInTooSmallUser() )
    CCIntooSmallUser.append( temp.toList() )
    
  notInversible = []
  solver.visitNotInversible();
  while not solver.endNotInversible() :
    temp = P_matrix_interval( [[],[]] )
    temp._mat = ( <matrix_interval> solver.nextNotInversible() )
    notInversible.append( temp.toList() )
    
  if verbosity==1 or verbosity>2:
    solver.printStatsColor()
  if verbosity==2:
    solver.printStats()
    
  #print "Profiling: "
  #global time_in_f
  #print " time_in_f ", time_in_f
  #global time_in_r
  #print " time_in_r ", time_in_r 
  return [res, tooSmallPrecision, tooSmallUser, CCIntooSmallUser, notInversible, counter] 

def SolveFromList( system, l, minsize, dealWithBorder, initialDomain, minsizeBorder, lastPrecision, nbsteps, orderEval, orderKraw, controlEvaluationPrecision, controlFcenterZero, controlKrawczykPrecision, verbosity):
  return CSolveFromList( <systempols> system, <object> l, <float> minsize, <int> dealWithBorder, <object> initialDomain, <float> minsizeBorder, <int> lastPrecision, <int> nbsteps, <int>orderEval, <int>orderKraw, <int> controlEvaluationPrecision, <int> controlFcenterZero, <int> controlKrawczykPrecision, <int> verbosity )

def areTheSame( system, sol1, sol2, verbosity):
  init_eval(system);
  
  cdef subdivision_solver_i solver = subdivision_solver_i ( fast_func_Sys, fast_func_JacSys, fast_func_HessSys );
  if verbosity<=2 :
    solver.setVerbosity(0);
  else :
    solver.setVerbosity(verbosity);
    
  solver.setControlKrawczykPrecision(False)
  P_sol1 = P_matrix_interval( sol1 )
  P_sol2 = P_matrix_interval( sol2 )
  return solver.areTheSame( (<P_matrix_interval> P_sol1)._mat, (<P_matrix_interval> P_sol2)._mat )
  
def Krawczyk( system, box, controlKrawczykPrecision, verbosity):
  
  #print "--------------solver_boost: begin Krawczyk"
  
  init_eval(system);
  
  cdef subdivision_solver_i solver = subdivision_solver_i ( fast_func_Sys, fast_func_JacSys, fast_func_HessSys );
  
  if verbosity<=2 :
    solver.setVerbosity(0);
  else :
    solver.setVerbosity(verbosity);
    
  if (controlKrawczykPrecision==True) or (controlKrawczykPrecision==False):
    solver.setControlKrawczykPrecision(controlKrawczykPrecision)
  
  P_box = P_matrix_interval(box)
  #print "--------------solver_boost: apply Krawczyk"
  flag = solver.ApplyKrawczykOnBox( (<P_matrix_interval> P_box)._mat )
  #print "--------------solver_boost: return Krawczyk: flag: ", flag
  
  res = []
  solver.visitSolutions();
  while not solver.endSolutions() :
    temp = P_matrix_interval( [[],[]] )
    temp._mat = ( <matrix_interval> solver.nextSolutions() )
    res.append( temp.toList() )
  
  tooSmallPrecision = []
  solver.visitTooSmallPrecision();
  while not solver.endTooSmallPrecision() :
    temp = P_matrix_interval( [[],[]] )
    temp._mat = ( <matrix_interval> solver.nextTooSmallPrecision() )
    tooSmallPrecision.append( temp.toList() )
  
  notInversible = []
  solver.visitNotInversible();
  while not solver.endNotInversible() :
    temp = P_matrix_interval( [[],[]] )
    temp._mat = ( <matrix_interval> solver.nextNotInversible() )
    notInversible.append( temp.toList() )

  #print "--------------solver_boost: end Krawczyk: len(res): ", len(res), ", len(tooSmallPrecision): ", len(tooSmallPrecision), ", len(notInversible): ", len(notInversible)
  return [res, tooSmallPrecision, notInversible, flag]

def KrawczykTest( system, box, controlKrawczykPrecision, verbosity):
  
  #print "--------------solver_boost: begin KrawczykTest"
  
  init_eval(system);
  
  cdef subdivision_solver_i solver = subdivision_solver_i ( fast_func_Sys, fast_func_JacSys, fast_func_HessSys );
  
  if verbosity<=2 :
    solver.setVerbosity(0);
  else :
    solver.setVerbosity(verbosity);
    
  if (controlKrawczykPrecision==True) or (controlKrawczykPrecision==False):
    solver.setControlKrawczykPrecision(controlKrawczykPrecision)
  
  P_box = P_matrix_interval(box)
  #print "--------------solver_boost: apply KrawczykTest"
  flag = solver.ApplyKrawczykTestOnBox( (<P_matrix_interval> P_box)._mat )
  #print "--------------solver_boost: return KrawczykTest: flag: ", flag
  
  res = []
  solver.visitSolutions();
  while not solver.endSolutions() :
    temp = P_matrix_interval( [[],[]] )
    temp._mat = ( <matrix_interval> solver.nextSolutions() )
    res.append( temp.toList() )
  
  tooSmallPrecision = []
  solver.visitTooSmallPrecision();
  while not solver.endTooSmallPrecision() :
    temp = P_matrix_interval( [[],[]] )
    temp._mat = ( <matrix_interval> solver.nextTooSmallPrecision() )
    tooSmallPrecision.append( temp.toList() )
  
  notInversible = []
  solver.visitNotInversible();
  while not solver.endNotInversible() :
    temp = P_matrix_interval( [[],[]] )
    temp._mat = ( <matrix_interval> solver.nextNotInversible() )
    notInversible.append( temp.toList() )

  #print "--------------solver_boost: end KrawczykTest: len(res): ", len(res), ", len(tooSmallPrecision): ", len(tooSmallPrecision), ", len(notInversible): ", len(notInversible)
  return [res, tooSmallPrecision, notInversible, flag]
