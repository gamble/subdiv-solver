__all__ = [ 'subdivision_solver', 'Polynomial', 'Interval' ]
from subdivision_solver.utils.interval import Interval as Interval
from subdivision_solver.utils.polynomial import Polynomial as Polynomial
from subdivision_solver.solver.solver_mp import subdivision_solver as subdivision_solver


def solve(system, domains, max_box_size, verbose=False):
    """ Solve a system of multivariante polynomials

    Args:
        system (List of strings): List of Python strings
            Each string represents a single expanded polynomial
            Ex: 'x - 1', 'x^2 - 2*x*y +3', 'x*y*z', 'x^2 + y^2 - 1' ... etc

            Notes:
                1- '(x - 1)*(x - 2)' is not a valid parameters
                    Because it is not in the expanded form.
                    Instead it must be written as 'x^2 - 3*x + 2'

                2- 'xyz' is not like 'x*y*z', 
                    The first will be parsed as a single variable named 'xyz'.
                    The second is 3 multiplied variables named 'x', 'y' and 'z'

        domains (List of tuples): Each tuple consists of 3 elements
            (str, float, float): e.g., ('x', -1.3, 2.5)
            A string represents the variable name
            and 2 floats that represents the lower and upper values of the interval
            to be used to search for solutions w.r.t this variable

        max_box_size (float): Maximum box size to be checked

        verbose (boolean) : optional, default is False
            True: to display verbose information

    Examples:
        >>> solve(['x^2 - 4*x + 3'], [('x', -5, 5)], 1e-5)
        ([[[('.99999999875899003', '1.0000000012410102')]],
        [[('2.9999999563961337', '3.0000000436038663')]]],
        [])
        
        A pair of lists:
        1- List of solutions (1, 3) as Intervals
        2- List of undetermined intervals, in this case it is empty list

        >>> solve(['x^2 + y^2 - 1', 'x*y'], [('x', -100.2, 200.3), ('y', -100.2, 200.3)], 1e-5)
    
    Returns:
        Pair of lists; solutions, and undetermineds if found,
        Otherwise None
    """
    if system is None or len(system) == 0:
        raise ValueError('List of polynomials cannot be empty')
    
    if domains is None or len(domains) == 0:
        raise ValueError('List of domians cannot be empty')

    if max_box_size is None:
        raise ValueError('max_box_size cannot be none')

    if len(system) != len(domains):
        raise ValueError('Number of polynomials must be equal to the number of domains')

    if verbose == False:
        verbose = 'silent'
    elif verbose == True:
        verbose = 'stats'
    else:
        raise ValueError('Verbose level boolean either True or False')

    precision = 53
    polynomials = []
    variables_set = set()
    intervals = []
    for i in range(len(system)):
        polynomial_string = system[i]
        p = Polynomial(polynomial_string)
        polynomials.append(p)
        variables_set.update(p.variables())

    variables = sorted(list(variables_set))
    if len(variables) != len(domains):
            raise ValueError('Count of all variables of the given Polynomials must be equal to '
                + 'the count of the given domains\n.'
                + 'Only ' + str(len(variables)) + ' variables ' + repr(variables)
                + '\nOnly ' + str(len(domains)) + ' variables ' + repr(domains))

    for variable in variables:
        for domain in domains:
            if variable == domain[0]:
                interval = Interval(domain[1], domain[2])
                intervals.append([interval])

    solver = subdivision_solver(polynomials, variables)
    status = solver.solve(intervals, max_box_size, precision, verbose)

    while status != 0:
        if status == 1: # requires more precision
            if verbose == 'stats':
                print('Status = ' + str(status))
                print("Boxes containing a unique solution:")
                print(solver.getSolutions())
                print(solver.getUndetermined())
                print('Increasing Maximum Precision')
                precision *= 2
                print('Maximum Precision = ' + str(precision))
            # solver = subdivision_solver(polynomials, variables)
            status = solver.solve(intervals, max_box_size, precision, verbose)
        elif status == 2: # minimum width reached
            break
        elif status == 3: # bad arguments
            break

    if status == 0:
        solutions = solver.getSolutions(max_box_size)
        if verbose == 'stats':
            print('Status = ' + str(status))
            print("Boxes containing a unique solution:")
            print(solutions)
        return solutions, []
    elif status == 2:
        solutions = solver.getSolutions(max_box_size)
        undetermineds = solver.getUndetermined()
        print('Status = ' + str(status))
        print('Maximum Box Width is reached, please try again with a different max_box_size (smaller value)')
        return solutions, undetermineds
    elif status == 3:
        print('Status = ' + str(status))
        print('Bad Arguments')

