#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++

from subdivision_solver.utils.interval import Interval

from subdivision_solver.mpfi.interfacempfi cimport cppinterval
from subdivision_solver.mpfi.mpfi cimport to_cppinterval, from_cppinterval

cdef class P_matrix_interval:
  
  #cdef matrix_interval _mat
  
  def __cinit__(self, tableauOrMatrix):
      cdef int r,c, R, C, precision
      cdef cppinterval temp
      #cdef cppfloat dummy
      #cdef cppinterval dummyinterval
	
      _has_been_inverted = 0
      
      if isinstance(tableauOrMatrix, P_matrix_interval):
        self._mat = matrix_interval ( (<P_matrix_interval> tableauOrMatrix)._mat )
        self._has_been_inverted = (<P_matrix_interval> tableauOrMatrix)._has_been_inverted
        if self._has_been_inverted:
          self._inv = matrix_interval ( (<P_matrix_interval> tableauOrMatrix)._inv )
          
      elif isinstance(tableauOrMatrix,list) :
        if len(tableauOrMatrix):
          if type(tableauOrMatrix[0]) == list :
            R = len(tableauOrMatrix)
            C = len(tableauOrMatrix[0])
          else :
            R = 1
            C = len(tableauOrMatrix)
            tableauOrMatrix = [tableauOrMatrix]
        else :
          R = 0
          C = 0
        
        self._mat = matrix_interval(R,C)
        if not (R==0 or C==0) :
          if hasattr(tableauOrMatrix[0][0], 'precision'):
            precision = tableauOrMatrix[0][0].precision()
          else :
            precision = 53
          #temp  
          #if precision < 53:
            #precision = 53
          #temp
          #print "precisionM: ", precision
          #dummy.set_prec(precision)###mpfi
          #dummyinterval.set_prec(precision)###mpfi
          #set_mpfi_mpfr_precision(precision)
          #temp = cppinterval(precision)  
        for r in range(0,R) :
          for c in range(0,C) :
            to_cppinterval(tableauOrMatrix[r][c], &temp )
            (self._mat).set(r,c, temp)
      else : 
        print "initialisation of matrix not possible" 
  
  def __copy__(self):
    return P_matrix_interval(self)
  
  def __str__(self):
    cdef int r, c, R, C
    cdef cppinterval temp
    R = self._mat.getNbLines()
    C = self._mat.getNbColumns()
    if R == 0 or C == 0 :
      return '[ ]'
    else: 
      res = ''
      for r in range(0,R):
        res = res + '['
        for c in range(0,C):
          temp = (self._mat).get(r,c)
          res = res + str(from_cppinterval( &( temp ) ) )
          if c < (C-1):
            res = res + ', '
          else :
            res = res + ']' 
        if r < (R-1):
          res = res + '\n' 
      return res
      
  def __getitem__(self, indice):
    cdef cppinterval temp
    if self._mat.getNbLines() > 1 :
      temp = (self._mat).get(indice[0],indice[1])
      return from_cppinterval(&temp)
    elif type(indice) == tuple :
      temp = (self._mat).get(0,indice[1])
      return from_cppinterval(&temp)
    else :
      temp = (self._mat).get(0,indice)
      return from_cppinterval(&temp)
    
  def __setitem__ (self, indice, valeur):
    cdef cppinterval temp
    cdef int precision
    if hasattr(valeur, 'precision'):
      precision = valeur.precision()
    # elif isinstance(valeur, RealNumber):
      # precision = valeur.precision()
    else :
      precision = 53
    #temp = cppinterval(precision)  
    to_cppinterval(valeur, &temp )      
    if self._mat.getNbLines() > 1 :
      (self._mat).set(indice[0],indice[1], temp)
    elif type(indice) == tuple :
      (self._mat).set(0,indice[1], temp)
    else :
      (self._mat).set(0,indice, temp)
      
  def toList ( self ):
    cdef int R, C, r, c
    R = self._mat.getNbLines()
    C = self._mat.getNbColumns()
    res = []
    for r in range(0,R) :
      res.append([])
      for c in range(0,C) :
        res[r].append( self[r,c] )
    return res
  
  def list(self):
    return self.toList()
      
  def getPrecision(self):
    cdef cppinterval temp
    temp = (self._mat).get(0,0)
    return <int> temp.get_prec()
  
  cpdef int nrows(self):
    return self._mat.getNbLines()
  
  cpdef int ncols(self):
    return self._mat.getNbColumns()
    
  #arithmetic operators
  def __add__(self, B):
    cdef matrix_interval res, Bmat
    cdef cppinterval Bval
    P_res = P_matrix_interval( [[],[]] )
    
    if not isinstance(B, P_matrix_interval): #B est un scalaire -> considéré comme self + mat*Id
      to_cppinterval( B, &Bval )
      Bmat = matrix_interval ( ((<P_matrix_interval> self)._mat).getNbLines(), ((<P_matrix_interval> self)._mat).getNbColumns(), Bval ) 
      res = ((<P_matrix_interval> self)._mat) + Bmat
      P_res._mat = res
    else :
      res = ((<P_matrix_interval> self)._mat) + ((<P_matrix_interval> B)._mat)
      P_res._mat = res
      
    return P_res
  
  def __sub__(self, B):
    cdef matrix_interval res, Bmat
    cdef cppinterval Bval
    P_res = P_matrix_interval( [[],[]] )
    
    if not isinstance(B, P_matrix_interval): #B est un scalaire -> considéré comme self + mat*Id
      to_cppinterval( B, &Bval )
      Bmat = matrix_interval ( ((<P_matrix_interval> self)._mat).getNbLines(), ((<P_matrix_interval> self)._mat).getNbColumns(), Bval ) 
      res = ((<P_matrix_interval> self)._mat) - Bmat
      P_res._mat = res
    else :
      res = ((<P_matrix_interval> self)._mat) - ((<P_matrix_interval> B)._mat)
      P_res._mat = res
      
    return P_res
  
  def __neg__(self):
    cdef matrix_interval res
    P_res = P_matrix_interval( [[],[]] )
    res = -((<P_matrix_interval> self)._mat)
    P_res._mat = res
    return P_res
  
  def __mul__(self, B):
    cdef matrix_interval res
    cdef cppinterval Bval
    P_res = P_matrix_interval( [[],[]] )
    
    if not isinstance(B, P_matrix_interval): #B est un scalaire
      to_cppinterval( B, &Bval )
      res = ((<P_matrix_interval> self)._mat)*Bval
      P_res._mat = res
    else:
      res = ((<P_matrix_interval> self)._mat)*((<P_matrix_interval> B)._mat)
      P_res._mat = res
      
    return P_res
  
  #the following functions modify self
  def delete_row(self, int rowIndex):
    (<P_matrix_interval> self)._mat = ((<P_matrix_interval> self)._mat).delL(rowIndex)
  
  def delete_column(self, int columnIndex):
    (<P_matrix_interval> self)._mat = ((<P_matrix_interval> self)._mat).delC(columnIndex)
    
  # be carefull: list of raws as to be in decreasing order
  def delete_rows(self, listofrows):
    P_res = P_matrix_interval(self)
    for i in range(0,len(listofrows)):
      P_res.delete_row(listofrows[i])
    return P_res
  
  # be carefull: list of raws as to be in decreasing order
  def delete_columns(self, listofcolumns):
    P_res = P_matrix_interval(self)
    for i in range(0,len(listofcolumns)):
      P_res.delete_column(listofcolumns[i])
    return P_res
  
  def transpose(self):
    cdef matrix_interval res
    P_res = P_matrix_interval( [[],[]] )
    res = ((<P_matrix_interval> self)._mat).transpose()
    P_res._mat = res
    return P_res
  
  def column(self, index):
    cdef matrix_interval res
    P_res = P_matrix_interval( [[],[]] )
    res = ((<P_matrix_interval> self)._mat).column( (<int> index) )
    P_res._mat = res
    return P_res
  
  def row(self, index):
    cdef matrix_interval res
    P_res = P_matrix_interval( [[],[]] )
    res = ((<P_matrix_interval> self)._mat).line( (<int> index) )
    P_res._mat = res
    return P_res
  
  def is_invertible(self):
    #for test only:
    #print "_has_been_inverted: ", str(self._has_been_inverted)
    #temp = P_matrix_interval ( [[]] )
    #temp._mat = (<P_matrix_interval> self)._inv
    #print "_inv: ", str(temp)
    #end test
    if self._has_been_inverted:
      if ((<P_matrix_interval> self)._inv).getNbLines() :
        return True
      else :
        return False
    else:
      self._has_been_inverted = 1
      return inverse( (<P_matrix_interval> self)._mat, &((<P_matrix_interval> self)._inv) )
  
  def inverse(self):
    #for test only:
    #print "_has_been_inverted: ", str(self._has_been_inverted)
    #temp = P_matrix_interval ( [[]] )
    #temp._mat = (<P_matrix_interval> self)._inv
    #print "_inv: ", str(temp)
    #end test
    inv = P_matrix_interval ( [[]] )
    if self._has_been_inverted:
      inv._mat = (<P_matrix_interval> self)._inv
    else :
      self._has_been_inverted = 1
      inverse( (<P_matrix_interval> self)._mat, &((<P_matrix_interval> self)._inv) )
      inv._mat = (<P_matrix_interval> self)._inv
    return inv
  
  def norm(self):
    cdef cppinterval res
    if (self.nrows()==1 and self.ncols()>=1) or (self.ncols()==1 and self.nrows()>=1) :
      res = ((<P_matrix_interval> self)._mat).norm()
    return from_cppinterval(&res)
  
  def augment(self, cols):
    cdef matrix_interval res
    P_res = P_matrix_interval ( [[]] )
    res = ((<P_matrix_interval> self)._mat).augment_cols((<P_matrix_interval> cols)._mat)
    P_res._mat = res
    return P_res
  
  def augment_row(self, rows):
    cdef matrix_interval res
    P_res = P_matrix_interval ( [[]] )
    res = ((<P_matrix_interval> self)._mat).augment_rows((<P_matrix_interval> rows)._mat)
    P_res._mat = res
    return P_res
  
  def determinant(self):
    cdef cppinterval res
    if (self.nrows()==self.ncols()) and self.nrows()>=1 :
      res = ((<P_matrix_interval> self)._mat).det()
    return from_cppinterval(&res)
  
  def center(self):
    cdef matrix_interval res
    P_res = P_matrix_interval ( [[]] )
    res = centerInterval( (<P_matrix_interval> self)._mat )
    P_res._mat = res
    return P_res

def middleInterval( A ):
  cdef matrix_interval res
  P_res = P_matrix_interval( [[],[]] )
  res = centerInterval( (<P_matrix_interval> A)._mat )
  P_res._mat = res
  return P_res

def overlaps( X, Y ):
  cdef intersection_result_interval over
  over = m_overlaps ( (<P_matrix_interval> X)._mat, (<P_matrix_interval> Y)._mat )
  P_res = P_matrix_interval( [[],[]] )
  P_res._mat = over._intersection
  return [over._overlaps, over._isIn, over._isInInt, P_res]

def P_inverse( A, B):
  return inverse( (<P_matrix_interval> A)._mat, &((<P_matrix_interval> B)._mat) )

#def overlaps_lists ( lA, lB ) :
  #cdef intersection_result_interval res
  #A = P_matrix_interval( lA )
  #B = P_matrix_interval( lB )
  #I = P_matrix_interval( [[],[]] )
  #res = overlaps[cppinterval](A._mat, B._mat )
  #I._mat = res._intersection
  #return [res._overlaps, res._isIn, res._isInInt, I.toList()]
  
#cdef from_list_matrix_interval (  list_matrix_interval l ) :
  ##temp = P_matrix_interval( [[],[]] )
  #res = []
  ##print l.isEmpty()
  #l.visit();
  #while not l.end() :
    #temp = P_matrix_interval( [[],[]] )
    #temp._mat = ( <matrix_interval> l.next() )
    #res.append( temp  )
  #return res

#def test_list_matrix_interval (Pm1, Pm2, Pm3, Pm4) :
  #cdef matrix_interval m1, m2, m3, m4
  #cdef list_matrix_interval l
  
  #m1 = (<P_matrix_interval> Pm1)._mat
  #m2 = (<P_matrix_interval> Pm2)._mat
  #m3 = (<P_matrix_interval> Pm3)._mat
  #m4 = (<P_matrix_interval> Pm4)._mat
  
  #l = list_matrix_interval ()
  #l.push_back(m1)
  #l.push_back(m2)
  #l.push_back(m3)
  #l.push_back(m4)
  
  #l2 = from_list_matrix_interval (l)
  
  #print len(l2)
  #for i in range(0,len(l2)) :
    #print l2[i]
  
  