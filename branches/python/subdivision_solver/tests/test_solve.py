from subdivision_solver import solve
import unittest


class SolveTest(unittest.TestCase):

    def setUp(self):
        pass
    
    def test1_monovariante1(self):
        res = solve(['x^2 - 4*x + 3'], [('x', -5, 5)], 1e-5, True)

    def test2_bivariante1(self):
        res = solve(['3*x - y - 7', '2*x + 3y - 1'], [('x', -100.2, 200.3), ('y', -100.2, 200.3)], 1e-5, True)

    # The following test case gives wrong results with 'zero' value solution
    def test3_bivariante2(self):
        res = solve(['x^2 + y^2 - 1', 'x*y'], [('x', -100.2, 200.3), ('y', -100.2, 200.3)], 1e-5, True)


if __name__ == '__main__':
    unittest.main()
