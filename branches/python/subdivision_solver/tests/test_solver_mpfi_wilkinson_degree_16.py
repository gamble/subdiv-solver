from time import time
from datetime import datetime
from random import randint

from subdivision_solver import subdivision_solver
from subdivision_solver import Polynomial
from subdivision_solver import Interval


# Wilkinson Polynomial with 16 roots
# Gives the following results
# *********************************Stats Subdivision Solver: precision 53*************
# *_counter: 44045
# *in queue: 0
# *boxes smallest than min size: 0
# *boxes requiring more precision: 8
# **detected by criterion empty:                 0
# **detected by criterion evaluation:            0
# **detected by criterion kraw:                  0
# **detected by criterion Fcenter contains zero: 8
# *boxes with not inversible jac: 0
# *_discarded: 22007
# *_discarded_by_evaluation: 22007
# *_discarded_by_krawczik: 0
# *nb solutions: 8
# ************************************************************************
# *********************************Stats Subdivision Solver: precision 106*************
# *_counter: 186
# *in queue: 0
# *boxes smallest than min size: 0
# *boxes requiring more precision: 0
# *boxes with not inversible jac: 0
# *_discarded: 89
# *_discarded_by_evaluation: 89
# *_discarded_by_krawczik: 0
# *nb solutions: 8
# ************************************************************************
# status = 0
# [[[(0.9851372070312495, 1.02326123046875)]], 
#  [[(1.994428833007812, 2.0039598388671878)]], 
#  [[(2.9995449905395497, 3.0001406784057623)]], 
#  [[(3.99989609146118, 4.000193935394288)]], 
#  [[(4.999949571609495, 5.000098493576051)]], 
#  [[(5.9999658398628215, 6.000003070354462)]], 
#  [[(6.9999820523262, 7.000019282817841)]], 
#  [[(15.999941942214962, 16.000090864181523)]], 
#  [[('7.99999827873706790964998391768516', '8.00000758635997778894737164978676')]], 
#  [[('8.99999589455127644965821831146621', '9.00000520217418688462222986845865')]], 
#  [[('9.99999351036548543436628521885358', '10.0000028179883952025303481859774')]], 
#  [[('10.9999957776665681149430360257762', '11.0000004314780229017834083399824')]], 
#  [[('11.9999980403184881976035982519269', '12.0000073479413977990676740715836')]], 
#  [[('12.9999956561326971263841802938222', '13.0000049637556068945482432609461')]], 
#  [[('13.9999932719469054442922986113409', '14.0000025795698158792563101683335')]], 
#  [[('14.9999908877611148729785530663285', '15.0000001953840248634092655634086')]]]
def test_wilkinson16():
    # p1 = Polynomial([(20922789888000.0, (0,)), (-70734282393600.0, (1,)),
    #                 (102992244837120.0, (2,)), (-87077748875904.0, (3,)),
    #                 (48366009233424.0, (4,)), (-18861567058880.0, (5,)),
    #                 (5374523477960.0, (6,)), (-1146901283528.0, (7,)),
    #                 (185953177553.0, (8,)), (-23057159840.0, (9,)),
    #                 (2185031420.0, (10,)), (-156952432.0, (11,)),
    #                 (8394022.0, (12,)), (-323680.0, (13,)),
    #                 (8500.0, (14,)), (-136.0, (15,)),
    #                 (1.0, (16,))], ['x'])
                        
    p1 = Polynomial('x - 1')
    for i in range (2, 17):
        p1 = p1 * Polynomial('x - ' + str(i))


    print(p1)

    tab = [[Interval(-1.3, 18.2)]]

    t = time()
    print 'Start time : ' + str(datetime.now())

    test = subdivision_solver([p1], ['x'])
    status = test.solve(tab, 10e-10, 1000, 'stats_color')
    
    print '\nElapsed time : ' + str(time() - t) + ' seconds.'
    print 'status = ', status
    res = test.getSolutions();
    print "Number of Solutions: ", len(res)
    for i in range(0,len(res)):
        print "Solution : ", res[i]


test_wilkinson16()
