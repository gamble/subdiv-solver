from time import time
from datetime import datetime
from random import randint

from subdivision_solver import subdivision_solver
from subdivision_solver import Polynomial
from subdivision_solver import Interval


def test_random_polynomial():
    p = generate_random_poly_2_variables()
    q = generate_random_poly_2_variables()
    # r = generate_random_poly_3_variables()
    print('')
    print(p.to_string())
    print('')
    print(q.to_string())
    print('')
    # print(r.to_string())
    # print('')
    Ix = Interval(-1.2, 2.3)
    Iy = Interval(-1.2, 2.3)
    # Iz = Interval(-1.2, 2.3)
    test = subdivision_solver([p, q], ['x', 'y'])
    status = test.solve([[Ix], [Iy]], 10e-6, 1000, 'stats_color')
    print('status = ' + str(status))
    print('')
    print(test.getSolutions())

def generate_random_poly():
    # number_of_terms = randint(1, 100)
    number_of_terms = 10
    coefficients_exponents = []

    for x in range(0, number_of_terms):
        coeff = randint(-10, 10)
        exp_x = randint(0, 10)
        exp_y = randint(0, 10)
        term = (coeff, (exp_x, exp_y))
        coefficients_exponents.append(term)

    return Polynomial(coefficients_exponents, ['x', 'y'])

def generate_random_poly_2_variables():
    degree = 20
    coefficients_exponents = []

    for x in range(0, degree):
        for y in range(0, degree):
            coeff = randint(-10**10, 10**10)
            exp_x = x
            exp_y = y
            term = (coeff, (exp_x, exp_y))
            coefficients_exponents.append(term)

    return Polynomial(coefficients_exponents, ['x', 'y'])

def generate_random_poly_3_variables():
    degree = 10
    coefficients_exponents = []

    for x in range(0, degree):
        for y in range(0, degree):
            for z in range(0, degree):
                coeff = randint(-10, 10)
                exp_x = x
                exp_y = y
                exp_z = z
                term = (coeff, (exp_x, exp_y, exp_z))
                coefficients_exponents.append(term)

    return Polynomial(coefficients_exponents, ['x', 'y', 'z'])


t = time()
print 'Start time : ' + str(datetime.now())
test_random_polynomial()
print '\nElapsed time : ' + str(time() - t) + ' seconds.'
