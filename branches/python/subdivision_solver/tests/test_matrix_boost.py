#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#


from subdivision_solver.matrix_interval.matrix_interval_boost import P_matrix_interval, middleInterval, P_inverse
from subdivision_solver.utils.interval import Interval
from subdivision_solver.utils.polynomial import Polynomial

m = P_matrix_interval( [ [Interval(2,2.01)],[Interval(3,3.01)] ] )

print m

n = P_matrix_interval( [ [Interval(2,2.01)],[Interval(3,3.01)] ] )

print n

print m+n
print m-n

p = P_matrix_interval( [ [Interval(0.,1.), Interval(0.,2.)],[Interval(0.,1.),Interval(0.,2.)] ] )

print p

print p+Interval(4.,5.)
print p-Interval(4.,5.)

print m-Interval(4.,5.)

print "\n"

print p*Interval(4.,5.)

print "\n"
print n*p

p.delete_row(0)
print p
#lm = m.toList()

p = P_matrix_interval( [ [Interval(0.,1.), Interval(0.,2.), Interval(0.,3.)],[Interval(0.,1.),Interval(0.,2.), Interval(0.,3.)] ] )
print len([2,1])
print p.delete_columns( [2,0] )
print p.delete_rows( [2,0] )

print "\n"
print p.transpose()
print "\n"
print middleInterval(p.transpose())

print "\n"
print p
print "\n"
print p.column(1)
print "\n"
print p.row(2)
#print lm 

A = P_matrix_interval( [ [Interval(1.,1.), Interval(0.5,0.5)],[Interval(4.0,4.0),Interval(5.0,5.0)] ] )
B = P_matrix_interval( [[]] )

P_inverse(A,B)

print "\n"
print "A: \n", A
print A.is_invertible()
print A.inverse()

print "norm of A: ", A.norm()

B = P_matrix_interval( [[Interval(1.,1.), Interval(0.5,0.5)]] )
print "augment rows: \n", A.augment_row(B)

C = P_matrix_interval( [[Interval(1.,1.)], [Interval(0.5,0.5)]] )
print "augment: \n", A.augment(C)
