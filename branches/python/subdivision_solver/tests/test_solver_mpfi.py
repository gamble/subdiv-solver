#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#

import time
from subdivision_solver import subdivision_solver
from subdivision_solver import Interval
from subdivision_solver import Polynomial
from subdivision_solver.system.system import systempols
#from subdivision_solver.system.eval_mpfi import test_init_eval, testFastcallSys
#from subdivision_solver.matrix_interval.matrix_interval_mpfi import P_matrix_interval as P_matrix_interval
from subdivision_solver.solver.solver_mpfi import SolveFromList, ContractFromList

prec = 113

#p1 = Polynomial('741340*x1^5*x2^15+551139*x1^3*x2^17-185345*x1^8*x2^11+581711*x1^10*x2^5+292021*x1^7*x2^6+195507*x1^5*x2^8-672309*x1^12+839627*x1^7*x2-428088*x1^3*x2^3-715427*x2^4')
#p2 = Polynomial('643015*x1^13*x2^6-446653*x1^4*x2^14+79913*x1^14*x2^3-217638*x1^6*x2^9+105495*x1*x2^10+425645*x1^10+894388*x1^7*x2^2-253708*x1^7*x2-546938*x1^2*x2^6+1006946*x2^2')

#test = systempols([p1,p2],['x1','x2'])

#p1 = (x1-9007199254740992)*(x1-9007199254740993)
# p1 = (x1-2.75)*(x1-5.2)
p1 = Polynomial('x1^2 - 7.95000000000000*x1 + 14.3000000000000')
tab = [ [Interval(-1,10)]  ]

#tab = [ [RIF(-1,1)],[RIF(-1,1)] ]
#print "tab: ", tab
#nbSteps=100
nbSteps=-1
minsize=1e-4
t1 = time.clock()
#test = systempols([p1,p2],[x1,x2])
test = systempols([p1],['x1'])
[res, tooSmallPrecision, tooSmallUser, CCIntooSmallUser, notInversible, counter] = SolveFromList(test,prec,[tab],minsize, nbSteps, False, 1)
t2 = time.clock()
t6 = t2-t1
print "temps pour realSolver: ", t6, "s"
print "############################"
print "Nombre de Solutions: ", len(res)
for i in range(0,len(res)):
  #print "Solution : ", res[i][0][0], ", ", res[i][1][0]
  print "Solution : ", res[i][0][0]
print "############################"
t1 = time.clock()
[contracted, tooSmallPrecision, notInversible] = ContractFromList(test,prec,res,1e-40, False, 1)
t2 = time.clock()
t6 = t2-t1
print "temps pour contraction: ", t6, "s"
print "############################"
print "Solutions contractees: ", len(contracted)
for i in range(0,len(contracted)):
  #print "Solution : ", contracted[i][0][0], ", ", contracted[i][1][0]
  print "Solution : ", contracted[i][0][0]
print "############################"
print "############################"
print "Besoin de plus de precision: ", len(tooSmallPrecision)
for i in range(0,len(tooSmallPrecision)):
  #print "boite : ", tooSmallPrecision[i][0][0], ", ", tooSmallPrecision[i][1][0]
  print "boite : ", tooSmallPrecision[i][0][0]
print "############################"
print "############################"
print "Pas Inversible: ", len(notInversible)
for i in range(0,len(notInversible)):
  #print "boite : ", notInversible[i][0][0], ", ", notInversible[i][1][0]
  print "boite : ", notInversible[i][0][0]
print "############################"

