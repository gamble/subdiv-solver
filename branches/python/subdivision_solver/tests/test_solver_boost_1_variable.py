from time import time
from datetime import datetime
from random import randint

from subdivision_solver import subdivision_solver
from subdivision_solver import Polynomial
from subdivision_solver import Interval


# Gives the following results
# *********************************Stats Subdivision Solver: precision 53*************
# *_counter: 33
# *in queue: 0
# *boxes smallest than min size: 0
# *boxes requiring more precision: 0
# *boxes with not inversible jac: 0
# *_discarded: 15
# *_discarded_by_evaluation: 15
# *_discarded_by_krawczik: 0
# *nb solutions: 2
# ************************************************************************
# 
# Elapsed time : 0.000785112380981 seconds.
# status =  0
# Number of Solutions:  2
# Solution :  [[(-0.0009765625000000002, 1.9541015625000002)]]
# Solution :  [[(2.9294433593749996, 3.4182128906250004)]]
def test_boost_1_variable():
    p = Polynomial('x^2 - 4*x + 3') # solutions are 1 and 3
    Ix = Interval(-1000,1000)


    t = time()
    print 'Start time : ' + str(datetime.now())

    system = subdivision_solver([p],['x'])
    status = system.solve( [ [ Ix ] ], 1e-5, 53, 'stats_color' )
    
    print '\nElapsed time : ' + str(time() - t) + ' seconds.'
    print 'status = ', status
    res = system.getSolutions()

    print "Number of Solutions: ", len(res)
    for i in range(0,len(res)):
        print "Solution : ", res[i]

    # system.getSolutions(1e-5)  # gives error 'float' object has no attribute 'precision'

test_boost_1_variable()
