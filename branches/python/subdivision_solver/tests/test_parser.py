import unittest
from random import randint

from subdivision_solver.utils.parser import Parser
from subdivision_solver.utils.polynomial import Polynomial


class ParserTest(unittest.TestCase):

    def setUp(self):
        pass

    def test1(self):
        s = 'x*y^3*z^4 + 2*x^2 - 3'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = 'x^3 - 6*x^2 + 11*x - 6'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = '4*x^3 + 6*x^2 + 10*x + 6'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = 'x^4 - 10*x^3 + 35*x^2 - 50*x + 24'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = 'x^2 - 3*x + 2'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = '-8*x^2*y^2 - 15*x^3 - y^3 + x*y - x + 1'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = 'y^3 + 15*x*y + 2*y^2 + 2*x'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = 'x^3 - 6*x^2 + 11*x - 6'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = '2*x - 3'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = '-16*x*y^2 - 45*x^2 + y - 1'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = '27*x*y^10*z^6 + x^6*y^3*z^5 + 17*x^2*y^3*z^8 + 24*x^3*y^2*z^7 + 27*x^2*y^10'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = 'x^3 - 6*x^2 + 11*x - 4'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = 'x^2 + y^2 + xy - 25'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = '27*x*y^10*z^6 + x^6*y^3*z^5 + 17*x^2*y^3*z^8 + 24*x^3*y^2*z^7 + 27*x^2*y^10'
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

        s = ''
        coeff_exp_list, variables = Parser(s).parse()
        p = Polynomial(coeff_exp_list, variables)
        self.assertEqual(p.to_string(), s)

    # @unittest.skip('') #uncomment to skip
    def test2(self):
        number_of_tests = 100
        for i in range(0, number_of_tests):
            p1 = self.generate_random_poly()
            coeff_exp_list, variables = Parser(p1.to_string()).parse()
            p2 = Polynomial(coeff_exp_list, variables)
            self.assertEqual(p1.to_string(), p2.to_string())


    def generate_random_poly(self):
        number_of_terms = randint(1, 1000)
        coefficients_exponents = []

        for x in range(0, number_of_terms):
            coeff = randint(-100, 100)
            exp_x = randint(1, 100)
            exp_y = randint(1, 100)
            exp_z = randint(1, 100)
            term = (coeff, (exp_x, exp_y, exp_z))
            coefficients_exponents.append(term)

        return Polynomial(coefficients_exponents)

    # @unittest.skip('') #uncomment to skip
    def test3(self):
        number_of_tests = 100
        for i in range(0, number_of_tests):
            p1 = self.generate_random_poly_3_variables()
            coeff_exp_list, variables = Parser(p1.to_string()).parse()
            p2 = Polynomial(coeff_exp_list, variables)
            self.assertEqual(p1.to_string(), p2.to_string())

    def generate_random_poly_3_variables(self):
        degree = 3
        coefficients_exponents = []

        for x in range(0, degree + 1):
            for y in range(0, degree + 1):
                for z in range(0, degree + 1):
                    coeff = randint(-100, 100)
                    exp_x = x
                    exp_y = y
                    exp_z = z
                    term = (coeff, (exp_x, exp_y, exp_z))
                    coefficients_exponents.append(term)

        return Polynomial(coefficients_exponents, ['x', 'y', 'z'])


if __name__ == '__main__':
    unittest.main()
