from time import time
from datetime import datetime
from random import randint

from subdivision_solver import subdivision_solver
from subdivision_solver import Polynomial
from subdivision_solver import Interval


# Wilkinson Polynomial with 4 roots
# Gives the following results
# *********************************Stats Subdivision Solver: precision 53*************
# *_counter: 47
# *in queue: 0
# *boxes smallest than min size: 0
# *boxes requiring more precision: 0
# *boxes with not inversible jac: 0
# *_discarded: 20
# *_discarded_by_evaluation: 20
# *_discarded_by_krawczik: 0
# *nb solutions: 4
# ************************************************************************
# status = 0
# [[[('0.92650390625', '1.04380859375')]],
#  [[('1.98119140625', '2.09849609375')]],
#  [[('2.9186328125', '3.1532421875')]],
#  [[('3.8561328125', '4.0907421875')]]]
def test_wilkinson4():
    p1 = Polynomial('x^4 - 10*x^3 + 35*x^2 - 50*x + 24')
    tab = [[Interval(-1.3, 6.2)]]

    t = time()
    print 'Start time : ' + str(datetime.now())

    test = subdivision_solver([p1], ['x'])
    status = test.solve(tab, 10e-10, 1000, 'stats_color')

    print '\nElapsed time : ' + str(time() - t) + ' seconds.'
    print 'status = ', status
    res = test.getSolutions();
    print "Number of Solutions: ", len(res)
    for i in range(0,len(res)):
        print "Solution : ", res[i]


test_wilkinson4()
