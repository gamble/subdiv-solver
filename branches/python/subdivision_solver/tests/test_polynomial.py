from subdivision_solver.utils.polynomial import Polynomial
from random import randint
import unittest


class PolynomialTest(unittest.TestCase):

    def setUp(self):
        # Monovairante
        # x^3 - 6*x^2 + 11*x - 6
        self.pl11 = [(1, (3,)), (-6, (2,)), (11, (1,)), (-6, (0,))]
        # 4*x^3 + 6*x^2 + 10*x + 6
        self.pl12 = [(4, (3,)), (6, (2,)), (10, (1,)), (6, (0,))]
        # x^4 - 10*x^3 + 35*x^2 - 50*x + 24
        self.pl13 = [(1, (4,)), (-10, (3,)), (-50, (1,)), (35, (2,)), (24, (0,))]
        # x^2 -3 x + 2
        self.pl14 = [(1, (2,)), (-3, (1,)), (2, (0,))]

        # Bivariante
        self.pl21 = [(1, (1, 0)), (-1, (0, 1)), (1, (0, 0))]  # x - y + 1
        self.pl21dx = [(1, (0, 0))]
        self.pl21dy = [(-1, (0, 0))]

        self.pl22 = [(1, (2, 0)), (1, (0, 2)), (-25, (0, 0))]  # x^2 + y^2 -25
        self.pl22dx = [(2, (1, 0))]
        self.pl22dy = [(2, (0, 1))]

        self.pl23 = [(1, (3, 0)), (1, (0, 3)), (-25, (0, 0))]  # x^3 + y^3 -25
        self.pl23dx = [(3, (2, 0))]
        self.pl23dy = [(3, (0, 2))]

        # -8*x^2*y^2 - 15*x^3 - y^3 + x*y - x + 1
        self.pl24 = [(-8, (2, 2)), (1, (1, 1)), (-15, (3, 0)), (-1, (1, 0)), (-1, (0, 3)), (1, (0, 0))]
        self.pl24dx = [(-16, (1, 2)), (-15, (3, 0)), (1, (0, 1)), (-1, (0, 0))]
        self.pl24dy = [(-16, (2, 1)), (-3, (0, 2)), (1, (1, 0))]
        # y^3 + 15*x*y + 2*y^2 + 2*x
        self.pl25 = [(1, (0, 3)), (15, (1, 1)), (2, (0, 2)), (2, (1, 0))]

        # Variables
        self.x = ['x']
        self.y = ['y']
        self.xy = ['x', 'y']

    def test_monovariante1(self):
        p = Polynomial(self.pl11, self.x)
        p_str = p.to_string()
        self.assertEqual(p_str, 'x^3 - 6*x^2 + 11*x - 6')
        coeffs = p.coefficients()
        self.assertEqual(coeffs, [1, -6, 11, -6])
        exps = p.exponents()
        self.assertEqual(exps, [(3,), (2,), (1,), (0,)])
        varibles = p.variables()
        self.assertEqual(varibles, ['x'])

    def test_monovariante2(self):
        p = Polynomial(self.pl12, self.x)
        p_str = p.to_string()
        self.assertEqual(p_str, '4*x^3 + 6*x^2 + 10*x + 6')
        coeffs = p.coefficients()
        self.assertEqual(coeffs, [4, 6, 10, 6])
        exps = p.exponents()
        self.assertEqual(exps, [(3,), (2,), (1,), (0,)])
        varibles = p.variables()
        self.assertEqual(varibles, ['x'])

    def test_monovariante3(self):
        p = Polynomial(self.pl13, self.x)
        p_str = p.to_string()
        self.assertEqual(p_str, 'x^4 - 10*x^3 + 35*x^2 - 50*x + 24')
        coeffs = p.coefficients()
        self.assertEqual(coeffs, [1, -10, 35, -50, 24])
        exps = p.exponents()
        self.assertEqual(exps, [(4,), (3,), (2,), (1,), (0,)])

    def test_monovariante4(self):
        p = Polynomial(self.pl14, self.x)
        p_str = p.to_string()
        self.assertEqual(p_str, 'x^2 - 3*x + 2')
        coeffs = p.coefficients()
        self.assertEqual(coeffs, [1, -3, 2])
        exps = p.exponents()
        self.assertEqual(exps, [(2,), (1,), (0,)])

    def test_bivariante(self):
        coefficients_exponents = self.pl24
        variables = self.xy
        p = Polynomial(coefficients_exponents, variables)
        p_str = p.to_string()
        self.assertEqual(p_str, '-8*x^2*y^2 - 15*x^3 - y^3 + x*y - x + 1')
        coeffs = p.coefficients()
        self.assertEqual(coeffs, [-8, -15, -1, 1, -1, 1])
        exps = p.exponents()
        self.assertEqual(exps, [(2, 2), (3, 0), (0, 3), (1, 1), (1, 0), (0, 0)])
        varibles = p.variables()
        self.assertEqual(varibles, ['x', 'y'])

    def test_empty_polynomial(self):
        p = Polynomial([])

        self.assertEqual(p.to_string(), '')
        self.assertEqual(p.coefficients(), [])
        self.assertEqual(p.exponents(), [])
        self.assertEqual(p.variables(), [])
        self.assertEqual(p.degree(), -1)


        d0 = p.derivative('x')
        self.assertIsInstance(d0, Polynomial)
        self.assertEqual(d0.to_string(), '')
        self.assertEqual(d0.coefficients(), [])
        self.assertEqual(d0.exponents(), [])
        self.assertEqual(d0.variables(), [])
        self.assertEqual(d0.degree(), -1)

        d99 = p.derivative('x99')
        self.assertIsInstance(d99, Polynomial)
        self.assertEqual(d99.to_string(), '')
        self.assertEqual(d99.coefficients(), [])
        self.assertEqual(d99.exponents(), [])
        self.assertEqual(d99.variables(), [])
        self.assertEqual(d99.degree(), -1)

    def test_zero_coefficients(self):
        p = Polynomial([(0, (2, 2))])

        self.assertEqual(p.to_string(), '')
        self.assertEqual(p.coefficients(), [])
        self.assertEqual(p.exponents(), [])
        self.assertEqual(p.variables(), [])
        self.assertEqual(p.degree(), -1)

        d0 = p.derivative('x')
        self.assertIsInstance(d0, Polynomial)
        self.assertEqual(d0.to_string(), '')
        self.assertEqual(d0.coefficients(), [])
        self.assertEqual(d0.exponents(), [])
        self.assertEqual(d0.variables(), [])
        self.assertEqual(d0.degree(), -1)

    def test_derivative_monovariante(self):
        coefficients_exponents = self.pl14
        variables = self.x
        p = Polynomial(coefficients_exponents, variables)
        d = p.derivative('x')
        self.assertIsInstance(d, Polynomial)
        d_str = d.to_string()
        self.assertEqual(d_str, '2*x - 3')
        d_coeffs = d.coefficients()
        self.assertEqual(d_coeffs, [2, -3])
        d_exps = d.exponents()
        self.assertEqual(d_exps, [(1,), (0,)])
        d_variables = d.variables()
        self.assertEqual(d_variables, ['x'])

    def test_derivative_bivariante(self):
        # -8*x^2*y^2 - 15*x^3 - y^3 + x*y - x + 1
        coefficients_exponents = [(-8, (2, 2)), (1, (1, 1)), (-15, (3, 0)), (-1, (1, 0)), (-1, (0, 3)), (1, (0, 0))]
        variables = ['x', 'y']
        p = Polynomial(coefficients_exponents, variables)

        dx = p.derivative('x')
        self.assertIsInstance(dx, Polynomial)
        dx_str = dx.to_string()
        self.assertEqual(dx_str, '-16*x*y^2 - 45*x^2 + y - 1')
        dx_coeffs = dx.coefficients()
        self.assertEqual(dx_coeffs, [-16, -45, 1, -1])
        dx_exps = dx.exponents()
        self.assertEqual(dx_exps, [(1, 2), (2, 0), (0, 1), (0, 0)])
        dx_variables = dx.variables()
        self.assertEqual(dx_variables, ['x', 'y'])

        dy = p.derivative('y')
        self.assertIsInstance(dy, Polynomial)
        dy_str = dy.to_string()
        self.assertEqual(dy_str, '-16*x^2*y - 3*y^2 + x')
        dy_coeffs = dy.coefficients()
        self.assertEqual(dy_coeffs, [-16, -3, 1])
        dy_exps = dy.exponents()
        self.assertEqual(dy_exps, [(2, 1), (0, 2), (1, 0)])
        dy_variables = dy.variables()
        self.assertEqual(dy_variables, ['x', 'y'])

    def test_degree_monovariante(self):
        # x^3 - 6*x^2 + 11*x - 6
        p = Polynomial([(1, (3,)), (-6, (2,)), (11, (1,)), (-6, (0,))], ['x'])

        self.assertEqual(p.degree(), 3)
        self.assertEqual(p.degree('x'), 3)
        self.assertEqual(p.degree('y'), 0)
        self.assertEqual(p.degree('x0'), 0)
        self.assertEqual(p.degree('abcd'), 0)

    def test_degree_trivariante(self):
        # 27*x*y^10*z^6 + x^6*y^3*z^5 + 17*x^2*y^3*z^8 + 24*x^3*y^2*z^7 + 27*x^2*y^10
        coefficients_exponents = [(27, (2, 10, 0)), (17, (2, 3, 8)), (27, (1, 10, 6)), (24, (3, 2, 7)), (1, (6, 3, 5))]
        variables = ['x', 'y', 'z']
        p = Polynomial(coefficients_exponents, variables)

        self.assertEqual(p.degree(), 10)
        self.assertEqual(p.degree('x'), 6)
        self.assertEqual(p.degree('y'), 10)
        self.assertEqual(p.degree('z'), 8)
        self.assertEqual(p.degree('w'), 0)
        self.assertEqual(p.degree('x99'), 0)

    def test_substitute_monovariante(self):
        # x^3 - 6*x^2 + 11*x - 4
        p = Polynomial([(1, (3,)), (-6, (2,)), (11, (1,)), (-4, (0,))], ['x'])

        q = p.substitute({'x': 2})
        self.assertEqual(q.degree('x'), 0)
        self.assertEqual(q.to_string(), '2')
        self.assertEqual(q.coefficients(), [2])
        self.assertEqual(q.exponents(), [()])

    def test_substitute_bivariante(self):
        # x^2 + y^2 + xy - 25
        coefficients_exponents = [(1, (2, 0)), (1, (0, 2)), (1, (1, 1)), (-25, (0, 0))]
        variables = ['x', 'y']
        p = Polynomial(coefficients_exponents, variables)

        q = p.substitute({'y': 2})

        self.assertEqual(q.to_string(), 'x^2 + 2*x - 21')
        self.assertEqual(q.coefficients(), [1, 2, -21])
        self.assertEqual(q.exponents(), [(2,), (1,), (0,)])
        self.assertEqual(q.degree(), 2)
        self.assertEqual(q.degree('x'), 2)
        self.assertEqual(q.degree('y'), 0)

    def test_substitute_trivariante1(self):
        # 27*x*y^10*z^6 + x^6*y^3*z^5 + 17*x^2*y^3*z^8 + 24*x^3*y^2*z^7 + 27*x^2*y^10
        coefficients_exponents = [(27, (2, 10, 0)), (17, (2, 3, 8)), (27, (1, 10, 6)), (24, (3, 2, 7)), (1, (6, 3, 5))]
        variables = ['x', 'y', 'z']
        p = Polynomial(coefficients_exponents, variables)

        q = p.substitute({'y': 2})

        self.assertEqual(q.to_string(), '8*x^6*z^5 + 96*x^3*z^7 + 136*x^2*z^8 + 27648*x*z^6 + 27648*x^2')
        self.assertEqual(q.coefficients(), [8, 96, 136, 27648, 27648])
        self.assertEqual(q.exponents(), [(6, 5), (3, 7), (2, 8), (1, 6), (2, 0)])

        self.assertEqual(q.degree(), 8)
        self.assertEqual(q.degree('x'), 6)
        self.assertEqual(q.degree('y'), 0)
        self.assertEqual(q.degree('z'), 8)
        self.assertEqual(q.variables(), ['x', 'z'])

    def test_substitute_trivariante2(self):
        # 1369*x^4 + 2738*x^2*y^2 + 2738*x^2*z^2
        # + 1369*y^4 + 2738*y^2*z^2 + 1369*z^4
        # - 11386*x^2 + 10752*x*y + 8064*x*z
        # - 1146*y^2 + 18816*y*z - 6634*z^2
        # + 12321
        coefficients_exponents = [(1369, (4, 0, 0)), (2738, (2, 2, 0)), (2738, (2, 0, 2)),
                (1369, (0, 4, 0)), (2738, (0, 2, 2)), (1369, (0, 0, 4)),
                (-11386, (2, 0, 0)), (10752, (1, 1, 0)), (8064, (1, 0, 1)),
                (-1146, (0, 2, 0)), (18816, (0, 1, 1)), (-6634, (0, 0, 2)),
                (12321, (0, 0, 0))]
        variables = ['x', 'y', 'z']
        p = Polynomial(coefficients_exponents, variables)

        q = p.substitute({'x': -10.0})

        self.assertEqual(q.to_string(), '1369*y^4 + 2738*y^2*z^2 + 1369*z^4 + 272654*y^2 + 18816*y*z + 267166*z^2 - 107520*y - 80640*z + 12563721')
        self.assertEqual(q.coefficients(), [1369, 2738, 1369, 272654, 18816, 267166, -107520, -80640, 12563721])
        self.assertEqual(q.exponents(), [(4, 0), (2, 2), (0, 4), (2, 0), (1, 1), (0, 2), (1, 0), (0, 1), (0, 0)])
        self.assertEqual(q.degree(), 4)
        self.assertEqual(q.degree('x'), 0)
        self.assertEqual(q.degree('y'), 4)
        self.assertEqual(q.degree('z'), 4)

    def test_reverse1(self):
        # 4*x^3*y^3*z^8 + 10*x^2*y*z^2 + 6*x^4
        p = Polynomial([(4, (3, 3, 8)), (6, (4, 0, 0)), (10, (2, 1, 2))], ['x', 'y', 'z'])

        p_revesed_x = p.reverse('x')
        self.assertEqual(p_revesed_x.to_string(), '4*x*y^3*z^8 + 10*x^2*y*z^2 + 6')

        p_revesed_y = p.reverse('y')
        self.assertEqual(p_revesed_y.to_string(), '4*x^3*z^8 + 6*x^4*y^3 + 10*x^2*y^2*z^2')

        p_revesed_z = p.reverse('z')
        self.assertEqual(p_revesed_z.to_string(), '6*x^4*z^8 + 10*x^2*y*z^6 + 4*x^3*y^3')

    def test_reverse2(self):
        # 5476*x^2*z + 5476*y^2*z + 5476*z^3 + 8064*x + 18816*y - 13268*z
        p = Polynomial([(5476, (2, 0, 1)), (5476, (0, 2, 1)), (5476, (0, 0, 3)),
            (8064, (1, 0, 0)), (18816, (0, 1, 0)), (-13268, (0, 0, 1))], ['x', 'y', 'z'])

        p_revesed_z = p.reverse('z')
        self.assertEqual(p_revesed_z.to_string(), '5476*x^2*z^2 + 8064*x*z^3 + 5476*y^2*z^2 + 18816*y*z^3 - 13268*z^2 + 5476')

    def test_add_same_degree_same_variables1(self):
        # x^3 - 6*x^2 + 11*x - 6
        p1 = Polynomial([(1, (3,)), (-6, (2,)), (11, (1,)), (-6, (0,))])
        # 4*x^3 + 6*x^2 + 10*x + 6
        p2 = Polynomial([(4, (3,)), (6, (2,)), (10, (1,)), (6, (0,))])

        p = p1 + p2

        self.assertEqual(p.to_string(), '5*x0^3 + 21*x0')
        self.assertEqual(p.coefficients(), [5, 21])
        self.assertEqual(p.exponents(), [(3,), (1,)])

    def test_add_to_empty_polynomial(self):
        variables = ['x', 'y']
        # x^2 + y^2 -25
        p1 = Polynomial([(1, (2, 0)), (1, (0, 2)), (-25, (0, 0))], variables)
        # p2 is empty polynomial
        p2 = Polynomial([], variables)

        p = p1 + p2

        self.assertIsNot(p, p1)
        self.assertIsNot(p, p2)
        self.assertEqual(p.to_string(), 'x^2 + y^2 - 25')
        self.assertEqual(p.coefficients(), [1, 1, -25])
        self.assertEqual(p.exponents(), [(2, 0), (0, 2), (0, 0)])

    def test_add_to_scalar(self):
        p1 = Polynomial('x^2 + y^2 - 25')
        scalar = 25

        p = p1 + scalar

        self.assertEqual(p.to_string(), 'x^2 + y^2')
        self.assertEqual(p.coefficients(), [1, 1])
        self.assertEqual(p.exponents(), [(2, 0), (0, 2)])

    def test_radd_to_scalar(self):
        p1 = Polynomial('x^2 + y^2 - 25')
        scalar = 25

        p = scalar + p1

        self.assertEqual(p.to_string(), 'x^2 + y^2')
        self.assertEqual(p.coefficients(), [1, 1])
        self.assertEqual(p.exponents(), [(2, 0), (0, 2)])

    def test_add_same_degree_same_variables2(self):
        variables = ['x', 'y']
        # x - y + 1
        p1 = Polynomial([(1, (1, 0)), (-1, (0, 1)), (1, (0, 0))], variables)
        # x^2 + y^2 -25
        p2 = Polynomial([(1, (2, 0)), (1, (0, 2)), (-25, (0, 0))], variables)

        p = p1 + p2

        self.assertEqual(p.to_string(), 'x^2 + y^2 + x - y - 24')
        self.assertEqual(p.coefficients(), [1, 1, 1, -1, -24])
        self.assertEqual(p.exponents(), [(2, 0), (0, 2), (1, 0), (0, 1), (0, 0)])

    def test_sub_from_scalar(self):
        p1 = Polynomial('x^2 + y^2 - 25')
        scalar = 25

        p = p1 - scalar

        self.assertEqual(p.to_string(), 'x^2 + y^2 - 50')
        self.assertEqual(p.coefficients(), [1, 1, -50])
        self.assertEqual(p.exponents(), [(2, 0), (0, 2), (0, 0)])

    def test_rsub_from_scalar(self):
        p1 = Polynomial('x^2 + y^2 - 25')
        scalar = 25

        p = scalar - p1

        # self.assertEqual(p.to_string(), '- x^2 - y^2')
        # self.assertEqual(p.coefficients(), [-1, -1])
        # self.assertEqual(p.exponents(), [(2, 0), (0, 2)])

    def test_sub_same_degree_same_variables1(self):
        # 4*x^3 + 6*x^2 + 10*x + 6
        p1 = Polynomial([(4, (3,)), (6, (2,)), (10, (1,)), (6, (0,))])
        # x^3 - 6*x^2 + 11*x - 6
        p2 = Polynomial([(1, (3,)), (-6, (2,)), (11, (1,)), (-6, (0,))])

        p = p1 - p2

        self.assertEqual(p.to_string(), '3*x0^3 + 12*x0^2 - x0 + 12')
        self.assertEqual(p.coefficients(), [3, 12, -1, 12])
        self.assertEqual(p.exponents(), [(3,), (2,), (1,), (0,)])

    def test_sub_same_degree_same_variables2(self):
        variables = ['x', 'y']
        # x^2 + y^2 -25
        p1 = Polynomial([(1, (2, 0)), (1, (0, 2)), (-25, (0, 0))], variables)
        # x - y + 1
        p2 = Polynomial([(1, (1, 0)), (-1, (0, 1)), (1, (0, 0))], variables)

        p = p1 - p2

        self.assertEqual(p.to_string(), 'x^2 + y^2 - x + y - 26')
        self.assertEqual(p.coefficients(), [1, 1, -1, 1, -26])
        self.assertEqual(p.exponents(), [(2, 0), (0, 2), (1, 0), (0, 1), (0, 0)])

    def test_mul_same_degree_same_variables1(self):
        # x - 1
        p1 = Polynomial([(1, (1,)), (-1, (0,))])
        # x + 1
        p2 = Polynomial([(1, (1,)), (1, (0,))])

        p = p1 * p2

        self.assertEqual(p.to_string(), 'x0^2 - 1')
        self.assertEqual(p.coefficients(), [1, -1])
        self.assertEqual(p.exponents(), [(2,), (0,)])

    def test_mul_same_degree_same_variables2(self):
        variables = ['x', 'y']
        # x - y + 1
        p1 = Polynomial([(1, (1, 0)), (-1, (0, 1)), (1, (0, 0))], variables)
        # x^2 + y^2 -25
        p2 = Polynomial([(1, (2, 0)), (1, (0, 2)), (-25, (0, 0))], variables)

        p = p1 * p2

        self.assertEqual(p.to_string(), 'x^3 - x^2*y + x*y^2 - y^3 + x^2 + y^2 - 25*x + 25*y - 25')
        self.assertEqual(p.coefficients(), [1, -1, 1, -1, 1, 1, -25, 25, -25])
        self.assertEqual(p.exponents(), [(3, 0), (2, 1), (1, 2), (0, 3), (2, 0), (0, 2), (1, 0), (0, 1), (0, 0)])

    def test_power_monovariante(self):
        p1 = Polynomial('x - 1')

        self.assertRaises(TypeError, p1.__pow__, 1.5)

        self.assertRaises(ValueError, p1.__pow__, -1)

        p0 = p1**0
        self.assertEqual(p0.to_string(), '1')

        p0 = p1**1
        self.assertEqual(p0.to_string(), 'x - 1')

        p2 = p1**2
        self.assertEqual(p2.to_string(), 'x^2 - 2*x + 1')

        p3 = p1**3
        self.assertEqual(p3.to_string(), 'x^3 - 3*x^2 + 3*x - 1')

        p4 = p1**4
        self.assertEqual(p4.to_string(), 'x^4 - 4*x^3 + 6*x^2 - 4*x + 1')

        p5 = p1**5
        self.assertEqual(p5.to_string(), 'x^5 - 5*x^4 + 10*x^3 - 10*x^2 + 5*x - 1')

        p10 = p1**10
        self.assertEqual(p10.to_string(), 'x^10 - 10*x^9 + 45*x^8 - 120*x^7 + 210*x^6 '
                                        + '- 252*x^5 + 210*x^4 - 120*x^3 + 45*x^2 - 10*x + 1')

        p16 = p1**16
        self.assertEqual(p16.to_string(), 'x^16 - 16*x^15 + 120*x^14 - 560*x^13 + 1820*x^12'
                                        + ' - 4368*x^11 + 8008*x^10 - 11440*x^9 + 12870*x^8'
                                        + ' - 11440*x^7 + 8008*x^6 - 4368*x^5 + 1820*x^4'
                                        + ' - 560*x^3 + 120*x^2 - 16*x + 1')

    def test_power_bivariante(self):
        p1 = Polynomial('x - y + 1')

        self.assertRaises(TypeError, p1.__pow__, 1.5)

        self.assertRaises(ValueError, p1.__pow__, -1)

        p0 = p1**0
        self.assertEqual(p0.to_string(), '1')

        p0 = p1**1
        self.assertEqual(p0.to_string(), 'x - y + 1')

        p2 = p1**2
        self.assertEqual(p2.to_string(), 'x^2 - 2*x*y + y^2 + 2*x - 2*y + 1')

        p3 = p1**3
        self.assertEqual(p3.to_string(), 'x^3 - 3*x^2*y + 3*x*y^2 - y^3 + 3*x^2'
                                    + ' - 6*x*y + 3*y^2 + 3*x - 3*y + 1')

        p4 = p1**4
        self.assertEqual(p4.to_string(), 'x^4 - 4*x^3*y + 6*x^2*y^2 - 4*x*y^3 + y^4'
                                    + ' + 4*x^3 - 12*x^2*y + 12*x*y^2 - 4*y^3 + 6*x^2'
                                    + ' - 12*x*y + 6*y^2 + 4*x - 4*y + 1')

        p5 = p1**5
        self.assertEqual(p5.to_string(), 'x^5 - 5*x^4*y + 10*x^3*y^2 - 10*x^2*y^3'
                                    + ' + 5*x*y^4 - y^5 + 5*x^4 - 20*x^3*y + 30*x^2*y^2'
                                    + ' - 20*x*y^3 + 5*y^4 + 10*x^3 - 30*x^2*y + 30*x*y^2'
                                    + ' - 10*y^3 + 10*x^2 - 20*x*y + 10*y^2 + 5*x - 5*y + 1')

        p10 = p1**10
        self.assertEqual(p10.to_string(), 'x^10 - 10*x^9*y + 45*x^8*y^2 - 120*x^7*y^3'
                                    + ' + 210*x^6*y^4 - 252*x^5*y^5 + 210*x^4*y^6'
                                    + ' - 120*x^3*y^7 + 45*x^2*y^8 - 10*x*y^9 + y^10'
                                    + ' + 10*x^9 - 90*x^8*y + 360*x^7*y^2 - 840*x^6*y^3'
                                    + ' + 1260*x^5*y^4 - 1260*x^4*y^5 + 840*x^3*y^6'
                                    + ' - 360*x^2*y^7 + 90*x*y^8 - 10*y^9 + 45*x^8'
                                    + ' - 360*x^7*y + 1260*x^6*y^2 - 2520*x^5*y^3'
                                    + ' + 3150*x^4*y^4 - 2520*x^3*y^5 + 1260*x^2*y^6'
                                    + ' - 360*x*y^7 + 45*y^8 + 120*x^7 - 840*x^6*y'
                                    + ' + 2520*x^5*y^2 - 4200*x^4*y^3 + 4200*x^3*y^4'
                                    + ' - 2520*x^2*y^5 + 840*x*y^6 - 120*y^7 + 210*x^6'
                                    + ' - 1260*x^5*y + 3150*x^4*y^2 - 4200*x^3*y^3 + 3150*x^2*y^4'
                                    + ' - 1260*x*y^5 + 210*y^6 + 252*x^5 - 1260*x^4*y + 2520*x^3*y^2'
                                    + ' - 2520*x^2*y^3 + 1260*x*y^4 - 252*y^5 + 210*x^4 - 840*x^3*y'
                                    + ' + 1260*x^2*y^2 - 840*x*y^3 + 210*y^4 + 120*x^3 - 360*x^2*y'
                                    + ' + 360*x*y^2 - 120*y^3 + 45*x^2 - 90*x*y + 45*y^2'
                                    + ' + 10*x - 10*y + 1')

    def generate_random_poly():
        number_of_terms = randint(1, 10)
        coefficients_exponents = []

        for x in range(1, number_of_terms):
            coeff = randint(1, 30)
            exp_x = randint(1, 10)
            exp_y = randint(1, 10)
            term = (coeff, (exp_x, exp_y))
            coefficients_exponents.append(term)

        return Polynomial(coefficients_exponents)


if __name__ == '__main__':
    unittest.main()
