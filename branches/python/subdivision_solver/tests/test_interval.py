from subdivision_solver.utils.interval import Interval
import unittest


class IntervalTest(unittest.TestCase):

    def setUp(self):
        pass

    def test1(self):
        i = Interval(-1.5, 2)

        self.assertEquals(i.lower(), -1.5)
        self.assertEquals(i.upper(), 2)
        self.assertEquals(i.absolute_diameter(), 3.5)
        self.assertEquals(i.center(), 0.25)
        self.assertEquals(i.invert().lower(), float('-inf'))
        self.assertEquals(i.invert().upper(), float('inf'))
        self.assertEquals(i.signs(), (-1, 1))

        i2 = Interval(1, 5)
        self.assertEquals(i.union(i2).lower(), '-1.5')
        self.assertEquals(i.union(i2).upper(), '5')

        s = 99
        self.assertEquals(i.union(s).lower(), '-1.5')
        self.assertEquals(i.union(s).upper(), '99')


if __name__ == '__main__':
    unittest.main()
