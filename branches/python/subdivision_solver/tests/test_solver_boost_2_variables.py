from time import time
from datetime import datetime
from random import randint

from subdivision_solver import subdivision_solver
from subdivision_solver import Polynomial
from subdivision_solver import Interval


# Gives the following results
# *********************************Stats Subdivision Solver: precision 53*************
# *_counter: 169
# *in queue: 0
# *boxes smallest than min size: 0
# *boxes requiring more precision: 0
# *boxes with not inversible jac: 0
# *_discarded: 81
# *_discarded_by_evaluation: 81
# *_discarded_by_krawczik: 0
# *nb solutions: 4
# ************************************************************************
#
# Elapsed time : 0.0025622844696 seconds.
# status =  0
# Number of Solutions:  4
# Solution :  [[(-1.0115601196289048, -0.9381224975585918)], [(-0.13122570800781067, 0.0156495361328144)]]
# Solution :  [[(-0.13122570800781067, 0.0156495361328144)], [(-1.0115601196289048, -0.9381224975585918)]]
# Solution :  [[(-0.13129907226562318, 0.16245141601562696)], [(0.8958739013671894, 1.0427491455078148)]]
# Solution :  [[(0.8958739013671894, 1.0427491455078148)], [(-0.13129907226562318, 0.16245141601562696)]]
def test_circle():
    p = Polynomial('x^2 + y^2 - 1')
    q = Polynomial('x*y')
    Ix = Interval(-100.2, 200.3)
    Iy = Interval(-100.2, 200.3)

    t = time()
    print 'Start time : ' + str(datetime.now())

    test = subdivision_solver([p, q], ['x', 'y'])
    status = test.solve([[Ix], [Iy]], 1e-5, 100, 'stats_color')
    
    print '\nElapsed time : ' + str(time() - t) + ' seconds.'
    print 'status = ', status
    
    res1 = test.getSolutions();
    print "Number of Solutions: ", len(res1)
    for i in range(0,len(res1)):
        print "Solution : ", res1[i]

    res2 = test.getSolutions(1e-10);
    print "Number of Solutions: ", len(res2)
    for i in range(0,len(res2)):
        print "Solution : ", res2[i]

test_circle()
