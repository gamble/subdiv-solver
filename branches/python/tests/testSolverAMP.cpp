#include "../build/realSolver_AMP.h"
#include "dim2deg4.h"

matrix_interval_mp Func_interval_mp ( matrix_interval_mp m ) { return Func<interval_mp,float_mp>(m); }
matrix_interval_mp Jaco_interval_mp ( matrix_interval_mp m ) { return Jaco<interval_mp,float_mp>(m); }
matrix_interval_mp Hess_interval_mp ( matrix_interval_mp m ) { return Hess<interval_mp,float_mp>(m); }

matrix_interval_dp Func_interval_dp ( matrix_interval_dp m ) { return Func<interval_dp,float_dp>(m); }
matrix_interval_dp Jaco_interval_dp ( matrix_interval_dp m ) { return Jaco<interval_dp,float_dp>(m); }
matrix_interval_dp Hess_interval_dp ( matrix_interval_dp m ) { return Hess<interval_dp,float_dp>(m); }

int main(){
  
  interval_mp Tab[2] = { interval_mp(-100.,100.), interval_mp(-100.,100.) };
 
  matrix_interval_mp M(2,1,Tab);
  
  cout << " @@@ Début testSolver_AMP " << endl;
  realSolver_AMP rs( Func_interval_dp,
                     Jaco_interval_dp,
                     Hess_interval_dp,
                     Func_interval_mp,
                     Jaco_interval_mp,
                     Hess_interval_mp );
  rs.addInitialBox(M);
  rs.setVerbosity(1);
  rs.solve();
  cout << " @@@ Fin   testSolver_AMP " << endl;
  
  return 0;
}
