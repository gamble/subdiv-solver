// #include "../mpfi/mpfixx.hpp"
// #include "../mpfi/mpfrxx.hpp"
// #include "../mpfi/mpfimpfr.hpp"
#include "../boost/boostxx.hpp"
#include "../generic/matrix.h" 
// #include "../generic/box.h" 
#include "../generic/list.h" 
#include "../generic/operation.h" 
#include <iostream>

#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
typedef boost::minstd_rand base_generator_type;

using namespace std;
using namespace matrix;
using namespace cpp_tools;

typedef Matrix<boost_class> matrix_interval;

// mp_prec_t boost_class::_prec = 53;
base_generator_type boost_class::_generator = base_generator_type(42u);

int main() {
  
//   base_generator_type generator = base_generator_type(42u);
  
  boost_class TabX[2] = { boost_class(10.,11.), boost_class(10.,12.)};
  matrix_interval X(2,1,TabX);
  cout << "X: " << X << endl;
  matrix_interval Xinf = eps_inflation_box ( X, 0.001);
  cout << "Xinf: " << Xinf << endl;
  
//   boost::uniform_real<> uni_dist(Xinf(1,0).get_left(),Xinf(1,0).get_right());
//   boost::variate_generator<base_generator_type&, boost::uniform_real<> > uni(generator, uni_dist);
//   for(int i = 0; i < 10; i++)
//     std::cout << uni() << '\n';
  
  for(int i = 0; i < 10; i++)
    cout << "random: " << alea_interval_point(interior(Xinf(1,0))) << endl;
  
  boost_class TabX2[2] = { boost_class(10.,next(next(10.))), boost_class(10.,next(next(10.)))};
  matrix_interval X2(2,1,TabX2);
  cout << "X2: " << X2 << endl;
  matrix_interval X2inf = eps_inflation_box ( X2, 0.001);
  cout << "X2inf: " << X2inf << endl;
//   boost_class interior(next(X2inf(1,0).get_left()),prev(X2inf(1,0).get_right()));
//   cout << "testInterior: " << interior << endl;
  
  for(int i = 0; i < 10; i++)
    cout << "random: " << alea_interval_point(interior(X2inf(1,0))) << endl;
    
  
//   boost::uniform_real<> uni_dist2(X2inf(1,0).get_left(),X2inf(1,0).get_right());
//   boost::variate_generator<base_generator_type&, boost::uniform_real<> > uni2(generator, uni_dist2);
//   for(int i = 0; i < 10; i++)
//     std::cout << uni2() << '\n';
//   
//   for(int i = 0; i < 10; i++) {
//     base_generator_type generatortest;
//     boost::uniform_real<> uni_disttest(0.,1.);
//     boost::variate_generator<base_generator_type&, boost::uniform_real<> > unitest(generatortest, uni_disttest);
//     std::cout << unitest() << '\n';
//   }
  return 0; 
} 
