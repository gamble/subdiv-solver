#include "../base/base.h"
#include "../build/matrix_boost.h"

#include <iostream>

typedef interval_boost::interval interval;
typedef interval_boost::matrix_interval matrix_interval;

int main() {
  
//   mp_prec_t prec = 113;
  
  interval Tab[4] = { interval(0.,1.), interval(0.,1.), interval(0.,1.), interval(0.,1.) };
 
  matrix_interval M(2,2,Tab);
  
  cout << M << endl;
  matrix_interval N(2,2,Tab);
  cout << N << endl;
  
  cout << "M+N" << M+N << endl;
  cout << "M-N" << M-N << endl;
  cout << "-N" << -N << endl;
  
//   inverseLowerTriangular<interval>(M,&N);
  
//   interval l;
//   l = interval(0.5,1.);
//   cout << get_prec(l) << endl;
// //   cout << iszero(l) << endl;
//   cout << l << endl;
//   interval m(l);
//   cout << m << endl;
// //   cout << M(0,0) << endl;
// //   inverseLowerTriangular<interval>(M,&N);
// //   cout << "M: " << M << endl;
//   cout << m.get_left() << endl;
//   cout << m.get_right() << endl;
//   interval n(1.,1.5);
//   m=n;
//   cout << m << endl;
// //   n=0;
// //   cout << n << endl;
//   
//   cout << n*l << endl;
//   cout << n+l << endl;
//   
//   n+=l;
//   cout << n << endl;
//   
//   cout << l-n << endl;
//   l-=n;
//   cout << l << endl;
//   
//   cout << n/l << endl;
//   n/=l;
//   cout << n << endl;
  return 0; 
} 
