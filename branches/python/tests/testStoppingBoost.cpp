#include "../base/base.h"
// #include "../build/matrix_mpfi.h"
// #include "../build/solver_mpfi.h"
#include "../build/matrix_boost.h"
#include "../build/solver_boost.h"
// #include "../generic/box.h"
// #include "dim2deg4.h"
// #include "dim1deg2.h"
#include "dim2deg2.h"

#include <stdio.h>
#include <sys/time.h>

// typedef interval_mpfi::interval interval_mpfi_;
typedef interval_boost::interval interval_boost_;
// typedef interval_mpfi::myfloat myfloat_mpfr;
typedef interval_boost::myfloat myfloat_double;
// typedef interval_mpfi::matrix_interval matrix_interval_mpfi;
typedef interval_boost::matrix_interval matrix_interval_boost;
// typedef interval_mpfi::subdivision_solver_i subdivision_solver_mpfi;
typedef interval_boost::subdivision_solver_i subdivision_solver_boost;


// matrix_interval_mpfi Func_interval_mpfi ( matrix_interval_mpfi m ) { return Func<interval_mpfi_,myfloat_mpfr>(m); }
// matrix_interval_mpfi Jaco_interval_mpfi ( matrix_interval_mpfi m ) { return Jaco<interval_mpfi_,myfloat_mpfr>(m); }
// matrix_interval_mpfi Hess_interval_mpfi ( matrix_interval_mpfi m ) { return Hess<interval_mpfi_,myfloat_mpfr>(m); }

matrix_interval_boost Func_interval_boost ( matrix_interval_boost m ) { return Func<interval_boost_,myfloat_double>(m); }
matrix_interval_boost Jaco_interval_boost ( matrix_interval_boost m ) { return Jaco<interval_boost_,myfloat_double>(m); }
matrix_interval_boost Hess_interval_boost ( matrix_interval_boost m ) { return Hess<interval_boost_,myfloat_double>(m); }

int main() {
  
  struct timeval tv1, tv2;
  long long diff = 0;
//   myfloat_mpfr dummyFloat;
//   interval_mpfi_ dummy;
//   dummyFloat.set_prec(15);
//   dummy.set_prec(15);
  cout << "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& boost: precision: " << endl;
  
//   interval_boost_ Tab_boost[1] = { interval_boost_(9.5,10.1)};
//   interval_mpfi_ Tab_mpfi[1] = { interval_mpfi_(-0.5,0.1)};
  interval_boost_ Tab_boost[2] = { interval_boost_(9.5,10.1), interval_boost_(9.5,10.1)};
  matrix_interval_boost M_boost(2,1,Tab_boost);
  
//   cout << M_mpfi << endl;
//   cout << Func_interval_mpfi(M_mpfi) << endl;
//   cout << Jaco_interval_mpfi(M_mpfi) << endl;
//   cout << Hess_interval_mpfi(M_mpfi) << endl;
  
  subdivision_solver_boost solver_interval_boost ( Func_interval_boost, Jaco_interval_boost, Hess_interval_boost );
  
  gettimeofday(&tv1,NULL);
  solver_interval_boost.setVerbosity(3);
  solver_interval_boost.setMinWidth(1e-30);
  solver_interval_boost.solve(M_boost, 1000);
  gettimeofday(&tv2,NULL);
  solver_interval_boost.printStats();
  
  cout << "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& working queue: " << endl;
  solver_interval_boost.visitWorking();
  while (not solver_interval_boost.endWorking())
    cout << solver_interval_boost.nextWorking() << endl;
  
  cout << "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& TooSmall queue: " << endl;
  solver_interval_boost.visitTooSmall();
  while (not solver_interval_boost.endTooSmall())
    cout << solver_interval_boost.nextTooSmall() << endl;
  
  diff = diff + (tv2.tv_sec - tv1.tv_sec) * 1000000L + (tv2.tv_usec - tv1.tv_usec);
  printf("durée: %lli usec\n", diff);
    
  return 0;
}