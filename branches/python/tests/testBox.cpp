#include "../mpfi/mpfixx.hpp"
#include "../mpfi/mpfrxx.hpp"
#include "../mpfi/mpfimpfr.hpp"
#include "../generic/matrix.h"  
#include "../generic/list.h" 
#include "../generic/operation.h"
#include "../generic/section.h"
#include "../generic/box.h"
#include "../generic/evaluation.h"
#include "../generic/contraction.h"

#include "dim2deg4.h"
#include <iostream>

using namespace std;
using namespace matrix;
using namespace cpp_tools;

typedef Matrix<mpfi_class> matrix_interval;
typedef Matrix<mpfr_class> matrix_float;

mp_prec_t mpfi_class::_prec = 53;
mp_prec_t mpfr_class::_prec = 53;

int main() {
  
  
  
//   mpfi_class Tab1[3] = { mpfi_class(0.,1.),    mpfi_class(0.,1.),    mpfi_class(0.,1.)     };
//   mpfi_class Tab2[3] = { mpfi_class(0.25,0.5), mpfi_class(0.5,0.75), mpfi_class(0.25,1.25) };
//  
//   matrix_interval M1(3,1,Tab1);
//   matrix_interval M2(3,1,Tab2);
//   
//   cout << "M1: " << M1 << endl;
//   cout << "M2: " << M2 << endl;
//   
//   struct intersection_result<mpfi_class> interM1M2 = overlaps<mpfi_class>(M2,M1);
//   cout << "intersectionM1M2: _overlaps: " << interM1M2._overlaps << ", _isIn: " << interM1M2._isIn << ", isInInt: " << interM1M2._isInInt << " _intersection: " << endl;
//   cout << interM1M2._intersection << endl;
//   
//   cout << "Min_absolute_diameter(M2): " << Min_absolute_diameter<mpfi_class,mpfr_class>(M2) << endl;
//   cout << "Max_absolute_diameter(M2): " << Max_absolute_diameter<mpfi_class,mpfr_class>(M2) << endl;
//   
//   cout << "contains_zero(M1): " << contains_zero(M1) << endl;
//   cout << "contains_zero(M2): " << contains_zero(M2) << endl;
//   
//   cpp_tools::list< matrix_interval > listeres = subdivide_box( M1 );
//   cout << listeres << endl;
//   
//   cpp_tools::list< matrix_interval > listeres2 = subdivide_Maximal_width<mpfi_class,mpfr_class>( M1 );
//   cout << listeres2 << endl;
  
// test of complementary function  
  mpfi_class TabY[2] = { mpfi_class(0.,1.), mpfi_class(0.,1.)};
//   mpfi_class TabX[2] = { mpfi_class(0.25,0.75), mpfi_class(0.25,0.75)};
//   mpfi_class TabX[2] = { mpfi_class(0.,1.), mpfi_class(0.,1.)};
  mpfi_class TabX[2] = { mpfi_class(0.,1.), mpfi_class(2.,3.)};
  matrix_interval X(2,1,TabX);
  matrix_interval Y(2,1,TabY);
  cout << "X: " << X << endl;
  cout << "Y: " << Y << endl;
//   cpp_tools::list< matrix_interval > listecomp = complementary(X,Y);
  cpp_tools::list< matrix_interval > listecomp;
  listecomp.clear();
  int res = complementary<mpfi_class>(X,Y, listecomp);
  cout << "complementary: " << res << endl;
//   cout << "complementary: " << endl;
  cout << listecomp << endl;
  
// //test of subdivide_box_limited dim2
//   mpfi_class TabX[2] = { mpfi_class(0.,1.), mpfi_class(0.,2.)};
//   matrix_interval X(2,1,TabX);
//   cpp_tools::list< matrix_interval > listeres = subdivide_box_limited<mpfi_class,mpfr_class>( X, 1.0 );
//   cout << "subdivision: " << endl;
//   cout << listeres << endl;
  
// //test of subdivide_box_limited dim3
//   mpfi_class TabX[3] = { mpfi_class(0.,2.), mpfi_class(0.,1.), mpfi_class(0.,2.)};
//   matrix_interval X(3,1,TabX);
//   cpp_tools::list< matrix_interval > listeres = subdivide_box_limited<mpfi_class,mpfr_class>( X, 1.0 );
//   cout << "subdivision: " << endl;
//   cout << listeres << endl;
  
// //test of subdivide_Maximal_width_limited dim2
//   mpfi_class TabX[2] = { mpfi_class(0.,1.), mpfi_class(0.,0.5)};
//   matrix_interval X(2,1,TabX);
//   cpp_tools::list< matrix_interval > listeres = subdivide_Maximal_width_limited<mpfi_class,mpfr_class>( X, 0.1 );
//   cout << "subdivision: " << endl;
//   cout << listeres << endl;
  
// //test of subdivide_Maximal_width_limited dim3
//   mpfi_class TabX[3] = { mpfi_class(0.,2.), mpfi_class(0.,1.), mpfi_class(0.,2.)};
//   matrix_interval X(3,1,TabX);
//   cpp_tools::list< matrix_interval > listeres = subdivide_Maximal_width_limited<mpfi_class,mpfr_class>( X, 1.0 );
//   cout << "subdivision: " << endl;
//   cout << listeres << endl;

// //test of subdivide_Maximal_smear_limited dim2
//   mpfi_class TabX[2] = { mpfi_class(0.,1.), mpfi_class(0.,2.)};
//   matrix_interval X(2,1,TabX);
//   mpfi_class TabJ[4] = { mpfi_class(0.,1.), mpfi_class(0.,3.), mpfi_class(0.,1.), mpfi_class(0.,1.)};
//   matrix_interval J(2,2,TabJ);
//   cpp_tools::list< matrix_interval > listeres = subdivide_Maximal_smear_limited<mpfi_class,mpfr_class>( X, J,1.0);
//   cout << "subdivision: " << endl;
//   cout << listeres << endl;
  
//test of subdivide_Maximal_smear_limited dim3
//   mpfi_class TabX[3] = { mpfi_class(0.,1.), mpfi_class(0.,1.), mpfi_class(0.,2.)};
//   matrix_interval X(3,1,TabX);
//   mpfi_class TabJ[9] = { mpfi_class(0.,1.), mpfi_class(0.,1.), mpfi_class(0.,6.), mpfi_class(0.,1.), mpfi_class(0.,1.), mpfi_class(0.,1.), mpfi_class(0.,1.), mpfi_class(0.,1.), mpfi_class(0.,1.)};
//   matrix_interval J(3,3,TabJ);
//   cpp_tools::list< matrix_interval > listeres = subdivide_Maximal_smear_limited<mpfi_class,mpfr_class>( X, J,1.0);
//   cout << "subdivision: " << endl;
//   cout << listeres << endl;
  
//test of Interval_hull
//   mpfi_class TabX[2] = { mpfi_class(0.,1.), mpfi_class(0.,1.)};
//   matrix_interval X(2,1,TabX);
//   mpfi_class TabY[2] = { mpfi_class(2.,3.), mpfi_class(2.,3.)};
//   matrix_interval Y(2,1,TabY);
//   cout << "Interval_hull(X,Y): " << Interval_hull<mpfi_class,mpfr_class>(X,Y) << endl;
  
//   mpfi_class TabX[2] = { mpfi_class(0.,1.), mpfi_class(0.,1.)};
//   matrix_interval X(2,1,TabX);
//   box<mpfi_class,mpfr_class> b(X);
//   cout << "b: " << b << endl;
//   
//   centerInterval<mpfi_class,mpfr_class>(b);
// //   cout << "temp: " << temp << endl;
//   cout << "b: " << b << endl;
//   matrix_float temp2 = centerFloat<mpfi_class,mpfr_class>(b);
//   cout << "temp2: " << temp2 << endl;
//   
//   eps_inflation_box<mpfi_class,mpfr_class> ( b, 1.001 );
//   cout << "b: " << b << endl;
// //   cout << "ici" << endl;
//   
//   matrix_interval X2 = eps_inflation_box<mpfi_class,mpfr_class> ( X, 1.001 );
// //   matrix_interval X2mid = centerInterval<mpfi_class>(X2);
// //   cout << "X2: " << X2 << endl;
// //   cout << "X2mid: " << X2mid << endl;
//   cout << "evaluation:" << endl;
// //   cout << evaluate_order_2<mpfi_class,mpfr_class>(X2, Func<mpfi_class,mpfr_class>, Jaco<mpfi_class,mpfr_class>, Hess<mpfi_class,mpfr_class>) << endl;
//   cout << evaluate_order_2<mpfi_class,mpfr_class>(b, Func<mpfi_class,mpfr_class>, Jaco<mpfi_class,mpfr_class>, Hess<mpfi_class,mpfr_class>) << endl;
//   
//   cout << "contraction:" << endl;
//   Matrix<mpfi_class> resK;
//   cout << krawczyk_order_2<mpfi_class,mpfr_class>(b, Func<mpfi_class,mpfr_class>, Jaco<mpfi_class,mpfr_class>, Hess<mpfi_class,mpfr_class>, &resK) << endl;
//   cout << "resK: " << resK << endl;
// //   cout << krawczyk_order_2<mpfi_class,mpfr_class>(X2, Func<mpfi_class,mpfr_class>, Jaco<mpfi_class,mpfr_class>, Hess<mpfi_class,mpfr_class>) << endl;
// //   cout << "resK: " << resK << endl;
//   
//   cpp_tools::list< box<mpfi_class,mpfr_class> > temp = subdivide_box<mpfi_class,mpfr_class> ( b, 1e-10 );
//   cout << " subdivide: " << endl;
//   cout << temp << endl;
//   
//   cpp_tools::list< box<mpfi_class,mpfr_class> > temp3 = subdivide_Maximal_smear<mpfi_class,mpfr_class> ( b, Jaco<mpfi_class,mpfr_class>(b.get_m()), 1e-10 );
//   cout << " subdivide: " << endl;
//   cout << temp3 << endl;
  return 0; 
} 
