#include "../mpfi/mpfixx.hpp"
#include "../mpfi/mpfrxx.hpp"
#include "../mpfi/mpfimpfr.hpp"
#include "../generic/matrix.h" 
#include "../generic/box.h" 
#include "../generic/queue.h" 
#include <iostream>

using namespace std;
using namespace matrix;
using namespace cpp_tools;

typedef Matrix<mpfi_class> matrix_interval;
typedef Matrix<mpfi_class> matrix_float;

int main() {
  
  mp_prec_t prec = 113;
  
  mpfi_class Tab1[3] = { mpfi_class(prec, 0.,1.), mpfi_class(prec, 0.,1.), mpfi_class(prec, 0.,1.) };
  mpfi_class Tab2[3] = { mpfi_class(prec, 0.25,0.5), mpfi_class(prec, 0.5,0.75), mpfi_class(prec, 0.25,1.25) };
 
  matrix_interval M1(3,1,Tab1);
  matrix_interval M2(3,1,Tab2);
  
  cout << "M1: " << M1 << endl;
  cout << "M2: " << M2 << endl;
  
  cpp_tools::list<matrix_interval> mqueue;
  mqueue.push_back(M1);
  mqueue.push_back(M2);
  
  cout << mqueue << endl;
  
  
  return 0; 
}
