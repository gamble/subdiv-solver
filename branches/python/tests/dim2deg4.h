#include "../base/base.h"
#include "../generic/matrix.h"

using namespace matrix;

template<class I, class F>
Matrix<I> Func ( Matrix<I> &m ) {
//   cout << "Func" << endl;
  Matrix<I> res ( 2,1 );
//   cout << "m: " << m ;
  I x = m(0,0);
  I y = m(1,0);
//   cout << "x: " << x << endl ;
//   cout << "y: " << y << endl ;
//   cout << "3*x*y*y + 5*y*x + 3: " << ((F) 3.)*x*y*y + ((F) 5.)*y*x + ((F) 3.) << endl ;
  res(0,0) = ((F) 3.)*x*y*y + ((F) 5.)*y*x + ((F) 3.);
  res(1,0) = ((F) 5.)*x*x*y*y + ((F) 3.)*y*x*x + 2.*y + ((F) 3.);
//   cout << "res(0,0): " << res(0,0) << endl ;
//   cout << "res(1,0): " << res(1,0) << endl ;
  return res;
}

template<class I, class F>
Matrix<I> Jaco ( Matrix<I> &m ) {
//   cout << "Jaco" << endl;
  Matrix<I> res ( 2,2 );
  I x = m(0,0);
  I y = m(1,0);
  res(0,0) = ((F) 3.)*y*y + ((F) 5.)*y;      res(0,1) = ((F) 6.)*x*y + ((F) 5.)*x;
  res(1,0) = ((F) 10.)*x*y*y + ((F) 6.)*y*x; res(1,1) = ((F) 10.)*x*x*y + ((F) 3.)*x*x + ((F) 2.);
  return res;
}

template<class I, class F>
Matrix<I> Hess ( Matrix<I> &m ) {
//   cout << "Hess" << endl;
  Matrix<I> res ( 2,4 );
  I x = m(0,0);
  I y = m(1,0);
  res(0,0) = 0;              res(0,1) = ((F) 6.)*y + ((F) 5.); res(0,2) = ((F) 10.)*y*y + ((F) 6.)*y; res(0,3) = ((F) 20.)*x*y + ((F) 6.)*x;
  res(1,0) = ((F) 6.)*y + ((F) 5.); res(1,1) = ((F) 6.)*x;            res(1,2) = ((F) 20.)*x*y + ((F) 6.)*x; res(1,3) = ((F) 10.)*x*x;
  return res;
}