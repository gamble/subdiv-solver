#include "../base/base.h"

#include "../arb/arfxx.hpp" 
// #include <arf.h>

#include "../generic/matrix.h"

using namespace cpp_tools;
using namespace matrix;

long arf_class::_prec = 53;

typedef Matrix<arf_class> matrix_float;

int main() {
  
  arf_class Tab[4] = { arf_class(0.), arf_class(1.), arf_class(2.), arf_class(3.) };
 
  matrix_float M(2,2,Tab);
  
  cout << "M: " << M << endl;
  return 0;
}
