#include "../build/matrix_mpfi.h"

int main() {
  
  mp_prec_t prec = 113;
  
  mpfi_class Tab1[3] = { mpfi_class(prec, 0.,1.), mpfi_class(prec, 0.,1.), mpfi_class(prec, 0.,1.) };
  mpfi_class Tab2[3] = { mpfi_class(prec, 0.25,0.5), mpfi_class(prec, 0.5,0.75), mpfi_class(prec, 0.25,1.25) };
 
  matrix_interval M1(3,1,Tab1);
  matrix_interval M2(3,1,Tab2);
  
  cout << "M1: " << M1 << endl;
  cout << "M2: " << M2 << endl;
  
  struct intersection_result<mpfi_class> interM1M2 = overlaps<mpfi_class>(M2,M1);
  cout << "intersectionM1M2: _overlaps: " << interM1M2._overlaps << ", _isIn: " << interM1M2._isIn << ", isInInt: " << interM1M2._isInInt << " _intersection: " << endl;
  cout << interM1M2._intersection << endl;
  
  cout << "Min_absolute_diameter(M2): " << Min_absolute_diameter<mpfi_class,mpfr_class>(M2) << endl;
  cout << "Max_absolute_diameter(M2): " << Max_absolute_diameter<mpfi_class,mpfr_class>(M2) << endl;
  
  cout << "contains_zero(M1): " << contains_zero(M1) << endl;
  cout << "contains_zero(M2): " << contains_zero(M2) << endl;
  
  cpp_tools::list< matrix_interval > listeres = subdivide_box( M1 );
  cout << listeres << endl;
  
  mpfi_class Tab3[4] = { mpfi_class(prec, 5.,6.5), mpfi_class(prec, 0.5,0.75), mpfi_class(prec, 0.25,1.25), mpfi_class(prec, -4.,-2.1) };
  matrix_interval M3(2,2,Tab3);
  cout << "M3: " << M3 << endl;
  matrix_interval M3_inv;
  inverse(M3, &M3_inv);
  cout << "M3_inv: " << M3_inv << endl;
  
  return 0; 
} 
