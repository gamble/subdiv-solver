//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#include "matrix_mpfi.h"

using namespace interval_mpfi;

mp_prec_t prec = 53;
mp_prec_t cpp_tools::mpfi_class::_prec = prec;
mpfr_prec_t cpp_tools::mpfr_class::_prec = prec;

//matrix
template class matrix::Matrix<interval>;
template class matrix::Matrix<myfloat>;

//matrix_mpfi
template bool inverseLowerTriangular<interval>(const matrix_interval& A, matrix_interval *B); 
template bool inverseUpperTriangular<interval>(const matrix_interval& A, matrix_interval *B); 
template bool inverse<interval>(const matrix_interval& A, matrix_interval *B); 

template interval det22<interval>(interval *);
template interval det33<interval>(interval *);
template interval det44<interval>(interval *);

template bool inverse22<interval>(const matrix_interval& A, matrix_interval *B);
template bool inverse33<interval>(const matrix_interval& A, matrix_interval *B);
template bool inverse44<interval>(const matrix_interval& A, matrix_interval *B);

//matrix_mpfr
template bool inverseLowerTriangular<myfloat>(const matrix_float& A, matrix_float *B); 
template bool inverseUpperTriangular<myfloat>(const matrix_float& A, matrix_float *B); 
template bool inverse<myfloat>(const matrix_float& A, matrix_float *B); 

template myfloat det22<myfloat>(myfloat *);
template myfloat det33<myfloat>(myfloat *);
template myfloat det44<myfloat>(myfloat *);

template bool inverse22<myfloat>(const matrix_float& A, matrix_float *B);
template bool inverse33<myfloat>(const matrix_float& A, matrix_float *B);
template bool inverse44<myfloat>(const matrix_float& A, matrix_float *B);

//list
template class cpp_tools::list<matrix_interval>;
//queue
// template class cpp_tools::queue<matrix_interval>;

//operation
template struct intersection_result<interval>;
template matrix_interval centerInterval<interval>( const matrix_interval & );
template Matrix<myfloat> centerFloat<interval,myfloat> ( const matrix_interval & );
template matrix_interval eps_inflation_box<interval,myfloat> ( const matrix_interval &, const myfloat &);
template matrix_interval centerInterval_Disturbed<interval,myfloat> ( const matrix_interval &, const myfloat &);
template bool contains_zero<interval> ( const matrix_interval &m );
template myfloat Min_absolute_diameter<interval,myfloat> ( const matrix_interval &m );
template myfloat Max_absolute_diameter<interval,myfloat> ( const matrix_interval &m );
template myfloat relative_diameter<interval,myfloat> ( const matrix_interval &m );
// template struct intersection_result<interval> overlaps<interval> ( const matrix_interval &X, const matrix_interval &Y );
template intersection_result_interval overlaps<interval> ( const matrix_interval &X, const matrix_interval &Y );
template matrix_interval Interval_hull<interval,myfloat> ( const matrix_interval &m, const matrix_interval &n );
template bool containsBorder<interval,myfloat>( const matrix_interval &m, const matrix_interval &n );
template int complementary<interval>( const matrix_interval &X, const matrix_interval &Y, cpp_tools::list<matrix_interval> &comp );
// template cpp_tools::list<matrix_interval> complementary( const matrix_interval &, const matrix_interval &);

//section
template cpp_tools::list<matrix_interval> subdivide_box<interval,myfloat>( const matrix_interval &m );
template cpp_tools::list<matrix_interval> subdivide_Maximal_width<interval,myfloat>( const matrix_interval & );
template cpp_tools::list<matrix_interval> subdivide_Maximal_smear<interval,myfloat>( const matrix_interval &, const matrix_interval & );

///DEPRECATED
// template cpp_tools::list<matrix_interval> subdivide_box_limited<interval,myfloat>( const matrix_interval &m, const myfloat ); 
// template cpp_tools::list<matrix_interval> subdivide_Maximal_width_limited<interval,myfloat>( const matrix_interval &, const myfloat );
// template cpp_tools::list<matrix_interval> subdivide_Maximal_smear_limited<interval,myfloat>( const matrix_interval &, const matrix_interval &, const myfloat );