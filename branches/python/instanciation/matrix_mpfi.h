//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#ifndef MATRIX_MPFI
#define MATRIX_MPFI

#include "../generic/operation.h"
#include "../generic/section.h"
#include "../generic/matrix.h"
#include "../generic/list.h" 
// #include "../generic/queue.h"
#include "../mpfi/mpfrxx.hpp"
#include "../mpfi/mpfixx.hpp"
#include "../mpfi/mpfimpfr.hpp"

using namespace matrix;
namespace interval_mpfi{

typedef cpp_tools::mpfi_class interval;
typedef cpp_tools::mpfr_class myfloat;

typedef Matrix< interval > matrix_interval;
typedef Matrix< myfloat > matrix_float;

typedef struct intersection_result< interval > intersection_result_interval;

}//namespace interval_mpfi
#endif
