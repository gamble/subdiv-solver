//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#include "solver_boost.h"

using namespace interval_boost;

//box
template class Box<interval,myfloat>;
template class cpp_tools::list<Box_i *>;

template matrix_interval * centerInterval<interval,myfloat> ( Box_i &b );
template matrix_float centerFloat<interval,myfloat> ( const Box_i &b );
template matrix_interval *eps_inflation_box<interval,myfloat> ( Box_i &b,  const myfloat &eps );
template bool contains_zero<interval,myfloat>( const Box_i &b );
template myfloat Min_absolute_diameter<interval,myfloat>( const Box_i &b );
template matrix_interval *evaluate_fmid<interval,myfloat> ( Box_i &b,  matrix_interval (*Func) (matrix_interval));
template matrix_interval *evaluate_jmid<interval,myfloat> ( Box_i &b,  matrix_interval (*Jaco) (matrix_interval));
template matrix_interval *evaluate_jinf<interval,myfloat> ( Box_i &b,  matrix_interval (*Jaco) (matrix_interval));
template matrix_interval *evaluate_hinf<interval,myfloat> ( Box_i &b,  matrix_interval (*Hess) (matrix_interval));
template cpp_tools::list< Box_i *> subdivide_box<interval,myfloat>( const Box_i &b );
template cpp_tools::list< Box_i *> subdivide_Maximal_smear<interval,myfloat>( const Box_i &b, const matrix_interval &jacm, bool, const myfloat min_width );

template int complementary<interval,myfloat>( const matrix_interval &X, const Box_i &Y, cpp_tools::list< Box_i *> & comp );

//evaluation
template matrix_interval evaluate_order_0<interval,myfloat> ( Box_i &b, matrix_interval (*func) (matrix_interval), int verbosity );
template matrix_interval evaluate_order_1<interval,myfloat> ( Box_i &b, matrix_interval (*Func) (matrix_interval), matrix_interval (*Jaco) (matrix_interval), int verbosity);
template matrix_interval evaluate_order_2<interval,myfloat> ( Box_i &b, matrix_interval (*Func) (matrix_interval), matrix_interval (*Jaco) (matrix_interval), matrix_interval (*Hess) (matrix_interval) , int verbosity);

//contraction
template struct contraction_result<interval>;
template int krawczyk_order_2<interval,myfloat>( Box_i &b, matrix_interval (*Func) (matrix_interval), matrix_interval (*Jaco) (matrix_interval), matrix_interval (*Hess) (matrix_interval), matrix_interval *resK, bool control_Krawczik_Precision, int verbosity);

template int krawczyk_order_2 <interval,myfloat> ( matrix_interval box, matrix_interval (*func) (matrix_interval), matrix_interval (*jac) (matrix_interval), matrix_interval (*Hess) (matrix_interval), matrix_interval *resK, bool control_Krawczik_Precision, int verbosity );

template struct contraction_result<interval> contraction_test<interval> ( matrix_interval box, matrix_interval boxInit, int verbosity);

template int krawczyk_order_1<interval,myfloat>( Box_i &b, matrix_interval (*Func) (matrix_interval), matrix_interval (*Jaco) (matrix_interval), matrix_interval *resK, bool control_Krawczik_Precision, int verbosity);

template int krawczyk_order_1<interval,myfloat>( matrix_interval box, matrix_interval (*Func) (matrix_interval), matrix_interval (*Jaco) (matrix_interval), matrix_interval *resK, bool control_Krawczik_Precision, int verbosity);

//subdivision
template class subdivision_solver<interval,myfloat>;

///DEPRECATED

// template matrix_interval evaluate_order_0<interval,myfloat> ( matrix_interval m, matrix_interval (*func) (matrix_interval), int verbosity );
// template matrix_interval evaluate_order_1<interval,myfloat> ( matrix_interval m, matrix_interval (*func) (matrix_interval), matrix_interval (*Jac) (matrix_interval) , int verbosity);
// template matrix_interval evaluate_order_2<interval,myfloat> ( matrix_interval m, matrix_interval (*Func) (matrix_interval), matrix_interval (*Jac) (matrix_interval), matrix_interval (*Hess) (matrix_interval) , int verbosity);

// template matrix_interval krawczyk<interval,myfloat> ( matrix_interval box, matrix_interval (*func) (matrix_interval), matrix_interval (*jac) (matrix_interval), int verbosity );

// template int test_krawczyk_order_2<interval,myfloat> ( matrix_interval box, matrix_interval (*func) (matrix_interval), matrix_interval (*jac) (matrix_interval), matrix_interval (*hess) (matrix_interval) , int verbosity);

// template int are_the_same_solutions<interval,myfloat> ( matrix_interval sol1, matrix_interval sol2, matrix_interval (*Func) (matrix_interval), matrix_interval (*Jaco) (matrix_interval), matrix_interval (*Hess) (matrix_interval), int verbosity);

// template matrix_interval separateSolutions<interval,myfloat> ( matrix_interval sol1, matrix_interval sol2, matrix_interval (*Func) (matrix_interval), matrix_interval (*Jaco) (matrix_interval), matrix_interval (*Hess) (matrix_interval), int verbosity);