import time

from subdivision_solver import subdivision_solver

Rr.<x1,x2,x3,x4,x5,x6> =ZZ[]

p1 = ((4*x3+3*x6)*x3 + 2*x5)*x3 + x4
p2 = ((4*x2+3*x6)*x2 + 2*x5)*x2 + x4
p3 = ((4*x1+3*x6)*x1 + 2*x5)*x1 + x4 
p4 = x4+x5+x6+1
p5 = (((x2+x6)*x2+x5)*x2+x4)*x2 + (((x3+x6)*x3+x5)*x3+x4)*x3
p6 = (((x1+x6)*x1+x5)*x1+x4)*x1 + (((x2+x6)*x2+x5)*x2+x4)*x2

prec = 53
RF = RealField(prec)
RIF = RealIntervalField(prec) 

sage.rings.real_mpfi.printing_style='brackets'

tab =  [ [RIF(0.0333, 0.2173)],
	 [RIF(0.4,0.6)],
	 [RIF(0.786,0.9666)],
	 [RIF(-3071,-0.1071)],
	 [RIF(1.1071,1.3071)],
	 [RIF(-2.1, -1.9)]  ]

minsize = 10e-10
t1 = time.clock()
test = subdivision_solver([p1,p2,p3,p4,p5,p6],[x1,x2,x3,x4,x5,x6])
status = test.solve(tab,minsize,113, 'stats_color')
t2 = time.clock()
t6 = t2-t1
print "status: ", status
res = test.getSolutions();
print "temps pour realSolver: ", t6, "s"  
print "############################"
print "Nombre de Solutions: ", len(res)
for i in range(0,len(res)):
  print "Solution : ", res[i][0][0], ", ", res[i][1][0] 
print "############################"
