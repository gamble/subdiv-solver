import time
from subdivision_solver import subdivision_solver
prec = 53
#prec = 113
RIF = RealIntervalField(prec)
RF = RealField(prec)
sage.rings.real_mpfi.printing_style='brackets'

Rr.<x1> =ZZ[]
degree = 129
bitsize = 14
p1 = x1^degree - ((2^(bitsize/2)-1)*x1-1)^2

#p1=p1.expand()
print p1

tab = [ [RIF(-10,10)]  ]
#tab = [ [RIF(0.0078740155731793493033, 0.0078740156459389254451)]  ]
minsize=10e-1000
t1 = time.clock()
test = subdivision_solver([p1],[x1])
status = test.solve(tab,minsize,10000,'stats_color')
#status = test.solve(tab,minsize,500,3)
#status = test.solve(tab,minsize,53,'stats_color')
t2 = time.clock()
t6 = t2-t1
print "temps pour realSolver: ", t6, "s"
print "status: ", status
res = test.getSolutions();
print "############################"
print "Nombre de Solutions: ", len(res)
for i in range(0,len(res)):
  #print "Solution : ", res[i][0][0], ", ", res[i][1][0]
  print "Solution : ", res[i][0][0]
print "############################"
#resContr = res = test.getSolutions(1e-1000);
#resContr = res = test.getSolutions(0.);
#print "############################"
#print "Solutions contractees: ", len(resContr)
#for i in range(0,len(resContr)):
  ##print "Solution : ", resContr[i][0][0], ", ", resContr[i][1][0]
  #print "Solution : ", resContr[i][0][0]
#print "############################"
undetermined = test.getUndetermined()
print "############################"
print "Boites indeterminees: ", len(undetermined)
#print "boite : ", undetermined[0][0][0]
#print "boite : ", undetermined[1][0][0]
#for i in range(0,len(undetermined)):
  #print "boite : ", undetermined[i][0][0], ", ", undetermined[i][1][0]
  #print "boite : ", undetermined[i][0][0]
print "############################"