//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////


#ifndef _BASE_H
#define _BASE_H 

#define QUIET 0 //no output
#define STATS 1 //just stats at the end of the process
#define DEBUGING_MODE 3 //very verbose

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

// operation.h
#define MAX(a,b) (a<b?b:a)
#define MIN(a,b) (a<b?a:b)

// #define SUBDIVIDE_OK 1
// #define SUBDIVIDE_TOO_SMALL 0
#define COMPLEMENTARY_CUTED 1
#define COMPLEMENTARY_UNCUTED 0

#endif
