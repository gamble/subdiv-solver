//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#ifndef _MPFIMPFR_H
#define _MPFIMPFR_H

#include "../base/base.h"
#include <mpfr.h>
#include <mpfi.h>
#include "mpfrxx.hpp"
#include "mpfixx.hpp"

// using namespace cpp_tools;
namespace cpp_tools {
// Arithmetic operators between mpfi and mpfr

mpfi_class operator* (const mpfi_class& x, const mpfr_class& y) {
  mpfi_class res;
  mpfi_mul_fr(res._value, x._value, y._value);
  return res;
}

mpfi_class operator* (const mpfr_class& y, const mpfi_class& x) {
  mpfi_class res;
  mpfi_mul_fr(res._value, x._value, y._value);
  return res;
}

// mpfi_class IntervalMultByFloat(const mpfi_class& x, const mpfr_class& y){
//   mpfi_class res;
//   mpfi_mul_fr(res._value, x._value, y._value);
//   return res;
// }

mpfi_class operator+ (const mpfi_class& x, const mpfr_class& y) {
  mpfi_class res;
  mpfi_add_fr(res._value, x._value, y._value);
  return res;
}

// mpfi_class IntervalAddToFloat(const mpfi_class& x, const mpfr_class& y){
//   mpfi_class res;
//   mpfi_add_fr(res._value, x._value, y._value);
//   return res;
// }

mpfi_class operator+ (const mpfr_class& y, const mpfi_class& x) {
  mpfi_class res;
  mpfi_add_fr(res._value, x._value, y._value);
  return res;
}

mpfi_class operator- (const mpfi_class& x, const mpfr_class& y) {
  mpfi_class res;
  mpfi_sub_fr(res._value, x._value, y._value);
  return res;
}

mpfi_class operator- (const mpfr_class& y, const mpfi_class& x) {
  mpfi_class res;
  mpfi_fr_sub(res._value, y._value, x._value);
  return res;
}

mpfi_class operator/ (const mpfi_class& x, const mpfr_class& y) {
  mpfi_class res;
  mpfi_div_fr(res._value, x._value, y._value);
  return res;
}

mpfi_class operator/ (const mpfr_class& y, const mpfi_class& x) {
  mpfi_class res;
  mpfi_fr_div(res._value, y._value, x._value);
  return res;
}
}
#endif