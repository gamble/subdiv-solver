//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#ifndef _MPFRXX_H
#define _MPFRXX_H

#include "../base/base.h"
#include <mpfr.h>

#define ROUNDING_MODE GMP_RNDN

// #define MAX_MPFR(a,b) (a<b?b:a)

// using namespace std; 

namespace cpp_tools{

class mpfr_class
{
    public:
        mpfr_t _value;
	static mpfr_prec_t _prec;
	
       mpfr_class(){
// 	 cout << "mpfr init: prec: " << _prec << endl;
	  mpfr_init2(_value, _prec);
       }
	
       mpfr_class(const mpfr_t x) { 
//            cout << "mpfr init: prec: " << _prec << endl;
          mpfr_init2(_value, _prec);
          mpfr_set(_value, x, ROUNDING_MODE); 
	}
	
        mpfr_class(const mpfr_class& x){
//             cout << "mpfr init: prec: " << _prec << endl;
          mpfr_init2(_value, _prec);
          mpfr_set(_value, x._value, ROUNDING_MODE);
	}
	
	mpfr_class(const double d){
          mpfr_init2(_value, _prec);
          mpfr_set_d(_value, d, ROUNDING_MODE);
	}
	
	mpfr_class(const int num, const int denom){
	  mpfr_init2(_value, _prec);
	  mpfr_set_si(_value, num, ROUNDING_MODE);
	  mpfr_div_si(_value, _value, denom, ROUNDING_MODE);
	}
	
// 	mpfr_class(mpfr__prec_t p, const unsigned long ui){
//           mpfr_init2(_value, _prec);
//           mpfr_set_ui(_value, ui, ROUNDING_MODE);
// 	}
	
	static mpfr_prec_t get_prec() {
	  return _prec;
	}
	
	static void set_prec( mp_prec_t prec ) {
//             cout << "mpfr set prec: prec: " << prec << endl;
	  ///TEST
// 	  if (prec>_prec)
	  ///fin TEST
	    _prec = prec;
            
	}
	
	long double get_ld() const {
	  return mpfr_get_ld(_value,ROUNDING_MODE);
	}
	
	double get_d() const {
	  return mpfr_get_d(_value,ROUNDING_MODE);
	}
	
        ~mpfr_class() { 
          mpfr_clear (_value); 
	}
	
// 	mpfr_class pow (int exp) {
// 	  mpfr_t res;
// 	  mpfr_init2(res, _prec);
// 	  mpfr_pow_si( res, _value, exp,ROUNDING_MODE); 
// 	  return mpfr_class(res);
// 	}
	
	mpfr_class & operator= (const mpfr_class& x) {
          mpfr_set(_value, x._value, ROUNDING_MODE);
          return *this;
        }
        
        mpfr_class & operator= (const int& x) {
	  mpfr_set_si(_value, x, ROUNDING_MODE);
          return *this;
        }
        
         mpfr_class & operator= (const double& x) {
	  mpfr_set_d(_value, x, ROUNDING_MODE);
          return *this;
        }
        
        mpfr_class operator* (const mpfr_class& x){
            //cout<<"mul"<<endl;
	    mpfr_class res;
            mpfr_mul(res._value, _value, x._value, ROUNDING_MODE);
            return res;
        }
        
        mpfr_class operator+ (const mpfr_class& x) {
	    mpfr_class res;
            mpfr_add(res._value, _value, x._value, ROUNDING_MODE);
            return res;
        }
        
        mpfr_class & operator+= (const mpfr_class& x) {
	    mpfr_add(_value, _value, x._value, ROUNDING_MODE);
            return *this;
        }
        
        mpfr_class operator- () {
	    mpfr_class res (*this);
            mpfr_neg(res._value, _value, ROUNDING_MODE);
            return res;
        }
        
        mpfr_class operator- (const mpfr_class& x){
	    mpfr_class res;
            mpfr_sub(res._value, _value, x._value, ROUNDING_MODE);
            return res;
        }
        
        mpfr_class & operator-= (const mpfr_class& x) {
	    mpfr_sub(_value, _value, x._value, ROUNDING_MODE);
            return *this;
        }
//         
        mpfr_class operator/ (const mpfr_class& x){
	    mpfr_class res;
            mpfr_div(res._value, _value, x._value, ROUNDING_MODE);
            return res;
        }
        
        bool operator< (const mpfr_class& x){
	    return mpfr_cmp ( _value, x._value) == -1;
        }
        
        bool operator> (const mpfr_class& x){
	    return mpfr_cmp ( _value, x._value) == 1;
        }
        
        mpfr_class & operator/= (const mpfr_class& x) {
	    mpfr_div(this->_value, this->_value, x._value, ROUNDING_MODE);
            return *this;
        }
        
        mpfr_class next () {
	  mpfr_t next;
	  mpfr_init2(next, this->_prec);
	  mpfr_set(next, this->_value, ROUNDING_MODE);
	  mpfr_nextabove(next);
	  return mpfr_class(next);
	}
	
	mpfr_class prev () {
	  mpfr_t prev;
	  mpfr_init2(prev, this->_prec);
	  mpfr_set(prev, this->_value, ROUNDING_MODE);
	  mpfr_nextbelow(prev);
	  return mpfr_class(prev);
	}
};

// void my_mpfr_set(mpfr_t dest, mpfr_t orig) {
//   mpfr_set(dest, orig, ROUNDING_MODE);
// };

ostream &operator<<(ostream &out, const mpfr_class &m) {
  out << "[" << mpfr_class::get_prec() << ", " << m.get_ld() << "]";
//   out << "[" << mpfr_get_prec(m._value) << ", " << m.get_ld() << "]";
  
//   mpfr_t next;
//   mpfr_init2(next, m._prec);
//   mpfr_set(next, m._value, ROUNDING_MODE); 
//   mpfr_nextabove(next);
//   
//   mpfr_t ulp;
//   mpfr_init2(ulp, m._prec);
//   mpfr_sub(ulp, next, m._value, ROUNDING_MODE);
//   
//   mpfr_exp_t dummy_number, dummy_next, dummy_ulp;
//   char * str_number = mpfr_get_str(NULL, &dummy_number, 10, 0, m._value, ROUNDING_MODE);
//   char * str_next   = mpfr_get_str(NULL, &dummy_next  , 10, 0, next    , ROUNDING_MODE);
//   char * str_ulp    = mpfr_get_str(NULL, &dummy_ulp   , 10, 0, ulp     , ROUNDING_MODE);
//   
//   out << "[" << mpfr_class::get_prec() << ", " << str_number << "e" << dummy_number << ", ulp: " << str_ulp << "e" << dummy_ulp << "]";
//   mpfr_free_str(str_number);
//   mpfr_free_str(str_next);
//   mpfr_free_str(str_ulp);
  
  return out;
};

mpfr_class sqrt(const mpfr_class& x) {
  mpfr_class res;
  mpfr_sqrt(res._value, x._value, ROUNDING_MODE);
  return res;
};

mpfr_class pow (const mpfr_class& x, int exp) {
  mpfr_class res;
  mpfr_pow_si(res._value, x._value, exp, ROUNDING_MODE); 
  return res;
}

mpfr_class exp (const mpfr_class& x) {
  mpfr_class res;
  mpfr_exp(res._value, x._value, ROUNDING_MODE); 
  return res;
}

mpfr_class log (const mpfr_class& x) {
  mpfr_class res;
  mpfr_log(res._value, x._value, ROUNDING_MODE); 
  return res;
}

mp_prec_t get_prec (const mpfr_class& x) {
  return mpfr_class::get_prec();
}

//arithmetic operators between mpfr and double
mpfr_class operator* (const mpfr_class& x, const double& y) {
  mpfr_class res;
  mpfr_mul_d(res._value, x._value, y , ROUNDING_MODE);
  return res;
}

mpfr_class operator* (const double& y, const mpfr_class& x) {
  mpfr_class res;
  mpfr_mul_d(res._value, x._value, y, ROUNDING_MODE);
  return res;
}

bool iszero(const mpfr_class& x){
  return mpfr_zero_p(x._value);
}

mpfr_class next (const mpfr_class & x) {
  mpfr_t next;
  mpfr_init2(next, x._prec);
  mpfr_set(next, x._value, ROUNDING_MODE);
  mpfr_nextabove(next);
  return mpfr_class(next);
}

mpfr_class prev (const mpfr_class & x) {
  mpfr_t prev;
  mpfr_init2(prev, x._prec);
  mpfr_set(prev, x._value, ROUNDING_MODE);
  mpfr_nextbelow(prev);
  return mpfr_class(prev);
}

}

#endif