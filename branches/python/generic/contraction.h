//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////


#ifndef _CONTRACTION_H
#define _CONTRACTION_H

#include "../base/base.h"
#include "../base/parameters.h"
#include "operation.h"
#include "evaluation.h"
#include "box.h"
using namespace matrix;

#define KRAWCZYK_OK 0
#define KRAWCZYK_OK_NO_SOLUTION 1
#define KRAWCZYK_OK_ONE_SOLUTION 2
#define KRAWCZYK_OK_CAN_NOT_DECIDE 3
#define KRAWCZYK_NOT_ENOUGH_PRECISION 4
#define KRAWCZYK_JACMIDM_NOT_INVERSIBLE 5
#define KRAWCZYK_OTHER 6

template <class I>
struct contraction_result {
  int _result;
  Matrix<I> _contraction;
};

template <class I, class F>
int krawczyk_order_2( Box<I,F> &b, 
		      Matrix<I> (*Func) (Matrix<I>), 
		      Matrix<I> (*Jaco) (Matrix<I>), 
		      Matrix<I> (*Hess) (Matrix<I>),
		      Matrix<I> *resK,
		      bool control_Krawczyk_Precision,
		      int verbosity = QUIET);

template <class I, class F> ///version without box data structure and eps_inflation
int krawczyk_order_2 ( Matrix<I> box, 
		       Matrix<I> (*func) (Matrix<I>), 
		       Matrix<I> (*jac) (Matrix<I>), 
		       Matrix<I> (*hess) (Matrix<I>) , 
		       Matrix<I> *resK,
		       bool control_Krawczyk_Precision,
		       int verbosity = QUIET);

template <class I>
struct contraction_result<I> contraction_test ( Matrix<I> box, 
						Matrix<I> boxInit, 
						int verbosity = QUIET);

template <class I, class F>
int krawczyk_order_1( Box<I,F> &b, 
		      Matrix<I> (*Func) (Matrix<I>), 
		      Matrix<I> (*Jaco) (Matrix<I>), 
		      Matrix<I> *resK,
		      bool control_Krawczyk_Precision,
		      int verbosity = QUIET);

template <class I, class F> ///version without box data structure and eps_inflation
int krawczyk_order_1( Matrix<I> box, 
		      Matrix<I> (*Func) (Matrix<I>), 
		      Matrix<I> (*Jaco) (Matrix<I>), 
		      Matrix<I> *resK,
		      bool control_Krawczyk_Precision,
		      int verbosity = QUIET);

template <class I, class F> //version utilisant la structure de donnée box -> économies
int krawczyk_order_2( Box<I,F> &b, 
		      Matrix<I> (*Func) (Matrix<I>), 
		      Matrix<I> (*Jaco) (Matrix<I>), 
		      Matrix<I> (*Hess) (Matrix<I>),
		      Matrix<I> *resK,
		      bool control_Krawczyk_Precision,
// 		      int verbosity = QUIET) {
                      int verbosity) {
  
  Matrix<I> prec_criterion;
  int res = KRAWCZYK_OK;
  
  Matrix<I> *midb = centerInterval<I> (b);
  Matrix<I> b_minus_midb = *(eps_inflation_box<I,F>(b)) - (*midb);
  Matrix<I> b_minus_midb_trans = b_minus_midb.transpose();
  Matrix<I> *fmidb = evaluate_fmid<I,F> (b, Func);
  Matrix<I> *jmidb = evaluate_jmid<I,F> (b, Jaco);
  Matrix<I> *hinfb = evaluate_hinf<I,F> (b, Hess);
  
  Matrix<I> jmidb_inv;
  bool is_inversible = inverse ( *jmidb, &jmidb_inv );
  if (not is_inversible) {
    midb = new Matrix<I> (*midb);
    fmidb = new Matrix<I> (*fmidb);
    jmidb = new Matrix<I> (*jmidb);
    hinfb = new Matrix<I> (*hinfb);
  }
  
  int i = 0;
  while ( (not is_inversible) and (i < MAX_TRIALS_TO_INVERSE) ) {
    *midb = centerInterval_Disturbed<I,F> ( *(eps_inflation_box<I,F>(b)), (F) COEFF_PERTURBATION );
    *jmidb = Jaco(*midb);
    is_inversible = inverse ( *jmidb, &jmidb_inv );
    i++;  
  }
  if (not is_inversible) {
    if (verbosity == DEBUGING_MODE) {
	cout << "krawczick jacobian non inversible after " << i << " trials "  << endl;
    }
    return KRAWCZYK_JACMIDM_NOT_INVERSIBLE;
  }
  
  if ( i>0 ) {
    *fmidb = Func(*midb);
    *hinfb = Hess(*midb);
    b_minus_midb = *(eps_inflation_box<I,F>(b)) - (*midb);
    b_minus_midb_trans = b_minus_midb.transpose();
  }

  int nbvars = b.getDim();
  
  if (control_Krawczyk_Precision){
  //check if actual precision is not enough
    prec_criterion = jmidb_inv * (*fmidb);
    Matrix<I> p_minus_prec_criterion = *midb - prec_criterion;
    struct intersection_result<I> Ires = overlaps<I>( p_minus_prec_criterion, b.get_m() );
//     if ( Min_absolute_diameter<I,F>(prec_criterion) > Min_absolute_diameter<I,F>(b.get_m()) ///why min_absolute_diameter?
//         and Ires._overlaps ) {
    if ( Max_absolute_diameter<I,F>(prec_criterion) > Max_absolute_diameter<I,F>(b.get_m())
        and Ires._overlaps ) {
        res = KRAWCZYK_NOT_ENOUGH_PRECISION;
        if (verbosity == DEBUGING_MODE) {
	  cout << "krawczick precision criterion reached" << endl;
	  cout << "p_minus_prec_criterion: " << p_minus_prec_criterion << endl;
          cout << "prec_criterion: " << prec_criterion << endl;
	  cout << "Min_absolute_diameter(prec_criterion): " << Min_absolute_diameter<I,F>(prec_criterion) << endl;
	  cout << "b.get_m(): " << b.get_m() << endl;
          cout << "Min_absolute_diameter(b.get_m()): " << Min_absolute_diameter<I,F>(b.get_m()) << endl;
	  cout << "overlaps<I>( p_minus_prec_criterion, b.get_m() ): " << Ires._overlaps << endl;
	}
    }
  } 

  //compute hessian
  Matrix<I> hessian_vec(nbvars,1);
  for (int i = 0; i < nbvars; i++ ) {
    hessian_vec(i,0) = (b_minus_midb_trans * (hinfb->subMatFrom(0,nbvars, i*nbvars, nbvars) * b_minus_midb))(0,0);
  }
  *resK = *midb - jmidb_inv * ( hessian_vec + (*fmidb) );
  
  if (verbosity == DEBUGING_MODE) {
      cout << "jmidb_inv: " << jmidb_inv << endl;
      cout << "hessian_vec: " << hessian_vec << endl;
      cout << "resK: " << *resK << endl;
  }
  
  if ( i>0 ) {
    delete midb;
    delete fmidb;
    delete jmidb;
    delete hinfb;
  }
  
  return res;
}

template <class I, class F> //version sans la structure de donnée
int krawczyk_order_2( Matrix<I> b, 
		      Matrix<I> (*Func) (Matrix<I>), 
		      Matrix<I> (*Jaco) (Matrix<I>), 
		      Matrix<I> (*Hess) (Matrix<I>),
		      Matrix<I> *resK,
		      bool control_Krawczyk_Precision,
// 		      int verbosity = QUIET) {
                      int verbosity) {
  
  Matrix<I> prec_criterion;
  int res = KRAWCZYK_OK;
  
  Matrix<I> midb = centerInterval<I> (b);
  Matrix<I> jmidb = Jaco(midb);
  Matrix<I> jmidb_inv;
  bool is_inversible = inverse ( jmidb, &jmidb_inv );
  
  int i = 0;
  while ( (not is_inversible) and (i < MAX_TRIALS_TO_INVERSE) ) {
    midb = centerInterval_Disturbed<I,F> ( b, (F) COEFF_PERTURBATION );
    jmidb = Jaco(midb);
    is_inversible = inverse ( jmidb, &jmidb_inv );
    i++;  
  }
  if (not is_inversible) {
    if (verbosity == DEBUGING_MODE) {
	cout << "krawczick jacobian non inversible after " << i << " trials "  << endl;
    }
    return KRAWCZYK_JACMIDM_NOT_INVERSIBLE;
  }
  Matrix<I> fmidb = Func(midb);
  Matrix<I> hessb = Hess(b);
  Matrix<I> b_minus_midb = b - midb;
  Matrix<I> b_minus_midb_trans = b_minus_midb.transpose();

  int nbvars = b.getNbLines();
  
//   if (control_Krawczyk_Precision){
//   //check if actual precision is not enough
//     prec_criterion = jmidb_inv * fmidb;
//     for (int i = 0; i < nbvars; i++ ){
//       if (( relative_diameter(fmidb(i,0)) > relative_diameter(b(i,0)) )
//       and ( relative_diameter(prec_criterion(i,0)) > relative_diameter(b(i,0)) ))
//       {
// 	if (verbosity == DEBUGING_MODE) {
// 	  cout << "krawczick precision criterion reached" << endl;
// 	  cout << "prec_criterion: " << prec_criterion << endl;
// 	  cout << "relative_diameter(fmidb->get(i,0)): " << relative_diameter(fmidb(i,0)) << endl;
// 	  cout << "relative_diameter(b(i,0)): " << relative_diameter(b(i,0)) << endl;
// 	  cout << "relative_diameter(prec_criterion(i,0)): " << relative_diameter(prec_criterion(i,0)) << endl;
// 	}
//         res = KRAWCZYK_NOT_ENOUGH_PRECISION;
//       }
//     }
//   }
  if (control_Krawczyk_Precision){
  //check if actual precision is not enough
    prec_criterion = jmidb_inv * (fmidb);
    Matrix<I> p_minus_prec_criterion = midb - prec_criterion;
    struct intersection_result<I> Ires = overlaps<I>( p_minus_prec_criterion, b );
    if ( Min_absolute_diameter<I,F>(prec_criterion) > Min_absolute_diameter<I,F>(b) 
        and Ires._overlaps ) {
        res = KRAWCZYK_NOT_ENOUGH_PRECISION;
        if (verbosity == DEBUGING_MODE) {
	  cout << "krawczick precision criterion reached" << endl;
	  cout << "p_minus_prec_criterion: " << p_minus_prec_criterion << endl;
          cout << "prec_criterion: " << prec_criterion << endl;
	  cout << "Min_absolute_diameter(prec_criterion): " << Min_absolute_diameter<I,F>(prec_criterion) << endl;
	  cout << "b: " << b << endl;
          cout << "Min_absolute_diameter(b): " << Min_absolute_diameter<I,F>(b) << endl;
	  cout << "overlaps<I>( p_minus_prec_criterion, b ): " << Ires._overlaps << endl;
	}
    }
  }

  //compute hessian
  Matrix<I> hessian_vec(nbvars,1);
  for (int i = 0; i < nbvars; i++ ) {
    hessian_vec(i,0) = (b_minus_midb_trans * (hessb.subMatFrom(0,nbvars, i*nbvars, nbvars) * b_minus_midb))(0,0);
  }
  *resK = midb - jmidb_inv * ( hessian_vec + fmidb );
  
  return res;
}

    
template <class I>
struct contraction_result<I> contraction_test ( Matrix<I> box, 
                                                Matrix<I> boxInit, 
//                                                 int verbosity = QUIET) {
                                                int verbosity) {
  struct contraction_result<I> res;
  if ( (box.getNbLines()==0) or (box.getNbColumns()==0) ) { // box given by krawczic was empty -> inversion did fail
    res._result = KRAWCZYK_OTHER;
    res._contraction = box;
  }
  else {
    struct intersection_result<I> restemp = overlaps<I>( box, boxInit );
    res._contraction = restemp._intersection;
    if (not restemp._overlaps)
      res._result = KRAWCZYK_OK_NO_SOLUTION;
    else if (restemp._isInInt)
      res._result = KRAWCZYK_OK_ONE_SOLUTION;
    else 
      res._result = KRAWCZYK_OK_CAN_NOT_DECIDE;
    
//     if ( ( not (restemp._isInInt) ) and (restemp._isIn) )
//       cout << "contraction_test: isIn but not isInInt " << endl;
  }
  
  return res;
}

template <class I, class F>
int krawczyk_order_1( Box<I,F> &b, 
		      Matrix<I> (*Func) (Matrix<I>), 
		      Matrix<I> (*Jaco) (Matrix<I>), 
		      Matrix<I> *resK,
		      bool control_Krawczyk_Precision,
// 		      int verbosity = QUIET){
                      int verbosity){
  Matrix<I> prec_criterion;
  int res = KRAWCZYK_OK;
  
//   Matrix<I> *infb = eps_inflation_box<I,F>(b);
  Matrix<I> *midb = centerInterval<I> (b);
  Matrix<I> b_minus_midb = *(eps_inflation_box<I,F>(b)) - (*midb);
  Matrix<I> b_minus_midb_trans = b_minus_midb.transpose();
  Matrix<I> *fmidb = evaluate_fmid<I,F> (b, Func);
  Matrix<I> *jmidb = evaluate_jmid<I,F> (b, Jaco);
  Matrix<I> *jinfb = evaluate_jinf<I,F> (b, Jaco);
  
  Matrix<I> jmidb_inv;
  bool is_inversible = inverse ( *jmidb, &jmidb_inv );
  if (not is_inversible) {
    midb = new Matrix<I> (*midb);
    fmidb = new Matrix<I> (*fmidb);
    jmidb = new Matrix<I> (*jmidb);
//     hinfb = new Matrix<I> (*hinfb);
  }
  
  int i = 0;
  while ( (not is_inversible) and (i < MAX_TRIALS_TO_INVERSE) ) {
    *midb = centerInterval_Disturbed<I,F> ( *(eps_inflation_box<I,F>(b)), (F) COEFF_PERTURBATION );
    *jmidb = Jaco(*midb);
    is_inversible = inverse ( *jmidb, &jmidb_inv );
    i++;  
  }
  if (not is_inversible) {
    if (verbosity == DEBUGING_MODE) {
	cout << "krawczick jacobian non inversible after " << i << " trials "  << endl;
    }
    return KRAWCZYK_JACMIDM_NOT_INVERSIBLE;
  }
  
  if ( i>0 ) {
    *fmidb = Func(*midb);
//     *hinfb = Hess(*midb);
    b_minus_midb = *(eps_inflation_box<I,F>(b)) - (*midb);
    b_minus_midb_trans = b_minus_midb.transpose();
  }
  
  if (control_Krawczyk_Precision){
  //check if actual precision is not enough
    prec_criterion = jmidb_inv * (*fmidb);
    Matrix<I> p_minus_prec_criterion = *midb - prec_criterion;
    struct intersection_result<I> Ires = overlaps<I>( p_minus_prec_criterion, b.get_m() );
    if ( Min_absolute_diameter<I,F>(prec_criterion) > Min_absolute_diameter<I,F>(b.get_m()) 
        and Ires._overlaps ) {
        res = KRAWCZYK_NOT_ENOUGH_PRECISION;
        if (verbosity == DEBUGING_MODE) {
	  cout << "krawczick precision criterion reached" << endl;
	  cout << "p_minus_prec_criterion: " << p_minus_prec_criterion << endl;
          cout << "prec_criterion: " << prec_criterion << endl;
	  cout << "Min_absolute_diameter(prec_criterion): " << Min_absolute_diameter<I,F>(prec_criterion) << endl;
	  cout << "b.get_m(): " << b.get_m() << endl;
          cout << "Min_absolute_diameter(b.get_m()): " << Min_absolute_diameter<I,F>(b.get_m()) << endl;
	  cout << "overlaps<I>( p_minus_prec_criterion, b.get_m() ): " << Ires._overlaps << endl;
	}
    }
  } 
  
//   Matrix<I> j_infb = Jaco(*infb);
  I un;
  un = 1;
  Matrix<I> Id(jmidb->getNbLines(), jmidb->getNbColumns(), un);
  Matrix<I> K = (*midb) - jmidb_inv*(*fmidb);
  Matrix<I> J = (Id - jmidb_inv*(*jinfb) ) * b_minus_midb;
  *resK = K+J;
  
  if ( i>0 ) {
    delete midb;
    delete fmidb;
    delete jmidb;
//     delete hinfb;
  }
  if (verbosity == DEBUGING_MODE)
    cout << "resK: " << *resK << endl;
  return res;
}

template <class I, class F>
int krawczyk_order_1( Matrix<I> b, 
		      Matrix<I> (*Func) (Matrix<I>), 
		      Matrix<I> (*Jaco) (Matrix<I>), 
		      Matrix<I> *resK,
		      bool control_Krawczyk_Precision,
// 		      int verbosity = QUIET){
                      int verbosity){
  Matrix<I> prec_criterion;
  int res = KRAWCZYK_OK;
  
  Matrix<I> midb = centerInterval<I> (b);
  Matrix<I> jmidb = Jaco(midb);
  Matrix<I> jmidb_inv;
  bool is_inversible = inverse ( jmidb, &jmidb_inv );
  
  int i = 0;
  while ( (not is_inversible) and (i < MAX_TRIALS_TO_INVERSE) ) {
    midb = centerInterval_Disturbed<I,F> ( b, (F) COEFF_PERTURBATION );
    jmidb = Jaco(midb);
    is_inversible = inverse ( jmidb, &jmidb_inv );
    i++;   
  }
  if (not is_inversible) {
    if (verbosity == DEBUGING_MODE) {
	cout << "krawczick jacobian non inversible after " << i << " trials "  << endl;
    }
    return KRAWCZYK_JACMIDM_NOT_INVERSIBLE;
  }
  
  Matrix<I> fmidb = Func(midb);
  Matrix<I> b_minus_midb = b - midb;
  Matrix<I> b_minus_midb_trans = b_minus_midb.transpose();
  
  if (control_Krawczyk_Precision){
  //check if actual precision is not enough
    prec_criterion = jmidb_inv * (fmidb);
    Matrix<I> p_minus_prec_criterion = midb - prec_criterion;
    struct intersection_result<I> Ires = overlaps<I>( p_minus_prec_criterion, b );
    if ( Min_absolute_diameter<I,F>(prec_criterion) > Min_absolute_diameter<I,F>(b) 
        and Ires._overlaps ) {
        res = KRAWCZYK_NOT_ENOUGH_PRECISION;
        if (verbosity == DEBUGING_MODE) {
	  cout << "krawczick precision criterion reached" << endl;
	  cout << "p_minus_prec_criterion: " << p_minus_prec_criterion << endl;
          cout << "prec_criterion: " << prec_criterion << endl;
	  cout << "Min_absolute_diameter(prec_criterion): " << Min_absolute_diameter<I,F>(prec_criterion) << endl;
	  cout << "b: " << b << endl;
          cout << "Min_absolute_diameter(b): " << Min_absolute_diameter<I,F>(b) << endl;
	  cout << "overlaps<I>( p_minus_prec_criterion, b ): " << Ires._overlaps << endl;
	}
    }
  }
  
  Matrix<I> jacb = Jaco(b);
  I un;
  un = 1;
  Matrix<I> Id(jmidb.getNbLines(), jmidb.getNbColumns(), un);
  Matrix<I> K = midb - jmidb_inv*fmidb;
  Matrix<I> J = (Id - jmidb_inv*jacb ) * b_minus_midb;
  *resK = K+J;
  
  if (verbosity == DEBUGING_MODE)
    cout << "resK: " << *resK << endl;
  return res;
}

///DEPRECATED
// template <class I, class F>
// int test_krawczyk_order_2 ( Matrix<I> box, 
// 			    Matrix<I> (*func) (Matrix<I>), 
// 			    Matrix<I> (*jac) (Matrix<I>), 
// 			    Matrix<I> (*hess) (Matrix<I>), 
// 			    int verbosity = QUIET);

// template <class I, class F>
// int are_the_same_solutions ( Matrix<I> sol1, Matrix<I> sol2, 
// 			      Matrix<I> (*Func) (Matrix<I>), 
// 		              Matrix<I> (*Jaco) (Matrix<I>), 
// 		              Matrix<I> (*Hess) (Matrix<I>),
// 		              int verbosity = QUIET);

// template <class I, class F>
// Matrix<I> separateSolutions ( Matrix<I> sol1, Matrix<I> sol2, 
// 			      Matrix<I> (*Func) (Matrix<I>), 
// 		              Matrix<I> (*Jaco) (Matrix<I>), 
// 		              Matrix<I> (*Hess) (Matrix<I>),
// 		              int verbosity = QUIET);

// template <class I, class F>
// int are_the_same_solutions ( Matrix<I> sol1, Matrix<I> sol2, Matrix<I> (*Func) (Matrix<I>), 
// 		                                     Matrix<I> (*Jaco) (Matrix<I>), 
// 		                                     Matrix<I> (*Hess) (Matrix<I>),
// 		                                     int verbosity = QUIET) {
// #define NOT_THE_SAME 0
// #define THE_SAME 1  
// #define CAN_NOT_DECIDE 2
//   
//   struct intersection_result<I> restemp = overlaps<I>( sol1, sol2 );
//   if (not restemp._overlaps) 
//     return NOT_THE_SAME;
//   else { //sol1 overlaps sol2
//     int res1, res2;
//     res1 = test_krawczyk_order_2<I,F>(sol1, Func, Jaco, Hess, verbosity);
//     res2 = test_krawczyk_order_2<I,F>(sol2, Func, Jaco, Hess, verbosity);
//     if (res1==1 or res2==1) 
//       return NOT_THE_SAME;
//     if (res1==0 and res2==0) 
//       return THE_SAME;
//     return CAN_NOT_DECIDE;
//   }
// }

// template <class I, class F>
// Matrix<I> separateSolutions ( Matrix<I> sol1, Matrix<I> sol2, Matrix<I> (*Func) (Matrix<I>), 
// 		                                     Matrix<I> (*Jaco) (Matrix<I>), 
// 		                                     Matrix<I> (*Hess) (Matrix<I>),
// 		                                     int verbosity = QUIET) {
//   Matrix<I> res = sol2;
//   struct intersection_result<I> restemp = overlaps<I>( sol1, res );
//   while (restemp._overlaps) {
//     if (test_krawczyk_order_2<I,F>(restemp._intersection, Func, Jaco, Hess, verbosity) == 2)
//       return Matrix<I>(0,0);
//     else {
//       res = krawczyk_order_2<I,F>(res, Func, Jaco, Hess, verbosity);
//       restemp = overlaps<I>( sol1, res );
//     }
//   }
//   
//   return res;
// }

///DEPRECATED

// template arguments: I: interval F: floating point
// template <class I, class F>
// Matrix<I> krawczyk ( Matrix<I> box, Matrix<I> (*func) (Matrix<I>), Matrix<I> (*jac) (Matrix<I>) , int verbosity = QUIET) {
//   Matrix<I> res(0,0);
//   Matrix<I> point = centerInterval<I> (box);
//   Matrix<I> jac_point = jac(point);
//   Matrix<I> jac_point_inv;
// //   cout << "inversion: " << endl;
//   bool is_inversible = inverse ( jac_point, &jac_point_inv );
//   int i = 0;
//   while ( (not is_inversible) and (i < MAX_TRIALS_TO_INVERSE) ) {
//     F diam = (Min_absolute_diameter<I,F>(box))/((double) 100);
//     for (int j = 0; j < point.getNbLines(); j++)
//       point(j,0) = point(j,0) + diam;
//     jac_point = jac(point);
//     is_inversible = inverse ( jac_point, &jac_point_inv );
//     i++;  
//   }
//   if (not is_inversible)
//     return res;
//   
//   if (verbosity == DEBUGING_MODE) {
//     cout << "jac_box: " << endl << jac(box) << endl;
//     cout << "jac_point: " << endl << jac_point << endl;
//     cout << "jac_point_inv: " << endl << jac_point_inv << endl;
//   }
//   Matrix<I> func_point = func(point);
//   I un;
//   un = 1;
//   Matrix<I> Id(jac_point.getNbLines(), jac_point.getNbColumns(), un);
//   Matrix<I> K = point - jac_point_inv*func_point;
//   Matrix<I> J = (Id - jac_point_inv*jac(box) ) * ( box - point );
//   res = K+J;
//   
//   return res;
// }

#endif 
