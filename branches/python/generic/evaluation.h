//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////


#ifndef _EVALUATION_H
#define _EVALUATION_H

#include "../base/base.h"
#include "matrix.h"
#include "operation.h"
#include "box.h"

// #define QUIET 0 //no output
// #define STATS 1 //just stats at the end of the process
// #define DEBUGING_MODE 3 //very verbose

// #include "../mpfi/mpfixx.hpp"
// #include "../mpfi/mpfrxx.hpp"
// #include "../mpfi/mpfimpfr.hpp"

using namespace matrix;

template <class I,class F>
Matrix<I> evaluate_order_0 ( Box<I,F> &b, Matrix<I> (*func) (Matrix<I>), int verbosity = QUIET ) {
  if (b.get_ford0()) 
    return *b.get_ford0();
  
  Matrix<I> *res = new Matrix<I>( func( *(eps_inflation_box<I,F>(b)) ) );
  b.set_ford0( res );
  
  return *res;
}

template <class I,class F>
Matrix<I> evaluate_order_1 ( Box<I,F> &b, Matrix<I> (*func) (Matrix<I>), Matrix<I> (*Jaco) (Matrix<I>), int verbosity = QUIET ) {
  if (b.get_ford1()) 
    return *b.get_ford1();
  
  Matrix<I> *res = new Matrix<I>;
  
  Matrix<I> *midb  = centerInterval<I>(b);
  Matrix<I> b_minus_midb = *(eps_inflation_box<I,F>(b)) - (*midb);
  Matrix<I> *fmidb = evaluate_fmid<I,F> (b, func);
  Matrix<I> *jinfb = evaluate_jinf<I,F> (b, Jaco);
  
  *res = *fmidb + (*jinfb)*b_minus_midb;
  b.set_ford1( res );
  
  return *res;
  
}

template <class I,class F>
Matrix<I> evaluate_order_2 ( Box<I,F> &b, Matrix<I> (*Func) (Matrix<I>), Matrix<I> (*Jaco) (Matrix<I>), Matrix<I> (*Hess) (Matrix<I>) , int verbosity = QUIET) {
  
  if (b.get_ford2()) 
    return *b.get_ford2();
  Matrix<I> *res = new Matrix<I>;
  
  Matrix<I> *midb = centerInterval<I>(b);
  Matrix<I> b_minus_midb = *(eps_inflation_box<I,F>(b)) - (*midb);
  Matrix<I> b_minus_midb_trans = b_minus_midb.transpose();
  Matrix<I> *fmidb = evaluate_fmid<I,F> (b, Func);
  Matrix<I> *jmidb = evaluate_jmid<I,F> (b, Jaco);
  Matrix<I> *hinfb = evaluate_hinf<I,F> (b, Hess);
  
  int nbvars = b.getDim();
  int nbfunc = fmidb->getNbLines();
  Matrix<I> hessian_vec(nbfunc,1);
  F half = 0.5;
  for (int i = 0; i < nbfunc; i++ ) {
    hessian_vec(i,0) = ((b_minus_midb_trans * (hinfb->subMatFrom(0,nbvars, i*nbvars, nbvars) * b_minus_midb))(0,0))*(half);
  }
  
  *res = *fmidb + (*jmidb)*(b_minus_midb) + hessian_vec;
  
//   if (verbosity == DEBUGING_MODE) {
//       cout << """"""""""""""""""" evaluate_order_2 """"""""""""""""""" << endl;
//       cout << "hessian_vec: " << hessian_vec << endl;
//       cout << "(*jmidb)*(b_minus_midb): " << (*jmidb)*(b_minus_midb) << endl;
//       cout << "final result: " << *res << endl;
//   }
  
  b.set_ford2( res );
  return *res;
  
}

///DEPRECATED:
// template arguments: I: interval F: floating point
template <class I, class F>
Matrix<I> evaluate_order_0 ( Matrix<I> m, Matrix<I> (*func) (Matrix<I>), int verbosity = QUIET ) {
  return func(m);
}

template <class I, class F>
Matrix<I> evaluate_order_1 ( Matrix<I> m, Matrix<I> (*func) (Matrix<I>), Matrix<I> (*Jac) (Matrix<I>), int verbosity = QUIET ) {
  Matrix<I> midm = centerInterval<I>(m);
  return func(midm) + Jac(m)*(m-midm);
}

template <class I,class F>
Matrix<I> evaluate_order_2 ( Matrix<I> m, Matrix<I> (*Func) (Matrix<I>), Matrix<I> (*Jac) (Matrix<I>), Matrix<I> (*Hess) (Matrix<I>) , int verbosity = QUIET) {
  
  Matrix<I> midm = centerInterval<I>(m);
  Matrix<I> m_minus_midm = m-midm;
  Matrix<I> m_minus_midm_trans = m_minus_midm.transpose();
  
  Matrix<I> func_midm = Func(midm);
  Matrix<I> jac_midm = Jac(midm);
  Matrix<I> hessians = Hess(m);
  
  int nbvars = m.getNbLines();
  int nbfunc = func_midm.getNbLines();
  Matrix<I> hessian_vec(nbfunc,1);
  F half = 0.5;
  for (int i = 0; i < nbfunc; i++ ) {
    hessian_vec(i,0) = ((m_minus_midm_trans * (hessians.subMatFrom(0,nbvars, i*nbvars, nbvars) * m_minus_midm))(0,0))*(half);
  }
//   return Func(midm) + Jac(midm)*(m_minus_midm) + hessian_vec;
  return func_midm + jac_midm*(m_minus_midm) + hessian_vec;
}

template <class I,class F>
Matrix<I> evaluate_order_2_saving_args ( Matrix<I> m, 
					 Matrix<I> (*Func) (Matrix<I>), 
					 Matrix<I> (*Jac) (Matrix<I>), 
					 Matrix<I> (*Hess) (Matrix<I>) ,
					 Matrix<I> *fmidm,
					 Matrix<I> *jacmidm,
					 Matrix<I> *hessm,
					 int verbosity = QUIET) {
  Matrix<I> res(0,0);
  if ( (!fmidm) or (!jacmidm) or (!hessm) ) return res;
  
  Matrix<I> midm = centerInterval(m);
  Matrix<I> m_minus_midm = m-midm;
  Matrix<I> m_minus_midm_trans = m_minus_midm.transpose();
  
  *fmidm = Func(midm);
  *jacmidm = Jac(midm);
  *hessm = Hess(m);
  
  if (verbosity == DEBUGING_MODE) {
    
//     (*fmidm)(0,0) = I(-200522891039484267094.402595520019531, -200522891039484267094.373420715332031);
//     (*fmidm)(1,0) = I(-497314287386234901381.223175048828125, -497314287386234901381.187469482421875);
//     
//     (*jacmidm)(0,0) = I(-53539133761777093313646776714.0036926, -53539133761777093313646776713.7136536);
//     (*jacmidm)(0,1) = I(-39202804168866069564717789195.9895020, -39202804168866069564717789195.6462402);
//     (*jacmidm)(1,0) = I(-132781544431274439772397407485.985611, -132781544431274439772397407485.667023);
//     (*jacmidm)(1,1) = I(-97227597093326031669291768679.6375732, -97227597093326031669291768679.2611694);
    
    
//     cout << "midm: " << endl << midm << endl;
//     cout << "fmidm: " << endl << *fmidm << endl;
//     cout << "jacmidm: " << endl << *jacmidm << endl;
//     cout << "hessm: " << endl << *hessm << endl;
  }
  
  int nbvars = m.getNbLines();
  int nbfunc = fmidm->getNbLines();
  Matrix<I> hessian_vec(nbfunc,1);
  F half = 0.5;
  for (int i = 0; i < nbfunc; i++ ) {
    hessian_vec(i,0) = ((m_minus_midm_trans * (hessm->subMatFrom(0,nbvars, i*nbvars, nbvars) * m_minus_midm))(0,0))*(half);
  }
  return (*fmidm) + (*jacmidm)*(m_minus_midm) + hessian_vec;
}

#endif