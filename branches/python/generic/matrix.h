//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

/* originally written by Pascal Mathis, modified by Rémi Imbach */

#ifndef _MATRIX_H
#define _MATRIX_H

#include "../base/base.h"

namespace matrix {

template <class T> class Matrix;

// additional functions (not in class)

template<class T> bool inverseLowerTriangular(const Matrix<T>& A, Matrix<T> *B); 
template<class T> bool inverseUpperTriangular(const Matrix<T>& A, Matrix<T> *B); 
template<class T> bool inverse(const Matrix<T>& A, Matrix<T> *B); 

// template<typename T>
bool mycmp(double a, double b, double eps) {
	double tmp = a-b;
	if (tmp < 0) { tmp = -tmp; }
	return (tmp < eps);
}

#define EPS_TEST_NULL 1.e-10

// template<typename T>
bool iszero(double x) { return mycmp(x, 0., EPS_TEST_NULL); }



// must be define before class matrix
template <class T>
std::ostream &operator<<(std::ostream &out, const Matrix<T> &m) {
	out << std::endl;
	if (!m._mat) { out << "nul matrix" << std::endl; }
 	for (int i=0;i<m.getNbLines();i++) {
	  for(int j=0;j<m.getNbColumns();j++) {
		out << setiosflags(std::ios_base::right) << std::setw(8) << std::setprecision(10) << m.get(i,j) << " "; 
	  }
	  	out << std::endl;
        }
	
  	return out ; //<< ex._nom << " " << ex._valeur; 
}

///////////////////////////////
//        Matrix class

template<typename T>
class Matrix {
 public :	
	Matrix();
	Matrix(int nb_lines, int nb_columns);
 	Matrix(const Matrix &);
	Matrix(int nb_lines, int nb_columns, T tab[]);
	Matrix(int nb_lines, int nb_columns, T val); // = T*identity
 	void operator=(const Matrix &);

	~Matrix();

	int getNbLines() const { return _nbl; }
	int getNbColumns() const { return _nbc; }
	T operator()(int i, int j) const { 
		if (i < 0 || i >= _nbl || j < 0 || j >= _nbc) { throw ; }
		return _mat[i*_nbc + j]; 
	}
	T &operator()(int i, int j) { 
		if (i < 0 || i >= _nbl || j < 0 || j >= _nbc) { throw; }
		return _mat[i*_nbc + j]; 
	}

	void swapLines(int, int);
	void swapColumns(int c1, int c2);
	Matrix<T> swapL(int, int);
	Matrix<T> swapC(int, int);
	Matrix<T> delL(int);
	Matrix<T> delC(int);
	Matrix<T> subMat(int, int);
	Matrix<T> subMatFrom(int, int, int, int);//line index of res(0,0), number of lines, column index of res(0,0), number of columns
	Matrix<T> line(int); //get a line
	Matrix<T> column(int); //get a column
	Matrix<T> transpose();
	void zeros(int, int);
	void zeros();
	T norm();//assuming the caller knows what he is doeing: 0 if empty matrix... just an exemple
	T det();
	Matrix<T> augment_cols( Matrix<T> cols); //append columns
	Matrix<T> augment_rows( Matrix<T> rows); //append rows

	Matrix<T> *decompLU();		// if success (return not NULL), current matrix is changed and hold LU matrix
	Matrix<T> getUpperTriangle();	// with diagonal
	Matrix<T> getLowerTriangle();	// with 1 on diagonal

	Matrix<T> mult(const Matrix<T> &);
	Matrix<T> operator*(const Matrix<T> &A) {return mult(A);}
	Matrix<T> operator*(const T &) const;
	Matrix<T> operator-(const Matrix<T> &) const;
	
	Matrix<T> operator+(const Matrix<T> &) const;
	Matrix<T> operator-() const;

	friend std::ostream& operator<< <T>(std::ostream& out, const Matrix<T>& x);
//         friend bool inverse55<T>(const Matrix<T>& A, Matrix<T> *B);

	/** deprecated */
	T get(int i, int j) const { return _mat[i*_nbc + j]; }
	void set(int i, int j, T x) { 
		if (i < 0 || i >= _nbl || j < 0 || j >= _nbc) { return; }
		_mat[i*_nbc + j] = x; 
	}

 protected :
	T *_mat;
	int _nbl, _nbc;
};

///////////////////////////////
//        Methods

template<typename T>
Matrix<T>::Matrix() : _mat(NULL), _nbl(0), _nbc(0) {  }

template<typename T>
Matrix<T>::Matrix(int nb_lines, int nb_columns) : _nbl(nb_lines), _nbc(nb_columns) {
	_mat = new T[_nbl*_nbc];
}

template<typename T>
Matrix<T>::~Matrix() { 
  if (_mat) delete[] _mat;
}

template<typename T>
Matrix<T>::Matrix(int nb_lines, int nb_columns, T tab[]) {
  _nbl = nb_lines;
  _nbc = nb_columns;
  if (_nbl*_nbc > 0) _mat = new T[_nbl*_nbc];
  else { _mat = NULL; }
  for (int i=0;i<_nbl;i++) 
    for(int j=0;j<_nbc;j++) 
      set(i,j, tab[i*_nbc + j]); 
}

template<typename T>
Matrix<T>::Matrix(int nb_lines, int nb_columns, T val) {
  _nbl = nb_lines;
  _nbc = nb_columns;
  if (_nbl*_nbc > 0) _mat = new T[_nbl*_nbc];
  else { _mat = NULL; }
  T zero;
  zero = 0;
  for (int i=0;i<_nbl;i++) 
    for(int j=0;j<_nbc;j++)
      if (i==j) 
        set(i,j, val);
      else 
	set(i,j, zero);
}

template<typename T>
Matrix<T>::Matrix(const Matrix &m) {
	_nbl = m.getNbLines();
	_nbc = m.getNbColumns();
	if (_nbl*_nbc > 0) _mat = new T[_nbl*_nbc];
	else { _mat = NULL; }

	for (int i=0;i<_nbl;i++) 
	  for(int j=0;j<_nbc;j++) 
		set(i,j, m.get(i,j)); 
}

template<typename T>
void Matrix<T>::swapLines(int l1, int l2) {
	int i;
	T tmp;

	if (l1 >= getNbLines() || l2 >= getNbLines()) {
		return;
	} 

	for(i=0; i < getNbColumns(); i++) {
		tmp = get(l1, i);
		set(l1, i, get(l2, i));
		set(l2, i, tmp);	
	}	
}

template<typename T>
void Matrix<T>::swapColumns(int c1, int c2) {
	int i;
	T tmp;

	if (c1 >= getNbColumns() || c2 >= getNbColumns()) {
		return;
	} 

	for(i=0; i < getNbLines(); i++) {
		tmp = get(i, c1);
		set(i, c1, get(i, c2));
		set(i, c2, tmp);	
	}	
}

template<typename T>
Matrix<T> Matrix<T>::swapL(int l1, int l2){
  if (l1 >= getNbLines() || l2 >= getNbLines()) {
		return Matrix<T> (0,0);
  } 
  Matrix<T> result=*this;
  for(int i=0; i < getNbColumns(); i++) {
		result.set(l1, i, get(l2, i));
		result.set(l2, i, get(l1, i));	
  }
  return result;
}

template<typename T>
Matrix<T> Matrix<T>::swapC(int c1, int c2){
  if (c1 >= getNbColumns() || c2 >= getNbColumns()) {
		return Matrix<T> (0,0);
  } 
  Matrix<T> result=*this;
  for(int i=0; i < getNbLines(); i++) {
		result.set(i,c1, get(i, c2));
		result.set(i,c2, get(i, c1));	
  }
  return result;
}
template<typename T>
Matrix<T> Matrix<T>::delL(int l){
  if (l >= getNbLines()) {
        Matrix<T> result=*this;
	return result;
  }
  Matrix<T> result(getNbLines()-1,getNbColumns());
  for(int i=0;i<result.getNbLines();i++)
    for (int j=0;j<result.getNbColumns();j++)
      if (i<l) result.set(i,j,get(i,j));
      else result.set(i,j,get(i+1,j));
  return result;
}
template<typename T>
Matrix<T> Matrix<T>::delC(int c){
  if (c >= getNbColumns()) {
	Matrix<T> result=*this;
	return result;
  }
  Matrix<T> result(getNbLines(),getNbColumns()-1);
  for(int i=0;i<result.getNbLines();i++)
    for (int j=0;j<result.getNbColumns();j++)
      if (j<c) result.set(i,j,get(i,j));
      else result.set(i,j,get(i,j+1));
  return result;
}

template<typename T>
void Matrix<T>::zeros(){
  T val;
  val = 0;
  for(int i=0;i<getNbLines();i++)
    for (int j=0;j<getNbColumns();j++)
      set(i,j,val);
}

template<typename T>
Matrix<T> Matrix<T>::subMat(int l, int c){
  if (l>=getNbLines()) l=getNbLines();
  if (c>=getNbColumns()) c=getNbColumns();
   Matrix<T> result(l,c);
   for(int i=0;i<l;i++)
    for (int j=0;j<c;j++)
      result.set(i,j,get(i,j));
   return result;
}

template<typename T> //line index of res(0,0), number of lines, column index of res(0,0), number of columns
Matrix<T> Matrix<T>::subMatFrom(int lb, int nbl, int cb, int nbc){
  if (lb+nbl>getNbLines()) nbl = 0;
  if (cb+nbc>getNbColumns()) nbc = 0;
   Matrix<T> result(nbl,nbc);
   for(int i=0;i<nbl;i++)
    for (int j=0;j<nbc;j++)
      result.set(i,j, get(i+lb,j+cb) );
   return result;
}

template<typename T>
Matrix<T> Matrix<T>::augment_cols( Matrix<T> cols){ //append columns
  if ( (_nbl==0) or (_nbc==0) 
      or (cols._nbl==0) or (cols._nbc==0) 
      or (not (_nbl==cols._nbl)) )
    return Matrix<T>();
  Matrix<T> result(_nbl, _nbc + cols._nbc );
  for(int i=0;i<_nbl;i++) {
    for (int j=0;j<_nbc;j++)
      result.set(i,j, get(i,j) );
    for (int j=0;j<cols._nbc;j++)
      result.set(i, j+_nbc , cols(i,j) );
  }
  return result;
}
template<typename T>
Matrix<T> Matrix<T>::augment_rows( Matrix<T> rows){ //append rows
  if ( (_nbl==0) or (_nbc==0) 
      or (rows._nbl==0) or (rows._nbc==0) 
      or (not (_nbc==rows._nbc)) )
    return Matrix<T>();
  Matrix<T> result(_nbl+ rows._nbl, _nbc);
  for(int i=0;i<_nbl;i++)
    for (int j=0;j<_nbc;j++)
      result.set(i,j, get(i,j) );
  for(int i=0;i<rows._nbl;i++)
    for (int j=0;j<_nbc;j++)
      result.set(i+_nbl, j , rows(i,j) );
  return result;
}

template<typename T>
Matrix<T> Matrix<T>::line(int lineIndex){
  return subMatFrom(lineIndex, 1, 0, getNbColumns() );
}

template<typename T>
Matrix<T> Matrix<T>::column(int columnIndex){
  return subMatFrom(0, getNbLines(), columnIndex, 1 );
}

template<typename T>
Matrix<T> Matrix<T>::transpose(){
  Matrix<T> result(_nbc,_nbl);
  for(int i=0;i<_nbl;i++)
    for (int j=0;j<_nbc;j++)
      result.set(j,i,get(i,j));
   return result;
}

template<typename T> //assuming the caller knows what he is doeing: 0 if empty matrix... just an exemple
T Matrix<T>::norm(){
  T res;
  res=0;
  for(int i=0;i<_nbl;i++)
    for (int j=0;j<_nbc;j++)
      res = res + get(i,j)*get(i,j);
  return sqrt(res);
}
///"rapide" si la matrice est triangulaire supérieure
///précondition: la matrice est carrée
template<typename T> T det22(T *);
template<typename T> T det33(T *);
template<typename T> T det44(T *);
template<typename T> T det55(T *);

template<typename T>
T Matrix<T>::det(){
  if (getNbLines()==1) return get(0,0);
  if (getNbLines()==2) return det22(_mat);
  if (getNbLines()==3) return det33(_mat);
  if (getNbLines()==4) return det44(_mat);
  if (getNbLines()==5) return det55(_mat);
  T temp;
  temp=0;
  //cette version ne colle pas aux intervalles...
//   for (int i=0;i<getNbLines();i++) temp = (get(i,0)==0?temp : temp + pow(-1,i)*get(i,0)*(((delL(i)).delC(0)).det()));
  for (int i=0;i<getNbLines();i++) temp = temp + pow(-1,i)*get(i,0)*(((delL(i)).delC(0)).det());
  return temp;
}

template<typename T>
void Matrix<T>::operator=(const Matrix &m){
	if (_mat) delete[] _mat;
	_nbl = m.getNbLines();
	_nbc = m.getNbColumns();

	if (_nbl*_nbc > 0) _mat = new T[_nbl*_nbc];
	else { _mat = NULL; }

	for (int i=0;i<_nbl;i++) 
	  for(int j=0;j<_nbc;j++) 
		set(i,j, m.get(i,j)); 

}


/*------------------------------------------------------------------------------
* DESCRIPTION : LU Decomposition LU (stored in mat) of matrix mat
 ------------------------------------------------------------------------------*/
// return permutation matrix (NULL if failed)

template<typename T>
Matrix<T> *Matrix<T>::decompLU()
 {
  int i,j,k;
  T x;
  bool ok = true;
  Matrix<T> mtmp(_nbl, _nbc);
  Matrix<T> *P = new Matrix<T>(_nbl, _nbc);
  mtmp = *this; // copy of current matrix
  // initialization of Permutation matrix (identity)
  T zero, un;
  zero = 0.;
  un = 1.;
  for(i=0; i < _nbl; i++) {
	for(j=0; j < _nbc; j++) {
// 		P->set(i, j, 0.);
		P->set(i, j, zero);
	}
//       	P->set(i, i, 1.);
	P->set(i, i, un);
  }
  // decomp LU

  for (j=0;j< getNbLines()-1 && ok;j++) {
	if (iszero(get(j,j))) {
		for(k=j+1; k < getNbLines() && iszero(get(k, j)); k++);
		if (k == getNbLines()) { ok = false; } // singular matrix
		else { swapLines(j, k); P->swapLines(j, k); }
	}
			
 	for (i=j+1;i< getNbLines() && ok;i++) {
 		set(i, j, get(i,j)/get(j,j)); // Construction de Ek
 		for (k=j+1;k<getNbLines() && ok;k++) {// Construction de Ak
			x = get(i,k);
 			set(i, k, x -get(i,j)*get(j,k));
 		}
	}
  }
  if (ok == false) { *this = mtmp; delete P; P=NULL; }
  return P;
 }

// precondition : square matrix
template<typename T>
Matrix<T> Matrix<T>::getUpperTriangle() {
	Matrix<T> U(getNbLines(), getNbColumns());
	int i,j;
        T zero;
	zero = 0.;
	for(i=0; i < getNbLines(); i++) {
	  for(j=0; j < getNbColumns(); j++) {
// 		if (j < i) { U.set(i, j, 0.); }
		if (j < i) { U.set(i, j, zero); }
		else { U.set(i, j, get(i, j)); }
	  }
	}
	return U;
}	

// precondition : square matrix
template<typename T>
Matrix<T> Matrix<T>::getLowerTriangle() {
	Matrix<T> L(getNbLines(), getNbColumns());
	int i,j;
        T zero, un;
	zero = 0.;
	un = 1.;
	for(i=0; i < getNbLines(); i++) {
	  for(j=0; j < getNbColumns(); j++) {
// 		if (j == i) { L.set(i, j, 1.); continue; }
		if (j == i) { L.set(i, j, un); continue; }
// 		if (j > i) { L.set(i, j, 0.); continue; }
		if (j > i) { L.set(i, j, zero); continue; }
		// else
		L.set(i, j, get(i, j));
	  }
	}
	return L;
}

/*------------------------------------------------------------------------------
* DESCRIPTION : Matrix addition
 ------------------------------------------------------------------------------*/
template<class T>
Matrix<T> Matrix<T>::operator+(const Matrix<T> &B) const {
	int i,j;
	T v;

	if (getNbLines() != B.getNbLines() || getNbColumns() != B.getNbColumns()) { return Matrix<T>(0,0); }

	Matrix<T> C(getNbLines(), getNbColumns());


	for(i=0; i<getNbLines(); i++) {
	  for(j=0; j<getNbColumns(); j++) {
		v = get(i, j) + B.get(i, j);
		C.set(i, j, v);
	  }
	}
	return C;
}
	
/*------------------------------------------------------------------------------
* DESCRIPTION : Matrix substraction
 ------------------------------------------------------------------------------*/

template<class T>
Matrix<T> Matrix<T>::operator-(const Matrix<T> &B) const {
	int i,j;
	T v;

	if (getNbLines() != B.getNbLines() || getNbColumns() != B.getNbColumns()) { return Matrix<T>(0,0); }

	Matrix<T> C(getNbLines(), getNbColumns());


	for(i=0; i<getNbLines(); i++) {
	  for(j=0; j<getNbColumns(); j++) {
		v = get(i, j) - B.get(i, j);
		C.set(i, j, v);
	  }
	}
	return C;			
}

/*------------------------------------------------------------------------------
* DESCRIPTION : Matrix negation
 ------------------------------------------------------------------------------*/
template<class T>
Matrix<T> Matrix<T>::operator-() const {
	int i,j;
	T v;

	Matrix<T> C(getNbLines(), getNbColumns());


	for(i=0; i<getNbLines(); i++) {
	  for(j=0; j<getNbColumns(); j++) {
		v = -get(i, j);
		C.set(i, j, v);
	  }
	}
	return C;
}

/*------------------------------------------------------------------------------
* DESCRIPTION : Matrix multiplication
 ------------------------------------------------------------------------------*/

template<class T>
Matrix<T> Matrix<T>::mult(const Matrix<T> &B) {
	int i,j,k;
	T v;

	if (getNbColumns() != B.getNbLines()) { return Matrix<T>(0,0); }

	Matrix<T> C(getNbLines(), B.getNbColumns());


	for(i=0; i<getNbLines(); i++) {
	  for(j=0; j<B.getNbColumns(); j++) {
		v = 0.;
		for(k=0;k<getNbColumns();k++) {
			v += get(i, k)*B.get(k, j);
// 			v = v + get(i, k)*B.get(k, j);
		}
		C.set(i, j, v);
	  }
	}
	return C;			
}

template<class T> 
Matrix<T> Matrix<T>::operator*(const T &f) const {
	Matrix<T> R(getNbLines(), getNbColumns());
//	cout << "val : " << f << endl;
	for(int i=0; i < getNbLines(); i++) {
	  for(int j=0; j < getNbColumns(); j++) {
		R.set(i, j, get(i,j)*f);
	  }
	}

	return R;
}

// result in B, B was allocated
template<class T> bool inverseLowerTriangular(const Matrix<T>& A, Matrix<T> *B) {
	int i, 	// current column in B
	    j, 	// current line in A
	    k, n;
	T v;

	// if A is not a square matrix
	if ((n=A.getNbLines()) != A.getNbColumns()) { return false; }

	// A and B are same size
	if (A.getNbLines() != B->getNbLines() || A.getNbColumns() != B->getNbColumns()) { return false; }

	// no nul element on diagonal
	for(k=0;k<n;k++) {
	        if (iszero(A.get(k,k))) { return false; }//check if 0 is in A
	}

	for(i=0; i<n; i++) {
	  for(j=0; j<n; j++) {
	  	v = (i == j?1.:0.); // get element of inverse matrix 
		for(k=0; k < j; k++) {
			v -= A.get(j, k) * B->get(k, i);
// 			v = v - A.get(j, k) * B->get(k, i);
		}
		v /= A.get(j, j);
// 		v = v / A.get(j, j);
		B->set(j, i, v);
	  }
	}
	return true;
}

// result in B, B was allocated
template<class T> bool inverseUpperTriangular(const Matrix<T>& A, Matrix<T> *B) {
	int i, 	// current column in B
	    j, 	// current line in A
	    k, n;
	T v;

	// if A is not a square matrix
	if ((n=A.getNbLines()) != A.getNbColumns()) { return false; }

	// A and B are same size
	if (A.getNbLines() != B->getNbLines() || A.getNbColumns() != B->getNbColumns()) { return false; }

	// no nul element on diagonal
	for(k=0;k<n;k++) {
		if (iszero(A.get(k,k))) { return false; }//check if 0 is in A
	}

	for(i=n-1; i>=0; i--) {
	  for(j=n-1; j>=0; j--) {
	  	v = (i == j?1.:0.); // get element of inverse matrix 
		for(k=n-1; k > j; k--) {
			v -= A.get(j, k) * B->get(k, i);
// 			v = v - A.get(j, k) * B->get(k, i);
		}
		v /= A.get(j, j);
// 		v = v / A.get(j, j);
		B->set(j, i, v);
	  }
	}
	return true;
}

// #define DEBUG_INV
// Idea : search B such that A*B = I
// we compute P*A = L*U
// => A = P-1*L*U, we know that (X*Y)-1 = X-1*Y-1
// => B = U-1*P-1*P

template<class T> bool inverse22(const Matrix<T>& A, Matrix<T> *B);
template<class T> bool inverse33(const Matrix<T>& A, Matrix<T> *B);
template<class T> bool inverse44(const Matrix<T>& A, Matrix<T> *B);
template<class T> bool inverse55(const Matrix<T>& A, Matrix<T> *B);

template<class T> bool inverse(const Matrix<T>& A, Matrix<T> *B) {
        if (A.getNbLines() == 2) return inverse22(A, B);
	if (A.getNbLines() == 3) return inverse33(A, B);
	if (A.getNbLines() == 4) return inverse44(A, B);
	Matrix<T> LU = A;
	Matrix<T> *P;	// permutation matrix
// 	cout << "ici" << endl;
#ifdef DEBUG_INV 
	cout << "A :\n" << A << endl; 
#endif

	// compute P, L, U
	if (!(P = LU.decompLU())) { return false; }

#ifdef DEBUG_INV 
 cout << "LU :\n" << LU << endl;
 cout << "P :\n" << *P << endl;
#endif

	// extract L
	Matrix<T> L = LU.getLowerTriangle();

#ifdef DEBUG_INV 
  cout << "L :\n" << L << endl;
#endif
	// compute L-1
	Matrix<T> L_inv(L.getNbLines(), L.getNbColumns());
	if (!inverseLowerTriangular(L, &L_inv)) { return false; }

#ifdef DEBUG_INV 
 cout << "L_inv:\n" << L_inv << endl;
#endif

	// extract U
	Matrix<T> U = LU.getUpperTriangle();


#ifdef DEBUG_INV 
 cout << "U :\n" << U << endl;
 cout << "L *U :\n" << L*U << endl;
#endif

	// compute U-1
	Matrix<T> U_inv(U.getNbLines(), U.getNbColumns());
	if (!inverseUpperTriangular(U, &U_inv)) { return false; }

#ifdef DEBUG_INV 
 cout << "U_inv:\n" << U_inv << endl;
#endif

	*B = (U_inv * L_inv)*(*P);

	delete P;
	return true;
}

// Assuming that B is a column vector
// template <typename T>
// Vector<T> operator-(const Vector<T> &A, const Matrix<T> &B) {
//      Vector<T> v(A.getNbLines());
// 
//      for(int i=0;i<v.getDim(); i++) {
//         v.set(i, A.get(i) - B.get(i, 0));
//      }
//      return v;
// }

template <typename T>
Matrix<T> operator+(const Matrix<T> &A, const Matrix<T> &B) {
     Matrix<T> R(A.getNbLines(), A.getNbColumns());

     for(int i=0;i<A.getNbLines(); i++) {
       for(int j=0;j<A.getNbColumns(); j++) {
	  R.set(i, j, A.get(i,j) + B.get(i,j));
       }
     }
     return R;
}

template<typename T>
T det22(T *tab){
  return tab[0*2 + 0]*tab[1*2 + 1] - tab[0*2 + 1]*tab[1*2 + 0];
}

template<typename T>
T det33(T *tab){
  T a = tab[0*3 + 0]; T b = tab[0*3 + 1];  T c = tab[0*3 + 2];
  T d = tab[1*3 + 0]; T e = tab[1*3 + 1];  T f = tab[1*3 + 2];
  T g = tab[2*3 + 0]; T h = tab[2*3 + 1];  T i = tab[2*3 + 2];
  return a*e*i - a*f*h - b*d*i + b*f*g + c*d*h - c*e*g;
}

template<typename T>
T det44(T *tab){
  T a = tab[0*4 + 0]; T b = tab[0*4 + 1];  T c = tab[0*4 + 2];  T d = tab[0*4 + 3];
  T e = tab[1*4 + 0]; T f = tab[1*4 + 1];  T g = tab[1*4 + 2];  T h = tab[1*4 + 3];
  T i = tab[2*4 + 0]; T j = tab[2*4 + 1];  T k = tab[2*4 + 2];  T l = tab[2*4 + 3];
  T m = tab[3*4 + 0]; T n = tab[3*4 + 1];  T o = tab[3*4 + 2];  T p = tab[3*4 + 3];
  return a*f*k*p - a*f*l*o - a*g*j*p + a*g*l*n + a*h*j*o - a*h*k*n - b*e*k*p + b*e*l*o + b*g*i*p - b*g*l*m - b*h*i*o + b*h*k*m + c*e*j*p - c*e*l*n - c*f*i*p + c*f*l*m + c*h*i*n - c*h*j*m - d*e*j*o + d*e*k*n + d*f*i*o - d*f*k*m - d*g*i*n + d*g*j*m;
}

template<typename T>
T det55(T *tab){
  T a = tab[0*5 + 0]; T b = tab[0*5 + 1];  T c = tab[0*5 + 2];  T d = tab[0*5 + 3]; T e = tab[0*5 + 4];
  T f = tab[1*5 + 0]; T g = tab[1*5 + 1];  T h = tab[1*5 + 2];  T i = tab[1*5 + 3]; T j = tab[1*5 + 4];
  T k = tab[2*5 + 0]; T l = tab[2*5 + 1];  T m = tab[2*5 + 2];  T n = tab[2*5 + 3]; T o = tab[2*5 + 4];
  T p = tab[3*5 + 0]; T q = tab[3*5 + 1];  T r = tab[3*5 + 2];  T s = tab[3*5 + 3]; T t = tab[3*5 + 4];
  T u = tab[4*5 + 0]; T v = tab[4*5 + 1];  T w = tab[4*5 + 2];  T x = tab[4*5 + 3]; T y = tab[4*5 + 4];
  return a*g*m*s*y - a*g*m*t*x - a*g*n*r*y + a*g*n*t*w + a*g*o*r*x - a*g*o*s*w - a*h*l*s*y + a*h*l*t*x + a*h*n*q*y - a*h*n*t*v - a*h*o*q*x
       + a*h*o*s*v + a*i*l*r*y - a*i*l*t*w - a*i*m*q*y + a*i*m*t*v + a*i*o*q*w - a*i*o*r*v - a*j*l*r*x + a*j*l*s*w + a*j*m*q*x
       - a*j*m*s*v - a*j*n*q*w + a*j*n*r*v - b*f*m*s*y + b*f*m*t*x + b*f*n*r*y - b*f*n*t*w - b*f*o*r*x + b*f*o*s*w + b*h*k*s*y
       - b*h*k*t*x - b*h*n*p*y + b*h*n*t*u + b*h*o*p*x - b*h*o*s*u - b*i*k*r*y + b*i*k*t*w + b*i*m*p*y - b*i*m*t*u - b*i*o*p*w
       + b*i*o*r*u + b*j*k*r*x - b*j*k*s*w - b*j*m*p*x + b*j*m*s*u + b*j*n*p*w - b*j*n*r*u + c*f*l*s*y - c*f*l*t*x - c*f*n*q*y
       + c*f*n*t*v + c*f*o*q*x - c*f*o*s*v - c*g*k*s*y + c*g*k*t*x + c*g*n*p*y - c*g*n*t*u - c*g*o*p*x + c*g*o*s*u + c*i*k*q*y
       - c*i*k*t*v - c*i*l*p*y + c*i*l*t*u + c*i*o*p*v - c*i*o*q*u - c*j*k*q*x + c*j*k*s*v + c*j*l*p*x - c*j*l*s*u - c*j*n*p*v
       + c*j*n*q*u - d*f*l*r*y + d*f*l*t*w + d*f*m*q*y - d*f*m*t*v - d*f*o*q*w + d*f*o*r*v + d*g*k*r*y - d*g*k*t*w - d*g*m*p*y
       + d*g*m*t*u + d*g*o*p*w - d*g*o*r*u - d*h*k*q*y + d*h*k*t*v + d*h*l*p*y - d*h*l*t*u - d*h*o*p*v + d*h*o*q*u + d*j*k*q*w
       - d*j*k*r*v - d*j*l*p*w + d*j*l*r*u + d*j*m*p*v - d*j*m*q*u + e*f*l*r*x - e*f*l*s*w - e*f*m*q*x + e*f*m*s*v + e*f*n*q*w
       - e*f*n*r*v - e*g*k*r*x + e*g*k*s*w + e*g*m*p*x - e*g*m*s*u - e*g*n*p*w + e*g*n*r*u + e*h*k*q*x - e*h*k*s*v - e*h*l*p*x
       + e*h*l*s*u + e*h*n*p*v - e*h*n*q*u - e*i*k*q*w + e*i*k*r*v + e*i*l*p*w - e*i*l*r*u - e*i*m*p*v + e*i*m*q*u;
}

template<class T> bool inverse22(const Matrix<T>& A, Matrix<T> *B) {
  
  T a = A(0,0); T b = A(0,1);
  T c = A(1,0); T d = A(1,1);
  T determinant = a*d - b*c;
  if (iszero(determinant)) return 0;
  *B = Matrix<T> (2,2);
  B->set(0,0, d/determinant);
  B->set(0,1,-b/determinant);
  B->set(1,0,-c/determinant);
  B->set(1,1, a/determinant);
  return 1;
}

template<class T> bool inverse33(const Matrix<T>& A, Matrix<T> *B) {
  
  T a = A(0,0); T b = A(0,1);  T c = A(0,2);
  T d = A(1,0); T e = A(1,1);  T f = A(1,2);
  T g = A(2,0); T h = A(2,1);  T i = A(2,2);
  T determinant = a*e*i - a*f*h - b*d*i + b*f*g + c*d*h - c*e*g;
  if (iszero(determinant)) return 0;
  *B = Matrix<T> (3,3);
  B->set(0,0,  (e*i - f*h)/determinant);
  B->set(0,1, -(b*i - c*h)/determinant);
  B->set(0,2,  (b*f - c*e)/determinant);
  B->set(1,0, -(d*i - f*g)/determinant);
  B->set(1,1,  (a*i - c*g)/determinant);
  B->set(1,2, -(a*f - c*d)/determinant);
  B->set(2,0,  (d*h - e*g)/determinant);
  B->set(2,1, -(a*h - b*g)/determinant);
  B->set(2,2,  (a*e - b*d)/determinant);
  return 1;
}

template<class T> bool inverse44(const Matrix<T>& A, Matrix<T> *B) {
  
  T a = A(0,0); T b = A(0,1);  T c = A(0,2);  T d = A(0,3);
  T e = A(1,0); T f = A(1,1);  T g = A(1,2);  T h = A(1,3);
  T i = A(2,0); T j = A(2,1);  T k = A(2,2);  T l = A(2,3);
  T m = A(3,0); T n = A(3,1);  T o = A(3,2);  T p = A(3,3);
  T determinant = a*f*k*p - a*f*l*o - a*g*j*p + a*g*l*n + a*h*j*o - a*h*k*n - b*e*k*p + b*e*l*o + b*g*i*p - b*g*l*m - b*h*i*o + b*h*k*m + c*e*j*p - c*e*l*n - c*f*i*p + c*f*l*m + c*h*i*n - c*h*j*m - d*e*j*o + d*e*k*n + d*f*i*o - d*f*k*m - d*g*i*n + d*g*j*m;
  if (iszero(determinant)) return 0;
  *B = Matrix<T> (4,4);
  B->set(0,0,  (f*k*p - f*l*o - g*j*p + g*l*n + h*j*o - h*k*n)/determinant);
  B->set(0,1, -(b*k*p - b*l*o - c*j*p + c*l*n + d*j*o - d*k*n)/determinant);
  B->set(0,2,  (b*g*p - b*h*o - c*f*p + c*h*n + d*f*o - d*g*n)/determinant);
  B->set(0,3, -(b*g*l - b*h*k - c*f*l + c*h*j + d*f*k - d*g*j)/determinant);
  B->set(1,0, -(e*k*p - e*l*o - g*i*p + g*l*m + h*i*o - h*k*m)/determinant);
  B->set(1,1,  (a*k*p - a*l*o - c*i*p + c*l*m + d*i*o - d*k*m)/determinant);
  B->set(1,2, -(a*g*p - a*h*o - c*e*p + c*h*m + d*e*o - d*g*m)/determinant);
  B->set(1,3,  (a*g*l - a*h*k - c*e*l + c*h*i + d*e*k - d*g*i)/determinant);
  B->set(2,0,  (e*j*p - e*l*n - f*i*p + f*l*m + h*i*n - h*j*m)/determinant);
  B->set(2,1, -(a*j*p - a*l*n - b*i*p + b*l*m + d*i*n - d*j*m)/determinant);
  B->set(2,2,  (a*f*p - a*h*n - b*e*p + b*h*m + d*e*n - d*f*m)/determinant);
  B->set(2,3, -(a*f*l - a*h*j - b*e*l + b*h*i + d*e*j - d*f*i)/determinant);
  B->set(3,0, -(e*j*o - e*k*n - f*i*o + f*k*m + g*i*n - g*j*m)/determinant);
  B->set(3,1,  (a*j*o - a*k*n - b*i*o + b*k*m + c*i*n - c*j*m)/determinant);
  B->set(3,2, -(a*f*o - a*g*n - b*e*o + b*g*m + c*e*n - c*f*m)/determinant);
  B->set(3,3,  (a*f*k - a*g*j - b*e*k + b*g*i + c*e*j - c*f*i)/determinant);
  return 1;
}

}

#endif

