//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////


#ifndef _LIST_H
#define _LIST_H

#include <iostream>

namespace cpp_tools {

template < typename T > class list;

template < typename T >
struct list_elem{
  T _elem;
  struct list_elem * _next;
  struct list_elem * _prev;
};

template < typename T >
std::ostream &operator<< (std::ostream &out, const list<T> &l) { 
  out << "size: " << l._size << ", elements: ";
//   l._traveller = l._begin;
  struct list_elem<T> * traveller = l._begin;
  while (traveller != NULL) {
    out << traveller->_elem << ", ";
    traveller = traveller->_next;
  }
  out << "NULL";
  return out;
}

template < typename T >  
class list {
public:
  
  list();
  
  void clear();
  void push_front( T );
  void push_back( T );
  T front();
  T back();
  T pop_front();
  T pop_back();
  void visit() { _traveller = _begin; } //non destructive access for cython...
  ///TEST
  void visit_back() { _traveller = _end; }
  
  bool end() { return _traveller == NULL; }
  T next() {
    T res = _traveller->_elem;
    _traveller = _traveller->_next;
    return res;
  }
  ///TEST
  T * prev() {
    T * res = &(_traveller->_elem);
    _traveller = _traveller->_prev;
    return res;
  }
  long unsigned int size () { return _size; }
  bool isEmpty() { return (_size==0); }
  ~list();
  
  friend std::ostream& operator<< <T>(std::ostream& out, const list<T>& l);
  
  list<T> & operator= (const list<T> & l) {
	  _begin = l._begin;
	  _end = l._end;
          _size = l._size;
          return *this;
        }
  
private:
  struct list_elem < T > *_begin;
  struct list_elem < T > *_end;
  struct list_elem < T > *_traveller;
  long unsigned int _size;
};

template < typename T >
list<T>::list(){
  _begin = NULL;
  _end = NULL;
  _size = 0;
}

template < typename T >
void list<T>::clear(){
  _traveller = _begin;
  while (_traveller!=NULL) {
    _traveller = _begin->_next;
    delete _begin;
    _begin = _traveller;
  }
  _end = NULL;
  _size = 0;
}

template < typename T >
list<T>::~list(){
//   cout << "delete list" << endl;
  clear();
//   cout << "fin delete list" << endl;
}

template < typename T >
void list<T>::push_front( T elem ) {
  struct list_elem < T > * nle = new list_elem < T >;
  nle->_elem = elem;
  nle->_next = _begin;
  nle->_prev = NULL;
  if (_size!=0) _begin->_prev = nle;
  _begin = nle;
  if (_end == NULL) _end = nle;
  _size = _size + 1;  
}

template < typename T >
void list<T>::push_back( T elem ) {
  if (_size == 0 ) return push_front( elem );
  struct list_elem < T > * nle = new list_elem < T >;
  nle->_elem = elem;
  nle->_next = NULL;
  nle->_prev = _end;
  _end->_next = nle;
  _end = nle;
  if (_begin == NULL) _begin = nle;
  _size = _size + 1;
}

template < typename T >
T list<T>::front() {
  if (_size == 0) return T();
  return _begin->_elem;
}

template < typename T >
T list<T>::back() {
  if (_size == 0) return T();
  return _end->_elem;
}

template < typename T >
T list<T>::pop_front() {
  if (_size==0) return T();
  T res = _begin->_elem;
  if (_size==1) {
    delete _begin;
    _begin = NULL;
    _end = NULL;
    _size = 0;
  }
  else {   
    _begin = _begin->_next;
    delete _begin->_prev;
    _begin->_prev = NULL;
    _size = _size - 1;
  }
  return res;
}

template < typename T >
T list<T>::pop_back() {
  if (_size==0) return T();
  T  res = _end->_elem;
  if (_size==1) {
    delete _begin;
    _begin = NULL;
    _end = NULL;
    _size = 0;
  }
  else {   
    _end = _end->_prev;
    delete _end->_next;
    _end->_next = NULL;
    _size = _size - 1;
  }
  return res;
}

}
#endif 
