//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#ifndef MATRIX_BOOST
#define MATRIX_BOOST

// #include <math>
#include "../generic/operation.h"
#include "../generic/section.h"
#include "../generic/matrix.h"
#include "../generic/list.h"
#include "../boost/boostxx.hpp"

//for function alea_interval_point
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
typedef boost::minstd_rand base_generator_type;

using namespace matrix;

namespace interval_boost{
  
typedef cpp_tools::boost_class interval;
typedef double myfloat;

typedef Matrix< interval > matrix_interval;
typedef Matrix< myfloat > matrix_float;

typedef struct intersection_result< interval > intersection_result_interval;

}//namespace interval_boost
#endif
