//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#ifndef _MPFIXX_H
#define _MPFIXX_H

#include "../base/base.h"
#include <mpfi.h>
// #include "mpfrxx_test.hpp"
#include "mpfrxx.hpp"

// #define MAX_MPFI(a,b) (a<b?b:a)

namespace cpp_tools{
  
class mpfi_class
{
    public:
        mpfi_t _value;
	static mp_prec_t _prec;
	
        mpfi_class(){
// 	  cout << "mpfi: prec: " << _prec << endl;
	  mpfi_init2(_value, _prec);
	}
	
        mpfi_class(const double& left, const double& right){
          mpfi_init2(_value, _prec); 
          mpfi_interv_d(_value, left, right);
        }
        
        mpfi_class(const mpfr_class& x, const mpfr_class& y){
          mpfi_init2(_value, _prec); 
          mpfi_interv_fr(_value, x._value, y._value);
        }
                                         
        mpfi_class(const mpfi_t& x){ 
          mpfi_init2(_value, _prec);
          mpfi_set(_value, x); 
	}
	
        mpfi_class(const mpfi_class& x){
          mpfi_init2(_value, _prec);
          mpfi_set(_value, x._value);
	}
	
	static mp_prec_t get_prec() {
	  return _prec;
	}
	
	static void set_prec( mp_prec_t prec ) {
	  _prec = prec;
	}
	
        mpfr_class get_left() const {
	  return mpfr_class( &((this->_value)->left) );
	}
	
	mpfr_class get_right() const {
	  return mpfr_class( &((this->_value)->right) );
	}
	
        ~mpfi_class(){
          mpfi_clear (_value);
	}
	
        mpfi_class & operator= (const mpfi_class& x) {
	  //this and x are supposed to share their precision
          mpfi_set(_value, x._value);
          return *this;
        }
        
        mpfi_class & operator= (const int& x) {
	  mpfi_set_si(_value, x);
          return *this;
        }
        
        mpfi_class operator* (const mpfi_class& x) {
          mpfi_class res;
          mpfi_mul(res._value, _value, x._value);
          return res;
        }
        
        mpfi_class operator+ (const mpfi_class& x) {
	  mpfi_class res;
          mpfi_add(res._value, _value, x._value);
          return res;
        }
        
        mpfi_class & operator+= (const mpfi_class& x) {
            mpfi_add(_value, _value, x._value);
            return *this;
        }
        
        mpfi_class operator- () {
	  mpfi_class res (*this);
          mpfi_neg(res._value, _value);
          return res;
        }
        
        mpfi_class operator- (const mpfi_class& x) {
	  mpfi_class res;
          mpfi_sub(res._value, _value, x._value);
          return res;
        }
        
        mpfi_class & operator-= (const mpfi_class& x) {
            mpfi_sub(_value, _value, x._value);
            return *this;
        }
        
        mpfi_class operator/ (const mpfi_class& x) {
	  mpfi_class res;
          mpfi_div(res._value, _value, x._value);
          return res;
        }
        
        mpfi_class & operator/= (const mpfi_class& x) {
	     mpfi_div(_value, _value, x._value);
            return *this;
        }
        
        //only for test
        mp_prec_t get_prec_mpfi_t() {
	  return mpfi_get_prec(_value);
	}
	
};

mpfr_class absolute_diameter (const mpfi_class& x);

ostream &operator<<(ostream &out, const mpfi_class &m) {
  out.precision(m._prec);
//   out << "[" << mpfi_class::get_prec() << ", " << (m.get_left()).get_ld() << ", " << (m.get_right()).get_ld() << ", width: " << (absolute_diameter(m)).get_ld() << ", next: " << (m.get_left()).next() << "]";
  out << "[" << mpfi_get_prec(m._value) << ", " << (m.get_left()) << ", " << (m.get_right()) << ", width: " << (absolute_diameter(m)) << ", next: " << (m.get_left()).next() << "]";
//   out << "[" << mpfi_get_prec(m._value) << ", " << (m.get_left()) << ", " << (m.get_right()) << "]";
  return out;
};

mpfi_class sqrt(const mpfi_class& x) {
  mpfi_class res;
  mpfi_sqrt(res._value, x._value);
  return res;
}

mpfi_class exp(const mpfi_class& x) {
  mpfi_class res;
  mpfi_exp(res._value, x._value);
  return res;
}

mpfi_class log(const mpfi_class& x) {
  mpfi_class res;
  mpfi_log(res._value, x._value);
  return res;
}

mp_prec_t get_prec (const mpfi_class& x) {
  return mpfi_class::get_prec();
}

mpfi_class inverse(const mpfi_class& x){
  mpfi_class res;
  mpfi_inv(res._value, x._value);
  return res;
}
//         
mpfi_class centerInterval (const mpfi_class& x) {
// 	    cout << "center()" << '\n';
//   mp_prec_t precision = mpfi_class::get_prec();
  mpfr_t middle_mpfr;
  mpfr_init2(middle_mpfr, mpfi_class::get_prec());
  mpfi_class res;
  mpfi_mid(middle_mpfr, x._value);
  mpfi_set_fr(res._value, middle_mpfr);
  mpfr_clear(middle_mpfr);
  return res;
}

mpfr_class centerFloat (const mpfi_class& x) {
//   mp_prec_t precision = mpfi_class::get_prec();
  mpfr_t middle_mpfr;
  mpfr_init2(middle_mpfr, mpfi_class::get_prec());
  mpfi_mid(middle_mpfr, x._value);
  mpfr_class res(middle_mpfr);
  mpfr_clear(middle_mpfr);
  return res;
}
//         
int contains_zero(const mpfi_class& x) {
  return mpfi_has_zero (x._value);
}

//for inversion of matrices with width almost null 
bool iszero(const mpfi_class& x){
  return mpfi_has_zero (x._value);
}

mpfr_class absolute_diameter (const mpfi_class& x) {
//     mp_prec_t precision = mpfi_get_prec(x.value);
    mpfr_t diameter_mpfr;
    mpfr_init2(diameter_mpfr, mpfi_class::get_prec());
//     mpfi_class res (x);
    mpfi_diam_abs(diameter_mpfr, x._value);
    mpfr_class res(diameter_mpfr);
//     mpfi_set_fr(res.value, diameter_mpfr);
    mpfr_clear(diameter_mpfr);
    return res;
}

mpfr_class relative_diameter (const mpfi_class& x) {
    mpfr_t diameter_mpfr, denom_mpfr, un_mpfr;
    mpfr_init2(diameter_mpfr, mpfi_class::get_prec());
    mpfr_init2(denom_mpfr, mpfi_class::get_prec());
    mpfr_init2(un_mpfr, mpfi_class::get_prec());
    mpfr_set_ui(un_mpfr, 1, ROUNDING_MODE);
    mpfi_mid(denom_mpfr, x._value);
    mpfr_abs(denom_mpfr,denom_mpfr, ROUNDING_MODE);
    mpfr_max(denom_mpfr,denom_mpfr, un_mpfr, ROUNDING_MODE);
    mpfi_diam_abs(diameter_mpfr, x._value);
    mpfr_div(diameter_mpfr, diameter_mpfr, denom_mpfr, ROUNDING_MODE);
    mpfr_class res(diameter_mpfr);
    mpfr_clear(diameter_mpfr);
    mpfr_clear(denom_mpfr);
    mpfr_clear(un_mpfr);
    return res;
}

mpfi_class intersect (const mpfi_class& x, const mpfi_class& y) {
    mpfi_class res;
    mpfi_intersect(res._value, x._value, y._value);
    return res;
}

mpfi_class hull (const mpfi_class& x, const mpfi_class& y) {
    mpfi_class res;
    mpfi_union(res._value, x._value, y._value);
    return res;
}

int overlaps (const mpfi_class& x, const mpfi_class& y) {
    mpfi_class res;
    mpfi_intersect(res._value, x._value, y._value);
    return (mpfi_is_empty(res._value)==0);
}

int is_in (const mpfi_class& x, const mpfi_class& y) {
    return mpfi_is_inside(x._value, y._value);
}

int is_empty_i(const mpfi_class& x) {
    return mpfi_is_empty(x._value);
}

int is_strictly_in (const mpfi_class& x, const mpfi_class& y) {
    return mpfi_is_strictly_inside(x._value, y._value);
}

int bisect ( mpfi_class* resInf, mpfi_class* resSup, const mpfi_class& x){
    return mpfi_bisect(resInf->_value, resSup->_value, x._value);
}

mpfr_class magnitude( const mpfi_class& x ) {
    mpfr_t magnitude_mpfr;
    mpfr_init2(magnitude_mpfr, mpfi_class::get_prec());
    mpfi_mag(magnitude_mpfr, x._value);
    mpfr_class res(magnitude_mpfr);
    mpfr_clear(magnitude_mpfr);
    return res;
}

mpfi_class interior ( const mpfi_class& x ){
  return mpfi_class (next(x.get_left()),prev(x.get_right()));
}

mpfi_class inflate ( const mpfi_class& x ){
  return mpfi_class (prev(x.get_left()),next(x.get_right()));
}

bool is_empty_prec (const mpfi_class& x) {
    return not(x.get_right() > (x.get_left()).next());
//     cout << "is_empty_prec: " << x << endl;
//     cout << "(x.get_left()).next(): " << (x.get_left()).next() << endl;
//     cout << "(x.get_right() > (x.get_left()).next()): " << (x.get_right() > (x.get_left()).next()) << endl;
//     cout << "(x.get_right().prev() > (x.get_left()).next()): " << (x.get_right() > (x.get_left()).next()) << endl;
//     cout << "interior(x): " << interior(x) << endl; 
//     return is_empty(interior(x));
}

mpfi_class alea_interval_point ( const mpfi_class& x ){
    mpfr_t alea_mpfr;
    mpfr_init2(alea_mpfr, mpfi_class::get_prec());
    mpfi_alea(alea_mpfr, x._value);
    return mpfi_class(alea_mpfr,alea_mpfr);
}

}//end namespace
#endif