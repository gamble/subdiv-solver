//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#ifndef _SUBDIVISION_H
#define _SUBDIVISION_H

#include "../base/base.h"
#include "../base/parameters.h"
#include "operation.h"
#include "section.h"
#include "evaluation.h"
#include "contraction.h"
#include "box.h"
#include "list.h"
#include "queue.h"

// #include<iostream>

// #define QUIET 0 //no output
// #define STATS 1 //just stats at the end of the process
// #define DEBUGING_MODE 3 //very verbose
// using namespace std;
using namespace matrix;

template < class I, class F >  
class subdivision_solver {

public:
  
  subdivision_solver( ){};
  
  subdivision_solver( Matrix<I> (*func) (Matrix<I>), 
                      Matrix<I> (*jaco) (Matrix<I>), 
                      Matrix<I> (*hess) (Matrix<I>) );
    
  ~subdivision_solver();
  
  void initialize();
  
  //setters
  void setEps(F eps) { _eps = eps; } ///eps for epsilon inflation -> should not be set by user
  void addWorking( const Matrix<I> &box ); ///put box in the queue
  void setVerbosity(int verbosity){_verbosity = verbosity;} ///prints
  
  //termination controls
  void setMaxStep(unsigned long int steps); ///stop the process after steps boxes have been explored
  void setMinWidth(F minWidth); ///do not explore boxes smaller than minWidth
  void setMinWidthBorder(F minWidthBorder); ///do not explore boxes smaller than minWidthBorder when dealing with border
  void setLastPrecision(bool control) {_last_Precision = control;} ///if true, boxes where it was not possible to invert the jacobian are put in _jac_not_inversible_boxes_list
  
  ///criterias to decide when increasing precision 
  void setControlKrawczykPrecision(bool control) {_control_Krawczyk_Precision = control;}
  void setControlFcenterZero(bool control) {_control_Fcenter_zero = control;}
  void setControlEvaluationPrecision(bool control) {_control_Evaluation_Precision = control;}
  
  ///deal with border of not
  void dealWithBorder( Matrix<I> initialDomain );
  
  /// order of evaluation and krawczyk
  void setOrderEval ( int order ) { _order_eval = order; } /// 0: order 0, 1: order 1, 2: order 2
  void setOrderKraw ( int order ) { _order_kraw = order; } /// 0: order 1, 1: order 2
  
  //constraints
  void setConstraintsEqZero ( Matrix<I> (*) (Matrix<I>), Matrix<I> (*) (Matrix<I>), Matrix<I> (*) (Matrix<I>) );
  bool areConstraintsEqZeroFulfilled(const Matrix<I> &);
  void setConstraintsNeqZero ( Matrix<I> (*) (Matrix<I>), Matrix<I> (*) (Matrix<I>), Matrix<I> (*) (Matrix<I>) );
  bool areConstraintsNeqZeroFulfilled(const Matrix<I> &);
  
  //main methods
  void begin();                                               // set the solving process
  void begin(const Matrix<I> &initial_box);                   // set the solving process from initial_box
  bool next();                                                // inspect the head of the queue
  bool end();                                                 // test wether a termination criterion is reached of not 
  bool solve();                                               // performs the solving process from boxes in queue
  bool solve(Matrix<I> initial_box);                          // performs the solving process from initial_box
  bool solve(Matrix<I> initial_box, unsigned long int steps); // performs N steps from initial_box
  bool solve(unsigned long int steps);                        // performs N steps from boxes in queue
  
  //getters
  void visitSolutions() { _result_list.visit(); }
  void visitWorking() { _working_queue.visit(); }
  void visitTooSmallPrecision() { _too_small_precision_list.visit(); }
  void visitTooSmallUser() { _too_small_user_list.visit(); }
  void visitNotInversible() { _jac_not_inversible_boxes_list.visit(); }
  void visitCCInTooSmallUser() { _connected_component_in_too_small_user.visit(); }
  
  bool endSolutions() { return _result_list.end(); }
  bool endWorking() { return _working_queue.end(); }
  bool endTooSmallPrecision() { return _too_small_precision_list.end(); }
  bool endTooSmallUser() { return _too_small_user_list.end(); }
  bool endNotInversible() { return _jac_not_inversible_boxes_list.end(); }
  bool endCCInTooSmallUser() { return _connected_component_in_too_small_user.end(); }
  
  Matrix<I> nextSolutions() { return _result_list.next(); }
  Matrix<I> nextWorking() { return (_working_queue.next())->get_m(); }
  Matrix<I> nextTooSmallPrecision() { return _too_small_precision_list.next(); }
  Matrix<I> nextTooSmallUser() { return _too_small_user_list.next(); }
  Matrix<I> nextNotInversible() { return _jac_not_inversible_boxes_list.next(); }
  Matrix<I> nextCCInTooSmallUser() { return _connected_component_in_too_small_user.next(); }
  
  unsigned long int getCounter() { return _counter; }
  
  //intermediate methods
  Matrix<I> evaluateFuncOnInflatedBox( Box<I,F> &b );
  int applyKrawczykTestOnInflatedBox( Box<I,F> &b );
  int complementResultInWorking( const Matrix<I> &Result );
  int controlEvaluationPrecision( Box<I,F> &b, cpp_tools::list< Box<I,F>* > &subBoxes );
  void computeConnectedComponentInTooSmallUser();
  int contractSolutions(F width);
  bool separateFromBorder( Matrix<I> solution );
  int areTheSame( const Matrix<I> &sol1, const Matrix<I> &sol2 );
  bool isAlreadyInSols( const Matrix<I> &sol);
  
  //tools for callers
  int ApplyKrawczykOnBox( Matrix<I> box );
  int ApplyKrawczykTestOnBox( Matrix<I> box );
  
  ///printing
  void printStats();
  void printStatsColor();
  void solverprint();
  
  ///plotting 2D
  void setPlotting(bool control) {_plotting = control;}
  void visitDisEvaluation() { _Bdiscarded_by_evaluation.visit(); }
  void visitDisKrawczyk()   { _Bdiscarded_by_krawczyk.visit(); }
  void visitValKrawczyk()   { _Bvalidated_by_krawczyk.visit(); }
  
  bool endDisEvaluation() { return _Bdiscarded_by_evaluation.end(); }
  bool endDisKrawczyk()   { return _Bdiscarded_by_krawczyk.end(); }
  bool endValKrawczyk()   { return _Bvalidated_by_krawczyk.end(); }
  
  Matrix<I> nextDisEvaluation() { return _Bdiscarded_by_evaluation.next(); }
  Matrix<I> nextDisKrawczyk()   { return _Bdiscarded_by_krawczyk.next(); }
  Matrix<I> nextValKrawczyk()   { return _Bvalidated_by_krawczyk.next(); }
  
  
  ///DEPRECATED
//   bool solveMore(unsigned long int N);                        // performs N steps from boxes in queue
//   Matrix<I> inflateSolution( Matrix<I> & solution, F eps );// inflates a solution while existence, uniqueness and areConstraintsNeqZeroFulfilled
//   Matrix<I> contractBox(Matrix<I> initial_box, unsigned long int steps); //contract a box around a solution at most steps times
  
private:
  //containers
//   cpp_tools::queue< Box<I,F> * > _working_queue;
  cpp_tools::list< Box<I,F> * > _working_queue;
  cpp_tools::list< Matrix<I> > _result_list;
  cpp_tools::list< Matrix<I> > _too_small_precision_list;
  cpp_tools::list< Matrix<I> > _too_small_user_list;
  cpp_tools::list< Matrix<I> > _jac_not_inversible_boxes_list;
  cpp_tools::list< Matrix<I> > _connected_component_in_too_small_user;
  
  //parameters
  Matrix<I> (*_func) (Matrix<I>);
  Matrix<I> (*_jaco) (Matrix<I>);
  Matrix<I> (*_hess) (Matrix<I>);
  F _eps; // parameter of epsilon_inflation
  
  //printings
  int _verbosity;
  //controls
  unsigned long int _counter;
  unsigned long int _maxSteps;
  F _minwidth;
  F _minwidthBorder;
  bool _finiteNumberOfSteps;
  bool _minWidthUserDefined;
  bool _minWidthBorderUserDefined;
  
  
  //stats
  
  unsigned long int _precision_empty;
  unsigned long int _precision_evaluation;
  unsigned long int _precision_krawc;
  unsigned long int _precision_center_zer;
  
  unsigned long int _discarded;
  unsigned long int _discarded_by_evaluation;
  unsigned long int _discarded_by_krawczyk;
  unsigned long int _discarded_by_constraints_eq_zero;
  
  //constraints equal zero
  Matrix<I> (*_CEZfunc) (Matrix<I>);
  Matrix<I> (*_CEZjaco)  (Matrix<I>);
  Matrix<I> (*_CEZhess) (Matrix<I>);
  
  //constraints not equal zero
  Matrix<I> (*_CNZfunc) (Matrix<I>);
  Matrix<I> (*_CNZjaco)  (Matrix<I>);
  Matrix<I> (*_CNZhess) (Matrix<I>);
  
  //dealing with borders
  Matrix<I> _initialDomain;
  bool _dealWithBorder;
  
  int _order_eval; //order of evaluation
  int _order_kraw; //order of krawczyk
  //precision control
  bool _last_Precision;
  bool _control_Fcenter_zero;
  bool _control_Evaluation_Precision;
  bool _control_Krawczyk_Precision;
  
  ///plotting 2D
  bool _plotting;
  cpp_tools::list< Matrix<I> > _Bdiscarded_by_evaluation;
  cpp_tools::list< Matrix<I> > _Bdiscarded_by_krawczyk;
  cpp_tools::list< Matrix<I> > _Bvalidated_by_krawczyk;
};

template < class I, class F >
subdivision_solver<I,F>::subdivision_solver( Matrix<I> (*func) (Matrix<I>), 
                                             Matrix<I> (*jaco) (Matrix<I>), 
                                             Matrix<I> (*hess) (Matrix<I>)) {

    _func = func;
    _jaco = jaco;
    _hess = hess;
    initialize();
//     setArgs(args); // default value of args is -1                                               
}

template < class I, class F >
void subdivision_solver<I,F>::initialize(  ) {
    _eps = EPS_INFLATION_DEFAULT_VALUE;
    _finiteNumberOfSteps = false;
    _minWidthUserDefined = false;
    _minWidthBorderUserDefined = false;
    _maxSteps = 0;
    _minwidth = 0;
    _minwidthBorder = _minwidth;
    _CEZfunc = NULL;
    _CEZjaco = NULL;
    _CEZhess = NULL;
    _CNZfunc = NULL;
    _CNZjaco = NULL;
    _CNZhess = NULL;
    _verbosity = 0;
    _dealWithBorder = false;
    _order_eval = 2;
    _order_kraw = 1; //corresponds to kraw order 2
    _last_Precision = false;
    _control_Fcenter_zero = true;
    _control_Evaluation_Precision = true;
    _control_Krawczyk_Precision = true;
    
    ///plotting 2D
    _plotting = false;
}

template < class I, class F >
subdivision_solver<I,F>::~subdivision_solver( ) {
//   cout << "subdivision.h, l118: ~subdivision_solver, begin" << endl;
  while (_working_queue.size())
    delete _working_queue.pop_front();
  _result_list.clear();
  _too_small_precision_list.clear();
  _too_small_user_list.clear();
  _jac_not_inversible_boxes_list.clear();
  _connected_component_in_too_small_user.clear(); 
//   cout << "subdivision.h, l121: ~subdivision_solver, fin" << endl;
  
  //Plotting
  _Bdiscarded_by_evaluation.clear();
  _Bdiscarded_by_krawczyk.clear();
  _Bvalidated_by_krawczyk.clear();
}

template < class I, class F >
void subdivision_solver<I,F>::setMaxStep(unsigned long int steps){
  _finiteNumberOfSteps = true;
  _maxSteps = steps;
  return;
}

template < class I, class F >
void subdivision_solver<I,F>::setMinWidth(F minWidth){
  _minWidthUserDefined = true;
  _minwidth = minWidth;
  if (not _minWidthBorderUserDefined) _minwidthBorder = minWidth;
  return;
}

template < class I, class F >
void subdivision_solver<I,F>::setMinWidthBorder(F minWidthBorder){
  F zero;
  zero = 0;
  if (minWidthBorder > zero){
  _minWidthBorderUserDefined = true;
  _minwidthBorder = minWidthBorder; 
  }
  return;
}

template < class I, class F >
void subdivision_solver<I,F>::addWorking( const Matrix<I> &m ) {
  _working_queue.push_back( new Box<I,F>(m) );
}

template < class I, class F >
void subdivision_solver<I,F>::dealWithBorder( Matrix<I> initialDomain ) {
  _initialDomain = initialDomain;
  _dealWithBorder = true;
}
  
template < class I, class F >
void subdivision_solver<I,F>::setConstraintsEqZero ( Matrix<I> (*func) (Matrix<I>), Matrix<I> (*jaco) (Matrix<I>), Matrix<I> (*hess) (Matrix<I>) ) {
  _CEZfunc = func;
  _CEZjaco = jaco;
  _CEZhess = hess;
}

template < class I, class F >
void subdivision_solver<I,F>::setConstraintsNeqZero ( Matrix<I> (*func) (Matrix<I>), Matrix<I> (*jaco) (Matrix<I>), Matrix<I> (*hess) (Matrix<I>) ) {
  _CNZfunc = func;
  _CNZjaco = jaco;
  _CNZhess = hess;
}

template < class I, class F >
bool subdivision_solver<I,F>::areConstraintsEqZeroFulfilled(const Matrix<I> &box){
  
  if (not _CEZfunc) return true;
  
  bool res = true;
  Matrix<I> F_box = _CEZfunc( box );
  if ( contains_zero<I>(F_box) and (_order_eval > 0)) {
    Matrix<I> F_box_order_eval;
    if (_order_eval == 1)
        F_box_order_eval = evaluate_order_1<I,F>(box, _CEZfunc, _CEZjaco);
    else
        F_box_order_eval = evaluate_order_2<I,F>(box, _CEZfunc, _CEZjaco, _CEZhess);
    struct intersection_result<I> Ires = overlaps<I>( F_box, F_box_order_eval );
    res = contains_zero<I>(Ires._intersection);
  }
  else res = false;
  
  return res;
}

template < class I, class F >
bool subdivision_solver<I,F>::areConstraintsNeqZeroFulfilled(const Matrix<I> &box){
  
  if (not _CNZfunc) return true;
  
  bool res = true;
  Matrix<I> F_box = _CNZfunc( box );
  if ( contains_zero<I>(F_box) and (_order_eval > 0) ) {
    Matrix<I> F_box_order_eval;
    if (_order_eval == 1)
        F_box_order_eval = evaluate_order_1<I,F>(box, _CEZfunc, _CEZjaco);
    else
        F_box_order_eval = evaluate_order_2<I,F>(box, _CEZfunc, _CEZjaco, _CEZhess);
    struct intersection_result<I> Ires = overlaps<I>( F_box, F_box_order_eval );
    res = not contains_zero<I>(Ires._intersection);
  }
  else res = true;
  
  return res;
}

template < class I, class F >
void subdivision_solver<I,F>::begin(const Matrix<I> &initial_box) {
  
  _working_queue.clear();
  _working_queue.push_back( new Box<I,F>(initial_box) );
  begin();
  return;
}

template < class I, class F >
void subdivision_solver<I,F>::begin() {
  
  _result_list.clear();
  _too_small_precision_list.clear();
  _too_small_user_list.clear();
  _jac_not_inversible_boxes_list.clear();
  _connected_component_in_too_small_user.clear();
  
  _counter                          =0; 
  _discarded                        =0;
  _discarded_by_evaluation          =0;
  _discarded_by_krawczyk            =0;
  _discarded_by_constraints_eq_zero =0;
  
  _precision_empty                 =0;
  _precision_evaluation            =0;
  _precision_krawc                 =0;
  _precision_center_zer            =0;
  
  //Plotting
  _Bdiscarded_by_evaluation.clear();
  _Bdiscarded_by_krawczyk.clear();
  _Bvalidated_by_krawczyk.clear();
  
  return;
}

template < class I, class F >
bool subdivision_solver<I,F>::end() {
  bool res = _working_queue.isEmpty();
  res = res or ( _finiteNumberOfSteps and _counter >= _maxSteps );
  return res;
}

template < class I, class F >
bool subdivision_solver<I,F>::next() {
  
  Box<I,F> *actual_box = _working_queue.pop_front();
  Matrix<I> *actual_inflated_box = eps_inflation_box<I,F> ( *actual_box, _eps);
  Matrix<I> F_actual_inflated_box = evaluateFuncOnInflatedBox(*actual_box);
  
  if (_verbosity == DEBUGING_MODE) {
    cout << "subdivision.h, next(): Nouvelle boîte, compteur: " << _counter << endl; 
    cout << "subdivision.h, next(): box: " << *actual_box << endl;
  }
  
  //control if min precision has been reached
  if (is_empty_prec(*actual_box)) {
    if (_verbosity == DEBUGING_MODE) 
        cout << "subdivision.h, next(): minimum precision reached" << endl;
    _precision_empty++;
    _too_small_precision_list.push_back( actual_box->get_m() );
    delete actual_box;
    return true;
  }
  
  if (_verbosity == DEBUGING_MODE) 
      cout << "subdivision.h, next(): precision test OK" << endl;
  
  if (not contains_zero<I>(F_actual_inflated_box)) {
    _discarded_by_evaluation++;
    if (_verbosity == DEBUGING_MODE) {
      cout << "subdivision.h, next(): discarded by evaluation" << endl;
      cout << "subdivision.h, next(): F_actual_inflated_box: " << F_actual_inflated_box << endl;
      ///test
//       applyKrawczykTestOnInflatedBox(*actual_box);
      ///fintest
    }
    
    //Plotting
    if (_plotting==true) {
        _Bdiscarded_by_evaluation.push_back( actual_box->get_m() );
    }
    
    delete actual_box;
    return true;
  }
  
  if (_verbosity == DEBUGING_MODE) 
      cout << "subdivision.h, next(): evaluation test OK" << endl;
  
  if (not areConstraintsEqZeroFulfilled(*actual_inflated_box) ) {
    _discarded_by_constraints_eq_zero++;
//     if (_verbosity == DEBUGING_MODE) cout << "discarded by constraints_eq_zero" << endl;
    delete actual_box;
    return true;
  }
  
  int resK = applyKrawczykTestOnInflatedBox(*actual_box);
  
  if (_verbosity == DEBUGING_MODE) 
      cout << "subdivision.h, next(): Krawczyk test OK" << endl;
  
  if ( resK == KRAWCZYK_JACMIDM_NOT_INVERSIBLE ) {
    if (not _last_Precision) //try with greater precision
      _too_small_precision_list.push_back(actual_box->get_m());
    else
      _jac_not_inversible_boxes_list.push_back(actual_box->get_m());
    if (_verbosity == DEBUGING_MODE) 
      cout << "subdivision.h, next(): jac not inversible in middle of box after " << MAX_TRIALS_TO_INVERSE << " trials " << endl;
    delete actual_box;
    return true;
  }
  
  if ( resK == KRAWCZYK_NOT_ENOUGH_PRECISION ) {    
    _precision_krawc++;
    _too_small_precision_list.push_back(actual_box->get_m());
    if (_verbosity == DEBUGING_MODE) {
      cout << "subdivision.h, next(): box too small for my criterion " << endl;
      cout << actual_box->get_m() << endl;
    }
    delete actual_box;
    return true;
  }
  
  if (resK == KRAWCZYK_OTHER) {
    if (_verbosity == DEBUGING_MODE) 
        cout << "subdivision.h, next():  res krawczyk = OTHER " << endl;
    delete actual_box;
    return false;
  }
  
  if (resK == KRAWCZYK_OK_NO_SOLUTION) {
    _discarded_by_krawczyk++;
    if (_verbosity == DEBUGING_MODE) 
        cout << "subdivision.h, next(): discarded by krawczyk" << endl;
    
    //Plotting
    if (_plotting==true) {
        _Bdiscarded_by_krawczyk.push_back( actual_box->get_m() );
    }
    
    delete actual_box;
    return true;
  }
  
  if (resK == KRAWCZYK_OK_ONE_SOLUTION) {
    if (_verbosity == DEBUGING_MODE) 
        cout << "subdivision.h, next(): Krawczyk OK ONE SOLUTION" << endl;
    if ( areConstraintsNeqZeroFulfilled(*actual_inflated_box) ) {
//       actual_inflated_box = inflateSolution(actual_inflated_box, 1.1);///not really efficient, maybe try with uniqueness
//       complementResultInWorking( *actual_inflated_box ); ///not really efficient
      if ( not (isAlreadyInSols(*actual_inflated_box)) ) {
        if (_dealWithBorder)
            separateFromBorder( *actual_inflated_box );
        else {
          _result_list.push_back( *actual_inflated_box );
        }
        //Plotting
          if (_plotting==true) {
            _Bvalidated_by_krawczyk.push_back( actual_box->get_m() );
          }
        delete actual_box;
        return true;
      }
      else {
        //Plotting
          if (_plotting==true) {
            _Bvalidated_by_krawczyk.push_back( actual_box->get_m() );
          }
        delete actual_box;
        return true;
      }
    }
    else 
      resK = KRAWCZYK_OK_CAN_NOT_DECIDE;
  }
  
  if (_verbosity == DEBUGING_MODE) 
      cout << "subdivision.h, next(): Krawczyk test return OK" << endl;
  
  if (resK == KRAWCZYK_OK_CAN_NOT_DECIDE) {
      
    int func_midpoint_contains_zero = 0;
    if ( contains_zero<I>( *evaluate_fmid(*actual_box, _func) ) ){
        if (_verbosity == DEBUGING_MODE) {
            cout << "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& subdivision.h, next():  " << endl;
            cout << "subdivision.h, next(): fmidb contains zero! " << endl;
            cout << *evaluate_fmid(*actual_box, _func) << endl;
            cout << "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& subdivision.h, next():  " << endl;
        }
        func_midpoint_contains_zero = 1;
    }
    
    cpp_tools::list< Box<I,F> *> subBoxes = subdivide_Maximal_smear<I,F>( *actual_box, _jaco(actual_box->get_m()), _minWidthUserDefined, _minwidth);
      
    if (subBoxes.size() == 1) { //actual_box is smallest than minimum size given by user
      _too_small_user_list.push_back( actual_box->get_m() );
      while (subBoxes.size())
        delete subBoxes.pop_front();
    }
    else if ( (_control_Evaluation_Precision) 
        and ( not controlEvaluationPrecision( *actual_box, subBoxes ) ) ) {
      if (_verbosity == DEBUGING_MODE) 
          cout << "subdivision.h, next(): evaluation precision reached!!!" << endl;
      _too_small_precision_list.push_back( actual_box->get_m() );
      _precision_evaluation++;
      while (subBoxes.size())
        delete subBoxes.pop_front();
    }
//     else if (func_midpoint_contains_zero and _control_Fcenter_zero) {
    else if (func_midpoint_contains_zero and _control_Fcenter_zero and (not _last_Precision)) {
        _precision_center_zer++;
        _too_small_precision_list.push_back( actual_box->get_m() );
    }
    else {
        if (_verbosity == DEBUGING_MODE) 
          cout << "subdivision.h, next(): subboxes: " << endl;
        while (subBoxes.size()) {
//         _working_queue.push_back( subBoxes.pop_front() ); //
        _working_queue.push_front( subBoxes.pop_back() ); //depth first search
          if (_verbosity == DEBUGING_MODE) 
            cout << (_working_queue.front())->get_m() << endl;
        }
    }
    delete actual_box;
    return true;
  }
  return false; //should never enter this case -> disables the warning when compiling
}

template < class I, class F >
bool subdivision_solver<I,F>::solve(){
  begin();
  while ( not end() ) {
    next();
    _counter = _counter + 1;
  }
  return true;
}

template < class I, class F >
bool subdivision_solver<I,F>::solve(Matrix<I> initial_box){
  begin( initial_box );
  while ( not end() ) {
    next();
    _counter = _counter + 1;
  }
  
  return true;
}

template < class I, class F >
bool subdivision_solver<I,F>::solve(Matrix<I> initial_box, unsigned long int steps){
  _finiteNumberOfSteps = true;
  _maxSteps = steps;
  return solve(initial_box);
}

template < class I, class F >
bool subdivision_solver<I,F>::solve(unsigned long int steps){
  _finiteNumberOfSteps = true;
  _maxSteps = steps;
  return solve();
}

template < class I, class F >
Matrix<I> subdivision_solver<I,F>::evaluateFuncOnInflatedBox( Box<I,F> &b ){
  
  Matrix<I> F_inflated_box = evaluate_order_0<I,F>( b, _func, _verbosity );
  if ( contains_zero<I>(F_inflated_box) and _order_eval > 0 ) {
    Matrix<I> F_box_order_eval;
    if (_order_eval == 1)
        F_box_order_eval = evaluate_order_1<I,F>(b, _func, _jaco, _verbosity);
    else
        F_box_order_eval = evaluate_order_2<I,F>(b, _func, _jaco, _hess, _verbosity);
    struct intersection_result<I> Ires = overlaps<I>( F_inflated_box, F_box_order_eval );
    F_inflated_box = Ires._intersection;
  }
  return F_inflated_box;
}

template < class I, class F >
int subdivision_solver<I,F>::applyKrawczykTestOnInflatedBox( Box<I,F> &b ){
  
  Matrix<I> K_inflated_box;
  int res_Krawczyk_1;
  
  if (_verbosity == DEBUGING_MODE) 
      cout << "subdivision.h, applyKrawczykTestOnInflatedBox(), _order_kraw: " << _order_kraw << endl;
  
  if (_order_kraw==1)
    res_Krawczyk_1 = krawczyk_order_2<I,F> (b, _func, _jaco, _hess, &K_inflated_box, _control_Krawczyk_Precision, _verbosity);
  else //kraw == 2
    res_Krawczyk_1 = krawczyk_order_1<I,F> (b, _func, _jaco, &K_inflated_box, _control_Krawczyk_Precision, _verbosity);
  
  if (res_Krawczyk_1 >= 5) // KRAWCZYK_JACMIDM_NOT_INVERSIBLE and KRAWCZYK_OTHER
    return res_Krawczyk_1;
  
  if (_verbosity == DEBUGING_MODE) 
      cout << "subdivision.h, applyKrawczykTestOnInflatedBox(), K_inflated_box: " << K_inflated_box << endl;
  
  Matrix<I> *inflated_box = eps_inflation_box<I,F> ( b, _eps);
  struct contraction_result<I> Kres = contraction_test ( K_inflated_box, *inflated_box, _verbosity);
  int res_Krawczyk_2 = Kres._result;
  
  if (res_Krawczyk_1 == KRAWCZYK_NOT_ENOUGH_PRECISION and res_Krawczyk_2 == KRAWCZYK_OK_CAN_NOT_DECIDE)
    return KRAWCZYK_NOT_ENOUGH_PRECISION;
  else   
    return res_Krawczyk_2;  
}

template < class I, class F >
int subdivision_solver<I,F>::complementResultInWorking( const Matrix<I> &Result ){
  int working_Size = _working_queue.size();
//   if (_verbosity == DEBUGING_MODE)
//     cout << "working_Size: " << working_Size << endl;
  cpp_tools::list< Box<I,F> *> comp;
  for (int i =0; i< working_Size; i++) {
    Box<I,F> *Ycur = _working_queue.pop_front();
    comp.clear();
    if ( complementary<I,F>( Result, *Ycur, comp ) == COMPLEMENTARY_CUTED ) {
        if (_verbosity == DEBUGING_MODE)
        cout << "complementResultInWorking: box in queue: " << Ycur->get_m() << endl;
      while (comp.size()) {
        _working_queue.push_back( comp.pop_front() );
        if (_verbosity == DEBUGING_MODE)
          cout << "new box: " << (_working_queue.back())->get_m() << endl;
      }
      delete Ycur;
    }
    else
      _working_queue.push_back( Ycur );
  }
  return true;
}

template < class I, class F >
int subdivision_solver<I,F>::controlEvaluationPrecision( Box<I,F> &b, cpp_tools::list< Box<I,F> *> &subBoxes ) {
  Matrix<I> funcb=evaluateFuncOnInflatedBox(b);
  Matrix<I> fhull(0,0);
  subBoxes.visit();
  bool subBoxesContainZero = true;
    while ( (!subBoxes.end()) and (subBoxesContainZero) ) {
      Box<I,F> *temp = subBoxes.next();
      Matrix<I> F_inflated_temp = evaluateFuncOnInflatedBox(*temp);
      if (not contains_zero<I>(F_inflated_temp)) 
        subBoxesContainZero = false; //this box will be discarded
      else {
        if (fhull.getNbLines()==0)
	    fhull = F_inflated_temp;
        else 
	    fhull = Interval_hull<I,F>(fhull, F_inflated_temp);
      }
    }
    if (not subBoxesContainZero)
      return true;
    struct intersection_result<I> Ires = overlaps<I>( fhull, funcb );
    if (_verbosity == DEBUGING_MODE) {
      cout << "controlEvaluationPrecision: " << endl;
      cout << "funcb: " << funcb << endl;
      cout << "fhull: " << fhull << endl;
    }
    return Ires._isIn;
}

template < class I, class F >
void subdivision_solver<I,F>::computeConnectedComponentInTooSmallUser(){
  cpp_tools::list< Matrix<I> > temp_list;
  temp_list.clear();
  _connected_component_in_too_small_user.clear();
  //copy _too_small_user_list in temp_list
//   cout << "subdivision.h: computeConnectedComponentInTooSmallUser, begin " << endl;
//   cout << "subdivision.h: computeConnectedComponentInTooSmallUser, _too_small_user_list.size(): " << _too_small_user_list.size() << endl;
  visitTooSmallUser();
  while ( not endTooSmallUser() )
    temp_list.push_back( nextTooSmallUser() );
//   cout << "subdivision.h: computeConnectedComponentInTooSmallUser, temp_list.size(): " << temp_list.size() << endl;
  ///TEST: methode bourrine
//   bool did_decrease = true;
  while (temp_list.size()) {
    Matrix<I> m = temp_list.pop_back();
    temp_list.visit_back();
    bool did_merge = false;
    while ( (not temp_list.end() ) and (not did_merge) ) {
      Matrix<I> * cur = temp_list.prev();
      struct intersection_result<I> res = overlaps<I>( m, *cur );
      if (res._overlaps) {
	*cur = Interval_hull<I,F>(m,*cur);
	did_merge = true;
      }
    }
    if ( temp_list.end() and (not did_merge) ) {
      _connected_component_in_too_small_user.push_back(m);
    }
  }
//   cout << "subdivision.h: computeConnectedComponentInTooSmallUser, temp_list.size(): " << temp_list.size() << endl;
//   cout << "subdivision.h: computeConnectedComponentInTooSmallUser, _connected_component_in_too_small_user.size(): " << _connected_component_in_too_small_user.size() << endl;
//   cout << "subdivision.h: computeConnectedComponentInTooSmallUser, end " << endl;
  return;
}

template < class I, class F >
int subdivision_solver<I,F>::ApplyKrawczykOnBox( Matrix<I> box ){
  Matrix<I> K_box;
  int res_Krawczyk;
  
  if (_verbosity == DEBUGING_MODE) 
      cout << "subdivision.h, ApplyKrawczykOnBox(), _order_kraw: " << _order_kraw << endl;
  
  if (_order_kraw == 1)
    res_Krawczyk = krawczyk_order_2<I,F> (box, _func, _jaco, _hess, &K_box, _control_Krawczyk_Precision, _verbosity);
  else
    res_Krawczyk = krawczyk_order_1<I,F> (box, _func, _jaco, &K_box, _control_Krawczyk_Precision, _verbosity);
  
  if (res_Krawczyk == KRAWCZYK_JACMIDM_NOT_INVERSIBLE )
      if (_control_Krawczyk_Precision)
        _too_small_precision_list.push_back( box );
      else 
        _jac_not_inversible_boxes_list.push_back( box );
      
  else if (res_Krawczyk == KRAWCZYK_NOT_ENOUGH_PRECISION ) 
      _too_small_precision_list.push_back( box );
  
  else if (res_Krawczyk == KRAWCZYK_OK ) {
      _result_list.push_back(K_box);
      
  }
  
//   cout << "ApplyKrawczykOnBox, res: " << res_Krawczyk << endl;
//   cout << "InitialBox: " << box << endl;
//   cout << "NewBox: " << K_box << endl;
  
  return res_Krawczyk;
}

template < class I, class F >
int subdivision_solver<I,F>::ApplyKrawczykTestOnBox( Matrix<I> box ){
  Matrix<I> K_box;
  
  if (_verbosity == DEBUGING_MODE) {
    cout << "subdivision.h: ApplyKrawczykTestOnBox, begin" << endl;
    cout << "subdivision.h: box: " << box <<endl;
  }
  
  int res_Krawczyk = ApplyKrawczykOnBox(box);
  
  if (_verbosity == DEBUGING_MODE) {
    cout << "subdivision.h: res_Krawczyk: " << res_Krawczyk <<endl;
  }
  
  if (not res_Krawczyk == KRAWCZYK_OK)
    return res_Krawczyk;
  
  K_box = _result_list.back();
  
  if (_verbosity == DEBUGING_MODE) {
    cout << "subdivision.h: K_box: " << K_box <<endl;
  }
  
  struct contraction_result<I> Kres = contraction_test<I> ( K_box, box, _verbosity);
  res_Krawczyk = Kres._result;
  
  if (_verbosity == DEBUGING_MODE) {
    cout << "subdivision.h: Kres._result: " << Kres._result <<endl;
  }
  
  return Kres._result;
	  
}

template < class I, class F >
int subdivision_solver<I,F>::contractSolutions(F width){
  
//   F F_relative_width = F(relative_width);
//   F F_relative_width = relative_width;
  
//   cout << "contract Solutions, relative_width: " << F_relative_width << endl;
  _result_list.clear();
  _too_small_precision_list.clear();
  _too_small_user_list.clear();
  _jac_not_inversible_boxes_list.clear();
  
  while (_working_queue.size()) {
    
    Box<I,F> *actual_box = _working_queue.pop_front();
    Matrix<I> box = actual_box->get_m();
    Matrix<I> K_box;
    int res_Krawczyk = KRAWCZYK_OK;
    
    while ( (res_Krawczyk == KRAWCZYK_OK ) and ( Max_absolute_diameter<I,F>(box) > width ) ) {
      if (_order_kraw == 1)
         res_Krawczyk = krawczyk_order_2<I,F> (box, _func, _jaco, _hess, &K_box, _control_Krawczyk_Precision, _verbosity);
      else
         res_Krawczyk = krawczyk_order_1<I,F> (box, _func, _jaco, &K_box, _control_Krawczyk_Precision, _verbosity);
      
      if (res_Krawczyk == KRAWCZYK_JACMIDM_NOT_INVERSIBLE )
        if (_control_Krawczyk_Precision)
          _too_small_precision_list.push_back( box );
        else 
          _jac_not_inversible_boxes_list.push_back( box );
      
      else if (res_Krawczyk == KRAWCZYK_NOT_ENOUGH_PRECISION ) 
        _too_small_precision_list.push_back( box );
      
      else {
        struct contraction_result<I> Kres = contraction_test ( K_box, box, _verbosity);
	res_Krawczyk = Kres._result;
// 	cout << "res_Krawczyk test: " << res_Krawczyk << endl;
	if ( not ( res_Krawczyk == KRAWCZYK_OK_ONE_SOLUTION ) )
	  _too_small_precision_list.push_back( box );
	else {
	  res_Krawczyk = KRAWCZYK_OK;
	  box = K_box;
	}
      }
//       cout << " relative_diameter(K_box): " << relative_diameter<I,F>(K_box) << endl;
//       cout << " relative_diameter(box): " << relative_diameter<I,F>(box) << endl;
    
      
    }
//     cout << " relative_diameter(box): " << relative_diameter<I,F>(box) << endl;
    if (res_Krawczyk == KRAWCZYK_OK )
      _result_list.push_back(box);
  }
//   cout << "Fin contract Solutions" << endl;
  return true;
}

#define NOT_THE_SAME 0
#define ARE_THE_SAME 1
#define CAN_NOT_DECIDE 2

template < class I, class F >
bool subdivision_solver<I,F>::isAlreadyInSols( const Matrix<I> &sol){
  
  visitSolutions();
  
  while (not endSolutions())
    if ( areTheSame ( sol, nextSolutions() ) == ARE_THE_SAME )
      return true;
  
  return false;
}

template < class I, class F >
int subdivision_solver<I,F>::areTheSame( const Matrix<I> &sol1, const Matrix<I> &sol2 ){
  Matrix<I> K_s1, K_s2;
  struct intersection_result<I> res = overlaps<I>(sol1,sol2);
  if (not res._overlaps) 
    return NOT_THE_SAME;
  int res_Krawczyk_sol1 = krawczyk_order_2<I,F> (sol1, _func, _jaco, _hess, &K_s1, false, _verbosity);
  int res_Krawczyk_sol2 = krawczyk_order_2<I,F> (sol2, _func, _jaco, _hess, &K_s2, false, _verbosity);
  if ( ( not (res_Krawczyk_sol1 == KRAWCZYK_OK) ) or ( not (res_Krawczyk_sol2 == KRAWCZYK_OK) ) )
    return CAN_NOT_DECIDE; //NOTE: should never arrise if sol1 and sol2 are s.t. K(sol1)\in\interior(sol1) and K(sol2)\in\interior(sol2)
  struct contraction_result<I> Kres1 = contraction_test ( K_s1, sol1, _verbosity);
  struct contraction_result<I> Kres2 = contraction_test ( K_s2, sol2, _verbosity);
  if ( ( not ( Kres1._result == KRAWCZYK_OK_ONE_SOLUTION ) ) or ( not ( Kres1._result == KRAWCZYK_OK_ONE_SOLUTION ) ) )
    return CAN_NOT_DECIDE; //NOTE: should never arrise if sol1 and sol2 are s.t. K(sol1)\in\interior(sol1) and K(sol2)\in\interior(sol2)
  res = overlaps<I>(K_s1,K_s2);
  if (res._overlaps) 
    return ARE_THE_SAME;
  else 
    return NOT_THE_SAME;
}

template < class I, class F >
bool subdivision_solver<I,F>::separateFromBorder( Matrix<I> solution ) {
  
  int res_Krawczyk = KRAWCZYK_OK;
  bool containsB = containsBorder<I,F>(solution, _initialDomain);
  Matrix<I> K_solution;
  bool result = false;
  
//   cout << "separateFromBorder: _initialDomain: " << _initialDomain << endl;
//   cout << "separateFromBorder: solution: " << solution << endl;
//   cout << "separateFromBorder: containsB: " << containsB << endl;
  
//   while (  (res_Krawczyk == KRAWCZYK_OK) and containsB
//       and (Max_absolute_diameter<I,F>(solution) > _minwidth) ) {
  while (  (res_Krawczyk == KRAWCZYK_OK) and containsB
      and (Max_absolute_diameter<I,F>(solution) > _minwidthBorder) ) {
    
    res_Krawczyk = krawczyk_order_2<I,F> (solution, _func, _jaco, _hess, &K_solution, _control_Krawczyk_Precision, _verbosity);
    
    if (res_Krawczyk == KRAWCZYK_JACMIDM_NOT_INVERSIBLE )
        if (_control_Krawczyk_Precision)
          _too_small_precision_list.push_back( solution );
        else 
          _jac_not_inversible_boxes_list.push_back( solution );
      
      else if (res_Krawczyk == KRAWCZYK_NOT_ENOUGH_PRECISION ) 
        _too_small_precision_list.push_back( solution );
      
      else {
        struct contraction_result<I> Kres = contraction_test ( K_solution, solution, _verbosity);
	res_Krawczyk = Kres._result;
// 	cout << "res_Krawczyk test: " << res_Krawczyk << endl;
	if ( not ( res_Krawczyk == KRAWCZYK_OK_ONE_SOLUTION ) )
	  _too_small_precision_list.push_back( solution );
	else {
	  res_Krawczyk = KRAWCZYK_OK;
	  solution = K_solution;
	  containsB = containsBorder<I,F>(solution, _initialDomain);
	}
      }
  }
//   cout << "separateFromBorder: solution: " << solution << endl;
//   cout << "separateFromBorder: containsB: " << containsB << endl;
  if ( res_Krawczyk == KRAWCZYK_OK and containsB ) {
    _too_small_user_list.push_back( solution );
//     cout << " new solution intersecting the border" << endl;
  }
  else if ( res_Krawczyk == KRAWCZYK_OK and not containsB ) {
    struct intersection_result<I> res = overlaps<I>(solution,_initialDomain);
    if (res._overlaps){
      _result_list.push_back( solution );
      result = true;
    }
  }
  
  return result;
}

template < class I, class F >
void subdivision_solver<I,F>::solverprint() {
  cout << "_working_queue: " << _working_queue << endl;
}

#define ROUGE "\033[31m"
#define VERT "\033[32m"
#define BLEU "\033[34m"
#define NORMAL "\033[00m"

template < class I, class F >
void subdivision_solver<I,F>::printStats() {
  cout << "*********************************Stats Subdivision Solver: precision " << NORMAL << I::get_prec() << NORMAL << "*************" << endl;
  
  if ( _finiteNumberOfSteps and (_counter==_maxSteps) ) {
    cout << "*_counter: " << NORMAL << _counter              << NORMAL << endl;
    cout << "*in queue: " << NORMAL << _working_queue.size() << NORMAL << endl;
  }
  else {
    cout << "*_counter: " << NORMAL  << _counter              << NORMAL << endl;
    cout << "*in queue: " << NORMAL  << _working_queue.size() << NORMAL << endl;
  }
  
  if (_too_small_user_list.size()) {
    cout << "*boxes smallest than min size: " << NORMAL << _too_small_user_list.size() << NORMAL;
    cout << " in " << NORMAL << _connected_component_in_too_small_user.size() << NORMAL << " connected components" << endl;
  }
  else 
    cout << "*boxes smallest than min size: " << NORMAL  << _too_small_user_list.size() << NORMAL << endl;
  
  
  if (_too_small_precision_list.size())
    cout << "*boxes requiring more precision: " << NORMAL << _too_small_precision_list.size() << NORMAL << endl;
  else 
    cout << "*boxes requiring more precision: " << NORMAL << _too_small_precision_list.size() << NORMAL << endl;
  
  if (_jac_not_inversible_boxes_list.size())
    cout << "*boxes with not inversible jac: " << NORMAL << _jac_not_inversible_boxes_list.size() << NORMAL << endl;
  else
    cout << "*boxes with not inversible jac: " << NORMAL  << _jac_not_inversible_boxes_list.size() << NORMAL << endl;
  
  _discarded = _discarded_by_evaluation + _discarded_by_krawczyk + _discarded_by_constraints_eq_zero;
  cout << "*_discarded: " << _discarded << endl;
  cout << "*_discarded_by_evaluation: " << _discarded_by_evaluation << endl;
  cout << "*_discarded_by_krawczik: " << _discarded_by_krawczyk << endl;
  if ( _CEZfunc )
    cout << "*_discarded_by_constraints_eq_zero: " << _discarded_by_constraints_eq_zero << endl;
  cout << "*nb solutions: " << _result_list.size() << endl;
  cout << "************************************************************************" << endl;
}

template < class I, class F >
void subdivision_solver<I,F>::printStatsColor() {
  cout << "*********************************Stats Subdivision Solver: precision " << VERT << I::get_prec() << NORMAL << "*************" << endl;
  
  if ( _finiteNumberOfSteps and (_counter==_maxSteps) ) {
    cout << "*_counter: " << ROUGE << _counter              << NORMAL << endl;
    cout << "*in queue: " << ROUGE << _working_queue.size() << NORMAL << endl;
  }
  else {
    cout << "*_counter: " << VERT  << _counter              << NORMAL << endl;
    cout << "*in queue: " << VERT  << _working_queue.size() << NORMAL << endl;
  }
  
  if (_too_small_user_list.size()) {
    cout << "*boxes smallest than min size: " << ROUGE << _too_small_user_list.size() << NORMAL;
    cout << " in " << ROUGE << _connected_component_in_too_small_user.size() << NORMAL << " connected components" << endl;
  }
  else 
    cout << "*boxes smallest than min size: " << VERT  << _too_small_user_list.size() << NORMAL << endl;
  
  
  if (_too_small_precision_list.size()) {
    cout << "*boxes requiring more precision: " << BLEU << _too_small_precision_list.size() << NORMAL << endl;
    cout << "**detected by criterion empty:                 " << _precision_empty << endl;
    cout << "**detected by criterion evaluation:            " << _precision_evaluation << endl;
    cout << "**detected by criterion kraw:                  " << _precision_krawc << endl;
    cout << "**detected by criterion Fcenter contains zero: " << _precision_center_zer << endl;
  }
  else 
    cout << "*boxes requiring more precision: " << VERT << _too_small_precision_list.size() << NORMAL << endl;
  
  if (_jac_not_inversible_boxes_list.size())
    cout << "*boxes with not inversible jac: " << ROUGE << _jac_not_inversible_boxes_list.size() << NORMAL << endl;
  else
    cout << "*boxes with not inversible jac: " << VERT  << _jac_not_inversible_boxes_list.size() << NORMAL << endl;
  
  _discarded = _discarded_by_evaluation + _discarded_by_krawczyk + _discarded_by_constraints_eq_zero;
  cout << "*_discarded: " << _discarded << endl;
  cout << "*_discarded_by_evaluation: " << _discarded_by_evaluation << endl;
  cout << "*_discarded_by_krawczik: " << _discarded_by_krawczyk << endl;
  if ( _CEZfunc )
    cout << "*_discarded_by_constraints_eq_zero: " << _discarded_by_constraints_eq_zero << endl;
  cout << "*nb solutions: " << _result_list.size() << endl;
  cout << "************************************************************************" << endl;
}





///DEPRECATED

// template < class I, class F >
// bool subdivision_solver<I,F>::solveMore(unsigned long int steps) {
//   _finiteNumberOfSteps = true;
//   _maxSteps = _maxSteps + steps;
//   
//   while ( not end() ) {
//     next();
//     _counter = _counter + 1;
//   }
//   return true;
// }

// template < class I, class F >
// Matrix<I> subdivision_solver<I,F>::inflateSolution( Matrix<I> & solution, F eps ) {
//       if (_verbosity == STATS) cout << "******************result!" << endl; 
//       if (_verbosity == STATS) cout << "actual_inflated_box before inflation: " << endl;
//       if (_verbosity == STATS) cout << solution << endl;
// //       
// //       inflate the box while it contains a unique solution
//       bool constNeqZeroFulfilled = true;
//       Matrix<I> sol = solution;
//       Matrix<I> actual_inflated_sol;
//       Matrix<I> K_actual_inflated_sol;
//       int nb_of_successfull_Inflation = 0;
//       struct contraction_result<I> Kres;
//       Kres._result = KRAWCZYK_OK_ONE_SOLUTION;
//       
//       while ( (Kres._result == KRAWCZIK_OK_ONE_SOLUTION) and constNeqZeroFulfilled ) {
// 	actual_inflated_sol = eps_inflation_box<I,F> ( sol, eps);
// 	K_actual_inflated_sol = krawczyk_order_2<I,F> (actual_inflated_sol, _func, _jaco, _hess, _verbosity);
// 	Kres = contraction_test ( K_actual_inflated_sol, actual_inflated_sol, _verbosity);
// 	if (Kres._result == KRAWCZIK_OK_ONE_SOLUTION) constNeqZeroFulfilled = areConstraintsNeqZeroFulfilled(actual_inflated_sol);
// 	if ( (Kres._result == KRAWCZIK_OK_ONE_SOLUTION) and constNeqZeroFulfilled ) {
// 	  sol = actual_inflated_sol;
// 	  nb_of_successfull_Inflation++;
// 	}
//       }
// //       Kres._result = ONE_SOLUTION; //otherwise box will be subdivided!
//       if (_verbosity == STATS) cout << "number of successfull inflations: " << nb_of_successfull_Inflation << endl;
//       if (_verbosity == STATS) cout << "actual_inflated_box after inflation: " << endl;
//       if (_verbosity == STATS) cout << sol << endl;
//       return sol;
// }

// template < class I, class F >
// Matrix<I> subdivision_solver<I,F>::contractBox(Matrix<I> initial_box, unsigned long int steps){
//   
//   Matrix<I> res = initial_box;
//   unsigned long int i = 0;
//   bool stop = false;
//   
//   while ( (not stop) and (i < steps) ) {
//     Matrix<I> K = krawczyk_order_2<I,F> (res, _func, _jaco, _hess, _verbosity);
// //     struct contraction_result<I> Kres = contraction_test_without_epsInflation ( K, res, _verbosity);
//     struct contraction_result<I> Kres = contraction_test ( K, res, _verbosity);
//     if (Kres._result == KRAWCZIK_OK_ONE_SOLUTION) res = Kres._contraction;
//     else stop = true;
//     i++;
//   }
//   if (_verbosity == STATS) cout << "function contractBox: " << i-1 << " successfull contractions" << endl; 
//   return res;
// }

#endif
