//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#ifndef _OPERATION_H
#define _OPERATION_H

#include "matrix.h"
#include "list.h"

// using namespace std;
using namespace matrix;
  
template <class I>
struct intersection_result {
  bool _overlaps;
  bool _isIn;
  bool _isInInt;
  Matrix<I> _intersection;
};

//template arguments: I: interval F: floating point
// template <class I, class F>
template <class I>
Matrix<I> centerInterval ( const Matrix<I> &m ) {
  Matrix<I> res(m);
  for(int l = 0; l<res.getNbLines(); l++) 
    for (int c = 0; c<res.getNbColumns(); c++){
      res(l,c) = centerInterval( m(l,c) );
      //test
//       res(l,c) = I(res(l,c).get_left(),res(l,c).get_left());
    }
  return res;
}

template <class I, class F>
Matrix<F> centerFloat ( const Matrix<I> &m ) {
  Matrix<F> res(m.getNbLines(), m.getNbColumns());
  for(int l = 0; l<res.getNbLines(); l++) 
    for (int c = 0; c<res.getNbColumns(); c++)
      res(l,c) = centerFloat( m(l,c) );
  return res;
}

template <class I, class F>
Matrix<I> eps_inflation_box ( const Matrix<I> &m, const F &eps) {
  Matrix<I> res(m);
  F centerTemp;
  for(int l = 0; l<res.getNbLines(); l++) 
    for (int c = 0; c<res.getNbColumns(); c++) {
      centerTemp = centerFloat( m(l,c) );
      res(l,c) = centerTemp + ( m(l,c) - centerTemp ) * eps ;
      res(l,c) = inflate(res(l,c));
    }
  return res;
}

template <class I, class F>
Matrix<I> centerInterval_Disturbed ( const Matrix<I> &m, const F &eps) {
  Matrix<I> res(m);
//   F centerTemp;
  I centerTemp;
  for(int l = 0; l<res.getNbLines(); l++) 
    for (int c = 0; c<res.getNbColumns(); c++) {
      centerTemp = centerInterval( m(l,c) );
      res(l,c) = centerTemp + ( m(l,c) - centerTemp ) * eps ;
      res(l,c) = interior(res(l,c));
//       cout << "interior(): " << res(l,c) << ", is empty? " << is_empty_prec(res(l,c)) << endl;
      if ( not is_empty_prec(res(l,c)) )
        res(l,c) = alea_interval_point(res(l,c));
    }
  return res;
}

template <class I>
bool contains_zero( const Matrix<I> &m ) {
  for(int l = 0; l<m.getNbLines(); l++) 
    for (int c = 0; c<m.getNbColumns(); c++)
      if ( not contains_zero(m(l,c)) )
	return false;
  return true;
}

template <class I, class F>
F Min_absolute_diameter ( const Matrix<I> &m ) {
//   F res;
  F res = nan("");
  F temp;
  for(int l = 0; l<m.getNbLines(); l++) 
    for (int c = 0; c<m.getNbColumns(); c++)
      if ((l==0) and (c==0) )
	res = absolute_diameter(m(l,c));
      else {
	temp = absolute_diameter(m(l,c));
	res = MIN( res, temp );
      }
  return res;
}

template <class I, class F>
F Max_absolute_diameter ( const Matrix<I> &m ) {
//   F res;
  F res = nan("");
  F temp;
  for(int l = 0; l<m.getNbLines(); l++) 
    for (int c = 0; c<m.getNbColumns(); c++)
      if ((l==0) and (c==0) )
	res = absolute_diameter(m(l,c));
      else {
	temp = absolute_diameter(m(l,c));
	res = MAX( res, temp );
      }
  return res;
}

template <class I, class F>
F relative_diameter ( const Matrix<I> &m ) {
//   F res;
  F res = nan("");
  F temp;
  for(int l = 0; l<m.getNbLines(); l++) 
    for (int c = 0; c<m.getNbColumns(); c++)
      if ((l==0) and (c==0) )
	res = relative_diameter(m(l,c));
      else {
	temp = relative_diameter(m(l,c));
	res = MAX( res, temp );
      }
  return res;
}

template <class I, class F>
Matrix<I> Interval_hull ( const Matrix<I> &m, const Matrix<I> &n ) {
//   F res;
  Matrix<I> res(m);
  for(int l = 0; l<m.getNbLines(); l++) 
    for (int c = 0; c<m.getNbColumns(); c++)
      res(l,c) = hull(m(l,c),n(l,c));
  return res;
}

template <class I>
struct intersection_result<I> overlaps ( const Matrix<I> &X, const Matrix<I> &Y ) {
  
//   cout << "*************overlaps: " << endl;
//   cout << "X: " << X << endl;
//   cout << "Y: " << Y << endl;
    
  struct intersection_result<I> res;
  I x,y;
  if ( not ( (X.getNbLines()==Y.getNbLines()) and (X.getNbColumns()==Y.getNbColumns()) ) ) {
    res._overlaps = false;
    res._isIn = false;
    res._isInInt = false;
    res._intersection = Matrix<I>(0,0);
    return res;
  }
  res._overlaps = true;
  res._isIn = true;
  res._isInInt = true;
  res._intersection = Matrix<I>(X);
  for(int l = 0; l<X.getNbLines(); l++) 
    for (int c = 0; c<X.getNbColumns(); c++) {
      x = X(l,c);
      y = Y(l,c);
//       cout << "l,c, x, y: " << l << ", " << c << ", " << x << ", " << y << endl;
//       cout << "intersection: " << intersect(x,y) << endl;
      res._intersection(l,c) = intersect(x,y);
      res._overlaps = not is_empty_i( res._intersection(l,c)); //changed
//       res._overlaps = not is_empty_prec(res._intersection(l,c)); //changed
      res._isIn = res._overlaps and res._isIn and is_in(x, y);
      res._isInInt = res._overlaps and res._isIn and res._isInInt and is_strictly_in(x, y);
      /*if (res._overlaps) 
	res._intersection(l,c) = intersect(x, y);
      else*/ 
      if (not res._overlaps) 
	return res; 
    }
  return res;
}

template <class I, class F>
bool containsBorder ( const Matrix<I> &m, const Matrix<I> &n ) {
  struct intersection_result<I> res = overlaps<I>(m,n);
  return (res._overlaps) and not (res._isInInt);  
}

// #define COMPLEMENTARY_CUTED 1
// #define COMPLEMENTARY_UNCUTED 0
template <class I>
int complementary( const Matrix<I> &X, const Matrix<I> &Y, cpp_tools::list< Matrix<I> > &comp ) { //complementary of X in Y
  
//   int res=0;
  I x,y,z;
  Matrix<I> Yc=Y;
  
//   if ( not ( (X.getNbLines()==Y.getNbLines()) and (X.getNbColumns()==Y.getNbColumns()) ) ) {
//     return OTHER; //not the best one can hope...
//   }
  for(int l = 0; l<X.getNbLines(); l++){
    x = X(l,0);
    y = Y(l,0);
    z = intersect(x,y);
//     if (is_empty(z)) { // no intersection between X and Y, return Y
    if (is_empty_prec(z)) { // no intersection between X and Y, return Y
      comp.clear();
      return 0;
    }
    else {
      Matrix<I> resM1 = Yc, resM2 = Yc;
      if ( z.get_left() > y.get_left() ) {
	resM1(l,0) = I(y.get_left(), z.get_left());
	comp.push_back(resM1);
      }
      if ( z.get_right() < y.get_right() ) {
	resM2(l,0) = I(z.get_right(), y.get_right());
	comp.push_back(resM2);
      }
      Yc(l,0) = z;
    }
  }
  return 1;
}

template <class I>
bool is_empty_prec ( const Matrix<I> &X ) {
  int i=0;
  while ( i < X.getNbLines() ) {
    if (is_empty_prec(X(i,0))) 
      return true;
    i++;
  }
  return false;
}

#endif
