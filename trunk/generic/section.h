//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#ifndef _SECTION_H
#define _SECTION_H 

#include "matrix.h"
#include "list.h"

/// return a list containing sub boxes of m !!! no control of size
template <class I, class F>
cpp_tools::list< Matrix<I> > subdivide_box( const Matrix<I> &m );

template <class I, class F>
cpp_tools::list< Matrix<I> > subdivide_Maximal_width( const Matrix<I> &m );

template <class I, class F>
cpp_tools::list< Matrix<I> > subdivide_Maximal_smear( const Matrix<I> &m, const Matrix<I> &jacm );

template <class I, class F>
cpp_tools::list< Matrix<I> > subdivide_Maximal_smear_limited( const Matrix<I> &m, const Matrix<I> &jacm, const F min_width );

///functions
template <class I, class F>
cpp_tools::list< Matrix<I> > subdivide_box( const Matrix<I> &m ) {
  cpp_tools::list< Matrix<I> > res;
  res.clear();
  res.push_back( m );
  int n = m.getNbLines();
  for (int i=0; i<n; i++ ) {
    int jmax = 0x1 << i;
    for (int j=0;j<jmax; j++) {
      Matrix<I> temp1, temp2;
      temp1 = res.front();
      temp2 = res.pop_front();
      
      bisect( &temp1(i,0), &temp2(i,0), m(i,0) );
      
      res.push_back(temp1);
      res.push_back(temp2);
    }
  }
  return res;
}

template <class I, class F>
cpp_tools::list< Matrix<I> > subdivide_Maximal_width( const Matrix<I> &m ) {
  
  cpp_tools::list< Matrix<I> > res;
  res.clear();
  Matrix<I> temp1, temp2;
  int n = m.getNbLines();
  int i = 0;
  F max_diameter, actual_diameter;
  max_diameter = absolute_diameter( m(i,0) );
  for (int j=1; j<n; j++) {
    actual_diameter = absolute_diameter( m(j,0) );
    if (actual_diameter > max_diameter) {
      max_diameter = actual_diameter;
      i = j;
    }
  }
  temp1 = m;
  temp2 = m;
  bisect( &temp1(i,0), &temp2(i,0), m(i,0) );
  res.push_back(temp1);
  res.push_back(temp2);
  return res;
}

template <class I, class F>
cpp_tools::list< Matrix<I> > subdivide_Maximal_smear( const Matrix<I> &m, const Matrix<I> &jacm ) {
  
  cpp_tools::list< Matrix<I> > res;
  res.clear();
  Matrix<I> temp1, temp2;
  int R = m.getNbLines();
  int C = jacm.getNbLines();
  int r = 0, c = 0, i = 0;
  F max_smear, actual_smear, diameter;
  
  diameter = absolute_diameter( m(r,0) );
  max_smear = magnitude( jacm(c,r) )*diameter;
  
  for (r = 0; r<R; r++){
    diameter = absolute_diameter( m(r,0) );
    for (c = 0; c<C; c++){
      actual_smear = magnitude( jacm(c,r) )*diameter;
      if (actual_smear > max_smear) {
	max_smear = actual_smear;
	i = r;
      }
    }
  }
  temp1 = m;
  temp2 = m;
  bisect( &temp1(i,0), &temp2(i,0), m(i,0) );
  res.push_back(temp1);
  res.push_back(temp2);
  return res;
}

template <class I, class F>
cpp_tools::list< Matrix<I> > subdivide_Maximal_smear_limited( const Matrix<I> &m, const Matrix<I> &jacm, const F min_width ) {
  
  cpp_tools::list< Matrix<I> > res;
  res.clear();
  Matrix<I> temp1, temp2;
  int R = m.getNbLines();
  int C = jacm.getNbLines();
  int r = 0, c = 0, i = 0;
  F max_smear, actual_smear, diameter;
  
//   cout << "subdivide_Maximal_smear_limited: min_width = " <<  min_width << endl;
//   cout << "input box: " << m << endl;
  
  diameter = absolute_diameter( m(r,0) );
  while ( ( not (diameter > min_width)) and r<R-1 ) { //go to the first coordinate such that diameter > min_width
    r++;
    diameter = absolute_diameter( m(r,0) );
  }
  if ((r == R-1) and ( not (diameter > min_width))) {
    res.push_back(m); //no appropriate coordinate
//     cout << "box too small" << endl;
  }
  else {
    max_smear = magnitude( jacm(c,r) )*diameter; //compute smear value
    i = r; // coordinate of maximum smear
    
    for ( ; r<R; r++){
      diameter = absolute_diameter( m(r,0) );
//       cout << "r:  " << r << ", diameter: " << diameter << endl;
      if (diameter > min_width) {
	for (c = 0; c<C; c++){
	  actual_smear = magnitude( jacm(c,r) )*diameter;
	  if (actual_smear > max_smear) {
	    max_smear = actual_smear;
	    i = r;
	  }
	}
      }
    }
    
//     cout << "choosen coordinate: " << i << endl;
    temp1 = m;
    temp2 = m;
    bisect( &temp1(i,0), &temp2(i,0), m(i,0) );
    res.push_back(temp1);
    res.push_back(temp2);
  }
//   cout << "size of result: " << res.size() << endl;
  return res;
}

#endif