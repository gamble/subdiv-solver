//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////


#ifndef _BOX_H
#define _BOX_H

#include "../base/base.h"
#include "../base/parameters.h"
#include "matrix.h"
#include "list.h"
#include "operation.h"
#include "section.h"

// using namespace std;
using namespace matrix;

template <class I, class F> class Box;

template <class I, class F>
std::ostream &operator<<(std::ostream& out, const Box<I,F>& b) {
  out << b._m;
  if (b._midm)  out << endl << "midm"  << *(b._midm);
  if (b._infm)  out << endl << "infm"  << *(b._infm);
  if (b._fmidm) out << endl << "fmidm" << *(b._fmidm);
  if (b._jmidm) out << endl << "jmidm" << *(b._jmidm);
  if (b._jinfm) out << endl << "jinfm" << *(b._jinfm);
  if (b._hinfm) out << endl << "hinfm" << *(b._hinfm);
//   out << endl << "is_empty: " << is_empty(b(0,0)) << endl;
//   out << endl << "is_empty_prec: " << is_empty_prec<I,F>(b) << endl;
  return out;
}

template <class I, class F>
Matrix<I> *centerInterval ( Box<I,F> &b );

template <class I, class F>
Matrix<F> centerFloat ( const Box<I,F> &b );

template <class I, class F>
Matrix<I> *eps_inflation_box ( Box<I,F> &b,  const F &eps = EPS_INFLATION_DEFAULT_VALUE);

template <class I, class F>
bool contains_zero( const Box<I,F> &b );

template <class I, class F>
F Min_absolute_diameter( const Box<I,F> &b );

template <class I, class F>
Matrix<I> *evaluate_fmid ( Box<I,F> &b,  Matrix<I> (*Func) (Matrix<I>));

template <class I, class F>
Matrix<I> *evaluate_jmid ( Box<I,F> &b,  Matrix<I> (*Jaco) (Matrix<I>));

template <class I, class F>
Matrix<I> *evaluate_jinf ( Box<I,F> &b,  Matrix<I> (*Jaco) (Matrix<I>));

template <class I, class F>
Matrix<I> *evaluate_hinf ( Box<I,F> &b,  Matrix<I> (*Hess) (Matrix<I>));

template <class I, class F>
cpp_tools::list< Box<I,F> *> subdivide_box( const Box<I,F> &b );

template <class I, class F>
cpp_tools::list< Box<I,F> *> subdivide_Maximal_smear( const Box<I,F> &b, const Matrix<I> &jacm, bool minWidthUserDefined, const F min_width );

template <class I, class F>
int complementary( const Matrix<I> &X, const Box<I,F> &Y, cpp_tools::list< Box<I,F> *> & comp ); //complementary of X in Y

template <class I, class F>
bool is_empty_prec( const Box<I,F> &b );

template <class I, class F>
class Box{
  public:
    Box( );
    Box( const Matrix<I>& m );
    ~Box();
    
    ///getter
    Matrix<I> get_m() { return _m; }
    Matrix<I>* get_m_addr() { return &_m; }
    Matrix<I>* get_midm()  { return _midm; }
    Matrix<I>* get_infm() { return _infm; }
    Matrix<I>* get_fmidm() { return _fmidm; }
    Matrix<I>* get_jmidm() { return _jmidm; }
    Matrix<I>* get_jinfm() { return _jinfm; }
    Matrix<I>* get_hinfm() { return _hinfm; }
    Matrix<I>* get_ford0() { return _ford0; }
    Matrix<I>* get_ford1() { return _ford1; }
    Matrix<I>* get_ford2() { return _ford2; }
    int getDim() {return _m.getNbLines(); }
    I operator()(int i, int j) const { return _m(i,j); }
    
    ///setter
    void set_ford2(Matrix<I>* ford2) { if (_ford2) delete _ford2; _ford2 = ford2;  }
    void set_ford1(Matrix<I>* ford1) { if (_ford1) delete _ford1; _ford1 = ford1;  }
    void set_ford0(Matrix<I>* ford0) { if (_ford0) delete _ford0; _ford0 = ford0;  }
    
    friend std::ostream& operator<< <I,F>(std::ostream& out, const Box<I,F>& b);
    
    //main operations
    friend Matrix<I> *        centerInterval<I,F>          ( Box<I,F> &b );
    friend Matrix<F>          centerFloat<I,F>             ( const Box<I,F> &b );
    friend Matrix<I> *        eps_inflation_box<I,F>       ( Box<I,F> &b,  const F &eps);
    friend bool               contains_zero<I,F>           ( const Box<I,F> &b );
    friend Matrix<I> *        evaluate_fmid<I,F>           ( Box<I,F> &b,  Matrix<I> (*Func) (Matrix<I>));
    friend Matrix<I> *        evaluate_jmid<I,F>           ( Box<I,F> &b,  Matrix<I> (*Jaco) (Matrix<I>));
    friend Matrix<I> *        evaluate_jinf<I,F>           ( Box<I,F> &b,  Matrix<I> (*Jaco) (Matrix<I>));
    friend Matrix<I> *        evaluate_hinf<I,F>           ( Box<I,F> &b,  Matrix<I> (*Hess) (Matrix<I>));
    friend F                  Min_absolute_diameter<I,F>   ( const Box<I,F> &b );
    friend cpp_tools::list< Box<I,F> *> subdivide_box<I,F> ( const Box<I,F> &b );
    friend cpp_tools::list< Box<I,F> *> subdivide_Maximal_smear<I,F>( const Box<I,F> &b, const Matrix<I> &jacm, bool minWidthUserDefined, const F min_width );
    friend int                complementary<I,F>           ( const Matrix<I> &X, const Box<I,F> &Y, cpp_tools::list< Box<I,F> *> &comp );
    friend bool               is_empty_prec<I,F>           ( const Box<I,F> &Y );
    
  private:
    Matrix<I>  _m;
    Matrix<I>* _midm;
    Matrix<I>* _infm;
    Matrix<I>* _fmidm;
    Matrix<I>* _jmidm;
    Matrix<I>* _jinfm;
    Matrix<I>* _hinfm;
    Matrix<I>* _ford0;
    Matrix<I>* _ford1;
    Matrix<I>* _ford2;
};

template <class I, class F>
Box<I,F>::Box() {
  _midm  = NULL;
  _infm  = NULL;
  _fmidm = NULL;
  _jmidm = NULL;
  _jinfm = NULL;
  _hinfm = NULL;
  _ford0 = NULL;
  _ford1 = NULL;
  _ford2 = NULL;
}

template <class I, class F>
Box<I,F>::Box( const Matrix<I> &m ) {
  _m = m;
  _midm  = NULL;
  _infm  = NULL;
  _fmidm = NULL;
  _jmidm = NULL;
  _jinfm = NULL;
  _hinfm = NULL;
  _ford0 = NULL;
  _ford1 = NULL;
  _ford2 = NULL;
}

template <class I, class F>
Box<I,F>::~Box( ) {
  if (_midm)  delete _midm;
  if (_infm)  delete _infm;
  if (_fmidm) delete _fmidm;
  if (_jmidm) delete _jmidm;
  if (_jinfm) delete _jinfm;
  if (_hinfm) delete _hinfm;
  if (_ford0) delete _ford0;
  if (_ford1) delete _ford1;
  if (_ford2) delete _ford2;
//   inc_nbBoxesDeleted ();
}

template <class I, class F>
Matrix<I> *centerInterval ( Box<I,F> &b ) {
  if (!b._midm)
    b._midm = new Matrix<I>( centerInterval<I> (b._m) );
  return b._midm;
}

template <class I, class F>
Matrix<F> centerFloat ( const Box<I,F> &b ) {
  return centerFloat<I,F> ( b._m );
}

template <class I, class F>
Matrix<I> *eps_inflation_box ( Box<I,F> &b,  const F &eps){
  if (!b._infm)
    b._infm = new Matrix<I>( eps_inflation_box<I,F> ( b._m,  eps) );
  return b._infm;
}

template <class I, class F>
bool contains_zero( const Box<I,F> &b ) {
  return contains_zero<I>(b._m);
}

template <class I, class F>
F Min_absolute_diameter( const Box<I,F> &b ){
  return Min_absolute_diameter<I,F>(b._m);
}

template <class I, class F>
Matrix<I> *evaluate_fmid ( Box<I,F> &b,  Matrix<I> (*Func) (Matrix<I>)){
  if (!b._fmidm)
    b._fmidm = new Matrix<I>( Func ( *b._midm ) );
  return b._fmidm;
}

template <class I, class F>
Matrix<I> *evaluate_jmid ( Box<I,F> &b,  Matrix<I> (*Jaco) (Matrix<I>)){
  if (!b._jmidm)
    b._jmidm = new Matrix<I>( Jaco ( *b._midm ) );
  return b._jmidm;
}

template <class I, class F>
Matrix<I> *evaluate_jinf ( Box<I,F> &b,  Matrix<I> (*Jaco) (Matrix<I>)){
  if (!b._jinfm)
    b._jinfm = new Matrix<I>( Jaco ( *b._infm ) );
  return b._jinfm;
}

template <class I, class F>
Matrix<I> *evaluate_hinf ( Box<I,F> &b,  Matrix<I> (*Hess) (Matrix<I>)){
  if (!b._hinfm)
    b._hinfm = new Matrix<I>( Hess ( *b._infm ) );
  return b._hinfm;
}

template <class I, class F>
cpp_tools::list< Box<I,F> *> subdivide_box( const Box<I,F> &b ){
  cpp_tools::list< Box<I,F> *> res;
  cpp_tools::list< Matrix<I> > restemp = subdivide_box<I,F> ( b._m );
  while (restemp.size()) 
      res.push_back ( new Box<I,F> ( restemp.pop_front() ) );
  return res;
}

template <class I, class F>
cpp_tools::list< Box<I,F> *> subdivide_Maximal_smear( const Box<I,F> &b, const Matrix<I> &jacm, bool minWidthUserDefined, const F min_width ){
  cpp_tools::list< Box<I,F> *> res;
  if (not minWidthUserDefined) {
    cpp_tools::list< Matrix<I> > restemp = subdivide_Maximal_smear<I,F> ( b._m, jacm );
    while (restemp.size()) 
      res.push_back ( new Box<I,F> ( restemp.pop_front() ) );
  }
  else {
    cpp_tools::list< Matrix<I> > restemp = subdivide_Maximal_smear_limited<I,F> ( b._m, jacm, min_width );
    while (restemp.size()) 
      res.push_back ( new Box<I,F> ( restemp.pop_front() ) );
  }
  return res;
}

// #define COMPLEMENTARY_CUTED 1
// #define COMPLEMENTARY_UNCUTED 0

template <class I, class F>
int complementary( const Matrix<I> &X, const Box<I,F> &Y, cpp_tools::list< Box<I,F> *> &comp ){ //complementary of X in Y
  cpp_tools::list< Matrix<I> > comptemp;
  int res = complementary<I>( X, Y._m, comptemp );
  if ( res==COMPLEMENTARY_CUTED )
    while ( comptemp.size() ) 
      comp.push_back( new Box<I,F>( comptemp.pop_front() ) );
  return res;
}

template <class I, class F>
bool is_empty_prec( const Box<I,F> &b ){
  return is_empty_prec<I>(b._m);
}
#endif
