############################################################################
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
############################################################################


===========================================
||NOTES ON THE DESIGN OF SUBDIVISION_SOLVER||
===========================================

I/ Introduction
===============

The main particularities of subdivision_solver are:
1) it is designed to solve systems of large dense polynomials that are costly to evaluate
2) it works in multi-precision

These two characteristics have strongly influenced the architecture/design of subdivision_solver, in the following ways:

1) subdivision_solver uses the package fast_polynomial written by Guillaume Moroz that allows to compute Horner forms of multi-variate polynomials, and that provides functions to evaluate quickly the horner forms for a given arithmetic precision.
fast_polynomial is written in Cython and is available for SageMath.
A class of subdivision_solver, named system, takes as inputs the polynomials, compute their partial derivatives (that are necessary for the numeric method) and for a given precision, call fast_polynomial to compute the functions for fast evaluation.
This part of subdivision_solver is written in Cython.
Now, for a given precision, a Cython module named solver_xxxx takes as input a system, and call the solver (written in C++) by giving to it pointers on functions for fast evaluation. 

2) subdivision_solver uses two interval arithmetic libraries, depending on the arithmetic precision that is needed.
The boost interval library is used for double precision and MPFI is used for higher precision.
Note that MPFI allows to work with double precision, but is approximatly ten times slower that the boost interval library.
As a consequence, the C++ code implementing the subdivision solver itself is templated by the types  of floating points and intervals that are used.

Combination of 1) and 2) = complications for the compilation! 
The cython compiler does not accept (at least when the code was designed) templates.
This pitfall can be addressed by instantiating "by hands" the generic code for the different interval libraries.
Generic code is instantiated in dynamique libraries .so, and the prototypes in headers can be imported in cython.

II/compilation and installation
================================

The file subdivision_solver/Makefile contains directives to compile either some part, or all the package.

The most simple compilation and installation process is:
- in the folder subdivision_solver: make package
it will create in subdivision_solver/.. a sage package named subdivision_solver-x.x.x.spkg
- in subdivision_solver/..: sage subdivision_solver-x.x.x.spkg

It will compile and install all the package as a part of sage. 
The only drawback of a full compilation is the long times it takes.

Notice that mpfi and boost interval are already included in sage: the installation process described above does not require any installation of mpfi, mpfr or boost. However, compiling apart a part of the c++ code requires these library to be installed.
The only strong dependancy is fast_polyomial, that can be download on the webpage of Guillaume Moroz.
It is installed with the command: sage fast_polynomial-X.X.X.spkg

NOTE: do not use or modify the file subdivision_solver/Makefile_for_package: it is copied in the .spkg as Makefile.

III/ The C++ part 
================

The goal of the C++ part is to provide the class subdivision_solver that is the angular stone of the package subdivision solver.
Roughly speaking, for a given precision, this class takes as inputs pointers on functions (the fast evaluations of polynomials and derivatives) and a box (a vector of interval) and finds the solutions of the system.
This class is instantiated for the boost interval library and for mpfi.

0) the folder subdivision_solver/base
-------------------------------------

contains files base.h and parameters.h that defines some #include and some constant need by the algorithms.

1) the folders subdivision_solver/boost and subdivision_solver/mpfi
-------------------------------------------------------------------

It contains c++ wrappers for boost and mpfi intervals that are named boost_class and mpfi_class.
These classes have two attributes: the interval that is proper to each object, and the precision that is a static attributes.
For boost intervals, the precision is always 53 and is a const attribute.
For mpfi intervals, the precision can be arbitrarily increased, but at a given time, all mpfi intervals (and mpfr doubles) have the same precision.
The main advantage of this lies in the automatic management of the precision when creating intervals.

Note that there is also wrappers for floating points (mpfr and double).

2) the folder subdivision_solver/generic
-------------------------------------------

It contains functions and classes templated that are templated.

-list.h : implements lists. STL lists are not used because of a conflict arising with versions of standard libraries used by sage (that compiles cython code) and the one used by the C++ compiler.

-matrix.h: implements matrices, templated by the type of interval. A box will be a column vector of intervals.

-operation.h: provides basic operators for boxes (column vector of intervals): center, diameter, interval hulls, intersection, complement...

-section.h: implements four bisections with different strategies

-box.h: provides the class box.h that is mainly a column vector of intervals. This class allow to save certain values, in particular the evaluations of polynomials on the considered box.
Nothing is implemented here, it is just an interface.

-evaluation.h: implements evaluation of functions F:R^n -> R^n on matrices of intervals and elements of class box.
The evaluations at order 0, at order 1 (with first odrer derivatives) and at order 2 (with second order derivatives) are implemented.
The evaluation that is used to eliminate boxes is F_order_0 (X) \cap F_order_2(X) (F_order_2 is not evaluated if F_order_0 does not contain zero).

-contraction.h: implements Krawczyk operator for matrices of intervals and elements of class box.
"Krawczyk order 2" uses evaluation at order 1 to evaluate the jacobian matrix involved in the Krawczyk operator. It uses hessian matrices thats why it is called "order 2".

The evaluation of the Krawczyk operator requires to invert the jacobian matrix evaluated on the middle of the box. 
In rare cases, the latter matrix is not invertible. In this case, it is tried MAX_TRIALS_TO_INVERSE (see subdivision_solver/base/parameters.h) times to perturbate a little bit ( of quantity COEFF_PERTURBATION defined in subdivision_solver/base/parameters.h) the center, to re-evaluate the jacobian matrix and to invert it.
If this has not allowed to invert it, the box is either considered as a box where it is not possible to decide anything, and it will be returned as an indeterminated box, if the maximum precision has been reached, or a box that needs more arithmetic precision.

The criterion to increase precision using Krawczyk operator is during the evaluations of krawczyk operators.

-subdivision.h ->TODO: voir si ça correspod à un design pattern
defines the class subdivision_solver that implements the branch and bound solver for a fixed arithmetic precision.
The algorithm is described in the technical report. 

basic idea of the implementation: a subdivision_solver is constructed for a given function, with its jacobian and hessian matrices. Other arguments are optional. The caller can either give one initial domain (matrix of intervals) of fill the working queue.
The solver has one begin method, one next method that performs one step of branch of bound algorithm (i.e. it treats one box)
and one method end() that returns true if the process is terminated.

3) the folder subdivision_solver/instanciation
-----------------------------------------------

contains .h and .cpp that instantiates the generic code for Boost intervals and mpfi intervals.

See the code for more details!

III/ The Sage/Cython part:
===========================

It provides: wrappers for mpfi and boost intervals, interfaces for matrix of boost or mpfi intervals in subdivision_solver/subdivision_solver/matrix_interval, a class system that implements a system of polynomials and a class solver.

1) The folders subdivision_solver/subdivision_solver/mpfi and subdivision_solver/subdivision_solver/boost
---------------------------------------------------------------------------------------------------------
contains the wrappers for mpfi and boost intervals.

2) The folder subdivision_solver/subdivision_solver/matrix_interval
-------------------------------------------------------------------
contains cython interfaces for matrices of boost and mpfi intervals.

3) The folder subdivision_solver/subdivision_solver/system
------------------------------------------------------------
implements a system of polynomial equations in the file system.pxd and system.pxi (see the class systempols)
It is created by specifying n polynomials belonging to the same ring and the n variables of the ring.
Notice it is possible to give it non-polynomial functions, but... its is still a very experimental part of the solver.

At initialization, first and second order derivatives are computed.
One can also specify constraints of equality and dis-equality.

Now comes one of the tricky parts of the implementation.
The files eval_mpfi.pxd and eval_mpfi.pyx provide evaluations of functions and there derivatives as functions and NOT methods of class systempols.
First, the method init_eval computes the fast evaluations on mpfi intervals of polynomials and their derivatives of systempols.
The precision used is given to init_eval.
Then it gives to a global variable the value of the actual instance of class systempol.

Since it is not possible to give to the C++ functions methods but only pointers on functions, eval_mpfi.pyx defines functions corresponding to the evaluation of polynomials and derivatives. These functions use the global variable to get fast evaluations of polynomials of the actual system, to evaluate them and to return their values.

This trick is not visible outside this part of the code.

The files eval_boost.pxd and eval_boost.pyx do the same thing, but for boost arithmetic.

4) The folder subdivision_solver/subdivision_solver/solver
----------------------------------------------------------

It contains two interfaces, solver_boost.xxx and solver_mpfi.xxx: one for a solver using boost interval arithmetic and one for solver a solver using mpfi_interval arithmetic with fixed precision.
The file solver_mp.pyx provides a class that implements a multiprecision solver, as described in the technical report.