//////////////////////////////////////////////////////////////////////////////
//      Copyright (C) 2015 Remi Imbach                                      //
//                                                                          //
//    This file is part of subdivision_solver.                              //
// Licensed under GNU General Public License v3.                            //
//                                                                          //
// This program is free software: you can redistribute it and/or modify     //
// it under the terms of the GNU General Public License as published by     //
// the Free Software Foundation, either version 3 of the License, or        //
// (at your option) any later version.                                      //
//                                                                          //
// This program is distributed in the hope that it will be useful,          //
// but WITHOUT ANY WARRANTY; without even the implied warranty of           //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
// GNU General Public License for more details.                             //
//                                                                          //
// You should have received a copy of the GNU General Public License        //
// along with this program.  If not, see <http://www.gnu.org/licenses/>.    //
//                                                                          //
//////////////////////////////////////////////////////////////////////////////

#ifndef BOOSTXX_H
#define BOOSTXX_H

#include <boost/numeric/interval/detail/interval_prototype.hpp>
#include <boost/numeric/interval.hpp>
#include <iostream>
#include <math.h> 
using namespace std;

//for function alea_interval_point
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
typedef boost::minstd_rand base_generator_type;

#include <mpfi.h>
#include <mpfr.h>

#define ROUNDING_MODE GMP_RNDN

namespace cpp_tools{

typedef boost::numeric::interval<double> boostInterval_base1;
typedef boost::numeric::interval_lib::change_checking<boostInterval_base1, boost::numeric::interval_lib::checking_no_nan<double> >::type boostInterval_base2;
typedef boost::numeric::interval_lib::unprotect<boostInterval_base2>::type boostInterval;
// typedef boost::numeric::interval_lib::unprotect<boostInterval_base1>::type boostInterval;

typedef long mp_prec_t;

class boost_class
{
    public:
      boostInterval _value;
      static mp_prec_t const _prec = 53;
      //for function alea_interval_point
      //static attribute commom ta all element of class but not const
      static base_generator_type _generator;
      
      boost_class() { 
// 	_value = boostInterval();
      }
      
      boost_class(const mpfi_t x){
	_value = boostInterval(mpfr_get_d(&(x->left),ROUNDING_MODE), mpfr_get_d(&(x->right),ROUNDING_MODE));
      }
      
//       boost_class(mp_prec_t p) { 
//       }
      
      boost_class(const double left, const double right){
	_value = boostInterval(left, right);
      }
      
      boost_class(mp_prec_t p, const double left, const double right){
	_value = boostInterval(left, right);
      }
      
      boost_class(const boostInterval& x){
          _value = x;
      }
      
      boost_class(const boost_class& x){
          _value = x._value;
      }
      
      static mp_prec_t get_prec() {
	  return _prec;
      }
      
      double get_left() const {
	  return _value.lower();
      }
	
      double get_right() const {
	  return _value.upper();
      }
      
      ~boost_class(){}
      
      boost_class & operator= (const boost_class& x) {
	_value = x._value;
	return *this;
      }
      
      boost_class & operator= (const int& x) {
        _value = boostInterval ( (double) x, (double) x );
        return *this;
      }
      
      boost_class operator* (const boost_class& x) {
        return boost_class(_value*x._value);
      }
      
      boost_class operator* (const double& x) {
        return boost_class(_value*x);
      }
      
      boost_class operator+ (const boost_class& x) {
        return boost_class(_value+x._value);
      }
      
      boost_class operator+ (const double& x) {
        return boost_class(_value + x);
      }
      
      boost_class & operator+= (const boost_class& x) {
          _value+=x._value;
	  return *this;
      }
      
      boost_class operator- () {
        return boost_class(-_value);
      }
      
      boost_class operator- (const double& x) {
        return boost_class(_value-x);
      }
      
      boost_class operator- (const boost_class& x) {
        return boost_class(_value-x._value);
      }
      
      boost_class & operator-= (const boost_class& x) {
          _value-=x._value;
	  return *this;
      }
      
      boost_class operator/ (const boost_class& x) {
        return boost_class(_value/x._value);
      }
      
      boost_class & operator/= (const boost_class& x) {
          _value/=x._value;
	  return *this;
      }
};

mp_prec_t get_prec(const boost_class& i){
  return 53;
}

double absolute_diameter(const boost_class& x);

std::ostream &operator<<(std::ostream &out, const boost_class& m) {
  out.precision(20);
//   out << "[" << get_prec(m) << ", " << m._value.lower() << ", " << m._value.upper() << ", width: " << absolute_diameter(m) << "]";
  out << "[" << get_prec(m) << ", " << m._value.lower() << ", " << m._value.upper() << ", width: " << absolute_diameter(m) << ", next: " << nextafter(m.get_left(), m.get_right()) <<  "]";
  return out;
}

boost_class sqrt(const boost_class& x) {
  return boost_class(sqrt(x._value));
}

boost_class inverse(const boost_class& x){
  return boost_class(1./x._value);
}

boost_class centerInterval (const boost_class& x) {
  //to be verified
  typename boost::numeric::interval<double>::traits_type::rounding rnd;
  double reslower = rnd.sub_down( x._value.upper(), x._value.lower() );
  reslower = rnd.div_down( reslower, 2. );
  reslower = rnd.add_down( x._value.lower(), reslower );
  double resupper = rnd.sub_up( x._value.upper(), x._value.lower() );
  resupper = rnd.div_up( resupper, 2. );
  resupper = rnd.add_up( x._value.lower(), resupper );
  return boost_class(reslower, resupper);
}

double centerFloat (const boost_class& x) {
  return median(x._value);
}

int contains_zero(const boost_class& x){
  return zero_in(x._value);
}
//for inversion of matrices with width almost null 
bool iszero(const boost_class& x){
  return zero_in(x._value);
}

double absolute_diameter(const boost_class& x){
  return width(x._value);
}

double relative_diameter(const boost_class& x){
  double absolute = width(x._value);
  double denominateur = fmax(1.,fabs(median(x._value)));
  return absolute/denominateur;
}

boost_class intersect(const boost_class& x, const boost_class& y){
//   cout << "boost intersect: x, y : " << x << ", " << y << endl;
  return boost_class(intersect(x._value, y._value));
}

boost_class hull(const boost_class& x, const boost_class& y){
//   cout << "boost intersect: x, y : " << x << ", " << y << endl;
  return boost_class(hull(x._value, y._value));
}

int overlaps(const boost_class& x, const boost_class& y){
  return overlap(x._value, y._value);
}

int is_in(const boost_class& x, const boost_class& y){
  return subset(x._value, y._value);
}

int is_empty_i(const boost_class& x) {
    return empty(x._value);
}

int is_strictly_in(const boost_class& x, const boost_class& y){
  return proper_subset(x._value, y._value);
}

//arithmetic operators between boost and double
// boost_class operator* (const boost_class& x, const double& y) {
//   return boost_class( x._value*y );
// }

// boost_class IntervalMultByFloat(const boost_class& x, const double& y){
//   return boost_class( x._value*y );
// }

boost_class operator* (const double& y, const boost_class& x) {
  return boost_class( y*x._value );
}

// boost_class operator+ (const boost_class& x, const double& y) {
//   return boost_class( x._value + y );
// }

boost_class operator+ (const double& y, const boost_class& x) {
  return boost_class( y + x._value);
}

// boost_class IntervalAddToFloat(const boost_class& x, const double& y){
//   return boost_class( x._value + y );
// }

int bisect ( boost_class* resInf, boost_class* resSup, const boost_class& x){
  //do not use bisect in utility.hpp because it uses std::pair -> trouble when compiling with cython...
  *resInf = boost_class( (x._value).lower(), median(x._value) );
  *resSup = boost_class( median(x._value), (x._value).upper());
  return true;
}

double magnitude( const boost_class& x ){
  return norm( (x._value) );
}

double next( const double& d){
//   return nextafter(d, d+1);// false when ulp > 1...
  return nextafter(d, +INFINITY);
}

double prev( const double& d){
//   cout << "prev! d: " << d << " -INF: " << -INFINITY << " nextafter(d, -INF): " << nextafter(d, -INFINITY) << endl; 
//   return nextafter(d, d-1); // false when ulp > 1...
  return nextafter(d, -INFINITY);
}

bool is_empty_prec (const boost_class& x) {
  return not(x.get_right() > nextafter(x.get_left(), x.get_right()));
  
}

boost_class interior ( const boost_class& x ){
  return boost_class (next(x.get_left()),prev(x.get_right()));
}

boost_class inflate ( const boost_class& x ){
  return boost_class (prev(x.get_left()),next(x.get_right()));
}

boost_class alea_interval_point ( const boost_class& x ){
  if (x.get_left()==x.get_right())
    return x;
  boost::uniform_real<> uni_dist(x.get_left(),x.get_right());
  boost::variate_generator<base_generator_type&, boost::uniform_real<> > uni(x._generator, uni_dist);
  double temp = uni();
  return boost_class(temp,temp);
}

}

///DEPRECATED
// namespace cpp_tools{
  
// typedef long mp_prec_t;

// typedef boost::numeric::interval_lib::unprotect<boost::numeric::interval<double> >::type boostInterval;
// mp_prec_t get_prec(const boostInterval& i){
//   return 53;
// }
// 
// std::ostream &operator<<(std::ostream &out, const boostInterval& m) {
//   out.precision(20);
//   out << "[" << get_prec(m) << ", " << m.lower() << ", " << m.upper() << "]";
//   return out;
// }
// 
// boostInterval center(boostInterval i){
//   return median(i);
// }
// 
// void assignPtr(boostInterval *destination, boostInterval source){
//   *destination = source;
// }
// 
// boostInterval inverse(boostInterval i){
//   return boostInterval(1)/i;
// }
// 
// int contains_zero(boostInterval i){
//   return zero_in(i);
// }
// //for inversion of matrices with width almost null 
// bool iszero(const boostInterval& i){
//   return zero_in(i);
// }
// 
// boostInterval diameter_cpp(boostInterval i){
//   return width(i);
// }
// 
// int overlaps_cpp(boostInterval x, boostInterval y){
//   return overlap(x, y);
// }
// 
// int is_in(boostInterval x, boostInterval y){
//   return subset(x, y);
// }
// 
// int is_strictly_in(boostInterval x, boostInterval y){
//   return proper_subset(x, y);
// }
// 
// boostInterval intersect_cpp(boostInterval x, boostInterval y){
//   return intersect(x, y);
// }
// 
// // ############################################################################dans #include <boost/numeric/interval/compare/certain.hpp>
// // template<class T, class Policies1, class Policies2> inline
// // bool operator>(const interval<T, Policies1>& x, const interval<T, Policies2>& y)
// // {
// //   if (detail::test_input(x, y)) throw comparison_error();
// //   return x.lower() > y.upper();
// // }
// 
// // ############################################################################dans #include <boost/numeric/interval/utility.hpp>
// // template<class T, class Policies> inline
// // T width(const interval<T, Policies>& x)
// // {
// //   if (interval_lib::detail::test_input(x)) return static_cast<T>(0);
// //   typename Policies::rounding rnd;
// //   return rnd.sub_up(x.upper(), x.lower());
// // }
// 
// // template<class T, class Policies> inline
// // bool subset(const interval<T, Policies>& x,
// //             const interval<T, Policies>& y)
// // {
// //   if (empty(x)) return true;
// //   return !empty(y) && y.lower() <= x.lower() && x.upper() <= y.upper();
// // }
// // 
// // template<class T, class Policies1, class Policies2> inline
// // bool proper_subset(const interval<T, Policies1>& x,
// //                    const interval<T, Policies2>& y)
// // {
// //   if (empty(y)) return false;
// //   if (empty(x)) return true;
// //   return y.lower() <= x.lower() && x.upper() <= y.upper() &&
// //          (y.lower() != x.lower() || x.upper() != y.upper());
// // }
// // 
// // template<class T, class Policies1, class Policies2> inline
// // bool overlap(const interval<T, Policies1>& x,
// //              const interval<T, Policies2>& y)
// // {
// //   if (interval_lib::detail::test_input(x, y)) return false;
// //   return (x.lower() <= y.lower() && y.lower() <= x.upper()) ||
// //          (y.lower() <= x.lower() && x.lower() <= y.upper());
// // }
// 
// // template<class T, class Policies> inline
// // interval<T, Policies> intersect(const interval<T, Policies>& x,
// //                                 const interval<T, Policies>& y)
// // {
// //   BOOST_USING_STD_MIN();
// //   BOOST_USING_STD_MAX();
// //   if (interval_lib::detail::test_input(x, y))
// //     return interval<T, Policies>::empty();
// //   const T& l = max BOOST_PREVENT_MACRO_SUBSTITUTION(x.lower(), y.lower());
// //   const T& u = min BOOST_PREVENT_MACRO_SUBSTITUTION(x.upper(), y.upper());
// //   if (l <= u) return interval<T, Policies>(l, u, true);
// //   else        return interval<T, Policies>::empty();
// // }
// 
// }

#endif