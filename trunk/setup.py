import os
import sys
import shutil

from Cython.Build  import cythonize
from distutils.core import setup
from distutils.sysconfig import get_python_lib
from distutils.extension import Extension
from Cython.Distutils import build_ext
#import Cython.Compiler.Options
#Cython.Compiler.Options.annotate = True

if not 'SAGE_ROOT' in os.environ:
    print " ERROR: The environment variable SAGE_ROOT must be defined."
    sys.exit(1)
else:
    SAGE_ROOT = os.environ['SAGE_ROOT']
    SAGE_LOCAL = os.environ['SAGE_LOCAL']

#PACKAGES_SAGE = SAGE_ROOT + "/local/lib/python2.7/site-packages/"
PACKAGES_SAGE = SAGE_LOCAL + "/lib/python2.7/site-packages/"
SOLVER_DIR = PACKAGES_SAGE + "subdivision_solver/"
#SAGE_LIBS = SAGE_ROOT + "/local/lib/"
SAGE_LIBS = SAGE_LOCAL + "/lib/"

print SAGE_ROOT

COMP_OPT = "-O2"
CFLAGS = "-I./build/" 
#LDFLAGS = ""
LDFLAGS="-L" + SAGE_LIBS

setup(   
    cmdclass = {'build_ext': build_ext},
    name = 'subdivision_solver',
    version = '0.0.2',
    packages = ['subdivision_solver']+\
      ['subdivision_solver.matrix_interval']+\
      ['subdivision_solver.system']+\
      ['subdivision_solver.tracker']+\
      ['subdivision_solver.implicit_plot']+\
      ['subdivision_solver.plot']+\
      ['subdivision_solver.solver'],
    ext_modules = [\
		      
            Extension('subdivision_solver.matrix_interval.matrix_interval_mpfi',
                    ['subdivision_solver/matrix_interval/matrix_interval_mpfi.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cysignals'),
            ],
		    libraries = ['mpfr','mpfi','matrixmpfi'],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				    ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
		    ),\
		      
		    Extension('subdivision_solver.matrix_interval.matrix_interval_boost',
                    ['subdivision_solver/matrix_interval/matrix_interval_boost.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cysignals'),
            ],
		    libraries = ['mpfr','mpfi','matrixboost'],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				    ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
		    ),\
                
            Extension('subdivision_solver.tracker.tracker_boost',
                    ['subdivision_solver/tracker/tracker_boost.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cysignals'),
            ],
		    libraries = ['mpfr','mpfi','matrixboost'],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				    ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
		    ),\
                
            Extension('subdivision_solver.implicit_plot.solution_boost',
                    ['subdivision_solver/implicit_plot/solution_boost.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cysignals'),
            ],
		    libraries = ['mpfr','mpfi','matrixmpfi','matrixboost', 'solvermpfi', 'solverboost'],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				    ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
		    ),\
                
            Extension('subdivision_solver.implicit_plot.trackerTopo_boost',
                    ['subdivision_solver/implicit_plot/trackerTopo_boost.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cysignals'),
            ],
		    libraries = ['mpfr','mpfi','matrixmpfi','matrixboost', 'solvermpfi', 'solverboost'],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				    ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
		    ),\
                
            Extension('subdivision_solver.implicit_plot.implicit_plot_boost',
                    ['subdivision_solver/implicit_plot/implicit_plot_boost.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cysignals'),
            ],
		    libraries = ['mpfr','mpfi','matrixmpfi','matrixboost', 'solvermpfi', 'solverboost'],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				    ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
		    ),\
                
            Extension('subdivision_solver.plot.plot_boost',
                    ['subdivision_solver/plot/plot_boost.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cysignals'),
            ],
		    libraries = ['mpfr','mpfi','matrixmpfi','matrixboost', 'solvermpfi', 'solverboost'],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				    ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
		    ),\
		      
		    Extension('subdivision_solver.system.system',
                    ['subdivision_solver/system/system.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
		      ],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				    ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
		    ),\
		      
                    Extension('subdivision_solver.system.eval_mpfi',
                    ['subdivision_solver/system/eval_mpfi.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
                    os.path.join(SAGE_ROOT, PACKAGES_SAGE)
		      ],
		    libraries = ['mpfr','mpfi','matrixmpfi'],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				    ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
                    ),\
		      
                    Extension('subdivision_solver.system.eval_boost',
                    ['subdivision_solver/system/eval_boost.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
                    os.path.join(SAGE_ROOT, PACKAGES_SAGE)
		      ],
		    libraries = ['mpfr','mpfi','matrixboost'],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				    ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
                    ),\
		      
		    Extension('subdivision_solver.solver.solver_mpfi',
                    ['subdivision_solver/solver/solver_mpfi.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cysignals'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','ext'),
                    os.path.join(SAGE_ROOT, PACKAGES_SAGE)
                    ],
		    libraries = ['mpfr','mpfi','matrixmpfi', 'solvermpfi'],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				   ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
                    ),\
		    
                    Extension('subdivision_solver.solver.solver_boost',
                    ['subdivision_solver/solver/solver_boost.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cysignals'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','ext'),
                    os.path.join(SAGE_ROOT, PACKAGES_SAGE)
                    ],
		    libraries = ['mpfr','mpfi','matrixboost', 'solverboost'],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				   ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
                    ),\
		      
		    Extension('subdivision_solver.solver.solver_mp',
                    ['subdivision_solver/solver/solver_mp.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cysignals'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
                    os.path.join(SAGE_ROOT, PACKAGES_SAGE)
                    ],
		    libraries = ['mpfr','mpfi','matrixmpfi','matrixboost', 'solvermpfi', 'solverboost' ],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				   ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
                    ),\
                    Extension('subdivision_solver.solver.solver_mp_allprec',
                    ['subdivision_solver/solver/solver_mp_allprec.pyx'],
                    language="c++",
                    include_dirs = [
                    os.path.join(SAGE_LOCAL, 'include','csage'),
                    os.path.join(SAGE_ROOT, 'src','sage', 'ext'),
                    os.path.join(SAGE_ROOT, 'src'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','cpython'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','sage','libs','ntl'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cypari2'),
                    os.path.join(SAGE_LIBS, 'python2.7','site-packages','cysignals'),
                    os.path.join(SAGE_ROOT, PACKAGES_SAGE)
                    ],
		    libraries = ['mpfr','mpfi','matrixmpfi', 'solvermpfi' ],
                    library_dirs = [os.path.join(SAGE_LIBS),
				    os.path.join(SAGE_ROOT, '.'),
				   ],
                    extra_compile_args=[CFLAGS, COMP_OPT],
                    extra_link_args=[LDFLAGS],
                    )\
                  ]
)
		    
if not os.path.exists(SOLVER_DIR + "build"):
  os.mkdir(SOLVER_DIR + "build")
  
#shutil.copy("./build/libmatrixmpfi.so",  SOLVER_DIR + "build/libmatrixmpfi.so" )
#shutil.copy("./build/libsolvermpfi.so",  SOLVER_DIR + "build/libsolvermpfi.so" )
#shutil.copy("./build/libmatrixboost.so", SOLVER_DIR + "build/libmatrixboost.so" )
#shutil.copy("./build/libsolverboost.so", SOLVER_DIR + "build/libsolverboost.so" )

#if os.path.exists(SAGE_LIBS + "libmatrixmpfi.so"):
  #os.remove(SAGE_LIBS + "libmatrixmpfi.so")
#os.link(SOLVER_DIR + "build/libmatrixmpfi.so",  SAGE_LIBS + "libmatrixmpfi.so")
  
#if os.path.exists(SAGE_LIBS + "libmatrixboost.so"):
  #os.remove(SAGE_LIBS + "libmatrixboost.so")
#os.link(SOLVER_DIR + "build/libmatrixboost.so", SAGE_LIBS + "libmatrixboost.so")
  
#if os.path.exists(SAGE_LIBS + "libsolvermpfi.so"):
  #os.remove(SAGE_LIBS + "libsolvermpfi.so")
#os.link(SOLVER_DIR + "build/libsolvermpfi.so",  SAGE_LIBS + "libsolvermpfi.so")

#if os.path.exists(SAGE_LIBS + "libsolverboost.so"):
  #os.remove(SAGE_LIBS + "libsolverboost.so")
#os.link(SOLVER_DIR + "build/libsolverboost.so", SAGE_LIBS + "libsolverboost.so")

#pour compiler: sage setup.py build_ext --inplace
