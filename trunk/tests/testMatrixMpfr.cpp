#include "../mpfi/mpfrxx.hpp"
#include "../generic/matrix.h" 
#include <iostream>

using namespace std;
using namespace matrix;
using namespace cpp_tools;

typedef Matrix<mpfr_class> matrix_float;

int main() {
  
  mp_prec_t prec = 113;
  
  mpfr_class Tab[4] = { mpfr_class(prec, (long double)0.), mpfr_class(prec, (long double)1.), mpfr_class(prec, (long double)2.), mpfr_class(prec, (long double)3.) };
 
  matrix_float M(2,2,Tab);
  
  cout << "M: " << M << endl;
  
  mpfr_class f1(prec, (long double)0.);
  mpfr_class f2(prec, (long double)1.);
  
  cout << "f1 < f2 ? " << (f2<f2) << endl;
  
  return 0; 
}