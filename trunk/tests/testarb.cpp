// #include "../base/base.h"
// 
// #include "../arb/arfxx.hpp" 
// #include "../arb/arbxx.hpp" 
#include <stdio.h> 
#include <arf.h>
#include <arb.h>
// using namespace cpp_tools;

// long arf_class::_prec = 53;
// long arb_class::_prec = 53;

int main() {
  
//   arf_class i0;
//   arf_class i1(10);
//   cout << "i1: " << i1 << endl; 
//   i0 = i1;
//   cout << "i0: " << i0 << endl;
//   i0 = 1;
//   cout << "i0: " << i0 << endl;
//   i0 = 2.;
//   cout << "i0: " << i0 << endl;
//   cout << "i0*i1: " << i0*i1 << endl;
//   cout << "i0+i1: " << i0+i1 << endl;
//   i0+=i1;
//   cout << "i0: " << i0 << endl;
//   cout << "i1: " << i1 << endl;
//   i0=-i0;
//   cout << "i0: " << i0 << endl;
//   cout << "i0-i1: " << i0-i1 << endl;
//   i0-=i1;
//   cout << "i0: " << i0 << endl;
//   cout << "i1: " << i1 << endl;
//   cout << "i0/i1: " << i0/i1 << endl;
//   i0/=i1;
//   cout << "i0: " << i0 << endl;
//   cout << "i1: " << i1 << endl;
//   cout << "i0<i1: " << (i0<i1) << endl;
//   cout << "i0>i1: " << (i0>i1) << endl;
//   cout << "sqrt(i1): " << sqrt(i1) << endl;
//   cout << "iszero(i1): " << iszero(i1) << endl;
//   arf_class i2(0.);
//   cout << "iszero(i2): " << iszero(i2) << endl;
  
//   arb_class a0;
//   arb_class a1(1.,2.);
//   cout << "a1: " << a1 << endl; 
//   a0 = a1;
//   cout << "a0: " << a0 << endl;
//   a0 = 10;
//   cout << "a0: " << a0 << endl;
//   cout << "a0*a1: " << a0*a1 << endl;
//   cout << "a0+a1: " << a0+a1 << endl;
//   a0+=a1;
//   cout << "a0: " << a0 << endl;
//   cout << "a1: " << a1 << endl;
//   a0=-a0;
//   cout << "a0: " << a0 << endl;
//   cout << "a0-a1: " << a0-a1 << endl;
//   a0-=a1;
//   cout << "a0: " << a0 << endl;
//   cout << "a1: " << a1 << endl;
//   cout << "a0/a1: " << a0/a1 << endl;
  
  arf_t b0l, b0u, b1l, b1u, b2l, b2u; //lower and upper bounds of intervals
  arf_init(b0l); arf_init(b0u); arf_init(b1l); arf_init(b1u); arf_init(b2l); arf_init(b2u); //initialisation
  arf_set_si(b0l,-14); arf_set_si(b0u,-12); arf_set_si(b1l,1); arf_set_si(b1u,2); //[-14,-12] and [1,2]
  arb_t b0,b1,b2; //balls representing intervals
  arb_init(b0);arb_init(b1);arb_init(b2); //initialisation
  arb_set_interval_arf(b0, b0l, b0u, 53); //set the ball b0 to the interval [-14,-12]
  arb_set_interval_arf(b1, b1l, b1u, 53); //set the ball b1 to the interval [1,2]
  arb_div(b2,b0,b1,53); //set the ball b2 to the interval [-14,-12]/[1,2] -> should be [-14,-6] in a perfect world
  arb_get_interval_arf(b0l,b0u,b0,53); //get bounds of ball b0
  arb_get_interval_arf(b1l,b1u,b1,53); //get bounds of ball b0
  arb_get_interval_arf(b2l,b2u,b2,53); //get bounds of ball b2
  printf(" interval b0: [ "); arf_printd(b0l,10); printf(", "); arf_printd(b0u,10); printf("]\n");
  printf(" interval b1: [ "); arf_printd(b1l,10); printf(", "); arf_printd(b1u,10); printf("]\n");
  printf(" interval b2: [ "); arf_printd(b2l,10); printf(", "); arf_printd(b2u,10); printf("]\n");
  arf_t b3u; arf_init(b3u);
  arf_div(b3u,b0u,b1u,53,ARF_RND_NEAR);
  printf("b2u: "); arf_printd(b3u,10); printf("\n");
//   a0/=a1;
//   cout << "a0: " << a0 << endl;
//   cout << "a1: " << a1 << endl;
  
  return 0;
  
}