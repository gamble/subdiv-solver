#include "../base/base.h"
#include "../build/matrix_mpfi.h"

#include <iostream>

typedef interval_mpfi::interval interval;
typedef interval_mpfi::matrix_interval matrix_interval;

// mp_prec_t mpfi_class::_prec = 53;
// mp_prec_t mpfr_class::_prec = 53;

int main() {

//   mpfr_class ratio(0.001);
//   
//   mpfi_class TabX[2] = { mpfi_class(10.,11.), mpfi_class(10.,12.)};
//   matrix_interval X(2,1,TabX);
//   cout << "X: " << X << endl;
//   matrix_interval Xinf = eps_inflation_box<mpfi_class,mpfr_class> ( X, ratio);
//   cout << "Xinf: " << Xinf << endl;
//   
//   for(int i = 0; i < 10; i++)
//     cout << "random: " << alea_interval_point(interior(Xinf(1,0))) << endl;
//   
//   mpfi_class TabX2[2] = { mpfi_class(10.,next(next(10.))), mpfi_class(10.,next(next(10.)))};
//   matrix_interval X2(2,1,TabX2);
//   cout << "X2: " << X2 << endl;
//   matrix_interval X2inf = eps_inflation_box<mpfi_class,mpfr_class> ( X2, ratio);
//   cout << "X2inf: " << X2inf << endl;
//   
//   for(int i = 0; i < 10; i++)
//     cout << "random: " << alea_interval_point(interior(X2inf(1,0))) << endl;
//   
//   mpfi_class TabX3[2] = { mpfi_class(10.,11.), mpfi_class(10.,12.)};
//   matrix_interval X3(2,1,TabX3);
//   cout << "X3: " << X3 << endl;
//   mpfi_class TabX4[2] = { mpfi_class(10,10.5), mpfi_class(11.,11.5)};
//   matrix_interval X4(2,1,TabX4);
//   cout << "X4: " << X4 << endl;
//   cout << "containsBorder(X4,X3): " << containsBorder<mpfi_class,mpfr_class>(X4,X3) << endl;
  
  interval Tab[4] = { interval(0.,1.), interval(0.,1.), interval(0.,1.), interval(0.,1.) };
 
  matrix_interval M(2,2,Tab);
  
  cout << M << endl;
  matrix_interval N(2,2,Tab);
  cout << N << endl;
  
  cout << "M+N" << M+N << endl;
  cout << "M-N" << M-N << endl;
  cout << "-N" << -N << endl;
  
  return 0; 
}
