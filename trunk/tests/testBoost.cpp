#include "../boost/boostxx.hpp"
#include <iostream>

//for function alea_interval_point
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
typedef boost::minstd_rand base_generator_type;

//for function alea_interval_point
base_generator_type cpp_tools::boost_class::_generator = base_generator_type(42u);

#include "../mpfi/mpfixx.hpp" 

using namespace std;
using namespace cpp_tools;

#define PREC 53
mp_prec_t mpfi_class::_prec = PREC;
mp_prec_t mpfr_class::_prec = PREC;

typedef cpp_tools::boost_class interval;

int main() {
  
  interval i0;
  cout << "i0: " << i0 << endl; 
  interval lboost(-55., 2.);
  cout << lboost << endl;
  cout << "relative_diameter: " << relative_diameter(lboost) << endl;
  
  mpfi_class lmpfi(-55., 2.);
  cout << lmpfi << endl;
  cout << "relative_diameter: " << relative_diameter(lmpfi) << endl;
  
  return 0; 
} 
