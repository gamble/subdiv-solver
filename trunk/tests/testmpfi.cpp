#include "../base/base.h"

#include "../mpfi/mpfixx.hpp" 

using namespace cpp_tools;
#define PREC 5
mp_prec_t mpfi_class::_prec = PREC;
mp_prec_t mpfr_class::_prec = PREC;

int main() {
  
//   mp_prec_t prec = 53;
  
  mpfi_class i0;
//   
  cout << i0 << endl;
//   
//   mpfi_class::set_prec(113);
//   
//   cout << i0 << endl;
//   
//   cout << i0.get_prec_mpfi_t() << endl;
//   mpfi_class i1(i0);
//   
//   cout << i1<< endl;
//   cout << i1.get_prec_mpfi_t() << endl;
//   
//   mpfi_class a0(-14,-12);
//   mpfi_class a1(1,2);
//   cout << "a0: " << a0 << endl;
//   cout << "a1: " << a1 << endl;
//   cout << "a0/a1: " << a0/a1 << endl;
  
//   mpfi_class i3(9.5,10.1);
//   cout << "i3  : " << i3 << endl;
//   mpfi_class ci3 = centerInterval(i3);
//   cout << "test: " << (i3-ci3)/(mpfi_class(100.,100.)) + ci3  << endl;
  
  return 0; 
}