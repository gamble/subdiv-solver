#include "../base/base.h"
#include "../base/base.h"
#include "../generic/matrix.h"

using namespace matrix;

template<class I, class F>
Matrix<I> Func ( Matrix<I> &m ) {
//   cout << "Func" << endl;
  Matrix<I> res ( 1,1 );
//   cout << "m: " << m ;
  I x = m(0,0);
//   cout << "x: " << x << endl ;
//   res(0,0) = ((F) 1.)*x*x;
  res(0,0) = ((F) 1.)*(x-((F) 10.))*(x-((F) 10.));
//   cout << "res(0,0): " << res(0,0) << endl ;
  return res;
}

template<class I, class F>
Matrix<I> Jaco ( Matrix<I> &m ) {
//   cout << "Jaco" << endl;
  Matrix<I> res ( 1,1 );
  I x = m(0,0);
//   res(0,0) = ((F) 2.)*x; 
  res(0,0) = ((F) 2.)*x - ((F) 20.); 
  return res;
}

template<class I, class F>
Matrix<I> Hess ( Matrix<I> &m ) {
//   cout << "Hess" << endl;
  Matrix<I> res ( 1,1 );
  I x = m(0,0);
  I un = I(1.,1.);
  res(0,0) = ((F) 2.)*un;
  return res;
} 
