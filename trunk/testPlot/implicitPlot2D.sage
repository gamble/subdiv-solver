import os
import sys
import time
import collections

from subdivision_solver import subdivision_solver

#from subdivision_solver.tracker.tracker_boost import parallelotope, krawczykContract, is_equal, contract, diffMax, m11, N_Inflate, kernel, buildTrialParallelotope, no_backtrack, looping_status, is_in_domain, adjust_Stepsize, loops, out_of_domain, stepsize_too_small
#load("../subdivision_solver/implicit_plot/tracker.sage")
#from subdivision_solver.implicit_plot.solution_boost import solution
#load("../subdivision_solver/implicit_plot/solution.sage")
#from subdivision_solver.implicit_plot.trackerTopo_boost import certifiedTrackerTopoSuivi
#load("../subdivision_solver/implicit_plot/trackerTopo.sage")
from subdivision_solver.implicit_plot.implicit_plot_boost import implicit_plot2D
#load("../subdivision_solver/implicit_plot/functionsImplicitPlot2D.sage")

arithmetic = "boost"
fast = "boost"
#arithmetic = "mpfi"
#fast = "mpfi" 
#fast = "other" 

prec = 53
RF = RealField(prec)
RIF = RealIntervalField(prec) 
sage.rings.real_mpfi.printing_style='brackets'

Rr.<x,y> = RR[]
#p = (x^4 + 3*y^3*x + 2*x^2*y -3*x*y + x^2 -1) ;

#q = -(1/3)*y^3 - 2*y +(3/2)*x^2;

#f1=9000001+24000000*y+4000000*x^6*y^2+6000000*x^4*y^4+4000000*x^2*y^6+8000000*x^6*y+24000000*x^4*y^3+24000000*x^2*y^5+12000000*x^4*y^2+36000000*x^2*y^4-24000000*x^4*y-16000000*x^2*y^3-116000000*x^2*y^2-136000000*x^2*y+1000000*y^8+1000000*x^8+8000000*y^7+20000000*y^6-4000000*x^6+8000000*y^5-34000000*y^4-2000000*x^4-40000000*y^3+4000000*y^2-52000000*x^2;

#f1 = Rr(-(1/3)*x^3 - 1*x +2*y);
#f2 = Rr(-(1/3)*y^3 - 2*y +(3/2)*x^2);

f1 = Rr(x^4 + 3*y^3*x + 2*x^2*y -3*x*y + x^2 -1);
f2 = Rr(2*y^4 - 5*y*x^3 + 4*x*y^2 -7*x*y + y^2 -2);

initBox = [ [RIF(-1,2)], [RIF(-1,2)] ]
Blist = initBox

#def implicit_plot2D( *args, **kwargs ):
    
    #minsize = kwargs.get('minsize', 1e-16)
    #maxsize = kwargs.get('maxsize', 1e-1)
    #precision = kwargs.get('precision', 53)
    #fast = kwargs.get('fast', "boost")
    #colors = kwargs.get('colors', ["purple","turquoise"] )
    #verbosity = kwargs.get('verbosity', 0)
    #view = kwargs.get('view', None) #either [ center, width ] or [ xmin, xmax, ymin, ymax ]
    
    #[l1, status, tsolve, ttrack] = implicitPlot2D( Blist, f1, [x,y], maxsize, minsize, precision, fast, colors[0], verbosity )
    #[l2, status, tsolve, ttrack] = implicitPlot2D( Blist, f2, [x,y], maxsize, minsize, precision, fast, colors[1], verbosity )
    
    #res = l1 + l2
    #res = res + text("func 1", (1,-0.05), color = colors[0], axis_coords=True, horizontal_alignment='right')
    #res = res + text("func 2", (1,-0.1),  color = colors[1], axis_coords=True, horizontal_alignment='right', axes=False, frame=True)
    
    #res.axes(False)
    #if not (view==None):
        #if len(view)==2:
            #res.xmin( view[0][0] - (1./2)*view[1][0] )
            #res.xmax( view[0][0] + (1./2)*view[1][0] )
            #res.ymin( view[0][1] - (1./2)*view[1][1] )
            #res.ymax( view[0][1] + (1./2)*view[1][1] )
        #elif len(view)==4:
            #res.xmin( view[0] )
            #res.xmax( view[1] )
            #res.ymin( view[2] )
            #res.ymax( view[3] )
    
    #return res

minsize=1e-6
maxprec=113
verbosity = "stats_color"

syst = subdivision_solver([f1,f2],[x,y]);
syst.setPlotting(True)
status = syst.solve(initBox,minsize,maxprec,verbosity)
solutions = syst.getSolutions()
#l1 = syst.plotSubdiv(  )
l1 = syst.plotSubdiv( view=[[0.,0.], [1, 1]] )
#l1.show()

#l2 = implicit_plot2D( syst, precision=53, view=[[0.,0.], [1, 1]] )
l2 = implicit_plot2D( syst, precision=53)
l3=l1+l2
l3.set_axes_range(l2.xmin(), l2.xmax(), l2.ymin(), l2.ymax() )
l3.show()
#l2 = implicit_plot2D( view=[[0.,0.], [1, 1]] )
#l = implicit_plot2D(colors=["blue","green"])
#l = implicit_plot2D(maxsize = 1, colors=["blue","green"])
#(l1+l2).show(axis=[-0.5, 0.5, -0.5, 0.5])
