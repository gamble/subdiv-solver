import os
import sys
import time
import collections

from subdivision_solver import subdivision_solver
#from subdivision_solver.implicit_plot.implicit_plot_boost import implicit_plot2D
#from subdivision_solver.plot.plot_boost import solveAndPlot
from subdivision_solver import solveAndPlot
from subdivision_solver import implicitPlot

Rr.<x,y> = RR[]

f1 = Rr(x^4 + 3*y^3*x + 2*x^2*y -3*x*y + x^2 -1);
f2 = Rr(2*y^4 - 5*y*x^3 + 4*x*y^2 -7*x*y + y^2 -2);
f1 = Rr(x^10 + 3*y^7*x^2 + 2*x^1*y^5 -3*x^2*y + x^3 -1);
f2 = Rr(2*y^8 - 5*y^4*x^5 + 4*x^4*y^3 -7*x*y^2 + y^2 -2);

#f1=Rr(9000001+24000000*y+4000000*x^6*y^2+6000000*x^4*y^4+4000000*x^2*y^6+8000000*x^6*y+24000000*x^4*y^3+24000000*x^2*y^5+12000000*x^4*y^2+36000000*x^2*y^4-24000000*x^4*y-16000000*x^2*y^3-116000000*x^2*y^2-136000000*x^2*y+1000000*y^8+1000000*x^8+8000000*y^7+20000000*y^6-4000000*x^6+8000000*y^5-34000000*y^4-2000000*x^4-40000000*y^3+4000000*y^2-52000000*x^2);
#f2 = Rr(2*y^4 - 5*y*x^3 + 4*x*y^2 -7*x*y + y^2 -2);

initBox = [ [RIF(-2,2)], [RIF(-2,2)] ]
#initBox = [ [RIF(-1.5,1.5)], [RIF(-1.5,1.5)] ]
#Blist = initBox
RF = RealField(53)

minsize=1e-6
maxprec=113
verbosity = "stats_color"

syst = subdivision_solver([f1,f2],[x,y]);

#l1 = solveAndPlot(syst, initBox,minsize,maxprec,verbosity, view=[[0.,0.], [1, 1]], withImplicitPlot=True );
l1 = solveAndPlot(syst, initBox,minsize,maxprec,verbosity, withImplicitPlot=True );

#l1.set_axes_range(-0.1,0.1, -1.1, -0.9)

#syst2 = subdivision_solver([f2],[x,y]);
#l2 = implicitPlot(syst2, initBox, RF(1e-16), maxprec, maxsize = RF(0.1), withParalls=True, verbosity = 0) 
#l3 = implicitPlot(syst2, initBox, RF(1e-16), maxprec, maxsize = RF(0.01)) 
#l2 = implicitPlot(syst2, view=[[0.0001,0.99999], [0.01, 0.01]], precision = 113, maxsize = 0.1) 
#l2=l2+l3
#view = [[1.15,1.45],[0.2,0.2]]
#l2.xmin( view[0][0] - (1./2)*view[1][0] )
#l2.xmax( view[0][0] + (1./2)*view[1][0] )
#l2.ymin( view[0][1] - (1./2)*view[1][1] )
#l2.ymax( view[0][1] + (1./2)*view[1][1] ) 
