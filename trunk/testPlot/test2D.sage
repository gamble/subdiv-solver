from subdivision_solver import subdivision_solver
prec = 53
RF = RealField(prec)
RIF = RealIntervalField(prec) 
sage.rings.real_mpfi.printing_style='brackets'

verbosity = "stats_color"
#verbosity = 3
minsize=1e-6
maxprec=113

Rr.<x,y> = QQ[]

#f_1 = Rr(x*y + x^2 + y^4-1);
#f_2 = Rr(y^2 - x^2 - x);

f_1 = Rr(x^4 + 3*y^3*x + 2*x^2*y -3*x*y + x^2 -1);
f_2 = Rr(2*y^4 - 5*y*x^3 + 4*x*y^2 -7*x*y + y^2 -2);

f_1 = Rr(-(1/3)*x^3 - x +2*y);
f_2 = Rr(-(1/3)*y^3 - 2*y +(3/2)*x^2);

initBox = [ [RIF(-1,2)], [RIF(-1,2)] ]
#initBox = [ [RIF(-1.13,-0.9)], [RIF(-0.13, 0.1)] ]
syst = subdivision_solver([f_1,f_2],[x,y]);
syst.setPlotting(True)
status = syst.solve(initBox,minsize,maxprec,verbosity)
solutions = syst.getSolutions()

l = syst.plotSubdiv(  )
#l = syst.plotSubdiv( view=[[0.,0.], [1, 1]] )
#l = syst.plotSubdiv( view=[0,1,0,1] )
#l = syst.plotSubdiv( view=[[0.,0.], [1, 1]], colors = ["red","purple","green"] )
l.show()

#[disE,disK,valK]=syst.getBoxesForPlotting()
##print solutions
#center = [solutions[0][0][0].center(), solutions[0][1][0].center()]

#def draw2DBox(B, col,opac):
    #corners = [ [B[0][0].lower(), B[1][0].lower()],
                #[B[0][0].upper(), B[1][0].lower()],
                #[B[0][0].upper(), B[1][0].upper()],
                #[B[0][0].lower(), B[1][0].upper()] ]
    #p = polygon(corners, color=col, alpha = opac, edgecolor=col) + polygon(corners, color=col, fill=False)
    ##p = polygon(corners, color=col, fill=False)
    #return p

#from sage.plot.plot3d.shapes import Box
#def draw2DBox(B, col,opac):
    #widths = [ B[0][0].upper() - B[0][0].lower(), B[1][0].upper() - B[1][0].lower(), 0. ]
    #center = [ B[0][0].center(), B[1][0].center(), 0.0 ]
    #b = Box(widths, color=col, opacity = opac, edgecolor=col).translate(center)
    #return b

#def draw2DBoxes( Bs, col,opac):
    #ps = sum( [draw2DBox(B, col,opac) for B in Bs] )
    #return ps
     
#show( draw2DBox(solutions[0], "red", 0.1), axes=false )
#len( draw2DBoxes(solutions, "red", 0.1) )
#show( draw2DBoxes(solutions, "red"), axes=false )

#def plot_subdiv( disE, disK, valK, center=None, widths=None ):
    #toDraw =   draw2DBoxes(disE, "red",0.1) \
             #+ draw2DBoxes(disK, "purple" ,0.1) \
             #+ draw2DBoxes(valK, "green" ,0.1)
    #t = text("red: discarded by evaluation", (0,-0.05), color = "red", axis_coords=True, horizontal_alignment='left') \
      #+ text("purple: discarded by Krawczyk", (0,-0.1), color = "purple", axis_coords=True, horizontal_alignment='left') \
      #+ text("green: validated by Krawczyk", (0,-0.15), color = "green", axis_coords=True, horizontal_alignment='left')
    #if center==None:
        #show( toDraw+t, axes=False, frame=True )
    #else:
        #show( toDraw+t, axes=False, frame=True, \
              #xmin = center[0] - (1./2)*widths[0], xmax = center[0] + (1./2)*widths[0], \
              #ymin = center[1] - (1./2)*widths[1], ymax = center[1] + (1./2)*widths[1] )
    
#show( draw2DBoxes(disE, "red",0.1) \
   #+  draw2DBoxes(disK, "purple" ,0.1) \
   #+  draw2DBoxes(valK, "green" ,0.1) \
   ##+ draw2DBox(initBox, "blue" ,0.1)
     #, axes=False \
     #, frame=True \
     #, xmin=-1.001, xmax = -0.999\
     #, ymin = -0.001, ymax = 0.001\
     #, aspect_ratio = 1)
 
#plot_subdiv( disE, disK, valK, [0.,0.], [10, 20] )
#plot_subdiv( disE, disK, valK )
