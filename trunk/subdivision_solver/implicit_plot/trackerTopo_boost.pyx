#**************************************************************************#
#      Copyright (C) 2019 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++  

#from subdivision_solver.boost.interfaceboost cimport cppinterval
#from subdivision_solver.boost.sageboost cimport to_cppinterval, from_cppinterval 

#from subdivision_solver.matrix_interval.matrix_interval_boost cimport matrix_interval  

from sage.rings.real_mpfi import  RealIntervalField, RealIntervalFieldElement
from sage.rings.real_mpfr import RealNumber, RealField_class, RealField
from sage.rings.integer import Integer

from subdivision_solver.matrix_interval.matrix_interval_boost cimport P_matrix_interval, Coverlaps

from subdivision_solver.tracker.tracker_boost import parallelotope, krawczykContract, is_equal, contract, diffMax, m11, N_Inflate, kernel, buildTrialParallelotope, no_backtrack, looping_status, is_in_domain, adjust_Stepsize, loops, out_of_domain, stepsize_too_small

from subdivision_solver.implicit_plot.solution_boost import solution

#from subdivision_solver.solver.solver_mp import subdivision_solver  

def contains_first_solution(xk, listOfPointsWithFlags, prec, index, verbose=0):
  
  #check if it contains the starting solution 
  [overlaps1, isIn1, isInInt1, intersection1] = xk.overlaps_with_box( (listOfPointsWithFlags[index][0]).getBoxM() )
  isInInt2=True
  while (isInInt2) and (overlaps1 == True) and (not isIn1) :
    matsave = P_matrix_interval((listOfPointsWithFlags[index][0]).getBoxM())
    #print "trackerTopo_boost.pyx, contains_first_solution: contract"
    listOfPointsWithFlags[index][0].contract(prec)
    #print "ici"
    #listOfPointsWithFlags[i][0].contract(500)
    [overlaps2, isIn2, isInInt2, intersection2] = Coverlaps( (listOfPointsWithFlags[index][0]).getBoxM(), matsave )
    #print "isInInt2: ", isInInt2
    if (isInInt2):
      [overlaps1, isIn1, isInInt1, intersection1] = xk.overlaps_with_box( (listOfPointsWithFlags[index][0]).getBoxM() )
  if isIn1: 
    return True
  else :
    return False

    
def intersecting_status (xk, listOfPointsWithFlags, prec, verbose=0, index=-1): #TODO
  res = True;
  i = 0;
  overlaps1 = True
  isInInt1 = True
  nbIntersections = 0
  
  while res and i < len(listOfPointsWithFlags) :
      
    if (i==index) :
      nbIntersections = nbIntersections + 1
      res = (not nbIntersections > 1 )
    else :
      #if verbose and i==11 and (index>=0):
        #print " listOfPointsWithFlags[i][2]: ", listOfPointsWithFlags[i][2]
        #[overlaps1, isIn1, isInInt1, intersection1] = xk.overlaps_with_box(listOfPointsWithFlags[i][0].getBoxM())
        #print "hull: ", xk.hull()
        #print "sol: ", listOfPointsWithFlags[i][0]
        #print "overlaps: ", overlaps1
      if listOfPointsWithFlags[i][2] == True:  
        #[overlaps1, isIn1, isInInt1, intersection1] = overlaps((listOfPointsWithFlags[i][0]).getBoxM(), xk.hull())
        [overlaps1, isIn1, isInInt1, intersection1] = xk.overlaps_with_box(listOfPointsWithFlags[i][0].getBoxM())
        while (overlaps1 == True) and (not isInInt1) : #TODO: this loop does not finish if the solution in  listOfPointsWithFlags[i] is on the boundary of xk
          #print "trackerTopo_boost.pyx, intersecting_status: contract"
          listOfPointsWithFlags[i][0].contract(prec)
          #[overlaps1, isIn1, isInInt1, intersection1] = overlaps((listOfPointsWithFlags[i][0]).getBoxM(), xk.hull())
          [overlaps1, isIn1, isInInt1, intersection1] = xk.overlaps_with_box(listOfPointsWithFlags[i][0].getBoxM())
        if isInInt1:
          nbIntersections = nbIntersections + 1
      #res = (not listOfPointsWithFlags[i][2]) or (overlaps1 == False) or isInInt1 or (not nbIntersections > 1 )
      res = (overlaps1 == False) or isInInt1 or (not nbIntersections > 1 )
    i = i+1
  if not res:
    if verbose:
      print "intersecting status is false"
  return res

def intersects (xk,listOfPointsWithFlags,valid, verbose=0):
  if valid == False:
    return valid
  else :
    res = False
    i = 0;
    #while (res==False) and i < len(listOfPointsWithFlags) :
    while i < len(listOfPointsWithFlags) :
      if listOfPointsWithFlags[i][2] == True : #point not yet crossed
        #[overlaps1, isIn1, isInInt1, intersection1] = overlaps((listOfPointsWithFlags[i][0]).getBoxM(), xk.hull())
        [overlaps1, isIn1, isInInt1, intersection1] = xk.overlaps_with_box(listOfPointsWithFlags[i][0].getBoxM())
        if isInInt1 :
          listOfPointsWithFlags[i][2] = False # point has been crossed
          if verbose:
            print "point crossed: ", i, " !"
          if listOfPointsWithFlags[i][1] == False: # it is a point on boundaries
            if verbose:
              print "-> boundary point!"
          res = True
      i = i+1 
    return False

##### Algo 2 of paper #####
def certifiedTrackerTopoSuivi(F, GradF, x0, listOfPointsWithFlags, indexOfX0InList, sign, h, hmin, alpha, beta, domain,prec, file_plot_parall, verbose = 0):
  #print "**********************tracker.sage: certifiedTracker"
  # F:R^{n+1}->R^n, GradF, GradF its jacobian matrix
  # sign: a tracking direction
  # h, hmin: the initial stepsize, and the minimum stepsize
  # alpha, beta: parameters of stepsize adjustment strategy
  # domain: the domain within wich the curve is followed
  pk = [] #the sequence of parallelotopes containing the curve
  yk = [] #the sequence of parallelotopes containing points of the curve
  
  #paralsTemp = []
  
  
  iteration = 1
  stop = False
  x=x0
  y0 = x #dummy
  pk.append(x) #dummy
  yk.append(x) #dummy
  y=x#dummy
  p=x
  hmax = h
  max_h = 0 # greatest h really used
  ykm1 = y
  #print "-----------------------------Beginning of tracking process:"
  while not stop :
    [res, p] =  buildTrialParallelotope( GradF, x, ykm1, (iteration==1), h, sign)
    success = N_Inflate( F, GradF, p, 1, 1.1, 1.e-12)
    if success:
      if iteration==1:
        y0 = p.inputSide()
        contract(F, GradF, y0)
        yk.append(y0)
      y = p.outputSide()
      contract(F, GradF, y)
    if iteration==1:
      
      valid = success and no_backtrack (iteration, p, pk[-1], y, yk[-2]) \
                    and looping_status (iteration, p, y0) \
                    and is_in_domain (iteration,p,y,domain)\
                    and intersecting_status(p, listOfPointsWithFlags,prec,verbose, indexOfX0InList)
    else :
      valid = success and no_backtrack (iteration, p, pk[-1], y, yk[-2]) \
                    and looping_status (iteration, p, y0) \
                    and is_in_domain (iteration,p,y,domain)\
                    and intersecting_status(p, listOfPointsWithFlags,prec,verbose, -1)
      
    if ( (valid) and (h>max_h) ):
      max_h=h
      
    h = adjust_Stepsize(h,alpha,beta,valid)
    if h>hmax :
      h = hmax
      
    #if verbose: 
        #print "trackerTopo_boost, certifiedTrackerTopoSuivi, iteration: ", iteration, "valid: ", valid, "stepsize: ", h
        #if not valid:
            #print "------ no backtrack: ", no_backtrack (iteration, p, pk[-1], y, yk[-2])
            #print "------ looping_status: ", looping_status (iteration, p, y0)
            #print "------ is_in_domain: ", is_in_domain (iteration,p,y,domain)
            #print "------ intersecting_status: ", intersecting_status(p, listOfPointsWithFlags,prec,verbose, -1)
            #paralsTemp.append(p) 
    
    stop = False
    if iteration == 1 and valid:
      stop = not contains_first_solution(p, listOfPointsWithFlags, prec, indexOfX0InList, verbose)
      if verbose: 
        print "********************* not contains first solution: ", stop
        
    if not stop and valid :
      #paralsTemp = []
      x = y.middle()
      iteration = iteration+1
      pk.append(p)
      yk.append(y)
      
    if not stop and valid:
        if not ( iteration==2 and out_of_domain(y,domain, valid) ): #must not mark points if first paralls goes out of domain
            intersects(p, listOfPointsWithFlags, valid, verbose)
            
    stop = stop or loops (iteration-1,p,y0,valid) \
                or out_of_domain(y,domain, valid) \
                or stepsize_too_small(h,hmin)
       
    if valid: ykm1 = y
    
  #endwhile
  pk = pk[1:]#dummy
  yk = yk[1:]#dummy
  flag = 0
  #flags values
  #0: iteration = 1 => not success and/or stepsize_too_small                    -> bad initial point or bad minsize or bad curve!
  #1: iteration = 2 and out_of_domain                                           -> bad direction
  #2: iteration > 1 and not success and/or not valid and/or stepsize_too_small  -> bad minsize or bad curve!
  #3: iteration > 1 and out_of_domain                                           -> OK, the CC is diffeomorphic to a segment
  #4: iteration > 1 and loops                                                   -> OK, the CC is diffeomorphic to a circle
  if verbose:
    print "-----------------------------End of tracking process:"
    print "--- iteration: ", iteration
    print "--- success: ", success
    print "--- valid: ", valid
    if success and (not valid):
      print "------ no backtrack: ", no_backtrack (iteration-1 , p, pk[-1], y, yk[-2])
      print "------ looping_status: ", looping_status (iteration-1, p, y0)
      print "------ is_in_domain: ", is_in_domain (iteration-1,p,y,domain)
    print "--- stop: ", stop
    if success and valid and stop :
      print "------ loops: ", loops (iteration-1,p,y0,valid)
      print "------ out of domain: ", out_of_domain(y,domain, valid)
      print "------ stepsize_too_small: ", stepsize_too_small(h,hmin)
  
  if iteration==1: 
    flag = 0
  elif iteration==2 and out_of_domain(y,domain, valid):
    flag = 1
    #must unmark points crossed
  else : #iteration > 1
    if (not success) or (not valid) or (stepsize_too_small(h,hmin)):
      flag = 2
      #for i in range(0,len(paralsTemp)):
          #pk.append(paralsTemp[i])
    else: #success and valid and stop
      if out_of_domain(y,domain, valid) :
        flag = 3
      elif loops (iteration,p,y0,valid) :
        flag = 4
      else : #should never arise
        flag = 5
        
  return [pk,yk,max_h,flag]

