#**************************************************************************#
#      Copyright (C) 2019 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++  

#from subdivision_solver.boost.interfaceboost cimport cppinterval
#from subdivision_solver.boost.sageboost cimport to_cppinterval, from_cppinterval 

#from subdivision_solver.matrix_interval.matrix_interval_boost cimport matrix_interval  

from sage.rings.real_mpfi import  RealIntervalField, RealIntervalFieldElement
from sage.rings.real_mpfr import RealNumber, RealField_class, RealField
from sage.rings.integer import Integer

from subdivision_solver.matrix_interval.matrix_interval_boost import P_matrix_interval, overlaps, middleInterval

from subdivision_solver.implicit_plot.solution_boost import solution 
from subdivision_solver.implicit_plot.trackerTopo_boost import certifiedTrackerTopoSuivi
from subdivision_solver.tracker.tracker_boost import parallelotope
from subdivision_solver.solver.solver_mp import subdivision_solver as subd_solv 
from fast_polynomial import mpfi_polynomial, boost_polynomial
from sage.rings.polynomial.polynomial_element import Polynomial, is_Polynomial####
from sage.rings.polynomial.multi_polynomial import MPolynomial####

from sage.plot.line import line
from sage.plot.text    import text
from sage.plot.polygon import polygon

import copy

import time 

def draw2DParallelotope ( p ):
    corners = p.get_corners2D()
    #print "corners: ", corners
    res = polygon(corners, color="blue", fill=False)
    return res

def draw2DParallelotopes ( ps ):
    res = sum( [draw2DParallelotope(p) for p in ps] )
    return res

def solveCritical( initialBox, pol, listOfVars, prec, minsize, verbosity):
    
    #print "solveCritical, prec: ", prec, isinstance(prec, Integer)
    
    stres = 1
    #polder  = derivative(pol,  listOfVars[1])
    polder  = pol.derivative(listOfVars[1])
    t1 = time.clock()
    criticalPointSystem = subd_solv([pol,polder],listOfVars)
    status = criticalPointSystem.solve(initialBox, minsize, prec, verbosity)
    t2 = time.clock()
    #res = criticalPointSystem.getSolutions(1e-10)
    res = criticalPointSystem.getSolutions()
    
    resfin = []
    for i in range(0,len(res)):
        #print "solution: ", res[i]
        resfin.append(solution(res[i], criticalPointSystem, -1))
    
    if not (status==0):
        print "implicit plot not certified!!! ( solving critical points system status: ", status, ")"
        stres = 0
    
    return [resfin, stres, t2-t1]

def solveOnBoundary2D( initialBox, pol, listOfVars, indexOfVar, value, prec, minsize, verbosity ):
    
    #print "solveOnBoundary2D, prec: ", prec, isinstance(prec, Integer)
    
    listOfVarsSubs = listOfVars[0:-1]
    
    #symbolic substitution
    newPol = pol.substitute( {listOfVars[indexOfVar]:value} )
    for j in range(indexOfVar + 1, len(listOfVars) ):
      newPol = newPol.substitute( {listOfVars[j]:listOfVars[j-1]} )
      
    #initialBox in new system
    initialBoxBoundary = []
    for i in range(0,len(initialBox)):
        if not (i==indexOfVar):
            initialBoxBoundary.append(initialBox[i])
            
    t1 = time.clock()
    #print "newPol: ", newPol
    #print "listOfVarsSubs: ", listOfVarsSubs
    boundarySystem = subd_solv([newPol],listOfVarsSubs)
    status = boundarySystem.solve(initialBoxBoundary, minsize, prec, verbosity)
    #res = boundarySystem.getSolutions(1e-10)
    res = boundarySystem.getSolutions()
    t2 = time.clock()
    RIF = RealIntervalField(53)
    for i in range(0,len(res)):
        res[i].insert(indexOfVar, [RIF(value)])
    resfin=[]
    for i in range(0,len(res)):
        resfin.append( solution(res[i], boundarySystem, indexOfVar) )
    return [resfin, status, t2-t1]

def solveOnBoundaries( initialBox, pol, listOfVars, prec, minsize, verbosity ):
    
    #print "solveOnBoundaries, prec: ", prec, isinstance(prec, Integer)
    
    res = []
    tres = 0
    stres = 1
    [sols, status, ts] = solveOnBoundary2D( initialBox, pol, listOfVars, 0, initialBox[0][0].lower(), prec, minsize, verbosity )
    for i in range(0,len(sols)):
        res.append( sols[i] )
    tres += ts
    if not (status==0):
        print "implicit plot not certified!!! ( solving boundary points system status: ", status, ")"
        stres = 0
    [sols, status, ts] = solveOnBoundary2D( initialBox, pol, listOfVars, 0, initialBox[0][0].upper(), prec, minsize, verbosity )
    for i in range(0,len(sols)):
        res.append( sols[i] )
    tres += ts
    if not (status==0):
        print "implicit plot not certified!!! ( solving boundary points system status: ", status, ")"
        stres = 0
    [sols, status, ts] = solveOnBoundary2D( initialBox, pol, listOfVars, 1, initialBox[1][0].lower(), prec, minsize, verbosity )
    for i in range(0,len(sols)):
        res.append( sols[i] )
    tres += ts
    if not (status==0):
        print "implicit plot not certified!!! ( solving boundary points system status: ", status, ")"
        stres = 0
    [sols, status, ts] = solveOnBoundary2D( initialBox, pol, listOfVars, 1, initialBox[1][0].upper(), prec, minsize, verbosity )
    for i in range(0,len(sols)):
        res.append( sols[i] )
    tres += ts
    if not (status==0):
        print "implicit plot not certified!!! ( solving boundary points system status: ", status, ")"
        stres = 0
        
    return [ res, stres, tres ]

def trackConnectedComponents( initBox, pol, listOfVars, boundaryPoints, criticalPoints, maxsize, minsize, prec, col, fast, drawParall, verbosity ):
    
    #define functions for tracking
    polx  = pol.derivative(listOfVars[0])
    poly  = pol.derivative(listOfVars[1])   

    if fast == "boost" :
        fastp            = boost_polynomial(pol   ,        mode='horner')
        fastpx           = boost_polynomial(polx  ,        mode='horner')
        fastpy           = boost_polynomial(poly  ,        mode='horner')
    
    elif fast == "mpfi" :
        fastp          = mpfi_polynomial(pol   ,    prec)
        fastpx         = mpfi_polynomial(polx  ,    prec)
        fastpy         = mpfi_polynomial(poly  ,    prec)
    
    else :
        fastp          = pol   
        fastpx         = polx  
        fastpy         = poly   

    def fp(X):
    # X is a column vector 2times1
        return P_matrix_interval( [fastp(X[0,0], X[1,0])] )

    def jacp(X):
    # X is a column vector 2times1
        return P_matrix_interval([fastpx(X[0,0], X[1,0]), fastpy(X[0,0], X[1,0])])
    
    domain = P_matrix_interval( initBox )
    #find the minimum width
    minWidth = domain[0,0].absolute_diameter()
    for i in range(1, domain.nrows()):
        if domain[i,0].absolute_diameter() < minWidth:
            minWidth = domain[i,0].absolute_diameter()
    #print "minWidth: ", minWidth
    
    listOfPoints  = []; # list of points with flags: [ point, IsCritical, IsAlreadyFound ]
    for i in range(0,len(criticalPoints)):
        listOfPoints.append( [criticalPoints[i], False, True] )
    for i in range(0,len(boundaryPoints)):
        listOfPoints.insert( 0, [boundaryPoints[i], False, True] )
    
    RF = RealField(53)    
    alpha = RF(0.5)
    beta = RF(1.1)
    direction = 1
    tres=0.
    lines = []
    status = 1
    
    for i in range(0, len(listOfPoints)):
        #print "i: ", i
        #print "listOfPoints[:][2]: ", listOfPoints[0][2], ", ", listOfPoints[1][2]
        if listOfPoints[i][2] == True :
    
            starting_point = middleInterval(listOfPoints[i][0].getBoxM())
            width_starting_int = listOfPoints[i][0].Width()
            
            flag = 0
            while (flag==0) and (width_starting_int > minsize) :
                t1 = time.clock()
                [parals, yp, max_h, flag] = certifiedTrackerTopoSuivi(fp, jacp, \
                                                                      starting_point, listOfPoints, i, \
                                                                      direction, minWidth*maxsize, minsize, \
                                                                      alpha, beta, domain, prec, False, verbosity)
#flags values
#0: iteration = 1 => not success and/or stepsize_too_small                    -> bad initial point or bad minsize or bad curve!
#1: iteration = 2 and out_of_domain                                           -> bad direction
#2: iteration > 1 and not success and/or not valid and/or stepsize_too_small  -> bad minsize or bad curve!
#3: iteration > 1 and out_of_domain                                           -> OK, the CC is diffeomorphic to a segment
#4: iteration > 1 and loops                                                   -> OK, the CC is diffeomorphic to a circle  
                t2 = time.clock()
                tres += t2-t1
                #print "flag: ", flag
                if (flag==0):
                    #print "implicit_plot_boost.pyx, trackConnectedComponents, flag: 0, contract"
                    #print "starting point: ", starting_point
                    listOfPoints[i][0].contract(prec)
                    starting_point = middleInterval ( listOfPoints[i][0].getBoxM() )
                    width_starting_int = listOfPoints[i][0].Width()
                    
            if flag==3 or flag==4 :
                points = []
                for j in range(0,len(yp)) :
                    temp = yp[j].middle()
                    points.append( (temp[0,0].center(), temp[1,0].center()) )
                lines.append( line(points, color = col) ) 
                if drawParall:
                    for j in range(0,len(parals)) :
                        lines.append( draw2DParallelotope( parals[j] ) )
            
            elif flag==0 or flag==2: 
                print "implicit plot not certified!!! ( tracking flag: ", flag, ")"
                #if flag==2:
                    #if drawParall:
                        #for j in range(0,len(parals)) :
                            #lines.append( draw2DParallelotope( parals[j] ) )
                status = 0
                
    return [lines, status, tres ]

def implicitPlot2D( initBox, pol, listOfVars, maxsize, minsize, prec, fast, col, drawParall, verbosity ):
    tsolve = 0.
    ttrack = 0.
    stres = 1
    #print "implicitPlot2D, prec: ", prec, isinstance(prec, Integer)
    #print "pol: ", pol
    #print "###################################################### Solve critical points system"
    [criticalPoints, status, ts] = solveCritical( initBox, pol, listOfVars, prec, minsize, verbosity)
    #print len(criticalPoints), " critical points"
    stres*=status
    tsolve += ts
    #print "###################################################### Solve system on boundaries"
    [boundaryPoints, status, ts] = solveOnBoundaries( initBox, pol, listOfVars, prec, minsize, verbosity)
    tsolve += ts
    stres*=status
    #print len(boundaryPoints), " boundary points"
    #print "###################################################### Follow curves from intersection points"
    [lines, status, ttrack] = trackConnectedComponents( initBox, pol, listOfVars, boundaryPoints, criticalPoints, \
                                                        maxsize, minsize, prec, col, fast, drawParall, verbosity )
    stres*=status
    l = sum( [li for li in lines] )
    return [l, stres, tsolve, ttrack]

#def implicit_plot2D(syst, *args, **kwargs ):
    
    #RF = RealField(53)
    #minsize = kwargs.get('minsize', RF(1e-16))
    #maxsize = kwargs.get('maxsize', RF(1e-1))
    #prec = kwargs.get('precision', None)
    #fast = kwargs.get('fast', "boost")
    #colors = kwargs.get('colors', ["purple","turquoise"] )
    #verbosity = kwargs.get('verbosity', 0)
    #view = kwargs.get('view', None) #either [ center, width ] or [ xmin, xmax, ymin, ymax ]
    
    ##prec = int(prec)
    #if prec == None:
        #prec = 53
    ##print "implicit_plot2D, prec: ", prec, isinstance(prec, Integer)
    
    #if view==None and syst._initBox ==None :
        #return None
    #if view==None:
        #Blist = syst._initBox
    #elif syst._initBox ==None:
        #RIF = RealIntervalField(prec)
        #if len(view)==2:
            #Blist = [ [RIF(view[0][0] - (1./2)*view[1][0], view[0][0] + (1./2)*view[1][0])], 
                      #[RIF(view[0][1] - (1./2)*view[1][1], view[0][1] + (1./2)*view[1][1])] ]
        #elif len(view)==4:
            #Blist = [ [RIF(view[0], view[1])], 
                      #[RIF(view[2], view[3])] ]
        #else:
            #print "implicit_plot2D(): bad initial box specification"
    #else:
        #Blist = syst._initBox
        
    #[l1, status, tsolve, ttrack] = implicitPlot2D( Blist, syst._ptabs[0], syst._vtabs, maxsize, minsize, prec, fast, colors[0], verbosity )
    #[l2, status, tsolve, ttrack] = implicitPlot2D( Blist, syst._ptabs[1], syst._vtabs, maxsize, minsize, prec, fast, colors[1], verbosity )
    
    #res = l1 + l2
    #res = res + text("func 1", (1,-0.05), color = colors[0], axis_coords=True, horizontal_alignment='right')
    #res = res + text("func 2", (1,-0.1),  color = colors[1], axis_coords=True, horizontal_alignment='right', axes=False, frame=True)
    
    #res.axes(False)
    #if not (view==None):
        ##print "ici"
        #if len(view)==2:
            ##print "la"
            #res.xmin( view[0][0] - (1./2)*view[1][0] )
            #res.xmax( view[0][0] + (1./2)*view[1][0] )
            #res.ymin( view[0][1] - (1./2)*view[1][1] )
            #res.ymax( view[0][1] + (1./2)*view[1][1] )
        #elif len(view)==4:
            #res.xmin( view[0] )
            #res.xmax( view[1] )
            #res.ymin( view[2] )
            #res.ymax( view[3] )
    ##elif not (syst._view == None):
        ##if len(syst._view)==2:
            ##res.xmin( syst._view[0][0] - (1./2)*syst._view[1][0] )
            ##res.xmax( syst._view[0][0] + (1./2)*syst._view[1][0] )
            ##res.ymin( syst._view[0][1] - (1./2)*syst._view[1][1] )
            ##res.ymax( syst._view[0][1] + (1./2)*syst._view[1][1] )
        ##elif len(syst._view)==4:
            ##res.xmin( syst._view[0] )
            ##res.xmax( syst._view[1] )
            ##res.ymin( syst._view[2] )
            ##res.ymax( syst._view[3] )
    
    #return res
