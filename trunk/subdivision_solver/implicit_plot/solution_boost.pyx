#**************************************************************************#
#      Copyright (C) 2019 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++  

#from subdivision_solver.boost.interfaceboost cimport cppinterval
#from subdivision_solver.boost.sageboost cimport to_cppinterval, from_cppinterval 

#from subdivision_solver.matrix_interval.matrix_interval_boost cimport matrix_interval  

from sage.rings.real_mpfi import  RealIntervalField, RealIntervalFieldElement
from sage.rings.real_mpfr import RealNumber, RealField_class, RealField
from sage.rings.integer import Integer

from subdivision_solver.matrix_interval.matrix_interval_boost cimport P_matrix_interval

#from subdivision_solver.solver.solver_mp import subdivision_solver as subd_solv

import copy

#ordering of border:
#XsupVal = 0 
#YsupVal = 1 
#XinfVal = 2 
#YinfVal = 3

class solution():
  
  _box = [] #the solution
  _sys = 0  #the system it satisfies
  _var = -1 #the variable that is substituted, -1 if no
  _border = -1 #if it is the intersection of the curve with the boundary of a cylinder,
               #the integer corresponding to this boundary as defined above
  _inflated = False
  _savedValue = 0
  _proj2DM = 0
  _proj2D = 0

  def __init__(self, box, sys, var):
    self._box = copy.deepcopy(box)
    self._sys = sys
    self._var = var
    if self._var >=0:
      self._savedValue = self._box[self._var][0]
    
  def __copy__(self):
    res = solution(self._box, self._sys, self._var)
    res._border = self._border
    res._inflated = self._inflated
    res._savedValue = self._savedValue
    return res
  
  def __str__(self):
    res = ""
    res = res + "Solution, substituted variable: " + str(self._var) + "\n"
    res = res + str(P_matrix_interval(self._box)) + "\n"
    return res
    
  def setBorder(self, border):
    self._border = border

  def getBorder(self):
    return self._border

  def getBox(self):
    return self._box
  
  def getBoxM(self): #as a matrix
    return P_matrix_interval(self._box)

  def getBoxProj2DM(self): #as a matrix
      if (self._proj2DM == 0):
          self._proj2DM = P_matrix_interval( [ [self._box[0][0]], [self._box[1][0]] ] )
      return self._proj2DM
        
      
  #def getBox3DM(self): #as a matrix
      #if (self._var < 0) or (self._var > 1): 
        #return matrix(self._box)
      #elif var == 0:
        #return matrix( [ [RIF(self._savedValue,self._savedValue)], [self._box[0][0]], [self._box[1][0]] ] )
      #else:
        #return matrix( [ [self._box[0][0]], [RIF(self._savedValue,self._savedValue)], [self._box[1][0]] ] )
      #end  
  
  def Width(self):
    res = 0.0
    for i in range(0,len(self._box)):
      if (self._box)[i][0].absolute_diameter() > res :
        res = (self._box)[i][0].absolute_diameter()
    return res
  
  def Width_restr(self):
    res = 0.0
    for i in range(0,len(self._box)):
      #print (self._box)[i][0].absolute_diameter()
      if (self._var != i) and ((self._box)[i][0].absolute_diameter() > res) :
        res = (self._box)[i][0].absolute_diameter()
    return res

  #def getCenterR(self):
    #res = []
    #for i in range(0,len(self._box)):
      #res.append( [self._box[i][0].center()] )
    #if self._var >= 0 and self._inflated :
      #if self._border ==0 or self._border == 1 : res[self._var][0] = self._box[self._var][0].upper()
      #else : res[self._var][0] = self._box[self._var][0].lower()
    #return res
                           
  def inflate(self):
    
    if self._var < 0 :
      return
    #width = self.Width_restr()
    width = 0.0
    if self._var==0 : width = (self._box)[1][0].absolute_diameter()
    if self._var==1 : width = (self._box)[0][0].absolute_diameter()
    prectemp = self._box[0][0].precision()
    RIFtemp = RealIntervalField(prectemp)
    a = RIFtemp(-width/2,width/2)
    self._box[self._var][0] = self._box[self._var][0] + a

    self._inflated = True
    self._proj2DM = 0
    return
    
  def contract(self, maxprec):
    sol = []
    for i in range(0,len(self._box)):
      if self._var != i :
        sol.append( [self._box[i][0]] )
        
    sol = (self._sys).Krawczyk(sol, maxprec, 0)
    if (len(sol)==0):
      print "!!!!!!!!!!!!!!!! contract, not enough precision. maxprec: ", maxprec
      print "point avant: \n", self._box
      #print "point apres: \n", sol
      return
    
    if self._var >=0 :
      sol.insert(self._var,[self._savedValue])
      
    self._box = sol 
    if self._inflated :
      self.inflate()
      
    self._proj2DM = 0
      
#return -1 if sol1<sol2, 1 if sol1>sol2
#def cmp_solsOnBoundary(sol1,sol2):
  #if sol1.getBorder() < sol2.getBorder(): return int(-1)
  #if sol1.getBorder() > sol2.getBorder(): return int(1)
  
  #coeff = int(1)
  #border = sol1.getBorder()
  #if border == YsupVal or border == XinfVal : coeff = int(-1)
  #if border == XinfVal or border == XsupVal :
    #if (sol1.getBox())[1][0].lower() < (sol2.getBox())[1][0].lower() :
      #return coeff*int(-1)
    #else :
      #return coeff*int(1)
  ##border == YinfVal or border == YsupVal    
  #if (sol1.getBox())[0][0].lower() < (sol2.getBox())[0][0].lower() :
    #return coeff*int(-1)
  #else :
    #return coeff*int(1) 
