#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#

HOME = os.environ['HOME']
sys.path.append(HOME + '/Bureau/realSolver/interfaceSage')

sage.rings.real_mpfi.printing_style='brackets'

from system.system import systempols
from system.eval_boost import test_init_eval, testFastcallSys

from matrix_interval.matrix_interval_boost import P_matrix_interval as P_matrix_interval

Rr.<x1,x2> =ZZ[]
p1 = 741340*x1^5*x2^15+551139*x1^3*x2^17-185345*x1^8*x2^11+581711*x1^10*x2^5+292021*x1^7*x2^6+195507*x1^5*x2^8-672309*x1^12+839627*x1^7*x2-428088*x1^3*x2^3-715427*x2^4
p2 = 643015*x1^13*x2^6-446653*x1^4*x2^14+79913*x1^14*x2^3-217638*x1^6*x2^9+105495*x1*x2^10+425645*x1^10+894388*x1^7*x2^2-253708*x1^7*x2-546938*x1^2*x2^6+1006946*x2^2

test = systempols([p1,p2],[x1,x2])

print test 

test_init_eval(test)

m = P_matrix_interval( [ [RIF(2,2.01)],[RIF(3,3.01)] ] )
print testFastcallSys(m)
