#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************# 

import time
from subdivision_solver import subdivision_solver
prec = 53
RIF = RealIntervalField(prec)
RF = RealField(prec)
sage.rings.real_mpfi.printing_style='brackets'

#Rr.<x1,x2> =ZZ[]
#p1 = -25*x1^5+85*x1^4*x2-255*x1^3*x2^2-90*x1^2*x2^3-134*x1*x2^4+45*x2^5-44*x1^4-153*x1^3*x2-156*x1^2*x2^2+140*x1*x2^3-40*x2^4+59*x1^3+139*x1^2*x2+92*x1*x2^2+136*x2^3+86*x1^2+81*x1*x2-8*x2^2-39*x1+163*x2-101
#p2 = -207*x1^5+119*x1^4*x2-120*x1^3*x2^2+226*x1^2*x2^3-231*x1*x2^4+156*x2^5+222*x1^4+83*x1^3*x2+21*x1^2*x2^2-178*x1*x2^3-22*x2^4-208*x1^3-29*x1^2*x2+139*x1*x2^2+184*x2^3+72*x1^2-198*x1*x2-58*x2^2-63*x1-69*x2-13

##p1 = (x1-2)*(x1-3)
##p2 = (x2-4)*(x2-5)
##tab = [ [RIF(-1,1)],[RIF(-1,1)] ] 
#tab = [ [RIF(-6,6)],[RIF(-6,6)] ]

Rr.<x1> =ZZ[]
#p1 = (x1-9007199254740992)*(x1-9007199254740993)
#p1 = (x1-RF(2.75))*(x1-RF(5.2))
#tab = [ [RIF(-1,RF(5.2))]  ]
#print "tab: ", tab[0][0]

p1 = (x1-1.0099)*(x1+1)*(x1^2+3)*(x1^2+5)*(x1^2+7)*(x1^2+9)*(x1^2+11)*(x1^2+13)
tab = [ [RIF(-101,99)]  ]

nbSteps=-1
minsize=10e-6
#minsize=0.
prec2=200
RF=RealField(prec2)
maxprec=RF(1e-80)
#minsize=maxprec
#print "maxprec: ", maxprec
t1 = time.clock()
#test = subdivision_solver([p1,p2],[x1,x2])
test = subdivision_solver([p1],[x1])
#test.setOrder1()
status = test.solve(tab,minsize,113,'stats_color')
#status = test.solve(tab,minsize,53,3)
t2 = time.clock()
t6 = t2-t1
print "temps pour realSolver: ", t6, "s"
print "status: ", status
res = test.getSolutions();
print "############################"
print "Nombre de Solutions: ", len(res)
for i in range(0,len(res)):
  #print "Solution : ", res[i][0][0], ", ", res[i][1][0]
  print "Solution : ", res[i][0][0]
print "############################"
#resContr = res = test.getSolutions(1e-1000);
resContr = res = test.getSolutions(0.);
print "############################"
print "Solutions contractees: ", len(resContr)
for i in range(0,len(resContr)):
  #print "Solution : ", resContr[i][0][0], ", ", resContr[i][1][0]
  print "Solution : ", resContr[i][0][0]
print "############################"
undetermined = test.getUndetermined()
print "############################"
print "Boites indeterminees: ", len(undetermined)
for i in range(0,len(undetermined)):
  #print "boite : ", undetermined[i][0][0], ", ", undetermined[i][1][0]
  print "boite : ", undetermined[i][0][0]
print "############################"