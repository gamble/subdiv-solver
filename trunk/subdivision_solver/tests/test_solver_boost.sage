#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#

import time
prec = 53
RIF = RealIntervalField(prec)

HOME = os.environ['HOME']
sys.path.append(HOME + '/Bureau/realSolver/interfaceSage')

sage.rings.real_mpfi.printing_style='brackets'

from system.system import systempols
#from system.eval_mpfi import test_init_eval, testFastcallSys

#from matrix_interval.matrix_interval_mpfi import P_matrix_interval as P_matrix_interval

from solver.solver_boost import SolveFromList

#Rr.<x1,x2> =ZZ[]
#p1 = 741340*x1^5*x2^15+551139*x1^3*x2^17-185345*x1^8*x2^11+581711*x1^10*x2^5+292021*x1^7*x2^6+195507*x1^5*x2^8-672309*x1^12+839627*x1^7*x2-428088*x1^3*x2^3-715427*x2^4
#p2 = 643015*x1^13*x2^6-446653*x1^4*x2^14+79913*x1^14*x2^3-217638*x1^6*x2^9+105495*x1*x2^10+425645*x1^10+894388*x1^7*x2^2-253708*x1^7*x2-546938*x1^2*x2^6+1006946*x2^2

#test = systempols([p1,p2],[x1,x2])

#print test 

#test_init_eval(test, 53)

#m = P_matrix_interval( [ [RIF(2,2.01)],[RIF(3,3.01)] ] )
#print testFastcallSys(m)

#test_init_eval(test, 113)

#print testFastcallSys(m) 

Rr.<x1> =ZZ[]
p1 = (x1-9007199254740992)*(x1-9007199254740993)
p1 = (x1-2.75)*(x1-5.2)
tab = [ [RIF(-1,10)]  ]
#tab = [ [RIF(9007199254740980,9007199254741000)]  ]
#tab = [ [RIF(9007199254740990,9007199254740996)]  ]

#tab = [ [RIF(-1,1)],[RIF(-1,1)] ]
#print "tab: ", tab
nbSteps=-1
minsize=1e-4
t1 = time.clock()
#test = systempols([p1,p2],[x1,x2])
test = systempols([p1],[x1])
[res, tooSmallPrecision, tooSmallUser, CCIntooSmallUser, notInversible, counter] = SolveFromList(test,[tab],minsize, nbSteps, True, 1)
t2 = time.clock()
t6 = t2-t1
print "temps pour realSolver: ", t6, "s"
print "############################"
print "Nombre de Solutions: ", len(res)
for i in range(0,len(res)):
  #print "Solution : ", res[i][0][0], ", ", res[i][1][0]
  print "Solution : ", res[i][0][0]
print "############################"
#t1 = time.clock()
#[contracted, tooSmallPrecision, notInversible] = ContractFromList(test,res,1e-40, True, 1)
#t2 = time.clock()
#t6 = t2-t1
#print "temps pour contraction: ", t6, "s"
#print "############################"
#print "Solutions contractees: ", len(contracted)
#for i in range(0,len(contracted)):
  ##print "Solution : ", contracted[i][0][0], ", ", contracted[i][1][0]
  #print "Solution : ", contracted[i][0][0]
#print "############################"
#print "############################"
#print "Besoin de plus de precision: ", len(tooSmallPrecision)
#for i in range(0,len(tooSmallPrecision)):
  ##print "boite : ", tooSmallPrecision[i][0][0], ", ", tooSmallPrecision[i][1][0]
  #print "boite : ", tooSmallPrecision[i][0][0]
#print "############################"
#print "############################"
#print "Pas Inversible: ", len(notInversible)
#for i in range(0,len(notInversible)):
  ##print "boite : ", notInversible[i][0][0], ", ", notInversible[i][1][0]
  #print "boite : ", notInversible[i][0][0]
#print "############################"