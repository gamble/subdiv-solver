#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++

from subdivision_solver.mpfi.interfacempfi cimport cppinterval
#from sage.libs.gmp.all    cimport mpz_t, mpq_t
from sage.libs.mpfr       cimport mpfr_t
from sage.rings.real_mpfi cimport mpfi_t, mpfi_set, mpfi_set_q, mpfi_set_z, mpfi_interv_fr, mpfi_interv_d,\
                                  mpfi_get_prec, mpfi_set_si
from sage.rings.integer   cimport Integer
from sage.rings.rational  cimport Rational
from sage.rings.real_mpfr cimport RealNumber
from sage.rings.real_mpfi cimport RealIntervalFieldElement, RealIntervalField_class
from sage.rings.real_mpfi import  RealIntervalField


cdef inline void to_cppinterval(n, cppinterval* destination):
    cdef RealIntervalFieldElement r
    #print "*************to_cppinterval"
    if isinstance(n, Integer):
        mpfi_set_z (destination.value, (<Integer> n).value)
    elif isinstance(n, RealNumber):
        mpfr_to_cppinterval(destination, (<RealNumber> n).value)
    elif isinstance(n, Rational):
        mpfi_set_q (destination.value, (<Rational> n).value)
    elif isinstance(n, RealIntervalFieldElement):
        #print "*************to_cppinterval, interval"
        mpfi_set(destination.value, (<RealIntervalFieldElement> n).value)
        #print "*************to_cppinterval, interval, fin"
    elif isinstance(n, int):
        mpfi_set_si (destination.value, <int> n)
    else:
        r = (<RealIntervalField_class> \
             RealIntervalField(mpfi_get_prec(destination.value))) (n)
        mpfi_set(destination.value, r.value)
    
cdef inline from_cppinterval (cppinterval* a):
    #print "*************from_cppinterval"
    cdef RealIntervalFieldElement r =\
        (<RealIntervalField_class> RealIntervalField(mpfi_get_prec(a.value)))._new()
    mpfi_set(r.value, a.value)
    return r

cdef inline void mpfr_to_cppinterval (cppinterval* destination, mpfr_t x):
    #print "*************mpfr_to_cppinterval"
    mpfi_interv_fr(destination.value, x, x)

