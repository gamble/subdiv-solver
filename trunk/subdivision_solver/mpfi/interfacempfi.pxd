#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++

#cdef extern from "../../mpfi/mpfrxx.hpp" namespace "cpp_tools" nogil:
    #ctypedef long  mp_prec_t
    #cppclass cppfloat "cpp_tools::mpfr_class":
        #cppfloat()
        #void set_prec(mp_prec_t)
        
cdef extern from "../../mpfi/mpfixx.hpp" namespace "cpp_tools" nogil:
#    ctypedef void* mpfr_t
    ctypedef struct __mpfi_struct:
        pass
    ctypedef __mpfi_struct* mpfi_t
    ctypedef long  mp_prec_t
    cppclass cppinterval "cpp_tools::mpfi_class":
        cppinterval()
        #cppinterval(mp_prec_t)
        cppinterval(mpfi_t)
        cppinterval(cppinterval) nogil
        mp_prec_t get_prec()
        void set_prec(mp_prec_t)
        
        mpfi_t value "_value"
        