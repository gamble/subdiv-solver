class parallelotope():
  
  _C = 0
  _w = 0
  _x = 0
  _Cm1 = 0 #inverse of C
  
  def __init__(self, C, w, x):
    self._C = copy(C)
    self._w = copy(w)
    self._x = copy(x)
    
  def __copy__(self):
    res = parallelotope(self._C, self._w, self._x)
    res._Cm1 = copy(self._Cm1)
    return res
  
  def setCm1(self, Cm1):
    self._Cm1 = copy(Cm1)
  
  def __str__(self):
    dim = (self._w).nrows()
    s = "_w: [" + str(self._w[0,0]) + "]  _x: [" + str(self._x[0,0]) + "]"
    for i in range(1,dim) :
      s = s + "\n    [" + str(self._w[i,0]) + "]      [" + str(self._x[i,0]) + "]"
    s = s + "\n_C: "
    for i in range(0,dim) :
      for j in range(0,dim) :
	s = s + " [" + str(self._C[i,j]) + "] "
      s = s + "\n    "
    s = s + "\n_Cm1: "
    for i in range(0,dim) :
      for j in range(0,dim) :
	s = s + " [" + str(self._Cm1[i,j]) + "] "
      s = s + "\n    "
    return s
  
  def evalFunc (self, Func):
    return Func(self._C * self._w + self._x)
  
  def evalGrad (self, Grad):
    return Grad(self._C * self._w + self._x) * self._C
  
  def evalHess (self, Hess):
    return Hess(self._C * self._w + self._x) * self._C * self._C
  
  def evalFuncOrder1 (self, Func, Grad): #faux?
    midw = (self._w).center()
    midwp = parallelotope(self._C, midw, self._x)
    return midwp.evalFunc(Func) + ( self.evalGrad(Grad) * ( self._w - midw ) )
  
  def getw (self):
    return self._w
  
  def setw (self, w):
    self._w = copy(w)
  
  def getx (self):
    return self._x
  
  def getC (self):
    return self._C

  def inputSide(self):
    res = copy(self)
    dim = res._w.nrows()
    res._w[dim-1,0] = RIF((self._w[dim-1,0]).lower(),(self._w[dim-1,0]).lower())
    return res
  
  def outputSide(self):
    res = copy(self)
    dim = res._w.nrows()
    res._w[dim-1,0] = RIF(self._w[dim-1,0].upper(),self._w[dim-1,0].upper())
    return res
  
  def middle(self):
    return (self._C).center() * (self._w).center() + (self._x).center()
  
  def hull(self):
    return self._C * self._w + self._x
  
  def overlaps_with_box(self, box_matrix):
    return overlaps( (self._Cm1)*(box_matrix - self._x) ,self._w)
    
      
  def empty_intersection(self,p):
    [overlapsT, isIn, isinInt, intersection] = overlaps( (self._Cm1)*(p.hull() - self._x) ,self._w) 
    return not overlapsT
  
  def empty_intersection_hulls(self,p):
    [overlapsT, isIn, isinInt, intersection] = overlaps( self.hull() ,p.hull() ) 
    return not overlapsT
  
  #def is_in(self,p): # TODO
    #[overlapsT, isIn, isinInt, intersection] = overlaps( (self._Cm1)*(p.hull() - self._x) ,self._w) 
    #return overlapsT

  def is_in(self,p):
    [overlapsT, isIn, isinInt, intersection] = overlaps( self._w, (self._Cm1)*(p.hull() - self._x))
    return isIn
  
  #def draw3DEmptyIntersection(self, p, file_plot_parall, color = 1):
    #plot_parall = " "
    #q = (self._Cm1)*(p.hull()- self._x);
    #p0 = matrix( [[q[0,0].lower()],[q[1,0].lower()],[q[2,0].lower()] ] ) 
    #p1 = matrix( [[q[0,0].upper()],[q[1,0].lower()],[q[2,0].lower()] ] ) 
    #p2 = matrix( [[q[0,0].upper()],[q[1,0].upper()],[q[2,0].lower()] ] ) 
    #p3 = matrix( [[q[0,0].lower()],[q[1,0].upper()],[q[2,0].lower()] ] ) 
    #p4 = matrix( [[q[0,0].lower()],[q[1,0].lower()],[q[2,0].upper()] ] ) 
    #p5 = matrix( [[q[0,0].upper()],[q[1,0].lower()],[q[2,0].upper()] ] ) 
    #p6 = matrix( [[q[0,0].upper()],[q[1,0].upper()],[q[2,0].upper()] ] ) 
    #p7 = matrix( [[q[0,0].lower()],[q[1,0].upper()],[q[2,0].upper()] ] ) 
    #plot_parall = plot_parall + "p0=["+ str(p0[0,0].center())+ ", "+ str(p0[1,0].center())+ ", "+ str(p0[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p1=["+ str(p1[0,0].center())+ ", "+ str(p1[1,0].center())+ ", "+ str(p1[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p2=["+ str(p2[0,0].center())+ ", "+ str(p2[1,0].center())+ ", "+ str(p2[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p3=["+ str(p3[0,0].center())+ ", "+ str(p3[1,0].center())+ ", "+ str(p3[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p4=["+ str(p4[0,0].center())+ ", "+ str(p4[1,0].center())+ ", "+ str(p4[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p5=["+ str(p5[0,0].center())+ ", "+ str(p5[1,0].center())+ ", "+ str(p5[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p6=["+ str(p6[0,0].center())+ ", "+ str(p6[1,0].center())+ ", "+ str(p6[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p7=["+ str(p7[0,0].center())+ ", "+ str(p7[1,0].center())+ ", "+ str(p7[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "t=linspace(0,1,10);\n"
    #plot_parall = plot_parall + "param3d1( p0(1)*t+p1(1)*(1-t), p0(2)*t+p1(2)*(1-t), list( p0(3)*t(:)+p1(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p1(1)*t+p2(1)*(1-t), p1(2)*t+p2(2)*(1-t), list( p1(3)*t(:)+p2(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p2(1)*t+p3(1)*(1-t), p2(2)*t+p3(2)*(1-t), list( p2(3)*t(:)+p3(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p3(1)*t+p0(1)*(1-t), p3(2)*t+p0(2)*(1-t), list( p3(3)*t(:)+p0(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p0(1)*t+p4(1)*(1-t), p0(2)*t+p4(2)*(1-t), list( p0(3)*t(:)+p4(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p1(1)*t+p5(1)*(1-t), p1(2)*t+p5(2)*(1-t), list( p1(3)*t(:)+p5(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p2(1)*t+p6(1)*(1-t), p2(2)*t+p6(2)*(1-t), list( p2(3)*t(:)+p6(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p3(1)*t+p7(1)*(1-t), p3(2)*t+p7(2)*(1-t), list( p3(3)*t(:)+p7(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p7(1)*t+p4(1)*(1-t), p7(2)*t+p4(2)*(1-t), list( p7(3)*t(:)+p4(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p4(1)*t+p5(1)*(1-t), p4(2)*t+p5(2)*(1-t), list( p4(3)*t(:)+p5(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p5(1)*t+p6(1)*(1-t), p5(2)*t+p6(2)*(1-t), list( p5(3)*t(:)+p6(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p6(1)*t+p7(1)*(1-t), p6(2)*t+p7(2)*(1-t), list( p6(3)*t(:)+p7(3)*(1-t(:)), "+ str(color) +") );\n"
    #r = self._w;
    #r0 = matrix( [[r[0,0].lower()],[r[1,0].lower()],[r[2,0].lower()] ] ) 
    #r1 = matrix( [[r[0,0].upper()],[r[1,0].lower()],[r[2,0].lower()] ] ) 
    #r2 = matrix( [[r[0,0].upper()],[r[1,0].upper()],[r[2,0].lower()] ] ) 
    #r3 = matrix( [[r[0,0].lower()],[r[1,0].upper()],[r[2,0].lower()] ] ) 
    #r4 = matrix( [[r[0,0].lower()],[r[1,0].lower()],[r[2,0].upper()] ] ) 
    #r5 = matrix( [[r[0,0].upper()],[r[1,0].lower()],[r[2,0].upper()] ] ) 
    #r6 = matrix( [[r[0,0].upper()],[r[1,0].upper()],[r[2,0].upper()] ] ) 
    #r7 = matrix( [[r[0,0].lower()],[r[1,0].upper()],[r[2,0].upper()] ] ) 
    #plot_parall = plot_parall + "r0=["+ str(r0[0,0].center())+ ", "+ str(r0[1,0].center())+ ", "+ str(r0[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "r1=["+ str(r1[0,0].center())+ ", "+ str(r1[1,0].center())+ ", "+ str(r1[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "r2=["+ str(r2[0,0].center())+ ", "+ str(r2[1,0].center())+ ", "+ str(r2[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "r3=["+ str(r3[0,0].center())+ ", "+ str(r3[1,0].center())+ ", "+ str(r3[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "r4=["+ str(r4[0,0].center())+ ", "+ str(r4[1,0].center())+ ", "+ str(r4[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "r5=["+ str(r5[0,0].center())+ ", "+ str(r5[1,0].center())+ ", "+ str(r5[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "r6=["+ str(r6[0,0].center())+ ", "+ str(r6[1,0].center())+ ", "+ str(r6[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "r7=["+ str(r7[0,0].center())+ ", "+ str(r7[1,0].center())+ ", "+ str(r7[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "t=linspace(0,1,10);\n"
    #plot_parall = plot_parall + "param3d1( r0(1)*t+r1(1)*(1-t), r0(2)*t+r1(2)*(1-t), list( r0(3)*t(:)+r1(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( r1(1)*t+r2(1)*(1-t), r1(2)*t+r2(2)*(1-t), list( r1(3)*t(:)+r2(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( r2(1)*t+r3(1)*(1-t), r2(2)*t+r3(2)*(1-t), list( r2(3)*t(:)+r3(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( r3(1)*t+r0(1)*(1-t), r3(2)*t+r0(2)*(1-t), list( r3(3)*t(:)+r0(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( r0(1)*t+r4(1)*(1-t), r0(2)*t+r4(2)*(1-t), list( r0(3)*t(:)+r4(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( r1(1)*t+r5(1)*(1-t), r1(2)*t+r5(2)*(1-t), list( r1(3)*t(:)+r5(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( r2(1)*t+r6(1)*(1-t), r2(2)*t+r6(2)*(1-t), list( r2(3)*t(:)+r6(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( r3(1)*t+r7(1)*(1-t), r3(2)*t+r7(2)*(1-t), list( r3(3)*t(:)+r7(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( r7(1)*t+r4(1)*(1-t), r7(2)*t+r4(2)*(1-t), list( r7(3)*t(:)+r4(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( r4(1)*t+r5(1)*(1-t), r4(2)*t+r5(2)*(1-t), list( r4(3)*t(:)+r5(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( r5(1)*t+r6(1)*(1-t), r5(2)*t+r6(2)*(1-t), list( r5(3)*t(:)+r6(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( r6(1)*t+r7(1)*(1-t), r6(2)*t+r7(2)*(1-t), list( r6(3)*t(:)+r7(3)*(1-t(:)), "+ str(color) +") );\n"
    #file_plot_parall.write(plot_parall)
  
  #def draw3D(self, file_plot_parall, color = 1):
    #plot_parall = " "
    #p0 = (self._C).center() * matrix( [[self._w[0,0].lower()],[self._w[1,0].lower()],[self._w[2,0].lower()] ] ) + (self._x).center()
    #p1 = (self._C).center() * matrix( [[self._w[0,0].upper()],[self._w[1,0].lower()],[self._w[2,0].lower()] ] ) + (self._x).center()
    #p2 = (self._C).center() * matrix( [[self._w[0,0].upper()],[self._w[1,0].upper()],[self._w[2,0].lower()] ] ) + (self._x).center()
    #p3 = (self._C).center() * matrix( [[self._w[0,0].lower()],[self._w[1,0].upper()],[self._w[2,0].lower()] ] ) + (self._x).center()
    #p4 = (self._C).center() * matrix( [[self._w[0,0].lower()],[self._w[1,0].lower()],[self._w[2,0].upper()] ] ) + (self._x).center()
    #p5 = (self._C).center() * matrix( [[self._w[0,0].upper()],[self._w[1,0].lower()],[self._w[2,0].upper()] ] ) + (self._x).center()
    #p6 = (self._C).center() * matrix( [[self._w[0,0].upper()],[self._w[1,0].upper()],[self._w[2,0].upper()] ] ) + (self._x).center()
    #p7 = (self._C).center() * matrix( [[self._w[0,0].lower()],[self._w[1,0].upper()],[self._w[2,0].upper()] ] ) + (self._x).center()
    #plot_parall = plot_parall + "p0=["+ str(p0[0,0].center())+ ", "+ str(p0[1,0].center())+ ", "+ str(p0[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p1=["+ str(p1[0,0].center())+ ", "+ str(p1[1,0].center())+ ", "+ str(p1[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p2=["+ str(p2[0,0].center())+ ", "+ str(p2[1,0].center())+ ", "+ str(p2[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p3=["+ str(p3[0,0].center())+ ", "+ str(p3[1,0].center())+ ", "+ str(p3[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p4=["+ str(p4[0,0].center())+ ", "+ str(p4[1,0].center())+ ", "+ str(p4[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p5=["+ str(p5[0,0].center())+ ", "+ str(p5[1,0].center())+ ", "+ str(p5[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p6=["+ str(p6[0,0].center())+ ", "+ str(p6[1,0].center())+ ", "+ str(p6[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p7=["+ str(p7[0,0].center())+ ", "+ str(p7[1,0].center())+ ", "+ str(p7[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "t=linspace(0,1,10);\n"
    #plot_parall = plot_parall + "param3d1( p0(1)*t+p1(1)*(1-t), p0(2)*t+p1(2)*(1-t), list( p0(3)*t(:)+p1(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p1(1)*t+p2(1)*(1-t), p1(2)*t+p2(2)*(1-t), list( p1(3)*t(:)+p2(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p2(1)*t+p3(1)*(1-t), p2(2)*t+p3(2)*(1-t), list( p2(3)*t(:)+p3(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p3(1)*t+p0(1)*(1-t), p3(2)*t+p0(2)*(1-t), list( p3(3)*t(:)+p0(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p0(1)*t+p4(1)*(1-t), p0(2)*t+p4(2)*(1-t), list( p0(3)*t(:)+p4(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p1(1)*t+p5(1)*(1-t), p1(2)*t+p5(2)*(1-t), list( p1(3)*t(:)+p5(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p2(1)*t+p6(1)*(1-t), p2(2)*t+p6(2)*(1-t), list( p2(3)*t(:)+p6(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p3(1)*t+p7(1)*(1-t), p3(2)*t+p7(2)*(1-t), list( p3(3)*t(:)+p7(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p7(1)*t+p4(1)*(1-t), p7(2)*t+p4(2)*(1-t), list( p7(3)*t(:)+p4(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p4(1)*t+p5(1)*(1-t), p4(2)*t+p5(2)*(1-t), list( p4(3)*t(:)+p5(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p5(1)*t+p6(1)*(1-t), p5(2)*t+p6(2)*(1-t), list( p5(3)*t(:)+p6(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p6(1)*t+p7(1)*(1-t), p6(2)*t+p7(2)*(1-t), list( p6(3)*t(:)+p7(3)*(1-t(:)), "+ str(color) +") );\n"
    #file_plot_parall.write(plot_parall)
    
  #def draw3DHull(self, file_plot_parall, color = 1):
    #plot_parall = " "
    #q = self.hull();
    #p0 = matrix( [[q[0,0].lower()],[q[1,0].lower()],[q[2,0].lower()] ] ) 
    #p1 = matrix( [[q[0,0].upper()],[q[1,0].lower()],[q[2,0].lower()] ] ) 
    #p2 = matrix( [[q[0,0].upper()],[q[1,0].upper()],[q[2,0].lower()] ] ) 
    #p3 = matrix( [[q[0,0].lower()],[q[1,0].upper()],[q[2,0].lower()] ] ) 
    #p4 = matrix( [[q[0,0].lower()],[q[1,0].lower()],[q[2,0].upper()] ] ) 
    #p5 = matrix( [[q[0,0].upper()],[q[1,0].lower()],[q[2,0].upper()] ] ) 
    #p6 = matrix( [[q[0,0].upper()],[q[1,0].upper()],[q[2,0].upper()] ] ) 
    #p7 = matrix( [[q[0,0].lower()],[q[1,0].upper()],[q[2,0].upper()] ] ) 
    #plot_parall = plot_parall + "p0=["+ str(p0[0,0].center())+ ", "+ str(p0[1,0].center())+ ", "+ str(p0[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p1=["+ str(p1[0,0].center())+ ", "+ str(p1[1,0].center())+ ", "+ str(p1[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p2=["+ str(p2[0,0].center())+ ", "+ str(p2[1,0].center())+ ", "+ str(p2[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p3=["+ str(p3[0,0].center())+ ", "+ str(p3[1,0].center())+ ", "+ str(p3[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p4=["+ str(p4[0,0].center())+ ", "+ str(p4[1,0].center())+ ", "+ str(p4[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p5=["+ str(p5[0,0].center())+ ", "+ str(p5[1,0].center())+ ", "+ str(p5[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p6=["+ str(p6[0,0].center())+ ", "+ str(p6[1,0].center())+ ", "+ str(p6[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "p7=["+ str(p7[0,0].center())+ ", "+ str(p7[1,0].center())+ ", "+ str(p7[2,0].center())+ "];\n"
    #plot_parall = plot_parall + "t=linspace(0,1,10);\n"
    #plot_parall = plot_parall + "param3d1( p0(1)*t+p1(1)*(1-t), p0(2)*t+p1(2)*(1-t), list( p0(3)*t(:)+p1(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p1(1)*t+p2(1)*(1-t), p1(2)*t+p2(2)*(1-t), list( p1(3)*t(:)+p2(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p2(1)*t+p3(1)*(1-t), p2(2)*t+p3(2)*(1-t), list( p2(3)*t(:)+p3(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p3(1)*t+p0(1)*(1-t), p3(2)*t+p0(2)*(1-t), list( p3(3)*t(:)+p0(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p0(1)*t+p4(1)*(1-t), p0(2)*t+p4(2)*(1-t), list( p0(3)*t(:)+p4(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p1(1)*t+p5(1)*(1-t), p1(2)*t+p5(2)*(1-t), list( p1(3)*t(:)+p5(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p2(1)*t+p6(1)*(1-t), p2(2)*t+p6(2)*(1-t), list( p2(3)*t(:)+p6(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p3(1)*t+p7(1)*(1-t), p3(2)*t+p7(2)*(1-t), list( p3(3)*t(:)+p7(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p7(1)*t+p4(1)*(1-t), p7(2)*t+p4(2)*(1-t), list( p7(3)*t(:)+p4(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p4(1)*t+p5(1)*(1-t), p4(2)*t+p5(2)*(1-t), list( p4(3)*t(:)+p5(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p5(1)*t+p6(1)*(1-t), p5(2)*t+p6(2)*(1-t), list( p5(3)*t(:)+p6(3)*(1-t(:)), "+ str(color) +") );\n"
    #plot_parall = plot_parall + "param3d1( p6(1)*t+p7(1)*(1-t), p6(2)*t+p7(2)*(1-t), list( p6(3)*t(:)+p7(3)*(1-t(:)), "+ str(color) +") );\n"
    #file_plot_parall.write(plot_parall)
    
  #def draw2D(self, file_plot_parall, color = 1):
    #plot_parall = " "
    #p0 = (self._C).center() * matrix( [[self._w[0,0].lower()],[self._w[1,0].lower()] ] ) + (self._x).center()
    #p1 = (self._C).center() * matrix( [[self._w[0,0].upper()],[self._w[1,0].lower()] ] ) + (self._x).center()
    #p2 = (self._C).center() * matrix( [[self._w[0,0].upper()],[self._w[1,0].upper()] ] ) + (self._x).center()
    #p3 = (self._C).center() * matrix( [[self._w[0,0].lower()],[self._w[1,0].upper()] ] ) + (self._x).center()
    #plot_parall = plot_parall + "p0=["+ str(p0[0,0].center())+ ", "+ str(p0[1,0].center())+ "];\n"
    #plot_parall = plot_parall + "p1=["+ str(p1[0,0].center())+ ", "+ str(p1[1,0].center())+ "];\n"
    #plot_parall = plot_parall + "p2=["+ str(p2[0,0].center())+ ", "+ str(p2[1,0].center())+ "];\n"
    #plot_parall = plot_parall + "p3=["+ str(p3[0,0].center())+ ", "+ str(p3[1,0].center())+ "];\n"
    #plot_parall = plot_parall + "t=linspace(0,1,10);\n"
    #plot_parall = plot_parall + "plot2d( p0(1)*t+p1(1)*(1-t), p0(2)*t+p1(2)*(1-t), "+ str(color) +" );\n"
    #plot_parall = plot_parall + "plot2d( p1(1)*t+p2(1)*(1-t), p1(2)*t+p2(2)*(1-t), "+ str(color) +" );\n"
    #plot_parall = plot_parall + "plot2d( p2(1)*t+p3(1)*(1-t), p2(2)*t+p3(2)*(1-t), "+ str(color) +" );\n"
    #plot_parall = plot_parall + "plot2d( p3(1)*t+p0(1)*(1-t), p3(2)*t+p0(2)*(1-t), "+ str(color) +" );\n"
    #file_plot_parall.write(plot_parall)
    

def krawczykContract( Func, Grad, p):
  #print "**********************tracker.sage: krawczykContract"
  # Func function R^{n+1}->R^{n}
  # Grad Matrix n lines, n+1 columns
  # p parallelotope with C Matrix (n+1)*(n+1), x and w matrices (n+1)*1
  dim = p.getw().nrows()
  wmat = p.getw()
  midw = wmat.center()
  dim = wmat.nrows()
  umat = wmat.delete_rows([dim-1]) # matrice n*1 of intervals
  vmat = matrix( [ wmat[dim-1,0] ] ) # matrice 1*1 of intervals
  midu = midw.delete_rows([dim-1]) # matrice n*1 of intervals
  midv = matrix( [ midw[dim-1,0] ] ) # matrice 1*1 of intervals
   
  miduvmat = midu.augment_row(vmat)
  miduvp = parallelotope( p.getC(), miduvmat, p.getx() ) # parallelotope
  
  Au = (p.evalGrad(Grad)).delete_columns([dim-1])
  Av = (miduvp.evalGrad(Grad)).column(dim-1)
  midwp = parallelotope(p.getC(), midw, p.getx() )
  
  b = midwp.evalFunc(Func) + Av*(vmat - midv)
  res = midu - b - (Au +(-1))*(umat - midu)
  #print "**********************tracker.sage: krawczykContract end"
  return res
  
def is_equal(B1,B2):
  stop = True
  dim = B1.nrows()
  i=0
  while stop and i < dim:
    stop = stop and ( B1[i,0].lower() == B2[i,0].lower() ) \
                and ( B1[i,0].upper() == B2[i,0].upper() )
    i = i+1
  return stop

def contract ( Func, Grad, p, MAX_ITS = 15 ):
  #print "**********************tracker.sage: contract"
  k=0
  w = p.getw()
  dim = w.nrows()
  uvar = w.delete_rows([dim-1])
  stop = True
  while k<MAX_ITS and stop :
    k=k+1
    newu = krawczykContract( Func, Grad, p)
    [overlapsT, isIn, isInInt, newu] = overlaps(newu,uvar)
    stop = overlapsT and isIn and not is_equal(newu,uvar)
    p.setw( newu.augment_row( matrix( [w[dim-1,0]] ) ) )
    uvar = newu
  #print "**********************tracker.sage: contract end"
      
def diffMax(a,b):
  dim = a.nrows()
  res = max ( abs(a[0,0].lower()-b[0,0].lower() ), abs(a[0,0].upper()-b[0,0].upper()) )
  for i in range(1,dim):
    temp = max ( abs(a[i,0].lower()-b[i,0].lower() ), abs(a[i,0].upper()-b[i,0].upper()) ) 
    if temp > res :
      res = temp
  return res

def m11(dim):
  reslist = [ [RIF(-1,1)] ]
  for i in range(1,dim-1): 
    reslist.append( [RIF(-1,1)] )
  return matrix(reslist)

def N_Inflate( Func, Grad, p, mu, delta, chi, MAX_ITS = 15):
  #print "**********************tracker.sage: N_Inflate"
  k=0
  m=0
  d = 1
  success = False
  w = p.getw()
  wsave = copy(p.getw())
  dim = w.nrows()
  cm11 = m11(dim)*chi
  uvar = w.delete_rows([dim-1])
  while (not success) and (k<MAX_ITS) and (m<mu) :
    k = k+1
    #print "d'abord ici"
    newu = krawczykContract( Func, Grad, p)
    dH = diffMax(newu,uvar)
    #print "ici"
    [overlapsT, isIn, success, uvar] = overlaps(newu,uvar)
    if not success:
      if k>=2 : mu = dH/d
      d = dH
      newmidu = newu.center()
      uvar = newmidu + (newu - newmidu)*delta + cm11 
      p.setw( uvar.augment_row( matrix( [w[dim-1,0]] ) ) )
  if not success :
    p.setw(wsave)
    #print "**********************tracker.sage: N_Inflate fails: k, k<MAX_ITS, m<mu: ", k, ", ", k<MAX_ITS, ", ", m<mu
  #print "**********************tracker.sage: N_Inflate end"
  return success

def kernel(fprimex):
  #print "**********************tracker.sage: kernel"
  #fprimex is a n\times n+1 matrix
  nbcols = fprimex.ncols()
  deleted_columns = nbcols-1
  scd_mbr = fprimex.column(deleted_columns)
  fst_mbr = fprimex.delete_columns( [deleted_columns] ) 
  test = fst_mbr.is_invertible()
  while (not test) and (deleted_columns>=0):
    scd_mbr = fprimex.column(deleted_columns)
    fst_mbr = fprimex.delete_columns( [deleted_columns] )
    test = fst_mbr.is_invertible()
    if not test:
      deleted_columns = deleted_columns -1
  if deleted_columns == -1: # jacobian matrix is not of full rank 
    res = False
    ker = matrix([])
  else :
    #print "**********************tracker.sage: kernel, is full rank"
    res=True
    ker = - fst_mbr.inverse()*scd_mbr
    kerlist = ker.list()
    kerlist.insert(deleted_columns,[RIF(1.,1.)])
    ker = matrix(kerlist)
  #print "**********************tracker.sage: kernel end"
  return [res, ker]
    
    
def buildTrialParallelotope( Grad, x, y, first_time, h, sign):
  #print "**********************tracker.sage: buildTrialParallelotope"
  fprimex = Grad(x)
  nbcols = fprimex.ncols()
  [isFullRank,ker] = kernel(fprimex)
  
  if not isFullRank :
    return [False, parallelotope(matrix([]), matrix([]), x)]

  ker = ker*(1/ker.norm())
  ker = ker.transpose()
  Cm1 = fprimex.augment_row(ker)
  #check the sign, inverse ker if necessary
  dCm1 = Cm1.determinant()
  if (dCm1*sign < 0) : 
    ker = ker*(-1)
    Cm1 = fprimex.augment_row(ker)
  #check if invertible
  if (not Cm1.is_invertible()):
    return [False, parallelotope(matrix([]), matrix([]), x)]
  #centers: otherwise the widths of elements of the matrix grow
  Cm1 = Cm1.center()
  C = Cm1.inverse()
  #centers: otherwise the widths of elements of the matrix grow
  C = C.center()
  if first_time :
    wlist = [[RIF(0,0)]]
    for i in range(1,nbcols-1):
      wlist.append([RIF(0,0)])
    wlist.append([RIF(-h/2,h/2)]) #in order the first parallelotope contains the initial point
    pres = parallelotope(C, matrix(wlist), x)
  else:
    w = Cm1*(y.hull()-x) #in order the parallelotope contains the output point of the last parallelotope
    for i in range(0,nbcols-1):
      w[i,0] = w[i,0].union(0)
    w[nbcols-1,0] = w[nbcols-1,0].union(RIF(0,h))
    pres = parallelotope(C, w, x)
  pres.setCm1(Cm1)
  #print "**********************tracker.sage: buildTrialParallelotope end"
  return [True, pres]

def no_backtrack (k, xk, xkm1, yk, ykm2):
  if k==1 : return True
  res = yk.empty_intersection_hulls(xkm1) or yk.empty_intersection(xkm1)
  if not res: return False
  res = ykm2.empty_intersection_hulls(xk) or ykm2.empty_intersection(xk)
  return res

def looping_status (k, xk, y0):
  return k==1 or\
         ( y0.empty_intersection(xk) or y0.empty_intersection_hulls(xk) ) or\
         y0.is_in(xk)

def is_in_domain (k,xk,yk,domain):
 [overlaps1, isIn1, isInInt1, intersection1] = overlaps(yk.hull(),domain)
 [overlaps2, isIn2, isInInt2, intersection2] = overlaps(xk.hull(),domain)
 return (not overlaps1) or isIn1

def adjust_Stepsize(h,alpha,beta,valid):
  if valid:
    #return min(beta*h,1e-2)
    return beta*h
  else :
    return alpha*h
  
def loops (k,xk,y0,valid):
  return valid and k>=2 and y0.is_in(xk)

#the name is strange but returns False if yk is out of the domain
def out_of_domain(yk,domain, valid):
  if valid == False: return False
  #print "ici"
  #print "hull: ", yk.hull()
  #print "domain: ", domain
  [overlaps1, isIn1, isInInt1, intersection1] = overlaps(yk.hull(),domain)
  #print "overlaps?: ", overlaps1
  #print "isIn1?: ", isIn1
  #print "isInInt1?: ", isInInt1
  #print "intersection: ", intersection1
  return not overlaps1

def stepsize_too_small(h,hmin):
  return h<=hmin
  
def certifiedTracker(F, GradF, x0, sign, h, hmin, alpha, beta, domain):
  #print "**********************subd.sage: certifiedTracker"
  # F:R^{n+1}->R^n, GradF, GradF its jacobian matrix
  # sign: a tracking direction
  # h, hmin: the initial stepsize, and the minimum stepsize
  # alpha, beta: parameters of stepsize adjustment strategy
  # domain: the domain within wich the curve is followed
  pk = [] #the sequence of parallelotopes containing the curve
  yk = [] #the sequence of parallelotopes containing points of the curve
  iteration = 1
  stop = False
  x=x0
  y0 = x #dummy
  pk.append(x) #dummy
  yk.append(x) #dummy
  y=x#dummy
  p=x
  #print "-----------------------------Beginning of tracking process:"
  while not stop :
    #print "-----------------------------iteration: ", iteration
    [res, p] =  buildTrialParallelotope( GradF, x, y, (iteration==1), h, sign)
    #print "res: ", res
    #print "p: ", p
    success = N_Inflate( F, GradF, p, 1, 1.1, 1.e-12)
    #print "success: ", success
    #print "p: ", p
    if success:
      if iteration==1:
	y0 = p.inputSide()
	contract(F, GradF, y0)
	yk.append(y0)
      y = p.outputSide()
      contract(F, GradF, y)
    #print "success: ", success
    #if success:
        #print "no_backtrack: ", no_backtrack (iteration, p, pk[-1], y, yk[-2])
        #print "looping_status: ", looping_status (iteration, p, y0)
        #print "is_in_domain: ", is_in_domain (iteration,p,y,domain)
    valid = success and no_backtrack (iteration, p, pk[-1], y, yk[-2]) \
                    and looping_status (iteration, p, y0) \
		    and is_in_domain (iteration,p,y,domain)
    h = adjust_Stepsize(h,alpha,beta,valid)
    if valid :
      x = y.middle()
      iteration = iteration+1
      pk.append(p)
      yk.append(y)
    stop = loops (iteration-1,p,y0,valid) \
	   or out_of_domain(y,domain, valid) \
	   or stepsize_too_small(h,hmin)
  #endwhile
  pk = pk[1:]#dummy
  yk = yk[1:]#dummy
  #print "-----------------------------End of tracking process:"
  print "--- iteration: ", iteration
  print "--- success: ", success
  print "--- valid: ", valid
  if success and (not valid):
    print "------ no backtrack: ", no_backtrack (iteration-1 , p, pk[-1], y, yk[-2])
    print "------ looping_status: ", looping_status (iteration-1, p, y0)
    print "------ is_in_domain: ", is_in_domain (iteration-1,p,y,domain)
  print "--- stop: ", stop
  if success and valid and stop:
    print "------ loops: ", loops (iteration,p,y0,valid)
    print "------ out_of_domain: ", out_of_domain(y,domain, valid)
    print "------ stepsize_too_small: ", stepsize_too_small(h,hmin)
  print "-----------------------------------------------------"
  return [pk,yk]

               
