#**************************************************************************#
#      Copyright (C) 2019 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++ 

from sage.rings.real_mpfi import RealIntervalField

from subdivision_solver.boost.interfaceboost cimport cppinterval
from subdivision_solver.boost.sageboost cimport to_cppinterval, from_cppinterval 

#from subdivision_solver.matrix_interval.matrix_interval_boost cimport matrix_interval   
from subdivision_solver.matrix_interval.matrix_interval_boost cimport P_matrix_interval, Coverlaps, createFromDimValue

#from subdivision_solver.system.system    cimport systempols
#from subdivision_solver.system.eval_boost cimport init_eval
#from subdivision_solver.system.eval_boost cimport fast_func_Sys, fast_func_JacSys   

cdef class parallelotope:
  
  cdef P_matrix_interval _C 
  cdef P_matrix_interval _w 
  cdef P_matrix_interval _x 
  cdef P_matrix_interval _Cm1  #inverse of C
  
  cpdef parallelotope copy(self)
