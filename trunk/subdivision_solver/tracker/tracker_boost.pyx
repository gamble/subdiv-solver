#**************************************************************************#
#      Copyright (C) 2019 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++

#from cysignals.signals cimport sig_on, sig_off

cdef class parallelotope:
  
  #cdef P_matrix_interval _C = 0
  #cdef P_matrix_interval _w = 0
  #cdef P_matrix_interval _x = 0
  #cdef P_matrix_interval _Cm1 = 0 #inverse of C
  
  def __cinit__(self, C, w, x):
    self._C = P_matrix_interval(C)
    self._w = P_matrix_interval(w)
    self._x = P_matrix_interval(x)
    
  def __copy__(self):
    res = parallelotope(self._C, self._w, self._x)
    res._Cm1 = P_matrix_interval(self._Cm1)
    return res

  cpdef parallelotope copy(self):
    res = parallelotope(self._C, self._w, self._x)
    res._Cm1 = P_matrix_interval(self._Cm1)
    return res
  
  def setCm1(self, Cm1):
    self._Cm1 = P_matrix_interval(Cm1)
  
  def __str__(self):
    cdef int dim = (self._w).nrows()
    cdef object s
    s = "_w: [" + str(self._w[0,0]) + "]  _x: [" + str(self._x[0,0]) + "]"
    for i in range(1,dim) :
      s = s + "\n    [" + str(self._w[i,0]) + "]      [" + str(self._x[i,0]) + "]"
    s = s + "\n_C: "
    for i in range(0,dim) :
      for j in range(0,dim) :
        s = s + " [" + str(self._C[i,j]) + "] "
      s = s + "\n    "
    s = s + "\n_Cm1: "
    for i in range(0,dim) :
      for j in range(0,dim) :
        s = s + " [" + str(self._Cm1[i,j]) + "] "
      s = s + "\n    "
    return s
  
  def evalFunc (self, Func):
    return Func(self._C * self._w + self._x)
  
  def evalGrad (self, Grad):
    return Grad(self._C * self._w + self._x) * self._C
  
  def evalFuncOrder1 (self, Func, Grad): #faux?
    cdef P_matrix_interval midw = (self._w).center()
    midwp = parallelotope(self._C, midw, self._x)
    return midwp.evalFunc(Func) + ( self.evalGrad(Grad) * ( self._w - midw ) )
  
  def getw (self):
    return self._w
  
  def setw (self, w):
    self._w = P_matrix_interval(w)
  
  def getx (self):
    return self._x
  
  def getC (self):
    return self._C

  def inputSide(self):
    res = self.copy()
    cdef int dim = res._w.nrows()
    res._w[dim-1,0] = (self._w[dim-1,0]).lower()
    return res
  
  def outputSide(self):
    res = self.copy()
    cdef dim = res._w.nrows()
    res._w[dim-1,0] = (self._w[dim-1,0]).upper()
    #res._w[dim-1,0] = RIF(self._w[dim-1,0].upper(),self._w[dim-1,0].upper())
    return res
  
  def middle(self):
    return (self._C).center() * (self._w).center() + (self._x).center()
  
  def hull(self):
    return self._C * self._w + self._x
  
  def overlaps_with_box(self, box_matrix):
    return Coverlaps( (self._Cm1)*(box_matrix - self._x) ,self._w)
    
      
  def empty_intersection(self,p):
    [overlapsT, isIn, isinInt, intersection] = Coverlaps( (self._Cm1)*(p.hull() - self._x) ,self._w) 
    return not overlapsT
  
  def empty_intersection_hulls(self,p):
    [overlapsT, isIn, isinInt, intersection] = Coverlaps( self.hull() ,p.hull() ) 
    return not overlapsT
  
  ##def is_in(self,p): # TODO
    ##[overlapsT, isIn, isinInt, intersection] = overlaps( (self._Cm1)*(p.hull() - self._x) ,self._w) 
    ##return overlapsT

  def is_in(self,p):
    [overlapsT, isIn, isinInt, intersection] = Coverlaps( self._w, (self._Cm1)*(p.hull() - self._x))
    return isIn

  def get_corners2D(self):
    c0 = (self._C).center() * P_matrix_interval( [[self._w[0,0].lower()],[self._w[1,0].lower()] ] ) + (self._x).center()
    c1 = (self._C).center() * P_matrix_interval( [[self._w[0,0].upper()],[self._w[1,0].lower()] ] ) + (self._x).center()
    c2 = (self._C).center() * P_matrix_interval( [[self._w[0,0].upper()],[self._w[1,0].upper()] ] ) + (self._x).center()
    c3 = (self._C).center() * P_matrix_interval( [[self._w[0,0].lower()],[self._w[1,0].upper()] ] ) + (self._x).center()
    p0 = [ c0[0,0].center(), c0[1,0].center() ]
    p1 = [ c1[0,0].center(), c1[1,0].center() ]
    p2 = [ c2[0,0].center(), c2[1,0].center() ]
    p3 = [ c3[0,0].center(), c3[1,0].center() ]
    return [p0,p1,p2,p3]

def krawczykContract( Func, Grad, p):
  #print "**********************tracker.sage: krawczykContract"
  # Func function R^{n+1}->R^{n}
  # Grad Matrix n lines, n+1 columns
  # p parallelotope with C Matrix (n+1)*(n+1), x and w matrices (n+1)*1
  cdef int dim = p.getw().nrows()
  cdef P_matrix_interval wmat = p.getw()
  cdef P_matrix_interval midw = wmat.center()
  dim = wmat.nrows()
  cdef P_matrix_interval umat = wmat.delete_rows([dim-1]) # matrice n*1 of intervals
  cdef P_matrix_interval vmat = P_matrix_interval( [ wmat[dim-1,0] ] ) # matrice 1*1 of intervals
  cdef P_matrix_interval midu = midw.delete_rows([dim-1]) # matrice n*1 of intervals
  cdef P_matrix_interval midv = P_matrix_interval( [ midw[dim-1,0] ] ) # matrice 1*1 of intervals
   
  cdef P_matrix_interval miduvmat = midu.augment_row(vmat)
  cdef parallelotope miduvp = parallelotope( p.getC(), miduvmat, p.getx() ) # parallelotope
  
  cdef P_matrix_interval Au = (p.evalGrad(Grad)).delete_columns([dim-1])
  cdef P_matrix_interval Av = (miduvp.evalGrad(Grad)).column(dim-1)
  cdef parallelotope midwp = parallelotope(p.getC(), midw, p.getx() )
  
  cdef P_matrix_interval b = midwp.evalFunc(Func) + Av*(vmat - midv)
  cdef P_matrix_interval res = midu - b - (Au +(-1))*(umat - midu)
  #print "**********************tracker.sage: krawczykContract end"
  return res

def is_equal(B1,B2):
  stop = True
  cdef int dim = B1.nrows()
  cdef int i=0
  while stop and i < dim:
    stop = stop and ( B1[i,0].lower() == B2[i,0].lower() ) \
                and ( B1[i,0].upper() == B2[i,0].upper() )
    i = i+1
  return stop

def contract ( Func, Grad, p, MAX_ITS = 15 ):
  #print "**********************tracker.sage: contract"
  cdef int k=0
  cdef P_matrix_interval w = p.getw()
  cdef int dim = w.nrows()
  cdef P_matrix_interval uvar = w.delete_rows([dim-1])
  stop = True
  cdef P_matrix_interval newu
  while k<MAX_ITS and stop :
    k=k+1
    newu = krawczykContract( Func, Grad, p)
    [overlapsT, isIn, isInInt, newu] = Coverlaps(newu,uvar)
    stop = overlapsT and isIn and not is_equal(newu,uvar)
    p.setw( newu.augment_row( P_matrix_interval( [w[dim-1,0]] ) ) )
    uvar = newu
  #print "**********************tracker.sage: contract end"
  
def diffMax(a,b):
  cdef int dim = a.nrows()
  res = max ( abs(a[0,0].lower()-b[0,0].lower() ), abs(a[0,0].upper()-b[0,0].upper()) )
  for i in range(1,dim):
    temp = max ( abs(a[i,0].lower()-b[i,0].lower() ), abs(a[i,0].upper()-b[i,0].upper()) ) 
    if temp > res :
      res = temp
  return res

def m11(dim):
  cdef cppinterval temp = cppinterval(-1.,1.) 
  return createFromDimValue( dim-1, 1, temp )

def N_Inflate( Func, Grad, p, mu, delta, chi, MAX_ITS = 15):
  #print "**********************tracker.sage: N_Inflate"
  #cdef int k=0
  #cdef int m=0
  #cdef int d = 1
  k=0
  m=0
  d = 1
  success = False
  cdef P_matrix_interval w = p.getw()
  cdef P_matrix_interval wsave = w.copy()
  dim = w.nrows()
  cdef P_matrix_interval cm11 = m11(dim)*chi
  cdef P_matrix_interval uvar = w.delete_rows([dim-1])
  cdef P_matrix_interval newu
  cdef P_matrix_interval newmidu
  while (not success) and (k<MAX_ITS) and (m<mu) :
    k = k+1
    #print "d'abord ici"
    newu = krawczykContract( Func, Grad, p)
    dH = diffMax(newu,uvar)
    #print "ici"
    [overlapsT, isIn, success, uvar] = Coverlaps(newu,uvar)
    if not success:
      if k>=2 : mu = dH/d
      d = dH
      newmidu = newu.center()
      uvar = newmidu + (newu - newmidu)*delta + cm11 
      p.setw( uvar.augment_row( P_matrix_interval( [w[dim-1,0]] ) ) )
  if not success :
    p.setw(wsave)
    #print "**********************tracker.sage: N_Inflate fails: k, k<MAX_ITS, m<mu: ", k, ", ", k<MAX_ITS, ", ", m<mu
  #print "**********************tracker.sage: N_Inflate end"
  return success

def kernel(fprimex):
  #print "**********************tracker.sage: kernel"
  #fprimex is a n\times n+1 matrix
  cdef int nbcols = fprimex.ncols()
  cdef int deleted_columns = nbcols-1
  cdef P_matrix_interval scd_mbr = fprimex.column(deleted_columns)
  cdef P_matrix_interval fst_mbr = fprimex.delete_columns( [deleted_columns] ) 
  cdef P_matrix_interval ker
  test = fst_mbr.is_invertible()
  
  while (not test) and (deleted_columns>=0):
    scd_mbr = fprimex.column(deleted_columns)
    fst_mbr = fprimex.delete_columns( [deleted_columns] )
    test = fst_mbr.is_invertible()
    if not test:
      deleted_columns = deleted_columns -1
  if deleted_columns == -1: # jacobian matrix is not of full rank 
    res = False
    ker = P_matrix_interval([])
  else :
    #print "**********************tracker.sage: kernel, is full rank"
    res=True
    ker = - fst_mbr.inverse()*scd_mbr
    kerlist = ker.list()
    #kerlist.insert(deleted_columns,[RIF(1.,1.)])
    kerlist.insert(deleted_columns,[1.])
    ker = P_matrix_interval(kerlist)
  #print "**********************tracker.sage: kernel end"
  return [res, ker]

def buildTrialParallelotope( Grad, x, y, first_time, h, sign):
  #print "**********************tracker.sage: buildTrialParallelotope"
  cdef P_matrix_interval fprimex = Grad(x)
  cdef int nbcols = fprimex.ncols()
  cdef P_matrix_interval ker
  cdef P_matrix_interval w
  cdef parallelotope pres
  [isFullRank,ker] = kernel(fprimex)
  
  if not isFullRank :
    return [False, parallelotope(P_matrix_interval([]), P_matrix_interval([]), x)]

  ker = ker*(1/ker.norm())
  ker = ker.transpose()
  cdef P_matrix_interval Cm1 = fprimex.augment_row(ker)
  #check the sign, inverse ker if necessary
  dCm1 = Cm1.determinant()
  if (dCm1*sign < 0) : 
    ker = ker*(-1)
    Cm1 = fprimex.augment_row(ker)
  #check if invertible
  if (not Cm1.is_invertible()):
    return [False, parallelotope(P_matrix_interval([]), P_matrix_interval([]), x)]
  #centers: otherwise the widths of elements of the matrix grow
  Cm1 = Cm1.center()
  cdef P_matrix_interval C = Cm1.inverse()
  #centers: otherwise the widths of elements of the matrix grow
  C = C.center()
  if first_time :
    
    #wlist = [[RIF(0,0)]]
    #for i in range(1,nbcols-1):
      #wlist.append([RIF(0,0)])
    #wlist.append([RIF(-h/2,h/2)]) #in order the first parallelotope contains the initial point
    #pres = parallelotope(C, P_matrix_interval(wlist), x)
    
    #cdef cppinterval temp = cppinterval(0.) 
    w = createFromDimValue( nbcols, 1, cppinterval(0.,0.) )
    (w._mat).set(nbcols-1,0, cppinterval(-h/2,h/2))
    pres = parallelotope(C, w, x)
    
  else:
    w = Cm1*(y.hull()-x) #in order the parallelotope contains the output point of the last parallelotope
    for i in range(0,nbcols-1):
      w[i,0] = w[i,0].union(0)
    #w[nbcols-1,0] = w[nbcols-1,0].union(RIF(0,h))
    w[nbcols-1,0] = w[nbcols-1,0].union(0)
    w[nbcols-1,0] = w[nbcols-1,0].union(h)
    pres = parallelotope(C, w, x)
  pres.setCm1(Cm1)
  #print "**********************tracker.sage: buildTrialParallelotope end"
  return [True, pres]

def no_backtrack (k, xk, xkm1, yk, ykm2):
  if k==1 : 
      return True
  
  #print "tracker_boost.pyx, no_backtrack, yk.empty_intersection_hulls(xkm1): ", yk.empty_intersection_hulls(xkm1)
  #print "tracker_boost.pyx, no_backtrack, yk.empty_intersection(xkm1):       ", yk.empty_intersection(xkm1)
  #print "tracker_boost.pyx, no_backtrack, xkm1.empty_intersection(yk):       ", xkm1.empty_intersection(yk)
  #print "tracker_boost.pyx, no_backtrack, ykm2.empty_intersection_hulls(xk): ", ykm2.empty_intersection_hulls(xk)
  #print "tracker_boost.pyx, no_backtrack, xk.empty_intersection(ykm2):       ", xk.empty_intersection(ykm2)
  
  res = yk.empty_intersection_hulls(xkm1) or xkm1.empty_intersection(yk) or yk.empty_intersection(xkm1)
  if not res: 
      return False
  res = ykm2.empty_intersection_hulls(xk) or xk.empty_intersection(ykm2) or ykm2.empty_intersection(xk) 
  return res

def looping_status (k, xk, y0):
  return k==1 or\
         ( y0.empty_intersection(xk) or y0.empty_intersection_hulls(xk) ) or\
         y0.is_in(xk)
     
def is_in_domain (k,xk,yk,domain):
 [overlaps1, isIn1, isInInt1, intersection1] = Coverlaps(yk.hull(),domain)
 [overlaps2, isIn2, isInInt2, intersection2] = Coverlaps(xk.hull(),domain)
 return (not overlaps1) or isIn1

def adjust_Stepsize(h,alpha,beta,valid):
  if valid:
    return beta*h
  else :
    return alpha*h

def loops (k,xk,y0,valid):
  return valid and k>=2 and y0.is_in(xk)

def out_of_domain(yk,domain, valid):
  if valid == False: 
      return False
  [overlaps1, isIn1, isInInt1, intersection1] = Coverlaps(yk.hull(),domain)
  return not overlaps1

def stepsize_too_small(h,hmin):
  return h<=hmin
