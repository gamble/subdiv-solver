#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++

from subdivision_solver.boost.interfaceboost cimport cppinterval

cdef extern from "../../build/matrix_boost.h" namespace "interval_boost":
        
  cppclass matrix_interval:
    matrix_interval()
    matrix_interval(matrix_interval)
    matrix_interval(int, int)
    matrix_interval(int, int, cppinterval *)
    int getNbLines()
    int getNbColumns()
    cppinterval operator()(int , int )#trop ambigu...
    cppinterval &operator()(int , int )#trop ambigu...
    cppinterval get(int , int )
    void set(int , int , cppinterval ) 
    
    #arithmetic operators:
    matrix_interval(int, int, cppinterval) # -> A*Id
    void operator=(matrix_interval)
    matrix_interval operator+(matrix_interval)
    matrix_interval operator-(matrix_interval)
    matrix_interval operator-()
    matrix_interval operator*(matrix_interval)
    matrix_interval operator*(cppinterval)
    #other operators
    matrix_interval delL(int)
    matrix_interval delC(int)
    matrix_interval transpose()
    matrix_interval line(int)
    matrix_interval column(int)
    cppinterval norm()
    matrix_interval augment_cols(matrix_interval)
    matrix_interval augment_rows(matrix_interval)
    cppinterval det()
    
  ctypedef struct intersection_result_interval:
    int _overlaps
    int _isIn
    int _isInInt
    matrix_interval _intersection

cdef extern from "../../build/matrix_boost.h":
  cdef matrix_interval centerInterval(matrix_interval)
  cdef int inverse(matrix_interval, matrix_interval *)
  cdef intersection_result_interval m_overlaps "overlaps"(matrix_interval, matrix_interval )
  
