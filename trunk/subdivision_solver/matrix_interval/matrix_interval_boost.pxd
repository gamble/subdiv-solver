#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++

include 'matrix_boost.pxi'

from subdivision_solver.boost.interfaceboost cimport cppinterval
from subdivision_solver.boost.sageboost cimport to_cppinterval, from_cppinterval 

from sage.rings.real_mpfr cimport RealNumber, RealField, RealField_class

from sage.rings.real_mpfi cimport RealIntervalFieldElement, RealIntervalField_class

from sage.rings.real_mpfi import  RealIntervalField

  
#cdef extern from "../build/matrix_boost.h" namespace "interval_boost":
        
  #cppclass matrix_interval:
    #matrix_interval()
    #matrix_interval(matrix_interval)
    #matrix_interval(int, int)
    #matrix_interval(int, int, cppinterval *)
    #int getNbLines()
    #int getNbColumns()
    #cppinterval operator()(int , int )#trop ambigu...
    #cppinterval &operator()(int , int )#trop ambigu...
    #cppinterval get(int , int )
    #void set(int , int , cppinterval )
    
  #ctypedef struct intersection_result_interval:
    #int _overlaps
    #int _isIn
    #int _isInInt
    #matrix_interval _intersection

#cdef extern from "../build/matrix_boost.h" : 
  #intersection_result_interval overlaps[cppinterval](matrix_interval, matrix_interval )
    
cdef class P_matrix_interval:
  
  cdef matrix_interval _mat
  cdef matrix_interval _inv
  cdef int _has_been_inverted
  
  cpdef int nrows(self)
  cpdef int ncols(self)
  cpdef P_matrix_interval copy(self)

cdef P_matrix_interval createFromDimValue( int nbRows, int nbCols, cppinterval value )

cdef Coverlaps( P_matrix_interval X, P_matrix_interval Y )
