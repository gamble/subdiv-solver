#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++ 

include "solver_boost.pxi"

from subdivision_solver.matrix_interval.matrix_interval_boost cimport matrix_interval   
from subdivision_solver.matrix_interval.matrix_interval_boost cimport P_matrix_interval

from subdivision_solver.system.system    cimport systempols
from subdivision_solver.system.eval_boost cimport init_eval
from subdivision_solver.system.eval_boost cimport fast_func_Sys, fast_func_JacSys, fast_func_HessSys
from subdivision_solver.system.eval_boost cimport fast_CEZ_Sys, fast_CEZ_JacSys, fast_CEZ_HessSys
from subdivision_solver.system.eval_boost cimport fast_CNZ_Sys, fast_CNZ_JacSys, fast_CNZ_HessSys 


cdef CSolveFromList( systempols system,\
                     object l,\
                     float minsize,\
                     int dealWithBorder,\
                     object initialDomain,\
                     float minsizeBorder,\
                     int lastPrecision,\
                     int nbsteps,\
                     int orderEval,\
                     int orderKraw,\
                     int controlEvaluationPrecision,\
                     int controlFcenterZero,\
                     int controlKrawczykPrecision,\
                     int plot,\
                     int verbosity)
