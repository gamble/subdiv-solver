#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++  

from sage.rings.real_mpfi import  RealIntervalFieldElement
from sage.rings.real_mpfr import RealNumber, RealField_class, RealField
from sage.rings.integer import Integer

from subdivision_solver.matrix_interval.matrix_interval_mpfi cimport P_matrix_interval

from subdivision_solver.system.system cimport systempols

from subdivision_solver.solver.solver_mpfi  import SolveFromList as SolveFromList_mpfi
from subdivision_solver.solver.solver_mpfi  import ContractFromList as ContractFromList_mpfi
from subdivision_solver.solver.solver_mpfi  import areTheSame as areTheSame_mpfi
from subdivision_solver.solver.solver_mpfi  import Krawczyk as Krawczyk_mpfi
from subdivision_solver.solver.solver_mpfi  import KrawczykTest as KrawczykTest_mpfi
#from subdivision_solver.solver.solver_boost import SolveFromList as SolveFromList_boost
#from subdivision_solver.solver.solver_boost import areTheSame as areTheSame_boost
#from subdivision_solver.solver.solver_boost import Krawczyk as Krawczyk_boost
#from subdivision_solver.solver.solver_boost import KrawczykTest as KrawczykTest_boost

class subdivision_solver_allprec:
  """ A certified solver for systems of large and dense polynomials
      
      INPUTS :
      
      Inputs of the initializer of the class are:
      
      + ptabs: a sage list of sage polynomials
      
      + vtabs: a sage list of the variables of the polynomial ring where elements of ptabs lie
       
      Inputs of the method solve are:
      
      + domain: a list of RealIntervalField elements corresponding to the domain where the system is solved. The i-th element of the list is the domain where values for the i-th element of vtabs is sougth. 
      The precision of the first component of the domain is used to determine the precision initialy used to solve the system.
      
      + minWidth: the minimum width of components of boxes that are explored. If minwidth is 0, the computation can never stops.
      
      + maxprec: upper limit of precision used to solve the system 
      
      + verbose_mode: a string taking its values in 'silent' (nothing is displayed during the solving), 'stats_color' (stats of the solving are displayed with colors), 'stats' (stats of the solving are displayed).
      
      OUTPUTS :
      
      The method solve returns an integer describing the status of the solving.
      We give here the correspondance between the integer value ad its meaning:
      
      + 0 : success! 
      
      The system has been successfully solved within the input domain.
      Each box given as results contains a unique solution of the system, 
      and the complementary of the union of boxes given as results does not contains
      any solution.
      
      + 1 : requires more precision
      
      The system has not been successfully solved within the input domain.
      Each box given as results contains a unique solution of the system, 
      but due to a lack of precision, sub-domains given in the list of 
      undetermined boxes. They may contain solutions.
      
      + 2 : minimum width reached  
      
      The system has not been successfully solved within the input domain.
      Each box given as results contains a unique solution of the system,
      but some sub-domains given in the list of undetermined boxes where 
      not explored. The minimum width of boxes given as input has been 
      reached for some boxes.
      
      + 3 : bad arguments.
      
      The method getSolutions() returns a sage list which elements are boxes containing a unique solution, represented by sage list of RealIntervalField elements.
      If a positive real number w is given as argument to getSolutions, then solutions are returned in boxes having width less than w if the maximum precision allows it. Otherwise solutions are returned in boxes having width as small as allowed by maximum precision.
      
      The method getUndetermined() returns a sage list which elements are boxes that have not been explored, either because they are too small or because nore precision is required to explore it. They are given as sage list of RealIntervalField elements.
      
      The method getNbIterations() returns the number of boxes that have been considered during the resolution.
      
      The method getSolvingStatus() returns the solving status.
      
      EXAMPLES
      
      sage: RIF = RealIntervalField(53)
      sage: sage.rings.real_mpfi.printing_style='brackets'
      sage: Rr.<x> = ZZ[]
      sage: p = x^2 - 4*x + 3 # solutions are 1 and 3
      sage: system = subdivision_solver([p],[x])
      sage: status = system.solve( [ [RIF(-1000,1000)] ], 1e-5, 'stats_color' )
      sage: system.getSolutions()
      sage: system.getSolutions(1e-5)
  """
  
  def __init__(self, ptabs, vtabs):
    
    #if minprec < 53:
      #minprec = 53
      
    #if maxprec < 53:
      #maxprec = 53
      
    #self._minprec = minprec
    #self._maxprec = maxprec
    self._minprec = 0
    self._maxprec = 0
    self._actualprec = self._minprec
    self._ptabs = ptabs
    self._vtabs = vtabs
    self._CEZlist = []
    self._CNZlist = []
    
    self._results = []
    self._contractedResults = []
    self._tooSmallUser = []
    self._CCInTooSmallUser = []
    self._tooSmallPrecision = []
    self._notInversible = []
    
    self._nbIts = 0;
    self._solvingStatus = 0
    
    self._actualSyst = systempols(ptabs,vtabs)
    
    self._orderEval = 2
    self._orderKraw = 1
    self._controlFcenterZero = True
    self._controlEvaluationCriterion = True
    self._controlKrawCriterion = True
    #self._ForceKrawCriterion = False
    self._lastPrecision = False
    self._verb = 0
    
    self._minsizeBorder = -1
  
  def __setSolvingStatus(self):
    if ( len(self._CCInTooSmallUser) == 0 ) and ( len(self._tooSmallPrecision) == 0 ) and ( len(self._notInversible) == 0 ):
      self._solvingStatus = 0
      return
    if ( len(self._CCInTooSmallUser) == 0 ) and ( len(self._notInversible) == 0 ):
      self._solvingStatus = 1
      return 
    self._solvingStatus = 2
    return 
  
  def getSolutions(self, width=None, verbose_mode =0 ):
    if width==None :
      return self.__getSolutionsAsFound()
    else :
      return self.__getSolutionsWithPrecision(width, verbose_mode)
    
  def __getSolutionsAsFound(self):
    return self._results
  
  def __getSolutionsWithPrecision(self,width, verbose_mode):
    self.__contractSolutions(width, verbose_mode)
    return self._contractedResults
  
  def getUndetermined(self):
    self._CCInTooSmallUser.extend(self._tooSmallPrecision)
    self._CCInTooSmallUser.extend(self._notInversible)
    return self._CCInTooSmallUser;
  
  def getNotInvertible(self):
    return self._notInvertible
  
  def getNbIterations(self):
    return self._nbIts
  
  def getSolvingStatus(self):
    return self.getSolvingStatus
  
  def addConstraintsEqZero(self, CEZlist):
    self._CEZlist = CEZlist
    (self._actualSyst).addConstraintsEqZero(self._CEZlist)
    
  def addConstraintsNeqZero(self, CNZlist):
    self._CNZlist = CNZlist
    (self._actualSyst).addConstraintsNeqZero(self._CNZlist)
    
  def areTheSame(self, sol1, sol2, verbose_mode):
    #NOTE: precondition: sol1 and sol2 are s.t. K(sol1)\in\interior(sol1) and K(sol2)\in\interior(sol2)
    #otherwise the result could be 2 := can not decide
    return areTheSame_mpfi(self._actualSyst, self._actualprec, sol1, sol2, verbose_mode)
  
  def __initLists(self):
    #reinitialize lists
    self._results = []
    self._contractedResults = []
    self._tooSmallUser = []
    self._CCInTooSmallUser = []
    self._tooSmallPrecision = []
    self._notInversible = []
    
  def __initPrec(self, tab, maxprec):
    #init precision
    self._minprec = tab[0][0].precision()
    #if self._minprec <= 53 :
      #self._minprec = 53
    self._maxprec = maxprec
    if self._maxprec < self._minprec:
      self._maxprec = self._minprec
    self._actualprec = self._minprec
    
  def __initVerbosity(self, verbose_mode):
    #init verbose mode
    self._verb=0
    if verbose_mode == 'stats_color':
      self._verb = 1
    if verbose_mode == 'stats':
      self._verb = 2
    if verbose_mode == 3 :
      self._verb = 3
      
    
  def solve(self, tab, minsize, maxprec, verbose_mode ):
    #check arguments
    if self.__checkSolveArguments(tab, minsize, maxprec, verbose_mode) == 3:
      return 3
    
    ##reinitialize lists
    self.__initLists()
    
    ##init precision
    self.__initPrec(tab, maxprec)
    
    #init step counting
    _limitedNbSteps = False
    _itLeft = -1 
    _nbSteps = 0
    itTemp=0
    
    ##init verbose mode
    self.__initVerbosity(verbose_mode)
    
    #_controlKrawczykPrecision = True
    if (self._actualprec==self._maxprec):
      #_controlKrawczykPrecision = False
      self._lastPrecision = True
    
    [self._results, tooSmallPrecision, self._tooSmallUser, self._CCInTooSmallUser, self._notInversible, itTemp] = SolveFromList_mpfi( self._actualSyst, [tab], self._actualprec, minsize, True, tab, self._minsizeBorder, self._lastPrecision, _itLeft,\
                           self._orderEval,\
                           self._orderKraw,\
                           self._controlEvaluationCriterion,\
                           self._controlFcenterZero,\
                           self._controlKrawCriterion,\
                           self._verb)
    
    _nbSteps = _nbSteps + itTemp;
    
    while ( ( self._actualprec < self._maxprec ) and (len(tooSmallPrecision)>0) ):  
        
      #compute new precisiom
      oldprec = self._actualprec
      if (self._actualprec*2 > self._maxprec):
        self._actualprec = self._maxprec
      else :
        self._actualprec = self._actualprec*2
        
      if (self._actualprec==self._maxprec):
        #_controlKrawczykPrecision = False
        self._lastPrecision = True
      
      #solve
      [res, tooSmallPrecision, tooSmallUser, CCInTooSmallUser, notInversible, itTemp] = SolveFromList_mpfi( self._actualSyst, tooSmallPrecision, self._actualprec, minsize, True, tab, self._minsizeBorder, self._lastPrecision, _itLeft,\
                           self._orderEval,\
                           self._orderKraw,\
                           self._controlEvaluationCriterion,\
                           self._controlFcenterZero,\
                           self._controlKrawCriterion,\
                           self._verb)
      _nbSteps = _nbSteps + itTemp;
      
      for i in range(0,len(res)):
        self._results.append(res[i])
      for i in range(0,len(tooSmallUser)):
        self._tooSmallUser.append(tooSmallUser[i])
      for i in range(0,len(CCInTooSmallUser)):
        self._CCInTooSmallUser.append(CCInTooSmallUser[i])
      for i in range(0,len(notInversible)):
        self._notInversible.append(notInversible[i])
    
    for i in range(0,len(tooSmallPrecision)):
      self._tooSmallPrecision.append(tooSmallPrecision[i])
    
    self._nbIts = _nbSteps
    self.__setSolvingStatus()
    #print "_nbSteps: ", _nbSteps
    #print "_itLeft: ", _itLeft
    #print notInversible
    #return self._results
    return self._solvingStatus

  def __contractSolutions(self, width, verbose_mode=0):
    
    if (len(self._results)==0) or (self._actualprec == 0) :
      return []
    if (not isinstance(width, RealNumber)) or width < 0 :
      print "getSolutions(): width must be a non negative RealNumber"
      return []
    
    _stopWhenMaxPrecIsReached = True
    if (width>0) :
      _stopWhenMaxPrecIsReached = False
      
    _controlKrawczykPrecision = True #should always be true?
    #if (self._actualprec==self._maxprec):
      #_controlKrawczykPrecision = False
    #print "contractSolutions, precision: ", self._actualprec
    
    if verbose_mode==3:
      print "solver_mp: __contractSolutions: _actualprec: ", self._actualprec
    
    [contracted, tooSmallPrecision, notInversible] = ContractFromList_mpfi(self._actualSyst,self._actualprec,self._results, width, _controlKrawczykPrecision, 0)
    for i in range(0,len(contracted)):
        self._contractedResults.append(contracted[i])
    
    #if len(notInversible): #should never happen when _controlKrawczykPrecision is True
      #print "There where some unsuccessful contractions: boxes:"
      #for i in range(0,len(notInversible)):
        #print "----------------------------"
        #print notInversible[i]
      
    #while ( ( self._actualprec < self._maxprec ) and (len(tooSmallPrecision)>0) ):
    while ( (len(tooSmallPrecision)>0) \
      and   ( ( self._actualprec < self._maxprec ) or (not _stopWhenMaxPrecIsReached) ) ) :
      
      if ( (_stopWhenMaxPrecIsReached) and (self._actualprec*2 > self._maxprec) ):
        self._actualprec = self._maxprec
      else :
        self._actualprec = self._actualprec*2
        
      if verbose_mode==3:
        print "solver_mp: __contractSolutions: _actualprec: ", self._actualprec
        
      #if (self._actualprec==self._maxprec):#should always be true?
        #_controlKrawczykPrecision = False
        
      #print "contractSolutions, precision: ", self._actualprec
      [contracted, tooSmallPrecision, notInversible] = ContractFromList_mpfi(self._actualSyst,self._actualprec,tooSmallPrecision, width, _controlKrawczykPrecision, 0)
      for i in range(0,len(contracted)):
        self._contractedResults.append(contracted[i])
      
      #if len(notInversible): #should never happen when _controlKrawczykPrecision is True
        #print "There where some unsuccessful contractions: boxes" 
        #for i in range(0,len(notInversible)):
          #print "----------------------------"
          #print notInversible[i]
    for i in range(0,len(tooSmallPrecision)): 
      self._contractedResults.append(tooSmallPrecision[i])
    return

  def Krawczyk(self, tab, maxprec, verbose_mode):
    
    #print "--------------solver_mp: begin Krawczyk"
    
    #reinitialize lists
    self.__initLists()
    
    #init precision
    self.__initPrec(tab, maxprec)
    #init verbose mode
    self.__initVerbosity(verbose_mode)
    
    #print "--------------solver_mp: Krawczyk: _actualprec: ", self._actualprec, ", _maxprec: ", self._maxprec
    #print "--------------solver_mp: Krawczyk: _verb: ", self._verb
    
    #_controlKrawczykPrecision = True
    #if (self._actualprec==self._maxprec):
    _controlKrawczykPrecision = False #should always be false
      
    [self._results, self._tooSmallPrecision, self._notInversible, flag] = Krawczyk_mpfi( self._actualSyst, self._actualprec, tab, _controlKrawczykPrecision, self._verb )
      
    while ( ( self._actualprec < self._maxprec ) and (len(self._tooSmallPrecision)>0) ):  
        
      #compute new precisiom
      oldprec = self._actualprec
      if (self._actualprec*2 > self._maxprec):
        self._actualprec = self._maxprec
      else :
        self._actualprec = self._actualprec*2
        
      if (self._actualprec==self._maxprec):
        _controlKrawczykPrecision = False
    
      [self._results, self._tooSmallPrecision, self._notInversible, flag] = Krawczyk_mpfi( self._actualSyst, self._actualprec, tab, _controlKrawczykPrecision, self._verb )
    
    #print "--------------solver_mp: end Krawczyk"
    if ( len(self._results) ==1 ) :
      return self._results[0]
    else :
      
      return self._results
    
  def KrawczykTest(self, tab, maxprec, verbose_mode):
    
    #print "--------------solver_mp: begin KrawczykTest"
    
    #reinitialize lists
    self.__initLists()
    
    #init precision
    self.__initPrec(tab, maxprec)
    #init verbose mode
    self.__initVerbosity(verbose_mode)
    
    #print "--------------solver_mp: KrawczykTest: _actualprec: ", self._actualprec, ", _maxprec: ", self._maxprec
    #print "--------------solver_mp: KrawczykTest: _verb: ", self._verb
    
    _controlKrawczykPrecision = True #should always be True
    #if (self._actualprec==self._maxprec):
      #_controlKrawczykPrecision = False
      
    [self._results, self._tooSmallPrecision, self._notInversible, flag] = KrawczykTest_mpfi( self._actualSyst, self._actualprec, tab, _controlKrawczykPrecision, self._verb )
      
    while ( ( self._actualprec < self._maxprec ) and (len(self._tooSmallPrecision)>0) ):  
        
      #compute new precisiom
      oldprec = self._actualprec
      if (self._actualprec*2 > self._maxprec):
        self._actualprec = self._maxprec
      else :
        self._actualprec = self._actualprec*2
        
      if (self._actualprec==self._maxprec):
        _controlKrawczykPrecision = False
    
      [self._results, self._tooSmallPrecision, self._notInversible, flag] = KrawczykTest_mpfi( self._actualSyst, self._actualprec, tab, _controlKrawczykPrecision, self._verb )
    
    #print "--------------solver_mp: end KrawczykTest"
    #flag values: 1: NO SOLUTION
    #             2: ONE SOLUTION
    #             3: CAN NOT DECIDE
    return flag
  
  def __checkSolveArguments(self, tab, minsize, maxprec, verbose_mode):
      
    if not isinstance(tab, list) :
      print "solve: initial domain must be a list"
      return 3
    if not (len(tab) == len(self._vtabs)):
      print "solve: initial domain has unappropriated dimensions"
      return 3
    
    if not isinstance(tab[0], list):
      print "solve: initial domain must be a list of lists (column vector)"
      return 3
    if not isinstance(tab[0][0], RealIntervalFieldElement):
      print "solve: components of initial domain have to be of type RealIntervalFieldElement"
      return 3
    prec = tab[0][0].precision()
    for i in range(1,len(tab)):
      if not isinstance(tab[i], list) or not isinstance(tab[i][0], RealIntervalFieldElement):
        print "solve: components of initial domain have to be a list of one RealIntervalFieldElement"
        return 3
      if not (prec == tab[i][0].precision()) :
        print "solve: components of initial domain do not have the same precision; ", prec, "will be used as initial precision"
        #return 3
    
    if (not isinstance(minsize, RealNumber)) or minsize < 0:
      print "solve: Minimum size must be a non-negative RealNumber"
      return 3
    
    if (not isinstance(maxprec, Integer)) or maxprec < 0:
      print "solve: Maximum precision must be a non-negative Integer"
      return 3
    
    #if prec < 53 :
      #prec = 53
      #print "solve:  ", prec, "will be used as initial precision"
    if maxprec < prec :
      print "solve: Precision of input box greater than maximum precision; ", prec, "will be used as maximum precision"
      #return 3
    
    return 0
  
  #For TEST only:
  def setMinSizeBorder(self, minSizeBorder):
    if minSizeBorder > 0:
      self._minsizeBorder = minSizeBorder
      
  def setOrderEval(self, order):
    self._orderEval = order
    
  def setOrderKraw(self, order):
    self._orderKraw = order
    
  def ControlKrawCriterion(self, control):
    self._controlKrawCriterion = control
  
  def ControlFcenterZero(self, control):
    self._controlFcenterZero = control
  
  def ControlEvaluationCriterion(self, control):
    self._controlEvaluationCriterion = control
    
  def __getTooSmallUser(self):
    return self._tooSmallUser
  
  def __getCCInTooSmallUser(self):
    return self._CCInTooSmallUser