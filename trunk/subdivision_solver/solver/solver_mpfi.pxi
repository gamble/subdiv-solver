#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++ 

from subdivision_solver.mpfi.interfacempfr cimport cppfloat
from subdivision_solver.matrix_interval.matrix_interval_mpfi cimport matrix_interval

cdef extern from "../../build/solver_mpfi.h" namespace "interval_mpfi":
    cppclass subdivision_solver_i:
      subdivision_solver_i();
      subdivision_solver_i( matrix_interval (*) (matrix_interval),\
                            matrix_interval (*) (matrix_interval),\
                            matrix_interval (*) (matrix_interval) );
      #void initialize( matrix_interval (*) (matrix_interval),\
                       #matrix_interval (*) (matrix_interval),\
                       #matrix_interval (*) (matrix_interval) );
      void setConstraintsEqZero ( matrix_interval (*) (matrix_interval),\
                                  matrix_interval (*) (matrix_interval),\
                                  matrix_interval (*) (matrix_interval) );
      void setConstraintsNeqZero( matrix_interval (*) (matrix_interval),\
                                  matrix_interval (*) (matrix_interval),\
                                  matrix_interval (*) (matrix_interval) );
      void addWorking(matrix_interval);
      int solve(matrix_interval);
      int solve(matrix_interval, int);
      int solve();
      int solve(int);
      int solveMore(int);
      void visitSolutions() ;
      void visitWorking() ;
      void visitTooSmallPrecision() ;
      void visitTooSmallUser() ;
      void visitNotInversible() ;
      int endSolutions() ;
      int endWorking() ;
      int endTooSmallPrecision() ;
      int endTooSmallUser() ;
      int endNotInversible() ;
      matrix_interval nextSolutions() ;
      matrix_interval nextWorking()   ;
      matrix_interval nextTooSmallPrecision() ;
      matrix_interval nextTooSmallUser() ;
      matrix_interval nextNotInversible() ;
      void printStats();
      void printStatsColor();
      int getCounter();
      void setMaxStep(int);
      void setMinWidth(cppfloat);
      void setMinWidthBorder(cppfloat);
      void setVerbosity(int);
      void setControlKrawczykPrecision(int);
      void setLastPrecision(int);
      void setControlFcenterZero(int);
      void setControlEvaluationPrecision(int);
      int contractSolutions(cppfloat relative_width);
      void computeConnectedComponentInTooSmallUser();
      void visitCCInTooSmallUser();
      int endCCInTooSmallUser();
      matrix_interval nextCCInTooSmallUser();
      void dealWithBorder( matrix_interval initialDomain );
      void setOrderEval (  int order  );
      void setOrderKraw (  int order  );
      #void setArgs ( int arg );
      
      int areTheSame(matrix_interval sol1, matrix_interval sol2);
      int ApplyKrawczykOnBox( matrix_interval box );
      int ApplyKrawczykTestOnBox( matrix_interval box );
      
      #Plotting
      void setPlotting(int);
      void visitDisEvaluation();
      void visitDisKrawczyk();
      void visitValKrawczyk();
      int endDisEvaluation();
      int endDisKrawczyk()  ;
      int endValKrawczyk() ;
      matrix_interval nextDisEvaluation();
      matrix_interval nextDisKrawczyk()  ;
      matrix_interval nextValKrawczyk()  ;
