#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++

cdef extern from "../../boost/boostxx.hpp" namespace "cpp_tools" nogil:
    
    ctypedef struct __mpfi_struct:
        pass
    ctypedef __mpfi_struct* mpfi_t
    
    ctypedef long  mp_prec_t
    
    cppclass boostInterval "boost::numeric::interval_lib::unprotect<boost::numeric::interval<double> >::type":
      pass
    
    #ctypedef boostInterval cppnumber
    
    cppclass cppinterval "cpp_tools::boost_class":
        cppinterval()
        #cppinterval(mp_prec_t)
        cppinterval(double, double) nogil
        #cppinterval(cppnumber) nogil
        cppinterval(mpfi_t)
        cppinterval(cppinterval) nogil
        mp_prec_t get_prec()
        double lower "get_left" ()
        double upper "get_right" ()
        
        boostInterval value "_value"
        
    #mp_prec_t get_prec(cppinterval)
        
