#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++

from subdivision_solver.boost.interfaceboost cimport cppinterval
from sage.libs.mpfr       cimport mpfr_t
from sage.rings.real_mpfi cimport mpfi_t, mpfi_set, mpfi_set_q, mpfi_set_z, mpfi_interv_fr, mpfi_interv_d,\
                                  mpfi_get_prec, mpfi_set_si, mpfi_init
from sage.rings.integer   cimport Integer
from sage.rings.rational  cimport Rational
from sage.rings.real_mpfr cimport RealNumber
from sage.rings.real_mpfi cimport RealIntervalFieldElement, RealIntervalField_class
from sage.rings.real_mpfi import  RealIntervalField

cdef inline void to_cppinterval(n, cppinterval* destination):
    cdef RealIntervalFieldElement r
    #print "*************to_cppinterval"
    cdef mpfi_t mpfi_interval
    mpfi_init(mpfi_interval);
    #print "*************to_cppinterval: creation mpfi_interval OK"
    if isinstance(n, Integer):
        #print "*************to_cppinterval: Interger"
        mpfi_set_z (mpfi_interval, (<Integer> n).value)
    elif isinstance(n, RealNumber):
        #print "*************to_cppinterval: RealNumber"
        #mpfr_to_cppinterval(destination, (<RealNumber> n).value)
        mpfi_interv_fr(mpfi_interval, (<RealNumber> n).value, (<RealNumber> n).value)
    elif isinstance(n, Rational):
        #print "*************to_cppinterval: Rational"
        mpfi_set_q (mpfi_interval, (<Rational> n).value)
    elif isinstance(n, RealIntervalFieldElement):
        #print "*************to_cppinterval, RealIntervalFieldElement"
        mpfi_set(mpfi_interval, (<RealIntervalFieldElement> n).value)
        #print "*************to_cppinterval, RealIntervalFieldElement, fin"
    elif isinstance(n, int):
        #print "*************to_cppinterval, int"
        mpfi_set_si (mpfi_interval, <int> n)
    else:
        r = (<RealIntervalField_class> \
             RealIntervalField(destination.get_prec())) (n)
        mpfi_set(mpfi_interval, r.value)
        
    destination.value = cppinterval(mpfi_interval).value
    
cdef inline from_cppinterval (cppinterval* a):
    #print "*************from_cppinterval"
    cdef double l = a.lower()
    cdef double u = a.upper()
    cdef RealIntervalFieldElement r = (<RealIntervalField_class> RealIntervalField(53))._new()
    mpfi_interv_d(r.value, l, u)
    return r
    

