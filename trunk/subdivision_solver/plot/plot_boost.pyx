#**************************************************************************#
#      Copyright (C) 2019 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++  

from sage.rings.real_mpfr import RealNumber, RealField_class, RealField
from sage.rings.real_mpfi import  RealIntervalField, RealIntervalFieldElement
from sage.rings.integer import Integer

from subdivision_solver.implicit_plot.implicit_plot_boost import implicitPlot2D

from sage.plot.text    import text

def implicitPlot(syst, view, minsize, maxprec, *args, **kwargs ):
    """ A certified implicit plot for plane algebraic curves
        
        INPUTS: 
        
        an system of class subdivision_solver with 1 of more polynomial equation
        
        + an initial domain: given either 
        
          ++ as a list [ [Xcenter, Ycenter], [Ywidth, Ywidth] ], or
        
          ++ as a list [ Xmin, Xmax, Ymin, Ymax ], or
                                       
          ++ as a list [ [Xrange], [Yrange] ] where Xrange and Yrange are intervals
        
        + a minimum size for the multi-precision solver
        
        + a maximum precision for the multi-precision solver
        
        + a list of optional arguments:
        
          ++ colors = [ "color1", "color2", ... ];
             this is mandatory if more than 2 polynomials
             
          ++ ...
        
        OUTPUT: 
        
        a list of graphic entity res, that can be plot with res.show()
        
        to adjust visualization domain, use:
        
        res.set_axes_range(xmin, xmax, ymin, ymax)
        
        If there is no warning message, the plot is certified,
        
        and the curves are guaranteed to have no singularities.
        
    """
    RF = RealField(53)
    #minsize = kwargs.get('minsize', RF(1e-16))
    maxsize = kwargs.get('maxsize', RF(1e-1))
    #maxprec = kwargs.get('maxprec', None)
    fast = kwargs.get('fast', "boost")
    colors = kwargs.get('colors', ["purple","turquoise"] )
    verbosity = kwargs.get('verbosity', 0)
    drawParalls = kwargs.get('withParalls', False)
    legend = kwargs.get('withLegend', True)
    #view = kwargs.get('view', None) #either [ center, width ] or [ xmin, xmax, ymin, ymax ]
    
    #if maxprec == None:
        #maxprec = 113
    
    RIF = RealIntervalField(53)
    if len(view)==2:
        if len(view[1])==2:
            Blist = [ [RIF(view[0][0] - (1./2)*view[1][0], view[0][0] + (1./2)*view[1][0])], 
                      [RIF(view[0][1] - (1./2)*view[1][1], view[0][1] + (1./2)*view[1][1])] ]
        else:
            Blist = view
    elif len(view)==4:
        Blist = [ [RIF(view[0], view[1])], 
                  [RIF(view[2], view[3])] ]
    else:
        print "implicitPlot(): bad initial box specification"
        
    [res, status, tsolve, ttrack] = implicitPlot2D( Blist, syst._ptabs[0], syst._vtabs, maxsize, minsize, maxprec, fast, colors[0], drawParalls, verbosity )
    if legend:
        res = res + text("func 1", (1,-0.05), color = colors[0], axis_coords=True, horizontal_alignment='right', axes=False, frame=True)
    ypos = -0.05
    yinc = -0.05
    
    for i in range(1, len(syst._ptabs) ):
        ypos = ypos + yinc
        txt = "func " + str(i+1)
        [restemp, status, tsolve, ttrack] = implicitPlot2D( Blist, syst._ptabs[i], syst._vtabs, maxsize, minsize, maxprec, fast, colors[i], drawParalls,  verbosity )
        res = res+restemp
        if legend:
            res = res + text(txt, (1,ypos), color = colors[i], axis_coords=True, horizontal_alignment='right', axes=False, frame=True)
    
    #res = res + text("func 2", (1,-0.1),  color = colors[1], axis_coords=True, horizontal_alignment='right', axes=False, frame=True)
    
    res.axes(False)
    if not (view==None):
        if len(view)==2:
            if len(view[1])==2:
                res.xmin( view[0][0] - (1./2)*view[1][0] )
                res.xmax( view[0][0] + (1./2)*view[1][0] )
                res.ymin( view[0][1] - (1./2)*view[1][1] )
                res.ymax( view[0][1] + (1./2)*view[1][1] )
            else:
                res.xmin( view[0][0].lower() )
                res.xmax( view[0][0].upper() )
                res.ymin( view[1][0].lower() )
                res.ymax( view[1][0].upper() )
        elif len(view)==4:
            res.xmin( view[0] )
            res.xmax( view[1] )
            res.ymin( view[2] )
            res.ymax( view[3] )
    
    return res

def solveAndPlot( syst, initBox, minsize, maxprec, verbose_mode, *args, **kwargs ):
    
    syst.setPlotting(True)
    status = syst.solve(initBox, minsize, maxprec, verbose_mode)
    
    viewVal = kwargs.get('view', None) #either [ center, width ] or [ xmin, xmax, ymin, ymax ]
    colorsVal = kwargs.get('colors', [ "black", "red", "green" ])
    legend = kwargs.get('withLegend', True)
    opacit = kwargs.get('opacity', [ 0.1, 0.1, 0.1 ])
    
    l1 = syst.plotSubdiv( view=viewVal, colors=colorsVal, withLegend = legend, opacity = opacit )
    
    withImplicitPlot = kwargs.get('withImplicitPlot', False)
    colorsImplicitPlot = kwargs.get('colorsImplicitPlot', ["purple","turquoise"])
    
    RF = RealField(53)
    
    if withImplicitPlot:
        if viewVal == None:
            viewVal = initBox
            
        l2 = implicitPlot( syst, viewVal, RF(1e-16), maxprec, maxsize = 0.1, colors = colorsImplicitPlot, withLegend = legend )
        l1=l1+l2
        l1.set_axes_range(l2.xmin(), l2.xmax(), l2.ymin(), l2.ymax() )
    
    return l1
