#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++

from sage.rings.real_mpfi import  RealIntervalField
from sage.rings.polynomial.multi_polynomial import is_MPolynomial####

cdef class systempols:
  
  #cdef int _nbPols     #number of polynomials
  #cdef int _dimension  #number of variables of polynomials
  #cdef int _nbConstraintsEqZero #number of constraints equal zero
  #cdef int _nbConstraintsNeqZero #number of constraints not equal zero
  
  #cdef public object _dummyPol
  #cdef public object _fdummyPol
  
  #_vtabs     = []      #list of variables
  #_ptab      = []      #list of polynomials
  #_ptabder   = []      #list of lists (one for each polynomial) of first order derivatives
  #_ptabder2  = []      #list of lists (one for each polynomial) of second order derivatives
  #_fptab     = []      #the same as _ptab     but with fast callable ojects
  #_fptabder  = []      #the same as _ptabder  but with fast callable ojects
  #_fptabder2 = []      #the same as _ptabder2 but with fast callable ojects
  ##constraints
  #_CEZtab     = []      #list of constraints equal zero (polynomials)
  #_CEZtabder  = []      #list of lists (one for each polynomial) of first order derivatives
  #_CEZtabder2 = []      #list of lists (one for each polynomial) of second order derivatives
  #_fCEZtab    = []      #the same as _CEZtab     but with fast callable ojects
  #_fCEZtabder = []      #the same as _CEZtabder  but with fast callable ojects
  #_fCEZtabder2= []      #the same as _CEZtabder2 but with fast callable ojects            
  #_CNZtab     = []      #list of constraints not equal zero (polynomials)
  #_CNZtabder  = []      #list of lists (one for each polynomial) of first order derivatives
  #_CNZtabder2 = []      #list of lists (one for each polynomial) of second order derivatives
  #_fCNZtab    = []      #the same as _CNZtab     but with fast callable ojects
  #_fCNZtabder = []      #the same as _CNZtabder  but with fast callable ojects
  #_fCNZtabder2= []      #the same as _CNZtabder2 but with fast callable ojects 
  
  def __cinit__ (self, plist, vlist):
    cdef int n,d,i
    
    ##initialize tables
    self._vtabs     = []  
    self._ptab      = []
    self._ispoly    = []####
    self._ptabder   = []  
    self._ptabder2  = []  
    self._fptab     = []  
    self._fptabder  = []  
    self._fptabder2 = []  
    #constraints
    self._CEZtab     = []
    self._CEZispoly  = []####
    self._CEZtabder  = [] 
    self._CEZtabder2 = [] 
    self._fCEZtab    = [] 
    self._fCEZtabder = [] 
    self._fCEZtabder2= [] 
    self._CNZtab     = []
    self._CNZispoly  = []####
    self._CNZtabder  = [] 
    self._CNZtabder2 = [] 
    self._fCNZtab    = [] 
    self._fCNZtabder = [] 
    self._fCNZtabder2= [] 
  
    #get parameters 
    self._nbPols = len(plist)
    self._dimension = len(vlist)
    self._onlyPolynomials = 1
    self._evalPrec = -1
    self._evalIntArithm = -1
     
    #get variables
    self._vtabs[:] = vlist[:]
    
    #create dummy polynomial
    self._dummyPol = 0;
    for n in range(0, self._dimension):
      self._dummyPol = self._dummyPol + self._vtabs[n];
    
    #get polynomials
    #print "init: _nbPols: ", self._nbPols, ", nbvariables = dimension: ", self._dimension
    #print "variables: ", self._vtabs
    #print "dummyPol: ", self._dummyPol
    #print "fdummyPol: ", self._fdummyPol
    for n in range(0, self._nbPols) :
      #print "++pol ", n, ": ", plist[n]
      self._ptab.append( plist[n] )
      self._fptab.append( 0 )
      if is_Polynomial( plist[n] ) or is_MPolynomial( plist[n] ):####
        self._ispoly.append( 1 )####
      else :####
        self._ispoly.append( 0 )####
      self._onlyPolynomials = self._onlyPolynomials * self._ispoly[n]####
      
      #first order derivatives
      ptabderd = []
      fptabderd = []
      for d in range(0, self._dimension) :
        ptabderd.append( (self._ptab[-1]).derivative( self._vtabs[d] ) )
        fptabderd.append( 0 )
      self._ptabder.append( ptabderd[:] ) #push back in lists
      self._fptabder.append( fptabderd[:] )
      #print "first order derivatives: ", ptabderd
      
      #second order derivatives
      ptabderd2 = []
      fptabderd2 = []
      for d in range(0, self._dimension) :
        for i in range(d, self._dimension) :
          ptabderd2.append( ptabderd[d].derivative( self._vtabs[i] ) )
          fptabderd2.append( 0 )
      self._ptabder2.append( ptabderd2[:] ) #push back in lists
      self._fptabder2.append( fptabderd2[:] )
      
    #global actualsystem
    #actualsystem = self
    
  def addConstraintsEqZero(self, CEZlist):
    cdef int n,d,i
    self._nbConstraintsEqZero = len(CEZlist)
    for n in range(0, self._nbConstraintsEqZero) :
      #print "++pol ", n, ": ", plist[n]
      self._CEZtab.append( CEZlist[n] )
      self._fCEZtab.append( 0 )
      if is_Polynomial( CEZlist[n] ) or is_MPolynomial( CEZlist[n] ):####
        self._CEZispoly.append( 1 )####
      else :####
        self._CEZispoly.append( 0 )####
      self._onlyPolynomials = self._onlyPolynomials * self._CEZispoly[n]####
      
      #first order derivatives
      CEZtabderd = []
      fCEZtabderd = []
      for d in range(0, self._dimension) :
        CEZtabderd.append( (self._CEZtab[-1]).derivative( self._vtabs[d] ) )
        fCEZtabderd.append( 0 )
      self._CEZtabder.append( CEZtabderd[:] ) #push back in lists
      self._fCEZtabder.append( fCEZtabderd[:] )
      #print "first order derivatives: ", ptabderd
      
      #second order derivatives
      CEZtabderd2 = []
      fCEZtabderd2 = []
      for d in range(0, self._dimension) :
        for i in range(d, self._dimension) :
          CEZtabderd2.append( CEZtabderd[d].derivative( self._vtabs[i] ) )
          fCEZtabderd2.append( 0 )
      self._CEZtabder2.append( CEZtabderd2[:] ) #push back in lists
      self._fCEZtabder2.append( fCEZtabderd2[:] )
      #print "second order derivatives: ", ptabderd2
      
  def addConstraintsNeqZero(self, CNZlist):
    cdef int n,d,i
    self._nbConstraintsNeqZero = len(CNZlist)
    for n in range(0, self._nbConstraintsNeqZero) :
      #print "++pol ", n, ": ", plist[n]
      self._CNZtab.append( CNZlist[n] )
      self._fCNZtab.append( 0 )
      if is_Polynomial( CNZlist[n] ) or is_MPolynomial( CNZlist[n] ):####
        self._CNZispoly.append( 1 )####
      else :####
        self._CNZispoly.append( 0 )####
      self._onlyPolynomials = self._onlyPolynomials * self._CNZispoly[n]####
      
      #first order derivatives
      CNZtabderd = []
      fCNZtabderd = []
      for d in range(0, self._dimension) :
        CNZtabderd.append( (self._CNZtab[-1]).derivative( self._vtabs[d] ) )
        fCNZtabderd.append( 0 )
      self._CNZtabder.append( CNZtabderd[:] ) #push back in lists
      self._fCNZtabder.append( fCNZtabderd[:] )
      #print "first order derivatives: ", ptabderd
      
      #second order derivatives
      CNZtabderd2 = []
      fCNZtabderd2 = []
      for d in range(0, self._dimension) :
        for i in range(d, self._dimension) :
          CNZtabderd2.append( CNZtabderd[d].derivative( self._vtabs[i] ) )
          fCNZtabderd2.append( 0 )
      self._CNZtabder2.append( CNZtabderd2[:] ) #push back in lists
      self._fCNZtabder2.append( fCNZtabderd2[:] )
      #print "second order derivatives: ", ptabderd2
  
  def onlyPolynomials(self):####
    return self._onlyPolynomials####
  
  def __str__ (self):
    res = "len(ptab): " + str(len(self._ptab)) + "\nlen(ptabder) : " + str(len(self._ptabder)) + " x " +  str(len(self._ptabder[0])) + "\n"
    res = res + "_ptab[0]: _ispoly[0]: " + str(self._ispoly[0]) + "\n" + str(self._ptab[0]) + "\n" 
    res = res + "_ptab[1]: _ispoly[1]:"  + str(self._ispoly[1]) + "\n" + str(self._ptab[1]) + "\n"
    
    res = res + "_ptabder[0][0]: \n" + str(self._ptabder[0][0]) + "\n" 
    res = res + "_ptabder[0][1]: \n" + str(self._ptabder[0][1]) + "\n"
    res = res + "_ptabder[1][0]: \n" + str(self._ptabder[1][0]) + "\n" 
    res = res + "_ptabder[1][1]: \n" + str(self._ptabder[1][1]) + "\n"
    
    
    #res = res + "fptab[0]: " + str(self._fptab[0]) + "\n"  
    return res