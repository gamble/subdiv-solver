#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++

#generic 
from sage.rings.polynomial.polynomial_element cimport Polynomial, is_Polynomial####
from sage.rings.polynomial.multi_polynomial cimport MPolynomial####
#from sage.rings.polynomial.multi_polynomial import is_MPolynomial####

cdef class systempols:
  
  cdef int _nbPols     #number of polynomials
  cdef int _dimension  #number of variables of polynomials
  cdef int _nbConstraintsEqZero #number of constraints equal zero
  cdef int _nbConstraintsNeqZero #number of constraints not equal zero
  cdef int _onlyPolynomials # 1 if each expression is polynomial, 0 otherwise####
  cdef int _evalPrec # precision of last time evaluation was initialized; -1 if it has never been initialized
  cdef int _evalIntArithm # -1 if it has never been initialized, 0, if it has been initialized with boost, 1 with mpfi
  
  cdef public object _dummyPol #polynomial with all variables of the ring
  cdef public object _fdummyPol #fast version: its input is filled and use as input for other polynomials
  
  cdef public object _vtabs           #list of variables
  cdef public object _ptab            #list of polynomials
  cdef public object _ispoly          #list of integers: _ispoly[i] = 1 if _ptabs[i] is a polynomial; 0 otherwise####
  cdef public object _ptabder         #list of lists (one for each polynomial) of first order derivatives
  cdef public object _ptabder2        #list of lists (one for each polynomial) of second order derivatives
  cdef public object _fptab           #the same as _ptab     but with fast callable ojects
  cdef public object _fptabder        #the same as _ptabder  but with fast callable ojects
  cdef public object _fptabder2       #the same as _ptabder2 but with fast callable ojects
  #constraints
  cdef public object _CEZtab           #list of constraints equal zero (polynomials)
  cdef public object _CEZispoly        #list of integers: _CEZispoly[i] = 1 if _CEZtabs[i] is a polynomial; 0 otherwise####
  cdef public object _CEZtabder        #list of lists (one for each polynomial) of first order derivatives
  cdef public object _CEZtabder2       #list of lists (one for each polynomial) of second order derivatives
  cdef public object _fCEZtab          #the same as _CEZtab     but with fast callable ojects
  cdef public object _fCEZtabder       #the same as _CEZtabder  but with fast callable ojects
  cdef public object _fCEZtabder2      #the same as _CEZtabder2 but with fast callable ojects            
  cdef public object _CNZtab           #list of constraints not equal zero (polynomials)
  cdef public object _CNZispoly        #list of integers: _CNZispoly[i] = 1 if _CNZtabs[i] is a polynomial; 0 otherwise####
  cdef public object _CNZtabder        #list of lists (one for each polynomial) of first order derivatives
  cdef public object _CNZtabder2       #list of lists (one for each polynomial) of second order derivatives
  cdef public object _fCNZtab          #the same as _CNZtab     but with fast callable ojects
  cdef public object _fCNZtabder       #the same as _CNZtabder  but with fast callable ojects
  cdef public object _fCNZtabder2      #the same as _CNZtabder2 but with fast callable ojects
