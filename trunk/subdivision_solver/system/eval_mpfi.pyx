#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++

from sage.ext.fast_callable                  import fast_callable####
from sage.rings.real_mpfi                    import RealIntervalField####
from sage.rings.real_mpfi                   cimport RealIntervalFieldElement####
cdef init_eval(systempols s, int precision):
  
  cdef int prec = precision
  global actualsystem
  actualsystem = s
  if s._evalPrec == prec and s._evalIntArithm == 1:
    return 
  s._evalPrec = prec
  s._evalIntArithm = 1
  
  cdef cppfloat dummy###mpfi
  dummy.set_prec(precision)###mpfi
  cdef cppinterval dummyinterval###mpfi
  dummyinterval.set_prec(precision)###mpfi

  #initialize dummy polynomial
  s._fdummyPol = fast_pol( s._dummyPol, prec)
  
  #initialize polynomials
  for n in range(0, s._nbPols) :
    if s._ispoly[n] == 1 :####
      s._fptab[n] = ( fast_pol( s._ptab[n], prec) )
      #first order derivatives
      for d in range(0, s._dimension) :
        s._fptabder[n][d] = fast_pol(s._ptabder[n][d], prec ) 
      #second order derivatives size 
      for d in range(0, len(s._ptabder2[n])) :
        s._fptabder2[n][d] = fast_pol(s._ptabder2[n][d], prec ) 
    else :####
      s._fptab[n] = fast_callable ( s._ptab[n], vars=s._vtabs, domain=RealIntervalField( prec ) )####
      #s._fptab[n] = fast_callable ( s._ptab[n], vars=s._vtabs )####
      #first order derivatives####
      for d in range(0, s._dimension) :####
        s._fptabder[n][d] = fast_callable(s._ptabder[n][d], vars=s._vtabs, domain=RealIntervalField( prec ) ) ####
        #s._fptabder[n][d] = fast_callable(s._ptabder[n][d], vars=s._vtabs ) ####
      #second order derivatives size ####
      for d in range(0, len(s._ptabder2[n])) :####
        s._fptabder2[n][d] = fast_callable(s._ptabder2[n][d], vars=s._vtabs, domain=RealIntervalField( prec ) ) ####
        #s._fptabder2[n][d] = fast_callable(s._ptabder2[n][d], vars=s._vtabs ) ####
        
  #initialize ConstraintsEqZero #TODO: switcher selon s._CEZispoly[n]
  for n in range(0, s._nbConstraintsEqZero) :
    s._fCEZtab[n] = ( fast_pol( s._CEZtab[n], prec) )
    #first order derivatives
    for d in range(0, s._dimension) :
      s._fCEZtabder[n][d] = fast_pol(s._CEZtabder[n][d], prec ) 
    #second order derivatives size 
    for d in range(0, len(s._CEZtabder2[n])) :
      s._fCEZtabder2[n][d] = fast_pol(s._CEZtabder2[n][d], prec ) 
      
  #initialize ConstraintsNEqZero #TODO: switcher selon s._CNZispoly[n]
  for n in range(0, s._nbConstraintsNeqZero) :
    s._fCNZtab[n] = ( fast_pol( s._CNZtab[n], prec) )
    #first order derivatives
    for d in range(0, s._dimension) :
      s._fCNZtabder[n][d] = fast_pol(s._CNZtabder[n][d], prec ) 
    #second order derivatives size 
    for d in range(0, len(s._CNZtabder2[n])) :
      s._fCNZtabder2[n][d] = fast_pol(s._CNZtabder2[n][d], prec )    
      
  
cdef matrix_interval fast_func_Sys (matrix_interval m):
  
  cdef matrix_interval res
  cdef int r, R, d, N
  cdef fast_pol fp, fpcur
  cdef object arglist####
  cdef object argtuple####
  cdef cppinterval temp####
  cdef RealIntervalFieldElement valeur####
  global actualsystem
  
  R = m.getNbLines()
  N = (<systempols> actualsystem)._nbPols
  res = matrix_interval(N,1)
  fp = (<systempols> actualsystem)._fdummyPol
  for r in range(0,R):
    fp.numbers.input[r] = cppnumber( (m.get(r,0)).value ) 
    
  if actualsystem.onlyPolynomials() == 0 :####
    arglist = []####
    for r in range(0,R):####
      temp = m.get(r,0)####
      arglist.append( from_cppinterval( &temp ) )####
    argtuple = tuple(arglist)####
  
  for d in range(0,N):  
    if actualsystem._ispoly[d] == 1:####
      fpcur = actualsystem._fptab[d]####
      fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
      res.set(d,0, cppinterval( fp.numbers.result.value ))
    else :####
      valeur = (actualsystem._fptab[d]).__call__(*argtuple)####
      temp = cppinterval(valeur.value)####
      res.set(d,0, temp)####
      
  return res

cdef matrix_interval fast_func_JacSys (matrix_interval m):
  
  cdef matrix_interval res
  cdef int r, R, n, N, d, D 
  cdef fast_pol fp, fpcur
  cdef object arglist####
  cdef object argtuple####
  cdef cppinterval temp####
  cdef RealIntervalFieldElement valeur####
  global actualsystem
  
  R = m.getNbLines()
  N = (<systempols> actualsystem)._nbPols
  D = (<systempols> actualsystem)._dimension 
  res = matrix_interval(N,D)
  fp = (<systempols> actualsystem)._fdummyPol
  for r in range(0,R):
    fp.numbers.input[r] = cppnumber( (m.get(r,0)).value )
  
  if actualsystem.onlyPolynomials() == 0 :####
    arglist = []####
    for r in range(0,R):####
      temp = m.get(r,0)####
      arglist.append( from_cppinterval( &temp ) )####
    argtuple = tuple(arglist)####
    
  for n in range(0,N):
    if actualsystem._ispoly[n] == 1:####
      for d in range(0,D): 
        fpcur = actualsystem._fptabder[n][d]
        fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
        res.set(n,d, cppinterval( fp.numbers.result.value )) 
    else :####
      for d in range(0,D):####
        valeur = (actualsystem._fptabder[n][d]).__call__(*argtuple)####
        temp = cppinterval(valeur.value)####
        res.set(n,d, temp)####
      
  return res

cdef matrix_interval fast_func_HessSys (matrix_interval m):
  
  cdef matrix_interval res
  cdef int r, R, n, N, d, D, i, c
  cdef fast_pol fp, fpcur
  cdef object arglist####
  cdef object argtuple####
  cdef cppinterval temp####
  cdef RealIntervalFieldElement valeur####
  global actualsystem
  
  R = m.getNbLines()
  N = (<systempols> actualsystem)._nbPols
  D = (<systempols> actualsystem)._dimension
  
  res = matrix_interval(D,N*D) #horizontal concatenation of Hessian matrices
  
  fp = (<systempols> actualsystem)._fdummyPol
  for r in range(0,R):
    fp.numbers.input[r] = cppnumber( (m.get(r,0)).value )
    
  if actualsystem.onlyPolynomials() == 0 :####
    arglist = []####
    for r in range(0,R):####
      temp = m.get(r,0)####
      arglist.append( from_cppinterval( &temp ) )####
    argtuple = tuple(arglist)####
    
  #i: column index in Hn
  #j: column index in H: j = n*D + i
  for n in range(0,N):
    c = 0 #actual index of table of second order derivatives
    
    if actualsystem._ispoly[n] == 1:####
      for d in range(0,D):
        #element of the diagonal
        fpcur = actualsystem._fptabder2[n][c]
        fast_eval(fp.numbers.result, fp.numbers.input, fpcur)
        res.set(d, d + n*D , cppinterval( fp.numbers.result.value )) 
        c = c + 1 
        for i in range (d+1,D):
          fpcur = actualsystem._fptabder2[n][c]
          fast_eval(fp.numbers.result, fp.numbers.input,fpcur )
          #element d,i of Hessian matrix <-> case c of _fptabder[n]
          res.set(d,i + n*D , cppinterval( fp.numbers.result.value )) 
          #element i,d of Hessian matrix <-> case c of _fptabder[n]
          res.set(i,d + n*D , cppinterval( fp.numbers.result.value )) 
          c = c + 1
    else :####
      for d in range(0,D):####
        #element of the diagonal####
        valeur = (actualsystem._fptabder2[n][c]).__call__(*argtuple)####
        temp = cppinterval(valeur.value)####
        res.set(d, d + n*D, temp)####
        c = c + 1 ####
        for i in range (d+1,D):####
          valeur = (actualsystem._fptabder2[n][c]).__call__(*argtuple)####
          temp = cppinterval(valeur.value)####
          #element d,i of Hessian matrix <-> case c of _fptabder[n]####
          res.set(d,i + n*D, temp)####
          #element i,d of Hessian matrix <-> case c of _fptabder[n]####
          res.set(i,d + n*D, temp)####
          c = c + 1####
  return res

cdef matrix_interval fast_CEZ_Sys (matrix_interval m):
  
  cdef matrix_interval res
  cdef int r, R, d, N
  cdef fast_pol fp, fpcur
  cdef object arglist####
  cdef object argtuple####
  cdef cppinterval temp####
  cdef RealIntervalFieldElement valeur####
  global actualsystem
  
  R = m.getNbLines()
  N = (<systempols> actualsystem)._nbConstraintsEqZero
  res = matrix_interval(N,1)
  fp = (<systempols> actualsystem)._fdummyPol
  for r in range(0,R):
    fp.numbers.input[r] = cppnumber( (m.get(r,0)).value ) 
    
  if actualsystem.onlyPolynomials() == 0 :####
    arglist = []####
    for r in range(0,R):####
      temp = m.get(r,0)####
      arglist.append( from_cppinterval( &temp ) )####
    argtuple = tuple(arglist)####
  
  for d in range(0,N):  
    if actualsystem._CEZispoly[d] == 1:####
      fpcur = actualsystem._fCEZtab[d]####
      fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
      res.set(d,0, cppinterval( fp.numbers.result.value ))
    else :####
      valeur = (actualsystem._fCEZtab[d]).__call__(*argtuple)####
      temp = cppinterval(valeur.value)####
      res.set(d,0, temp)####
      
  return res

cdef matrix_interval fast_CEZ_JacSys (matrix_interval m):
  
  cdef matrix_interval res
  cdef int r, R, n, N, d, D 
  cdef fast_pol fp, fpcur
  cdef object arglist####
  cdef object argtuple####
  cdef cppinterval temp####
  cdef RealIntervalFieldElement valeur####
  global actualsystem
  
  R = m.getNbLines()
  N = (<systempols> actualsystem)._nbConstraintsEqZero
  D = (<systempols> actualsystem)._dimension 
  res = matrix_interval(N,D)
  fp = (<systempols> actualsystem)._fdummyPol
  for r in range(0,R):
    fp.numbers.input[r] = cppnumber( (m.get(r,0)).value )
  
  if actualsystem.onlyPolynomials() == 0 :####
    arglist = []####
    for r in range(0,R):####
      temp = m.get(r,0)####
      arglist.append( from_cppinterval( &temp ) )####
    argtuple = tuple(arglist)####
    
  for n in range(0,N):
    if actualsystem._CEZispoly[n] == 1:####
      for d in range(0,D): 
        fpcur = actualsystem._fCEZtabder[n][d]
        fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
        res.set(n,d, cppinterval( fp.numbers.result.value )) 
    else :####
      for d in range(0,D):####
        valeur = (actualsystem._fCEZtabder[n][d]).__call__(*argtuple)####
        temp = cppinterval(valeur.value)####
        res.set(n,d, temp)####
      
  return res

cdef matrix_interval fast_CEZ_HessSys (matrix_interval m):
  
  cdef matrix_interval res
  cdef int r, R, n, N, d, D, i, c
  cdef fast_pol fp, fpcur
  cdef object arglist####
  cdef object argtuple####
  cdef cppinterval temp####
  cdef RealIntervalFieldElement valeur####
  global actualsystem
  
  R = m.getNbLines()
  N = (<systempols> actualsystem)._nbConstraintsEqZero
  D = (<systempols> actualsystem)._dimension
  
  res = matrix_interval(D,N*D) #horizontal concatenation of Hessian matrices
  
  fp = (<systempols> actualsystem)._fdummyPol
  for r in range(0,R):
    fp.numbers.input[r] = cppnumber( (m.get(r,0)).value )
    
  if actualsystem.onlyPolynomials() == 0 :####
    arglist = []####
    for r in range(0,R):####
      temp = m.get(r,0)####
      arglist.append( from_cppinterval( &temp ) )####
    argtuple = tuple(arglist)####
    
  #i: column index in Hn
  #j: column index in H: j = n*D + i
  for n in range(0,N):
    c = 0 #actual index of table of second order derivatives
    
    if actualsystem._CEZispoly[n] == 1:####
      for d in range(0,D):
        #element of the diagonal
        fpcur = actualsystem._fCEZtabder2[n][c]
        fast_eval(fp.numbers.result, fp.numbers.input, fpcur)
        res.set(d, d + n*D , cppinterval( fp.numbers.result.value )) 
        c = c + 1 
        for i in range (d+1,D):
          fpcur = actualsystem._fCEZtabder2[n][c]
          fast_eval(fp.numbers.result, fp.numbers.input,fpcur )
          #element d,i of Hessian matrix <-> case c of _fptabder[n]
          res.set(d,i + n*D , cppinterval( fp.numbers.result.value )) 
          #element i,d of Hessian matrix <-> case c of _fptabder[n]
          res.set(i,d + n*D , cppinterval( fp.numbers.result.value )) 
          c = c + 1
    else :####
      for d in range(0,D):####
        #element of the diagonal####
        valeur = (actualsystem._fCEZtabder2[n][c]).__call__(*argtuple)####
        temp = cppinterval(valeur.value)####
        res.set(d, d + n*D, temp)####
        c = c + 1 ####
        for i in range (d+1,D):####
          valeur = (actualsystem._fCEZtabder2[n][c]).__call__(*argtuple)####
          temp = cppinterval(valeur.value)####
          #element d,i of Hessian matrix <-> case c of _fptabder[n]####
          res.set(d,i + n*D, temp)####
          #element i,d of Hessian matrix <-> case c of _fptabder[n]####
          res.set(i,d + n*D, temp)####
          c = c + 1####
  return res

cdef matrix_interval fast_CNZ_Sys (matrix_interval m):
  
  cdef matrix_interval res
  cdef int r, R, d, N
  cdef fast_pol fp, fpcur
  cdef object arglist####
  cdef object argtuple####
  cdef cppinterval temp####
  cdef RealIntervalFieldElement valeur####
  global actualsystem
  
  R = m.getNbLines()
  N = (<systempols> actualsystem)._nbConstraintsNeqZero
  res = matrix_interval(N,1)
  fp = (<systempols> actualsystem)._fdummyPol
  for r in range(0,R):
    fp.numbers.input[r] = cppnumber( (m.get(r,0)).value ) 
    
  if actualsystem.onlyPolynomials() == 0 :####
    arglist = []####
    for r in range(0,R):####
      temp = m.get(r,0)####
      arglist.append( from_cppinterval( &temp ) )####
    argtuple = tuple(arglist)####
  
  for d in range(0,N):  
    if actualsystem._CNZispoly[d] == 1:####
      fpcur = actualsystem._fCNZtab[d]####
      fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
      res.set(d,0, cppinterval( fp.numbers.result.value ))
    else :####
      valeur = (actualsystem._fCNZtab[d]).__call__(*argtuple)####
      temp = cppinterval(valeur.value)####
      res.set(d,0, temp)####
      
  return res

cdef matrix_interval fast_CNZ_JacSys (matrix_interval m):
  
  cdef matrix_interval res
  cdef int r, R, n, N, d, D 
  cdef fast_pol fp, fpcur
  cdef object arglist####
  cdef object argtuple####
  cdef cppinterval temp####
  cdef RealIntervalFieldElement valeur####
  global actualsystem
  
  R = m.getNbLines()
  N = (<systempols> actualsystem)._nbConstraintsNeqZero
  D = (<systempols> actualsystem)._dimension 
  res = matrix_interval(N,D)
  fp = (<systempols> actualsystem)._fdummyPol
  for r in range(0,R):
    fp.numbers.input[r] = cppnumber( (m.get(r,0)).value )
  
  if actualsystem.onlyPolynomials() == 0 :####
    arglist = []####
    for r in range(0,R):####
      temp = m.get(r,0)####
      arglist.append( from_cppinterval( &temp ) )####
    argtuple = tuple(arglist)####
    
  for n in range(0,N):
    if actualsystem._CNZispoly[n] == 1:####
      for d in range(0,D): 
        fpcur = actualsystem._fCNZtabder[n][d]
        fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
        res.set(n,d, cppinterval( fp.numbers.result.value )) 
    else :####
      for d in range(0,D):####
        valeur = (actualsystem._fCNZtabder[n][d]).__call__(*argtuple)####
        temp = cppinterval(valeur.value)####
        res.set(n,d, temp)####
      
  return res

cdef matrix_interval fast_CNZ_HessSys (matrix_interval m):
  
  cdef matrix_interval res
  cdef int r, R, n, N, d, D, i, c
  cdef fast_pol fp, fpcur
  cdef object arglist####
  cdef object argtuple####
  cdef cppinterval temp####
  cdef RealIntervalFieldElement valeur####
  global actualsystem
  
  R = m.getNbLines()
  N = (<systempols> actualsystem)._nbConstraintsNeqZero
  D = (<systempols> actualsystem)._dimension
  
  res = matrix_interval(D,N*D) #horizontal concatenation of Hessian matrices
  
  fp = (<systempols> actualsystem)._fdummyPol
  for r in range(0,R):
    fp.numbers.input[r] = cppnumber( (m.get(r,0)).value )
    
  if actualsystem.onlyPolynomials() == 0 :####
    arglist = []####
    for r in range(0,R):####
      temp = m.get(r,0)####
      arglist.append( from_cppinterval( &temp ) )####
    argtuple = tuple(arglist)####
    
  #i: column index in Hn
  #j: column index in H: j = n*D + i
  for n in range(0,N):
    c = 0 #actual index of table of second order derivatives
    
    if actualsystem._CNZispoly[n] == 1:####
      for d in range(0,D):
        #element of the diagonal
        fpcur = actualsystem._fCNZtabder2[n][c]
        fast_eval(fp.numbers.result, fp.numbers.input, fpcur)
        res.set(d, d + n*D , cppinterval( fp.numbers.result.value )) 
        c = c + 1 
        for i in range (d+1,D):
          fpcur = actualsystem._fCNZtabder2[n][c]
          fast_eval(fp.numbers.result, fp.numbers.input,fpcur )
          #element d,i of Hessian matrix <-> case c of _fptabder[n]
          res.set(d,i + n*D , cppinterval( fp.numbers.result.value )) 
          #element i,d of Hessian matrix <-> case c of _fptabder[n]
          res.set(i,d + n*D , cppinterval( fp.numbers.result.value )) 
          c = c + 1
    else :####
      for d in range(0,D):####
        #element of the diagonal####
        valeur = (actualsystem._fCNZtabder2[n][c]).__call__(*argtuple)####
        temp = cppinterval(valeur.value)####
        res.set(d, d + n*D, temp)####
        c = c + 1 ####
        for i in range (d+1,D):####
          valeur = (actualsystem._fCNZtabder2[n][c]).__call__(*argtuple)####
          temp = cppinterval(valeur.value)####
          #element d,i of Hessian matrix <-> case c of _fptabder[n]####
          res.set(d,i + n*D, temp)####
          #element i,d of Hessian matrix <-> case c of _fptabder[n]####
          res.set(i,d + n*D, temp)####
          c = c + 1####
  return res


#cdef matrix_interval fast_CEZ_Sys (matrix_interval m):
  
  ##global time_in_f
  ##global time_in_r
  ##t1 = time.clock()
  
  #cdef matrix_interval res
  #cdef int r, R, d, N
  #cdef fast_pol fp, fpcur
  #R = m.getNbLines()
  #global actualsystem
  #N = (<systempols> actualsystem)._nbConstraintsEqZero
  #res = matrix_interval(N,1)
  #fp = (<systempols> actualsystem)._fdummyPol
  #for r in range(0,R):
    #fp.numbers.input[r] = cppnumber( (m.get(r,0)).value )
  #for d in range(0,N):
    #fpcur = actualsystem._fCEZtab[d]
    
    ##t2 = time.clock()
    #fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
    ##t3 = time.clock()
    ##time_in_f = time_in_f + t3-t2
    
    #res.set(d,0, cppinterval( fp.numbers.result.value ))
  
  ##t4 = time.clock()
  ##time_in_r = time_in_r + t4-t1  
  #return res

#cdef matrix_interval fast_CEZ_JacSys (matrix_interval m):
  
  ##global time_in_f
  ##global time_in_r
  ##t1 = time.clock()
  
  #cdef matrix_interval res
  #cdef int r, R, n, N, d, D 
  #cdef fast_pol fp, fpcur
  #R = m.getNbLines()
  #global actualsystem
  #N = (<systempols> actualsystem)._nbConstraintsEqZero
  #D = (<systempols> actualsystem)._dimension 
  #res = matrix_interval(N,D)
  #fp = (<systempols> actualsystem)._fdummyPol
  #for r in range(0,R):
    #fp.numbers.input[r] = cppnumber( (m.get(r,0)).value ) 
  #for n in range(0,N):
    #for d in range(0,D):
      #fpcur = actualsystem._fCEZtabder[n][d]
      
      ##t2 = time.clock()
      #fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
      ##t3 = time.clock()
      ##time_in_f = time_in_f + t3-t2
      
      #res.set(n,d, cppinterval( fp.numbers.result.value ))
      
  ##t4 = time.clock()
  ##time_in_r = time_in_r + t4-t1 
  #return res

#cdef matrix_interval fast_CEZ_HessSys (matrix_interval m):
  
  ##global time_in_f
  ##global time_in_r
  ##t1 = time.clock()
  
  #cdef matrix_interval res
  #cdef int r, R, n, N, d, D, i, c
  #cdef fast_pol fp
  ##print "debut fast_func_HessSys"
  #R = m.getNbLines()
  #global actualsystem
  #N = (<systempols> actualsystem)._nbConstraintsEqZero
  #D = (<systempols> actualsystem)._dimension
  #res = matrix_interval(D,N*D) #horizontal concatenation of Hessian matrices
  #fp = (<systempols> actualsystem)._fdummyPol
  
  #for r in range(0,R):
    #fp.numbers.input[r] = cppnumber( (m.get(r,0)).value ) 
    
  ##i: column index in Hn
  ##j: column index in H: j = n*D + i
  #for n in range(0,N):
    #c = 0 #actual index of table of second order derivatives
    #for d in range(0,D):
      ##element of the diagonal
      #fpcur = actualsystem._fCEZtabder2[n][c]
      
      ##t2 = time.clock()
      #fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
      ##t3 = time.clock()
      ##time_in_f = time_in_f + t3-t2
      
      #res.set(d, d + n*D , cppinterval( fp.numbers.result.value ))
      #c = c + 1 
      #for i in range (d+1,D):
        #fpcur = actualsystem._fCEZtabder2[n][c]
        
        ##t2 = time.clock()
        #fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
        ##t3 = time.clock()
        ##time_in_f = time_in_f + t3-t2
        
        ##element d,i of Hessian matrix <-> case c of _fptabder[n]
        #res.set(d,i + n*D , cppinterval( fp.numbers.result.value ))
        ##element i,d of Hessian matrix <-> case c of _fptabder[n]
        #res.set(i,d + n*D , cppinterval( fp.numbers.result.value )) 
        #c = c + 1
        
  ##t4 = time.clock()
  ##time_in_r = time_in_r + t4-t1
  #return res

#cdef matrix_interval fast_CNZ_Sys (matrix_interval m):
  
  ##global time_in_f
  ##global time_in_r
  ##t1 = time.clock()
  
  #cdef matrix_interval res
  #cdef int r, R, d, N
  #cdef fast_pol fp, fpcur
  #R = m.getNbLines()
  #global actualsystem
  #N = (<systempols> actualsystem)._nbConstraintsNeqZero
  #res = matrix_interval(N,1)
  #fp = (<systempols> actualsystem)._fdummyPol
  #for r in range(0,R):
    #fp.numbers.input[r] = cppnumber( (m.get(r,0)).value )
  #for d in range(0,N):
    #fpcur = actualsystem._fCNZtab[d]
    
    ##t2 = time.clock()
    #fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
    ##t3 = time.clock()
    ##time_in_f = time_in_f + t3-t2
    
    #res.set(d,0, cppinterval( fp.numbers.result.value ))
  
  ##t4 = time.clock()
  ##time_in_r = time_in_r + t4-t1  
  #return res

#cdef matrix_interval fast_CNZ_JacSys (matrix_interval m):
  
  ##global time_in_f
  ##global time_in_r
  ##t1 = time.clock()
  
  #cdef matrix_interval res
  #cdef int r, R, n, N, d, D 
  #cdef fast_pol fp, fpcur
  #R = m.getNbLines()
  #global actualsystem
  #N = (<systempols> actualsystem)._nbConstraintsNeqZero
  #D = (<systempols> actualsystem)._dimension 
  #res = matrix_interval(N,D)
  #fp = (<systempols> actualsystem)._fdummyPol
  #for r in range(0,R):
    #fp.numbers.input[r] = cppnumber( (m.get(r,0)).value )
  #for n in range(0,N):
    #for d in range(0,D):
      #fpcur = actualsystem._fCNZtabder[n][d]
      
      ##t2 = time.clock()
      #fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
      ##t3 = time.clock()
      ##time_in_f = time_in_f + t3-t2
      
      #res.set(n,d, cppinterval( fp.numbers.result.value ))
  
  ##t4 = time.clock()
  ##time_in_r = time_in_r + t4-t1
  #return res

#cdef matrix_interval fast_CNZ_HessSys (matrix_interval m):
  
  ##global time_in_f
  ##global time_in_r
  ##t1 = time.clock()
  
  #cdef matrix_interval res
  #cdef int r, R, n, N, d, D, i, c
  #cdef fast_pol fp, fpcur
  #R = m.getNbLines()
  #global actualsystem
  #N = (<systempols> actualsystem)._nbConstraintsNeqZero
  #D = (<systempols> actualsystem)._dimension
  
  #res = matrix_interval(D,N*D) #horizontal concatenation of Hessian matrices
  
  #fp = (<systempols> actualsystem)._fdummyPol
  #for r in range(0,R):
    #fp.numbers.input[r] = cppnumber( (m.get(r,0)).value )
  ##i: column index in Hn
  ##j: column index in H: j = n*D + i
  #for n in range(0,N):
    #c = 0 #actual index of table of second order derivatives
    #for d in range(0,D):
      ##element of the diagonal
      #fpcur = actualsystem._fCNZtabder2[n][c]
      
      ##t2 = time.clock()
      #fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
      ##t3 = time.clock()
      ##time_in_f = time_in_f + t3-t2
      
      #res.set(d, d + n*D , cppinterval( fp.numbers.result.value ))
      #c = c + 1 
      #for i in range (d+1,D):
        #fpcur = actualsystem._fCNZtabder2[n][c]
        
        ##t2 = time.clock()
        #fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
        ##t3 = time.clock()
        ##time_in_f = time_in_f + t3-t2
        
        ##element d,i of Hessian matrix <-> case c of _fptabder[n]
        #res.set(d,i + n*D , cppinterval( fp.numbers.result.value ))
        ##element i,d of Hessian matrix <-> case c of _fptabder[n]
        #res.set(i,d + n*D , cppinterval( fp.numbers.result.value ))
        #c = c + 1
        
  ##t4 = time.clock()
  ##time_in_r = time_in_r + t4-t1
  #return res

#cdef matrix_interval fast_func_Sys (matrix_interval m):
  
  ##global time_in_f
  ##global time_in_r
  ##t1 = time.clock()
  #cdef matrix_interval res
  #cdef int r, R, d, N
  #cdef fast_pol fp, fpcur
  
  #R = m.getNbLines()
  #global actualsystem
  #N = (<systempols> actualsystem)._nbPols
  #res = matrix_interval(N,1)
  #fp = (<systempols> actualsystem)._fdummyPol
  #for r in range(0,R):
    #fp.numbers.input[r] = cppnumber( (m.get(r,0)).value ) 
    
  #for d in range(0,N):
    #fpcur = actualsystem._fptab[d]
    
    ##t2 = time.clock()
    #fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
    ##t3 = time.clock()
    ##time_in_f = time_in_f + t3-t2
    #res.set(d,0, cppinterval( fp.numbers.result.value ))
  
      
  ##t4 = time.clock()
  ##time_in_r = time_in_r + t4-t1
  #return res

#cdef matrix_interval fast_func_JacSys (matrix_interval m):
  
  ##global time_in_f
  ##global time_in_r
  ##t1 = time.clock()
  
  #cdef matrix_interval res
  #cdef int r, R, n, N, d, D 
  #cdef fast_pol fp, fpcur
  #R = m.getNbLines()
  #global actualsystem
  #N = (<systempols> actualsystem)._nbPols
  #D = (<systempols> actualsystem)._dimension 
  #res = matrix_interval(N,D)
  #fp = (<systempols> actualsystem)._fdummyPol
  #for r in range(0,R):
    #fp.numbers.input[r] = cppnumber( (m.get(r,0)).value )
  #for n in range(0,N):
    #for d in range(0,D): 
      #fpcur = actualsystem._fptabder[n][d]
      ##print "fpcur: ", fpcur
      ##t2 = time.clock()
      #fast_eval(fp.numbers.result, fp.numbers.input, fpcur )
      ##t3 = time.clock()
      ##time_in_f = time_in_f + t3-t2
      
      #res.set(n,d, cppinterval( fp.numbers.result.value )) 
      
  ##t4 = time.clock()
  ##time_in_r = time_in_r + t4-t1
  #return res

#cdef matrix_interval fast_func_HessSys (matrix_interval m):
  
  ##global time_in_f
  ##global time_in_r
  ##t1 = time.clock()
  
  #cdef matrix_interval res
  #cdef int r, R, n, N, d, D, i, c
  #cdef fast_pol fp, fpcur
  #R = m.getNbLines()
  #global actualsystem
  #N = (<systempols> actualsystem)._nbPols
  #D = (<systempols> actualsystem)._dimension
  
  #res = matrix_interval(D,N*D) #horizontal concatenation of Hessian matrices
  
  #fp = (<systempols> actualsystem)._fdummyPol
  #for r in range(0,R):
    #fp.numbers.input[r] = cppnumber( (m.get(r,0)).value ) 
  ##i: column index in Hn
  ##j: column index in H: j = n*D + i
  #for n in range(0,N):
    #c = 0 #actual index of table of second order derivatives
    #for d in range(0,D):
      ##element of the diagonal
      #fpcur = actualsystem._fptabder2[n][c]
	
      ##print "fpcur: ", fpcur
      ##t2 = time.clock()
      #fast_eval(fp.numbers.result, fp.numbers.input, fpcur)
      ##t3 = time.clock()
      ##time_in_f = time_in_f + t3-t2
      
      #res.set(d, d + n*D , cppinterval( fp.numbers.result.value )) 
      #c = c + 1 
      #for i in range (d+1,D):
        #fpcur = actualsystem._fptabder2[n][c]
	
        ##t3 = time.clock()
        #fast_eval(fp.numbers.result, fp.numbers.input,fpcur )
        ##t4 = time.clock()
        ##time_in_f = time_in_f + t3-t2
        
        ##element d,i of Hessian matrix <-> case c of _fptabder[n]
        #res.set(d,i + n*D , cppinterval( fp.numbers.result.value )) 
        ##element i,d of Hessian matrix <-> case c of _fptabder[n]
        #res.set(i,d + n*D , cppinterval( fp.numbers.result.value )) 
        #c = c + 1
        
  ##t4 = time.clock()
  ##time_in_r = time_in_r + t4-t1
  #return res

#for test only
def test_init_eval(s,precision):
  init_eval(<systempols> s, <int> precision)
  
def testFastcallSys(m) :
  arg = P_matrix_interval( m )
  res = P_matrix_interval( [[],[]] )
  (<P_matrix_interval> res)._mat = fast_func_Sys( (<P_matrix_interval> arg)._mat )
  return res.toList()

def testFastcallJacSys(m) :
  arg = P_matrix_interval( m )
  res = P_matrix_interval( [[],[]] )
  (<P_matrix_interval> res)._mat = fast_func_JacSys( (<P_matrix_interval> arg)._mat )
  return res.toList()

def testFastcallHessSys(m) :
  arg = P_matrix_interval( m )
  res = P_matrix_interval( [[],[]] )
  (<P_matrix_interval> res)._mat = fast_func_HessSys( (<P_matrix_interval> arg)._mat )
  return res.toList()