#**************************************************************************#
#      Copyright (C) 2015 Remi Imbach                                      #
#                                                                          #
#    This file is part of subdivision_solver.                              #
# Licensed under GNU General Public License v3.                            #
#                                                                          #
# This program is free software: you can redistribute it and/or modify     #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# This program is distributed in the hope that it will be useful,          #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
#                                                                          #
#**************************************************************************#
# distutils: language = c++ 

from subdivision_solver.boost.interfaceboost                   cimport cppinterval       
from subdivision_solver.boost.sageboost                        cimport to_cppinterval    
from subdivision_solver.boost.sageboost                        cimport from_cppinterval  
from subdivision_solver.matrix_interval.matrix_interval_boost  cimport matrix_interval   
from subdivision_solver.matrix_interval.matrix_interval_boost  cimport P_matrix_interval 

from fast_polynomial.interfaces.boost           cimport cppnumber  
from fast_polynomial.boost.polynomial           cimport polynomial as fast_pol
from fast_polynomial.boost.evaluation.fast_eval cimport fast_eval  

#from sage.ext                     cimport fast_callable####

from subdivision_solver.system.system cimport systempols

cdef object actualsystem

cdef init_eval(systempols s) 

cdef matrix_interval fast_func_Sys     (matrix_interval m)
cdef matrix_interval fast_func_JacSys  (matrix_interval m)
cdef matrix_interval fast_func_HessSys (matrix_interval m)

cdef matrix_interval fast_CEZ_Sys     (matrix_interval m)
cdef matrix_interval fast_CEZ_JacSys  (matrix_interval m)
cdef matrix_interval fast_CEZ_HessSys (matrix_interval m)

cdef matrix_interval fast_CNZ_Sys     (matrix_interval m)
cdef matrix_interval fast_CNZ_JacSys  (matrix_interval m)
cdef matrix_interval fast_CNZ_HessSys (matrix_interval m)