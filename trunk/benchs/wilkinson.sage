import time
from subdivision_solver import subdivision_solver
prec = 53
maxprec = 113
RIF = RealIntervalField(prec)
RF = RealField(prec)
sage.rings.real_mpfi.printing_style='brackets'

Rr.<x1> =ZZ[]
p1 = (x1-1)
#for i in range (2,16):
#for i in range (2,20):
for i in range (2,22):
  p1 = p1*(x1-i)
  

#p1 = -x1^2 + 2*x1 +1
print p1

#def Xtransform(p, ring): 
  #res = ring(0)
  #pmon = p.monomials()
  #pcoe = p.coefficients()
  #pdegvar = p.degree(x1)
  #for i in range(0,len(pmon)):
    #temp = pmon[i]
    #res = res + pcoe[i]*temp(x1=1)*(x1)^(pdegvar - temp.degree(x1))
  #return res

#dp1=p1.degree()
#p2 = x1^(dp1)*p1.subs(x1=1/x1)
#p2=p2.numerator()

#p2 = Xtransform(p1, Rr)



tab = [ [RIF(-200,200)]  ]
#tab = [ [RIF(9.9,10.01)]  ]
#tab = [ [RIF(9.29,10.31)]  ]
#tab = [[RIF(9.9999969482421882106, 10.000004730224610228)]]

#tab = [ [RIF(4.999542236328125, 5.00030517578125)]  ]
#tab = [ [RIF(10.102081298828125, 10.10284423828125)]  ]
#tab = [ [RIF(10.5438232421875, 10.54534912109375)]  ]

#tab = [ [RIF(9.99999999999999,10.000000000000001)]  ]
#minsize=10e-16
minsize=10e-20
t1 = time.clock()
test = subdivision_solver([p1],[x1])
test.ControlEvaluationCriterion(True)
test.ControlFcenterZero(False)
test.ControlKrawCriterion(True)
test.setOrderKraw(1)
status = test.solve(tab,minsize,maxprec,'stats_color')
#status = test.solve(tab,minsize,226,'stats_color')
#status = test.solve(tab,minsize,113,'stats_color')
#status = test.solve(tab,minsize,113,3)
#status = test.solve(tab,minsize,53,'stats_color')
#status = test.solve(tab,minsize,53,3)
t2 = time.clock()
t6 = t2-t1
print "temps pour realSolver: ", t6, "s"
print "status: ", status
res = test.getSolutions();
print "############################"
print "Nombre de Solutions: ", len(res)
for i in range(0,len(res)):
  #print "Solution : ", res[i][0][0], ", ", res[i][1][0]
  print "Solution : ", res[i][0][0]
print "############################"
#resContr = res = test.getSolutions(1e-1000);
#resContr = res = test.getSolutions(0.);
#print "############################"
#print "Solutions contractees: ", len(resContr)
#for i in range(0,len(resContr)):
  ##print "Solution : ", resContr[i][0][0], ", ", resContr[i][1][0]
  #print "Solution : ", resContr[i][0][0]
#print "############################"
undetermined = test.getUndetermined()
print "############################"
print "Boites indeterminees: ", len(undetermined)
#print "boite : ", undetermined[0][0][0]
#print "boite : ", undetermined[1][0][0]
#for i in range(0,len(undetermined)):
  #print "boite : ", undetermined[i][0][0], ", ", undetermined[i][1][0]
  #print "boite : ", undetermined[i][0][0]
print "############################"

##tab = [ [RIF(-200,200)]  ]
#tab = [ [RIF(-1,1)]  ]
##tab = [ [RIF(9.999999999,10.0000000001)]  ]

##tab = [ [RIF(9.99999999999999,10.000000000000001)]  ]
##minsize=10e-16
#minsize=10e-20
#t1 = time.clock()
#test = subdivision_solver([p2],[x1])
#status = test.solve(tab,minsize,500,'stats_color')
##status = test.solve(tab,minsize,226,'stats_color')
##status = test.solve(tab,minsize,113,'stats_color')
##status = test.solve(tab,minsize,113,3)
##status = test.solve(tab,minsize,53,'stats_color')
##status = test.solve(tab,minsize,53,3)
#t2 = time.clock()
#t6 = t2-t1
#print "temps pour realSolver: ", t6, "s"
#print "status: ", status
#res = test.getSolutions();
#print "############################"
#print "Nombre de Solutions: ", len(res)
#for i in range(0,len(res)):
  ##print "Solution : ", res[i][0][0], ", ", res[i][1][0]
  #print "Solution : ", res[i][0][0]
#print "############################"
##resContr = res = test.getSolutions(1e-1000);
##resContr = res = test.getSolutions(0.);
##print "############################"
##print "Solutions contractees: ", len(resContr)
##for i in range(0,len(resContr)):
  ###print "Solution : ", resContr[i][0][0], ", ", resContr[i][1][0]
  ##print "Solution : ", resContr[i][0][0]
##print "############################"
#undetermined = test.getUndetermined()
#print "############################"
#print "Boites indeterminees: ", len(undetermined)
##print "boite : ", undetermined[0][0][0]
##print "boite : ", undetermined[1][0][0]
##for i in range(0,len(undetermined)):
  ##print "boite : ", undetermined[i][0][0], ", ", undetermined[i][1][0]
  ##print "boite : ", undetermined[i][0][0]
#print "############################"