import time
from subdivision_solver import subdivision_solver
prec = 53
RIF = RealIntervalField(prec)
RF = RealField(prec)
sage.rings.real_mpfi.printing_style='brackets'

x = SR.symbol("x", domain='real')
y = SR.symbol("y", domain='real')
Rr.<x,y> =ZZ[]

c= x + i*y

p = c^4-4*c^3+3*c^2-2*c +1

p1 = real(p)
p2 = imag(p)

tab = [ [RIF(-200,200)], [RIF(-200,200)]  ]
minsize=10e-10
t1 = time.clock()
test = subdivision_solver([p1,p2],[x,y])

#p3 = 0*x + y
p3 = Rr(y)
test.addConstraintsNeqZero([p3])

status = test.solve(tab,minsize,113,'stats_color')
t2 = time.clock()
t6 = t2-t1
print "temps pour realSolver: ", t6, "s"
print "status: ", status
sols = test.getSolutions(1e-10)
print "sols: ", sols
#print "p3(sols[4]):" p3( sols[4] )