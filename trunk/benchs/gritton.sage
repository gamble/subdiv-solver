import time
from subdivision_solver import subdivision_solver
prec = 53
RIF = RealIntervalField(prec)
RF = RealField(prec)
sage.rings.real_mpfi.printing_style='brackets'

Rr.<x1> =ZZ[]
p1 = -371.9362500 - 791.2465656*x1 + 4044.944143*x1^2 + 978.1375167*x1^3 - 16547.89280*x1^4 + 22140.72827*x1^5 - 9326.549359*x1^6 - 3518.536872*x1^7 + 4782.532296*x1^8 - 1281.479440*x1^9 - 283.4435875*x1^10 + 202.6270915*x1^11 - 16.17913459*x1^12 - 8.883039020*x1^13 + 1.575580173*x1^14 + 0.1245990848*x1^15 - 0.03589148622*x1^16 - 0.0001951095576*x1^17 + 0.0002274682229*x1^18

tab = [ [RIF(-12,8)]  ]
minsize=10e-20
t1 = time.clock()
test = subdivision_solver([p1],[x1])
status = test.solve(tab,minsize,113,'stats_color')
t2 = time.clock()
t6 = t2-t1
print "temps pour realSolver: ", t6, "s"
print "status: ", status
res = test.getSolutions();
print "############################"
print "Nombre de Solutions: ", len(res)
for i in range(0,len(res)):
  #print "Solution : ", res[i][0][0], ", ", res[i][1][0]
  print "Solution : ", res[i][0][0]