R := PolynomialRing([x1,x2,x3]): 

randomize():

sigma := 512:
deg := 20  :

p1 := randpoly([x1,x2,x3], degree=deg, dense, coeffs=rand(-2^sigma..2^sigma)):
p2 := randpoly([x1,x2,x3], degree=deg, dense, coeffs=rand(-2^sigma..2^sigma)):
p3 := randpoly([x1,x2,x3], degree=deg, dense, coeffs=rand(-2^sigma..2^sigma)):

#############################################################Creating Arborescence
strDirName := cat("dim3degree",deg,"sigma",sigma,"dense"):

#### IBEX
######## system
strIBEXFileName := cat( strDirName, ".bch"): 
strIBEXFilePath := cat( strDirName, "/", strIBEXFileName):

#### SSLGP
######## system
strSSLGPSysFileName := cat( strDirName, "sys.sage"): 
strSSLGPSysFilePath := cat( strDirName, "/", strSSLGPSysFileName):
strSSLGPFileName := cat( strDirName, ".sage"): 
strSSLGPFilePath := cat( strDirName, "/", strSSLGPFileName):

mkdir(strDirName):

#############################################################Generate IBEX file system
file := FileTools:-Text:-Open(strIBEXFilePath,overwrite=true):
fprintf(file, "Variables\n"):
fprintf(file, "x1 in [-1,1];\n"):
fprintf(file, "x2 in [-1,1];\n"):
fprintf(file, "x3 in [-1,1];\n"):
fprintf(file, "\nConstraints\n"):
fprintf(file, "%a = 0;\n", p1 ):
fprintf(file, "%a = 0;\n", p2 ):
fprintf(file, "%a = 0;\n", p3 ):
fprintf(file, "\nend"):
FileTools:-Text:-Close (file):

#############################################################Generate SSLGP file system
file := FileTools:-Text:-Open(strSSLGPSysFilePath,overwrite=true):
fprintf(file, "Rr.<x1,x2,x3> =ZZ[]\n"):
fprintf(file, "p1 = %a\n", p1 ):
fprintf(file, "p2 = %a\n", p2 ):
fprintf(file, "p3 = %a\n", p3 ):
FileTools:-Text:-Close (file):


file := FileTools:-Text:-Open(strSSLGPFilePath,overwrite=true):
fprintf(file, "import time\n"):
fprintf(file, "from subdivision_solver import subdivision_solver\n"):
fprintf(file, "prec = 53\nRIF = RealIntervalField(prec)\nsage.rings.real_mpfi.printing_style=\'brackets\'\n\n"):
fprintf(file, "load(\"%s\")\n", strSSLGPSysFilePath):
fprintf(file, "tab = [ [RIF(-1,1)],[RIF(-1,1)],[RIF(-1,1)] ]\n" ):
fprintf(file, "minsize=10e-6\n" ):

fprintf(file, "t1 = time.clock()\n" ):
fprintf(file, "test = subdivision_solver([p1,p2,p3],[x1,x2,x3])\n" ):
fprintf(file, "test.setOrder1(2)\n"):
fprintf(file, "status = test.solve(tab,minsize,113,\'stats_color\')\n" ):
fprintf(file, "t2 = time.clock()\n" ):
fprintf(file, "t6 = t2-t1\n" ):
fprintf(file, "print \"solving status: \", status\n"):
fprintf(file, "print \"temps pour realSolver: \", t6, \"s\"\n" ):

#fprintf(file, "t1 = time.clock()\n" ):
#fprintf(file, "test = subdivision_solver([p1,p2,p3],[x1,x2,x3])\n" ):
#fprintf(file, "test.setOrder1(3)\n"):
#fprintf(file, "status = test.solve(tab,minsize,113,\'stats_color\')\n" ):
#fprintf(file, "t2 = time.clock()\n" ):
#fprintf(file, "t6 = t2-t1\n" ):
#fprintf(file, "print \"solving status: \", status\n"):
#fprintf(file, "print \"temps pour realSolver: \", t6, \"s\"\n" ):
#
#fprintf(file, "t1 = time.clock()\n" ):
#fprintf(file, "test = subdivision_solver([p1,p2,p3],[x1,x2,x3])\n" ):
#fprintf(file, "test.setOrder1(1)\n"):
#fprintf(file, "status = test.solve(tab,minsize,113,\'stats_color\')\n" ):
#fprintf(file, "t2 = time.clock()\n" ):
#fprintf(file, "t6 = t2-t1\n" ):
#fprintf(file, "print \"solving status: \", status\n"):
#fprintf(file, "print \"temps pour realSolver: \", t6, \"s\"\n" ):
#
#fprintf(file, "t1 = time.clock()\n" ):
#fprintf(file, "test = subdivision_solver([p1,p2,p3],[x1,x2,x3])\n" ):
#fprintf(file, "test.setOrder1(0)\n"):
#fprintf(file, "status = test.solve(tab,minsize,113,\'stats_color\')\n" ):
#fprintf(file, "t2 = time.clock()\n" ):
#fprintf(file, "t6 = t2-t1\n" ):
#fprintf(file, "print \"solving status: \", status\n"):
#fprintf(file, "print \"temps pour realSolver: \", t6, \"s\"\n" ):

fprintf(file, "print \"############################\"\n" ):
fprintf(file, "res = test.getSolutions()\n" ):
fprintf(file, "print \"Nombre de Solutions: \", len(res)\n" ):
fprintf(file, "for i in range(0,len(res)):\n  print \"Solution : \", res[i][0][0], \", \", res[i][1][0], \", \", res[i][2][0]\nprint \"############################\"\n" ):



FileTools:-Text:-Close (file):