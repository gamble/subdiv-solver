with(RegularChains):
with(ChainTools):
with(Student[VectorCalculus]):
with(LinearAlgebra):

R:=PolynomialRing([z],{x,y});

randomize():

sigma := 8:
deg := 7  :

#p := randpoly([x,y,z], degree=deg, dense, coeffs=rand(-2^sigma..2^sigma)):
#q := diff(p,z):

p := 130+154*x+79*y-189*z+95*x^6*y+121*x^6*z-167*y*z+95*x^5*y^2-16*x^5*y-183*x^5*z^2-100*x^5*z-22*y^2*z+130*y*z^2-65*x^4*y^3-126*x^4*y^2+249*x^4*y-116*x^4*z^3-172*x^4*z^2+109*x^4*z-202*y^3*z+245*y^2*z^2-87*y*z^3+188*x^3*y^4+236*x^3*y^3-162*x^3*y^2-88*x^3*y+161*x^3*z^4+80*x^3*z^3+142*x^3*z^2+190*x^3*z-10*y^4*z-34*y^3*z^2+112*y^2*z^3-234*y*z^4-116*x^2*y^5+209*x^2*y^4-72*x^2*y^3+56*x^2*y^2+43*x^2*y+19*x^2*z^5-129*x^2*z^4-25*x^2*z^3-52*x^2*z^2-14*x^2*z+244*y^5*z-152*y^4*z^2-175*y^3*z^3-109*y^2*z^4-234*y*z^5-124*y^7+21*z^7+123*z^5-15*x^2-233*y^6+126*z^6-77*y^3-61*z^3-229*x^4+196*y^4+220*z^4-212*x^3-236*y^5+160*x^7+77*x^6+111*y^2-144*z^2+60*x^5+75*x^4*y*z^2-128*x^4*y*z-14*x^3*y^3*z-153*x^3*y^2*z^2+231*x^3*y^2*z-92*x^3*y*z^3+42*x^3*y*z^2-164*x^3*y*z-171*x^2*y^4*z+33*x^2*y^3*z^2+250*x^2*y^3*z-46*x^2*y^2*z^3-180*x^2*y^2*z^2-182*x^2*y*z^4+68*x^2*y*z^3-190*x^2*y*z^2-61*x^2*y*z+132*x*y^5*z+43*x*y^4*z^2-41*x*y^4*z+74*x*y^3*z^3-152*x*y^3*z^2-90*x*y^3*z-229*x*y^2*z^4-180*x*y^2*z^3+224*x*y^2*z^2-225*x*y^2*z+171*x*y*z^5+255*x*y*z^4+165*x*y*z^3-78*x*y*z^2+12*x*y*z-180*x^5*y*z-169*x^4*y^2*z-70*x*y^6-230*x*y^5-230*x*y^4-71*x*y^3-12*x*y^2-202*x*y+180*x*z^6+62*x*z^5-81*x*z^4-110*x*z^3-132*x*z^2-34*x*z-218*y^6*z-122*y^5*z^2-61*y^4*z^3-196*y^3*z^4+100*y^2*z^5+247*y*z^6;
q := -189-34*x-167*y-288*z+260*y*z-180*x^5*y-366*x^5*z+490*y^2*z-261*y*z^2-169*x^4*y^2-128*x^4*y-348*x^4*z^2-344*x^4*z-68*y^3*z+336*y^2*z^2-936*y*z^3-14*x^3*y^3+231*x^3*y^2-164*x^3*y+644*x^3*z^3+240*x^3*z^2+284*x^3*z-304*y^4*z-525*y^3*z^2-436*y^2*z^3-1170*y*z^4-171*x^2*y^4+250*x^2*y^3-61*x^2*y+95*x^2*z^4-516*x^2*z^3-75*x^2*z^2-104*x^2*z-244*y^5*z-183*y^4*z^2-784*y^3*z^3+500*y^2*z^4+1482*y*z^5+756*z^5-14*x^2-218*y^6+147*z^6-202*y^3+880*z^3+109*x^4-10*y^4+615*z^4+190*x^3+244*y^5+121*x^6-22*y^2-183*z^2-100*x^5+150*x^4*y*z-306*x^3*y^2*z-276*x^3*y*z^2+84*x^3*y*z+66*x^2*y^3*z-138*x^2*y^2*z^2-360*x^2*y^2*z-728*x^2*y*z^3+204*x^2*y*z^2-380*x^2*y*z+86*x*y^4*z+222*x*y^3*z^2-304*x*y^3*z-916*x*y^2*z^3-540*x*y^2*z^2+448*x*y^2*z+855*x*y*z^4+1020*x*y*z^3+495*x*y*z^2-156*x*y*z+132*x*y^5-41*x*y^4-90*x*y^3-225*x*y^2+12*x*y+1080*x*z^5+310*x*z^4-324*x*z^3-330*x*z^2-264*x*z;



sc := SubresultantChain(p,q,z,R):
S0  := SubresultantOfIndex(0,sc,R):
S1 := SubresultantOfIndex(1,sc,R):
S2 := SubresultantOfIndex(2,sc,R):

s11 := coeff(S1,z):
s10 := eval(S1,z=0):

#############################################################Creating Arborescence
strDirName := cat("Discdim2degree",deg,"sigma",sigma,"dense"):

#### IBEX
######## system
strIBEXFileName := cat( strDirName, ".bch"): 
strIBEXFilePath := cat( strDirName, "/", strIBEXFileName):

#### SSLGP
######## system
strSSLGPSysFileName := cat( strDirName, "sys.sage"): 
strSSLGPSysFilePath := cat( strDirName, "/", strSSLGPSysFileName):
strSSLGPFileName := cat( strDirName, ".sage"): 
strSSLGPFilePath := cat( strDirName, "/", strSSLGPFileName):

mkdir(strDirName):

#############################################################Generate IBEX file system
file := FileTools:-Text:-Open(strIBEXFilePath,overwrite=true):
fprintf(file, "Variables\n"):
fprintf(file, "x in [-1,1];\n"):
fprintf(file, "y in [-1,1];\n"):
fprintf(file, "\nConstraints\n"):
fprintf(file, "%a = 0;\n", s11 ):
fprintf(file, "%a = 0;\n", s10 ):
fprintf(file, "\nend"):
FileTools:-Text:-Close (file):

#############################################################Generate SSLGP file system
file := FileTools:-Text:-Open(strSSLGPSysFilePath,overwrite=true):
fprintf(file, "Rr.<x,y> =ZZ[]\n"):
fprintf(file, "p1 = %a\n", s11 ):
fprintf(file, "p2 = %a\n", s10 ):
FileTools:-Text:-Close (file):

file := FileTools:-Text:-Open(strSSLGPFilePath,overwrite=true):
fprintf(file, "import os\nimport sys\nimport time\n"):
fprintf(file, "HOME = os.environ[\'HOME\']\n"):
fprintf(file, "sys.path.append(HOME + \'/Bureau/realSolver/interfaceSage\')\n"):
fprintf(file, "from solver.solver_mp import subdivision_solver\n"):
fprintf(file, "prec = 53\nRIF = RealIntervalField(prec)\nsage.rings.real_mpfi.printing_style=\'brackets\'\n\n"):
fprintf(file, "load(\"%s\")\n", strSSLGPSysFilePath): 
fprintf(file, "tab = [ [RIF(-1,1)],[RIF(-1,1)] ]\n" ):
fprintf(file, "minsize=1e-6\n" ):
fprintf(file, "t1 = time.clock()\n" ):
fprintf(file, "test = subdivision_solver([p1,p2],[x,y],53,113)\n" ):
fprintf(file, "status = test.solve(tab,minsize,\'stats_color\')\n" ):
fprintf(file, "t2 = time.clock()\n" ):
fprintf(file, "t6 = t2-t1\n" ):
fprintf(file, "print \"solving status: \", status\n"):
fprintf(file, "print \"temps pour realSolver: \", t6, \"s\"\nprint \"############################\"\n" ):
fprintf(file, "res = test.getResults()\n" ):
fprintf(file, "print \"Nombre de Solutions: \", len(res)\n" ):
fprintf(file, "for i in range(0,len(res)):\n  print \"Solution : \", res[i][0][0], \", \", res[i][1][0]\nprint \"############################\"\n" ):
FileTools:-Text:-Close (file):